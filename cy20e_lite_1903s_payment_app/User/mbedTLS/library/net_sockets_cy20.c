/*
 *  TCP/IP or UDP/IP networking functions for MDK-Pro Network Dual Stack
 *
 *  Copyright (C) 2006-2019, Arm Limited, All Rights Reserved
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may
 *  not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 *  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  This file is part of mbed TLS (https://tls.mbed.org)
 */

#if !defined(MBEDTLS_CONFIG_FILE)
 #include "mbedtls/config.h"
#else
 #include MBEDTLS_CONFIG_FILE
#endif

#if defined(MBEDTLS_NET_C)

#include <stdio.h>
#include <string.h>

#include "mbedtls/net_sockets.h"


/**
 * \brief          Initialize a context
 *                 Just makes the context ready to be used or freed safely.
 *
 * \param ctx      Context to initialize
 */
void mbedtls_net_init (mbedtls_net_context *ctx) {
	ctx->fd = -1;
}

/**
 * \brief          Initiate a connection with host:port in the given protocol
 *
 * \param ctx      Socket to use
 * \param host     Host to connect to
 * \param port     Port to connect to
 * \param proto    Protocol: MBEDTLS_NET_PROTO_TCP or MBEDTLS_NET_PROTO_UDP
 *
 * \return         0 if successful, or one of:
 *                      MBEDTLS_ERR_NET_SOCKET_FAILED,
 *                      MBEDTLS_ERR_NET_UNKNOWN_HOST,
 *                      MBEDTLS_ERR_NET_CONNECT_FAILED
 *
 * \note           Sets the socket in connected mode even with UDP.
 */
int mbedtls_net_connect (mbedtls_net_context *ctx, const char *host, const char *port, int proto) {
	int32_t  ret;

	ctx->fd=1;
	
	return (0);
}


/*
 * Create a listening socket on bind_ip:port
 */
int mbedtls_net_bind (mbedtls_net_context *ctx, const char *bind_ip, const char *port, int proto) {
    return (MBEDTLS_ERR_NET_SOCKET_FAILED);
}

/*
 * Accept a connection from a remote client
 */
int mbedtls_net_accept (mbedtls_net_context *bind_ctx,
                        mbedtls_net_context *client_ctx,
                        void *client_ip, size_t buf_size, size_t *ip_len) {
	return (MBEDTLS_ERR_NET_ACCEPT_FAILED);
}

/**
 * \brief          Check and wait for the context to be ready for read/write
 *
 * \param ctx      Socket to check
 * \param rw       Bitflag composed of MBEDTLS_NET_POLL_READ and
 *                 MBEDTLS_NET_POLL_WRITE specifying the events
 *                 to wait for:
 *                 - If MBEDTLS_NET_POLL_READ is set, the function
 *                   will return as soon as the net context is available
 *                   for reading.
 *                 - If MBEDTLS_NET_POLL_WRITE is set, the function
 *                   will return as soon as the net context is available
 *                   for writing.
 * \param timeout  Maximal amount of time to wait before returning,
 *                 in milliseconds. If \c timeout is zero, the
 *                 function returns immediately. If \c timeout is
 *                 -1u, the function blocks potentially indefinitely.
 *
 * \return         Bitmask composed of MBEDTLS_NET_POLL_READ/WRITE
 *                 on success or timeout, or a negative return code otherwise.
 */
/*
int mbedtls_net_poll( mbedtls_net_context *ctx, uint32_t rw, uint32_t timeout )
{
	
}
*/

/*
 * Set the socket blocking or non-blocking
 */
int mbedtls_net_set_block (mbedtls_net_context *ctx) {
  return 0;
}

int mbedtls_net_set_nonblock (mbedtls_net_context *ctx) {
  return 0;
}

/*
 * Portable usleep helper
 */
void mbedtls_net_usleep (unsigned long usec) {
  mdelay((usec + 999) / 1000);
}

/*
 * Read at most 'len' characters
 */
int mbedtls_net_recv (void *ctx, unsigned char *buf, size_t len) {
  mbedtls_net_context *ctxt = ctx;
  int32_t ret;

	
	ret=iCommTcpRecv(buf, len, 30*1000);

  return (ret);
}

/*
 * Read at most 'len' characters, blocking for at most 'timeout' ms
 */
int mbedtls_net_recv_timeout (void *ctx, unsigned char *buf, size_t len, uint32_t timeout) {
  mbedtls_net_context *ctxt = ctx;
  int32_t ret;

	ret=iCommTcpRecv(buf, len, timeout);

  /* This call will not block */
  return ret;
}

/*
 * Write at most 'len' characters
 */
int mbedtls_net_send( void *ctx, const unsigned char *buf, size_t len ) {
  mbedtls_net_context *ctxt = ctx;
  int32_t ret;


  return (ret);
}

/*
 * Gracefully close the connection
 */
void mbedtls_net_free (mbedtls_net_context *ctx) {
  if (ctx->fd < 0) {
    return;
  }
  //iotSocketClose (ctx->fd);
  ctx->fd = -1;
}

#endif /* MBEDTLS_NET_C */

