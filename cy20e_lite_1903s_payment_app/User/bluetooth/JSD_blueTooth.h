#ifndef JSD_BLUETOOTH_H
#define  JSD_BLUETOOTH_H

#include "Define.h"
//#include "TypeDefine.h"
//#include "Typedef.h"
//#include "app_comm_interface.h"
#ifdef BT_DEC
	#define BT_Ext
#else
	#define BT_Ext extern
#endif

#define JSD_SUCCESS   0x00
#define JSD_ERROR	 0x01
#define JSD_TIMEOUT	 0x8A
#define JSD_ABOLISH	 0x1B
#define JSD_ERRPARAM  0x8B
#define JSD_ERRHANDLE 0x8C
#define JSD_FAILED    0x8F
#define JSD_CONFIG_DONE	0x24

#define JSD_BT_UNDISCOVERABLE	0x00
#define JSD_BT_DISCOVERABLE		0x01

#define BM57	0x00	//0x25,0x93--1.93
#define BM77	0x01	//0x20,0x24--1.24

#define JSD_BT_SDC_DISCOVERABLE		0x01
#define JSD_BT_SDC_NAME				0x02
#define JSD_BT_SDC_PINCODE			0x04
#define JSD_BT_SDC_BT_MAC			0x08

#define JSD_BT_SET_ALL	JSD_BT_SDC_DISCOVERABLE|JSD_BT_SDC_NAME|JSD_BT_SDC_PINCODE|JSD_BT_SDC_BT_MAC

#define CONFIG_ONE_TIME 0x01//配置项仅出厂设置一次，若之前已经配置过，则配置不执行
#define CONFIG_ENFORCE  0x02//强制配置，不论配置是否执行过，强制执行当前设置

typedef struct
{
    u8		ucBTMac[12];		//蓝牙mac,强制设置	//yc1021蓝牙mac地址为12位的---小端格式
    u8 		ucBTName[17];		//蓝牙设备名称，最长16字节，以'\0'结束
    u8 		ucFWVersion[2];	//蓝牙固件版本，强制设置
    u8		ucBTPincode[6];	//蓝牙pincode，固定4位数字
    unsigned long   ucBTDiscoveralbe;	//蓝牙可见性
}JSD_BT_INFO;
extern JSD_BT_INFO BT_info_t;
extern JSD_BT_INFO BT_info_Backup_t;

//蓝牙状态
typedef enum
{
	JSD_BTCONNECTED,	//连接
	JSD_BTDISCONNECTED,	//未连接
	JSD_BTPOWERDOWN		//未上电
}ENUM_BTSTATUS;

#define EM_BT_SET_DISCOVERABLE		0x01
#define EM_BT_SET_NAME					0x02
#define EM_BT_SET_PINCODE				0x04

/*
extern UINT8 btstatus;
extern volatile int CommucateOUT;//  蓝牙断开
extern UINT8 BT_BLE_TYPE;
*/
BT_Ext uchar JSD_BtDevOpen(void);
BT_Ext void  JSD_BtDisconnectLink(void);
BT_Ext uchar JSD_BtDevClose(void);
BT_Ext uchar JSD_BtDev_Sleep_Mode(void);
BT_Ext uchar JSD_SetBTInfo(JSD_BT_INFO *info,uchar flag,uchar enforce);
BT_Ext uchar JSD_GetBTInfo(JSD_BT_INFO *info);
BT_Ext uchar JSD_GetBTLinkStatus(int *status);
BT_Ext u8 	 JSD_ucSetBTParamConfig(u8 cmd, u8 *data);
BT_Ext void  JSD_BT_Init(void);
void SetBtPincode(void);
void UpdatePincode(void);
void DispBtName(void);

#define JSD_BT_WakeUp(void)				Yc_WakeUp(void);
#define JSD_BT_RST(void) 				YC_softReset(void);
#define JSD_BT_Send_Data(data,len) 	YC_Send_Data(data,len);
#define JSD_BT_Recieve_Data(data,len,nTimeOutMs)	YC_Recieve_Data(data,len,nTimeOutMs);
#define JSD_BT_setBaudrate(baud) 		YC_HCI_CMD_SET_UART_BAUD(baud);

#endif


