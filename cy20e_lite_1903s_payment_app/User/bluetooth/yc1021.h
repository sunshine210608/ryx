#ifndef __YC1021_H_
#define __YC1021_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include "mhscpu.h"
//#include "TypeDefine.h"
//#include "Hal_Uart.h"
//#include "App_queue.h"
#include "define.h"

//#define FILE_SYSTEM_SUPPORT	

#define YC_UART_CLOCK                48000000
#define YC_WORK_BAUDRATE             115200//921600



#define YC_PATCH_FILE_NAME           "YC_BTPatchFile.bin"
#define YC_PATCH_FILE_LEN_SIZE       2
#define YC_PATCH_BLOCK_LEN_SIZE      1

#define YC_CMD				         0x01
#define YC_CMD_OGF			         0xfc
#define YC_CMD_RESET		         0x00
#define YC_CMD_BAUD			         0x02
#define YC_CMD_ECHO			         0x05

#define YC_SUCCESS 	                 0
#define YC_ERROR 	                 1	

#define ONE_BAG_LEN                 127   //  单包长度

	
#define YC_RESET_PIN_GROUP           GPIOA
#define YC_RESET_PIN_NUM             GPIO_Pin_15
#define YC_RESET_PIN_LOW()           GPIO_ResetBits(YC_RESET_PIN_GROUP, YC_RESET_PIN_NUM)
#define YC_RESET_PIN_HIGH()          GPIO_SetBits(YC_RESET_PIN_GROUP, YC_RESET_PIN_NUM)

// 唤醒
#define BT_WAKEUP_PIN_GROUP           GPIOB
#define BT_WAKEUP_PIN_NUM             GPIO_Pin_1
#define YC_WAKEUP_PIN_LOW()           GPIO_ResetBits(BT_WAKEUP_PIN_GROUP, BT_WAKEUP_PIN_NUM)
#define YC_WAKEUP_PIN_HIGH()          GPIO_SetBits(BT_WAKEUP_PIN_GROUP, BT_WAKEUP_PIN_NUM)


#define  PACKET_TYPE_CMD              0x01
#define  PACKET_TYPE_EVENT            0x02


#define  HCI_CMD_SET_BT_ADDR			0x00  // 设置 BT3.0 地址
#define  HCI_CMD_SET_BLE_ADDR			0x01  // 设置 BLE 地址
#define  HCI_CMD_SET_VISIBILITY		0x02  // 设置可发现和广播
#define  HCI_CMD_SET_BT_NAME			0x03  // 设置 BT3.0 名称
#define  HCI_CMD_SET_BLE_NAME			0x04  // 设置 BLE 名称
#define  HCI_CMD_SEND_SPP_DATA		0x05  // 发送 BT3.0（SPP）数据
#define  HCI_CMD_SEND_BLE_DATA		0x09  // 发送 BLE 数据
#define  HCI_CMD_STATUS_REQUEST		0x0B  // 请求蓝牙状态
#define  HCI_CMD_SET_PAIRING_MODE		0x0C  // 设置配对模式
#define  HCI_CMD_SET_PINCODE			0x0D  // 设置配对码
#define  HCI_CMD_SET_UART_FLOW		0x0E  // 设置 UART 流控
#define  HCI_CMD_SET_UART_BAUD		0x0F  // 设置 UART 波特率
#define  HCI_CMD_VERSION_REQUEST		0x10  // 查询模块固件版本
#define  HCI_CMD_BT_DISCONNECT		0x11  // 断开 BT3.0 连接
#define  HCI_CMD_BLE_DISCONNECT		0x12  // 断开 BLE 连接
#define  HCI_CMD_SET_COD				0x15  // 设置 COD
#define  HCI_CMD_SET_NVRAM				0x26  // 下发 NV 数据
#define  HCI_CMD_ENTER_SLEEP_MODE		0x27  // 进入睡眠模式
#define  HCI_CMD_SPP_DATA_COMPLETE	0x29  // SPP 数据处理完成
#define  HCI_CMD_SET_ADV_DATA			0x2A  // 设置 ADV 数据
#define  HCI_CMD_POWER_REQ				0x2B



// 响应事件
#define  HCI_EVENT_BT_CONNECTED        0x00   // BT3.0 连接建立
#define  HCI_EVENT_BLE_CONNECTED       0x02   // BLE 连接建立
#define  HCI_EVENT_BT_DISCONNECTED     0x03   // BT3.0 连接断开
#define  HCI_EVENT_BLE_DISCONNECTED    0x05   // BLE 连接断开
#define  HCI_EVENT_CMD_COMPLETE        0x06   // 命令已完成
#define  HCI_EVENT_SPP_DATA_RECEIVED   0x07   // 接收到 BT3.0 数据（SPP）
#define  HCI_EVENT_BLE_DATA_RECEIVED   0x08   // 接收到 BLE 数据
#define  HCI_EVENT_I_AM_READY          0x09   // 模块准备好
#define  HCI_EVENT_STAUS_RESPONSE      0x0A   // 状态回复
#define  HCI_EVENT_NVRAM_CHANGED       0x0D   // 上传 NVRAM 数据
#define  HCI_EVENT_UART_EXCEPTION      0x0F   // HCI 包格式错误






#define  HCI_EVENT_STATUS_BT_FUOND_ENALBED      0x01  //  bit0:3.0 可发现；
#define  HCI_EVENT_STATUS_BT_CONNECT_ENALBED    0x02  //  bit1:3.0 可连接；
#define  HCI_EVENT_STATUS_BLE_FUOND_ENALBED     0x04  //  bit2: 4.0 可发现 （广播 ADV）；
#define  HCI_EVENT_STATUS_BT_CONNECTED          0x10  //  BT3.0（SPP 协议）已连接；
#define  HCI_EVENT_STATUS_BLE_CONNECTED         0x20  //  bit5:BLE 已连接



typedef enum
{
    pincode = 0,
    just_work,
    passkey,
    confirm,
}PAIRING_MODE;


#ifdef FILE_SYSTEM_SUPPORT
extern int HOST_fileOpen(uint8* file_name, int io_config);
extern uint32 HOST_fileRead(int fileHandle, uint32 offset, uint32 len, uint8* buff);//return read length?
extern int HOST_fileClose(int fileHandle);
#endif	
	
int YC_isCmdComplete(uint8* pEvent, uint8 len, uint8 ocf);	
int YC_softReset(void);
int YC_setBaudrate(uint32 baud);
int YC_Echo(void);
int YC_Patch_(void);
int YC_BT_init(void);
int YC_Get_Ready(void);
int YC_Set_BT_Name(uint8 *Name, uint8 Len);
int YC_EVENT_CMD_COMPLETE(uint8* pEvent, uint8 *len, uint16 *Opcode);
int  YC_Set_Name(uint8 *Name, uint8 Len);
static  void AscToHex(uint8 *_d,uint8 *_s,uint16 dlen);
int  YC_Set_ADD(uint8 *ADD);
int YC_HCI_CMD_STATUS_REQUEST(void);
int YC_DisConnect(void);
int YC_Recieve_Data(uint8* data, uint16 nNeedReadMaxLen,uint16 nTimeOutMs);
int YC_HCI_CMD_SET_VISIBILITY(uint8 enable);

int Bt_Trans_Receive_process(void);
uint8 Bt_Detect_ConnectState(void);
void Yc_PowerOff(void);
int YC_HCI_CMD_ENTER_SLEEP_MODE(void);
uint8 blue_data_deal(void);
void Close_BT(void);
int YC_WRITE_NVDATA(void);
void Yc_Get_Voltage(void);
void Yc_WakeUp(void);
int YC_Send_Data(uint8* data, uint16 len);
int YC_HCI_CMD_SET_PINCODE(uint8 *pinCode,uint8 pinCodeLen);

void uart_delay(uint16 ms);
u8 YC_BT_Dev_init(void);

uchar DC_BT_ReadFrame(void *hDevHandle, unsigned int uiTimeOut, unsigned int uiLength, void *pvBuf,  unsigned int *puiActLen);

void CheckIsNewMasterBtConnect(unsigned char * psNvramData,unsigned int nLen);


#ifdef __cplusplus
}
#endif


#endif   ///< __YC1021_H_

