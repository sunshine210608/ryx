#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "VposFace.h"
#include "pub.h"
#include "cJSON.h"
#include "debug.h"
#include "sys_littlefs.h"
#include "tag.h"
#include "func.h"
#include "AppGlobal_cjt.h"
#include "MemMana_cjt.h"
#include "Manage_cjt.h"

extern int SecReadTusnKey(byte *tusnKey);
extern int gprs_at_IMSI(char* pszIMSI);
extern int iGetICCID(char *pszICCID);
extern int gprs_at_GetCellInfo(char *pszCellInfo);
extern int SecCalTusnCiphertext(char *tusn, char *randomFactor, unsigned char *tusnKey, 
		char *szTusnCiphertextOut);


typedef struct __SCRT_TMNL_INFO
{
    u8 ucTmnlType[4];//设备类型(字符串) "01":ATM,"02":传统POS,"03":MPOS,"04":智能POS,"05":扫码支付POS
    u32 uiTmnlSrlNmbLen;//终端硬件序列号长度
    u8 ucTmnlSrlNmb[52];//终端硬件序列号:6位厂商编号+2位终端类型（同设备类型）+42位自定义序列号   
    u8 ucfactCipher[8];//硬件序列号密文数据
    u8 ucRevered[32];//保留位    
}SCRT_TMNL_INFO;//终端密文信息


u16 GetTLVFieldData(char sp,u16 sn,u8 *lpIn,u32 nLc,u8 *lpOut)
{
	u8 *p1,*p2,k;
	u16 len = 0;
	u32 j = 0;

	p1 = lpIn;

//	Trace("huangxin", "p1=%s\r\n",p1);
	
	//防止下面的算法溢出
	for(k = 0, j = 0; j < nLc; j++)
	{
		if(p1[j] == sp)
			k++;
	}
//	Trace("huangxin", "j=%d,k=%d\r\n",j,k);

	if(k < sn)
		return 0;
	//end 防止下面的算法溢出

	//460,07,18CC,8646E92
	for(k = 0;k < sn;k ++)
	{
		if(k == sn)	break;
		p1 = strchr(p1,sp);
		if(p1 == NULL)
		{
			return 0;
		}
		p1 ++;
	}
//	Trace("huangxin", "p1=%s\r\n",p1);
	p2 = strchr(p1,sp);
	if(p2 == NULL) p2 = lpIn + nLc;
//	Trace("huangxin", "p2=%s\r\n",p2);
	len = p2 - p1;
//	Trace("huangxin", "len=%d\r\n",len);
	memcpy(lpOut,p1,len);
	return len;
}


u16 SetTlvData(u8 *pOutData,u8 ucTag,u8 *pInData,u16 usInLen)
{
    u8 temp[128] = {0};
    u16 usLen = 0;
    if (pOutData == NULL || pInData == NULL)
    {
        return 0;
    }

    sprintf(&temp[usLen],"%02d",ucTag);
    usLen += 2;
    sprintf(&temp[usLen],"%03d",usInLen);
    usLen += 3;
    memcpy(&temp[usLen],pInData,usInLen);
    usLen += usInLen;
    TraceHex("value",temp, usLen);

    memcpy(pOutData,temp,usLen);
    return usLen;
}

//pszRandom: 加密随机因子:银行卡交易采用卡号后6位；扫码付交易采用C2B码后6位
void GetPublicField059(char *pszEncPin, uchar *pszRandom, uchar asOut[])
{
	char szBuf[200];
	unsigned char tusnKey[20];
	char szTusnCiphertextOut[10];
	uchar *p,*pA2,*pA5;
	int len;
	int ret;
	uchar tmp[100];
	uchar ucMcc[30+1]={0}, ucMnc[30+1]={0}, ucLac[30+1]={0}, ucCi[30+1]={0},iccid[50] = {0}; 
	int val=0;
	uchar ucLac2[30+1]={0}, ucCi2[30+1]={0},ucLac3[30+1]={0}, ucCi3[30+1]={0};
	uchar ucval2[10],ucval3[10];

	ret = SecReadTusnKey(tusnKey);
	if (ret != 0)
	{
	dbg("No TusnKey, no Field59\n");
	return;
	}
	TraceHex("tusnKey",tusnKey,16);  
	
	p=asOut+2;
	if(gl_SysInfo.ucSmFlag == 1 && pszEncPin && pszEncPin[0])
	{ 
	        len=strlen(pszEncPin);
	        sprintf(p, "A1%03d", len/2);
	        vTwoOne(pszEncPin, len, p+5);
	        p+=(5+len/2);		
	}

	if( gl_TransRec.uiTransType==TRANS_TYPE_SALE || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH 
		||gl_TransRec.uiTransType==TRANS_TYPE_REFUND || gl_TransRec.uiTransType==TRANS_TYPE_BALANCE)    
	{
		pA2 = p;
		sprintf(p, "A2%03d", 0);        //0占位,后面再做修改
		p+=5;
			
		len = 2;
		sprintf(p, "%2.2s%03d%s", "01",len,"02");//02:传统POS
		p += 5 + len;

		_uiGetSerialNo(szBuf);		//TUSN
		if(szBuf[0])
		{
			len=strlen(szBuf);
			sprintf(p, "%2.2s%03d%s", "02",len,szBuf);
			p += 5 + len;		
		}

		dbg("pszRandom:%s\n",pszRandom);		
		 if(pszRandom && pszRandom[0])
		{
			len = strlen(pszRandom);
			sprintf(p, "%2.2s%03d%s", "03", len, pszRandom);  //加密随机因子(磁道2后6位)
			p+=5+len;
		   		
			szTusnCiphertextOut[0]=0;
			ret=SecCalTusnCiphertext(szBuf, pszRandom, tusnKey, szTusnCiphertextOut);
			if(ret==0 && szTusnCiphertextOut[0])
			{
				len=strlen(szTusnCiphertextOut);
				sprintf(p, "%2.2s%03d%s", "04", len, szTusnCiphertextOut); //硬件序列号密文数据	
				p+=5+len;
			}
		}
			
		len = 8;
		 _pszGetAppVer(tmp);
		sprintf(p, "%2.2s%03d%-8.8s", "05",len,tmp);  //
		p += 5+len;

	        //填充A2实际长度
	        len=p-pA2;
	        sprintf(szBuf, "A2%03d", len-5);        //减去"A2"和长度3字节
	        memcpy(pA2, szBuf, 5);

               //A5
               	pA5 = p;
		sprintf(p, "A5%03d", 0);        //0占位,后面再做修改
		p+=5;

		memset(szBuf,0,sizeof(szBuf));
		gprs_at_GetCellInfo(szBuf);
		dbg("1:gprs_at_GetCellInfo:%s\r\n",szBuf);
		GetTLVFieldData(',', 0, szBuf, strlen(szBuf), ucMcc);
		if(ucMcc[0])
		{
			len=strlen(ucMcc);
			sprintf(p, "%2.2s%03d%s", "03",len,ucMcc);
			p += 5 + len;		
		}
		
		GetTLVFieldData(',', 1, szBuf, strlen(szBuf), ucMnc);
		if(ucMnc[0])
		{
			len=strlen(ucMnc);
			sprintf(p, "%2.2s%03d%s", "04",len,ucMnc);
			p += 5 + len;		
		}
				
		GetTLVFieldData(',', 2, szBuf, strlen(szBuf), ucLac);
		if(ucLac[0])
		{
			len=strlen(ucLac);
			sprintf(p, "%2.2s%03d%s", "06",len,ucLac);
			p += 5 + len;		
		}
				
		GetTLVFieldData(',', 3, szBuf, strlen(szBuf), ucCi);
               	if(ucCi[0])
		{
			len=strlen(ucCi);
			sprintf(p, "%2.2s%03d%s", "07",len,ucCi);
			p += 5 + len;		
		}

		val=gprs_at_GetSignalQuality();
		if(val != 0)
		{
		//	sprintf(tmp,"%x",val & 0xffff);	
		        sprintf(tmp,"%x",val);	
			len=strlen(tmp);
			sprintf(p, "%2.2s%03d%s", "08",len,tmp);
			p += 5 + len;		
		}	

	{
	//	char cell1[30],cell2[30],cell3[30]; 
                uchar szTmp[50];	       
		int  i;
                dbg("gl_SysInfo.intervaltime1:%d\n",gl_SysInfo.intervaltime);
		if((gl_SysInfo.intervaltime > 0 && gl_SysInfo.intervaltime < 10 ) || gl_SysInfo.intervaltime > 120)
			gl_SysInfo.intervaltime = 120;		
		
		if(gl_SysInfo.intervaltime == 0)
		{
			gl_cell1[0] = 0;
			gl_cell2[0] = 0;
			gl_cell3[0] = 0;
		//	for(i = 0;i < 10;i++)
			{
				ret=gprs_at_NeighbourCell(gl_cell1, gl_cell2, gl_cell3);
		//		if((gl_cell1[0] && memcmp(gl_cell1,"0,0,0,0,0",9) != 0) 
		//			|| (gl_cell2[0] && memcmp(gl_cell2,"0,0,0,0,0",9) != 0 )
		//			|| (gl_cell3[0] &&memcmp(gl_cell3,"0,0,0,0,0",9) != 0))
		//			break;
		//		_vDelay(500);
			}	
			dbg("cell1:gprs_at_GetCellInfo:%s\r\n",gl_cell1);
                	dbg("cell2:gprs_at_GetCellInfo:%s\r\n",gl_cell2);
			dbg("cell3:gprs_at_GetCellInfo:%s\r\n",gl_cell3);	
		}else if(_uiTestTimer(gl_ulTimer))	
		{
			gl_cell1[0] = 0;
			gl_cell2[0] = 0;
			gl_cell3[0] = 0;
		//	for(i = 0;i < 10;i++)
			{
				ret=gprs_at_NeighbourCell(gl_cell1, gl_cell2, gl_cell3);
		//		if((gl_cell1[0] && memcmp(gl_cell1,"0,0,0,0,0",9) != 0) 
		//			|| (gl_cell2[0] && memcmp(gl_cell2,"0,0,0,0,0",9) != 0 )
		//			|| (gl_cell3[0] &&memcmp(gl_cell3,"0,0,0,0,0",9) != 0))
		//			break;
		//		_vDelay(500);
			}	
			dbg("cell1.1:gprs_at_GetCellInfo:%s\r\n",gl_cell1);
                	dbg("cell2.2:gprs_at_GetCellInfo:%s\r\n",gl_cell2);
			dbg("cell3.3:gprs_at_GetCellInfo:%s\r\n",gl_cell3);

			//_vSetTimer(&gl_ulTimer, 12000L); //120秒2分钟
			_vSetTimer(&gl_ulTimer, gl_SysInfo.intervaltime*6000L);  //默认120分钟* 60秒
		}

		//相邻基站1
		memset(ucval2,0,sizeof(ucval2));
		memset(ucval3,0,sizeof(ucval3));
		dbg("2:gprs_at_GetCellInfo:%s\r\n",gl_cell1);
		
		if(gl_cell1[0]) {
			GetTLVFieldData(',', 2, gl_cell1, strlen(gl_cell1), ucLac2);
		        if(strlen(ucLac2) > 1 || ucLac2[0] != '0')
		       {
				len=strlen(ucLac2);
				sprintf(p, "%2.2s%03d%s", "09",len,ucLac2);
				p += 5 + len;		
			}

			GetTLVFieldData(',', 3, gl_cell1, strlen(gl_cell1), ucCi2);			
		        if(strlen(ucCi2) > 1 ||ucCi2[0] != '0' )
		       {
				len=strlen(ucCi2);
				sprintf(p, "%2.2s%03d%s", "10",len,ucCi2);
				p += 5 + len;		
			}

			GetTLVFieldData(',', 4, gl_cell1, strlen(gl_cell1), ucval2);
		        if(atoi(ucval2) > 0)
		       {
				len=strlen(ucval2);
				sprintf(p, "%2.2s%03d%s", "11",len,ucval2);
				p += 5 + len;		
			}else if(ucLac2[0] != '0' && strlen(ucLac2) != 1 && ucCi2[0] != '0' && strlen(ucCi2) != 1)
			{
				sprintf(tmp,"%x",val-5);	
				len=strlen(tmp);
				sprintf(p, "%2.2s%03d%s", "11",len,tmp);
				p += 5 + len;	
			}
		}	
              
                //相邻基站2
  		dbg("3:gprs_at_GetCellInfo:%s\r\n",gl_cell2);
		if(gl_cell2[0]) {		
			GetTLVFieldData(',', 2, gl_cell2, strlen(gl_cell2), ucLac3);
		        if(strlen(ucLac3) > 1 || ucLac3[0] != '0')
		       {
				len=strlen(ucLac3);
				sprintf(p, "%2.2s%03d%s", "12",len,ucLac3);
				p += 5 + len;		
			}

			GetTLVFieldData(',', 3, gl_cell2, strlen(gl_cell2), ucCi3);
		        if(strlen(ucCi3) > 1 ||ucCi3[0] != '0' )
		       {
				len=strlen(ucCi3);
				sprintf(p, "%2.2s%03d%s", "13",len,ucCi3);
				p += 5 + len;		
			}

	                GetTLVFieldData(',', 4, gl_cell2, strlen(gl_cell2), ucval3);
		        if(atoi(ucval3) > 0)
		       {
				len=strlen(ucval3);
				sprintf(p, "%2.2s%03d%s", "14",len,ucval3);
				p += 5 + len;		
			}
			else if(ucLac3[0] != '0' && strlen(ucLac3) != 1 && ucCi3[0] != '0' && strlen(ucCi3) != 1)
			{
				sprintf(tmp,"%x",val-5-5);	
				len=strlen(tmp);
				sprintf(p, "%2.2s%03d%s", "14",len,tmp);
				p += 5 + len;	
			}
		}	
	}

		iGetICCID(iccid); 
		if(iccid[0])
		{
			len=strlen(iccid);
			sprintf(p, "%2.2s%03d%s", "19",len,iccid);
			p += 5 + len;		
		}	
	        //填充A5实际长度
	        len=p-pA5;
	        sprintf(szBuf, "A5%03d", len-5);        //减去"A2"和长度3字节
	        memcpy(pA5, szBuf, 5);
	}

//扫码交易用到
	//A3
//        if(strcmp(gl_Send8583.Field23,"03"))
//	{
//        }	
	len=p-asOut-2;
	asOut[0]=len/256;
	asOut[1]=len%256;
	TraceHex("gl_Send8583.Field59",asOut+2,len);  
}


//pszRandom: 加密随机因子:银行卡交易采用卡号后6位；扫码付交易采用C2B码后6位
void GetPublicField059_QR(uchar *pszRandom, uchar asOut[])
{
	char szBuf[200];
	unsigned char tusnKey[20];
	char szTusnCiphertextOut[10];
	uchar *p,*pA2,*pA5;
	int len;
	int ret;
	uchar tmp[100];
	uchar ucMcc[30+1]={0}, ucMnc[30+1]={0}, ucLac[30+1]={0}, ucCi[30+1]={0},iccid[50] = {0}; 
	int val=0;
	uchar ucLac2[30+1]={0}, ucCi2[30+1]={0},ucLac3[30+1]={0}, ucCi3[30+1]={0};
	uchar ucval2[10],ucval3[10];
	
	ret = SecReadTusnKey(tusnKey);
	if (ret != 0)
	{
	dbg("No TusnKey, no Field59\n");
	return;
	}
	TraceHex("tusnKey",tusnKey,16);  
	
	p=asOut+2;    
	
	pA2 = p;
	sprintf(p, "A2%03d", 0);        //0占位,后面再做修改
	p+=5;
		
	len = 2;
	sprintf(p, "%2.2s%03d%s", "01",len,"02");//02:传统POS
	p += 5 + len;

	_uiGetSerialNo(szBuf);		//TUSN
	if(szBuf[0])
	{
		len=strlen(szBuf);
		sprintf(p, "%2.2s%03d%s", "02",len,szBuf);
		p += 5 + len;		
	}

	dbg("pszRandom:%s\n",pszRandom);		
	 if(pszRandom && pszRandom[0])
	{
		len = strlen(pszRandom);
		sprintf(p, "%2.2s%03d%s", "03", len, pszRandom);  //加密随机因子(磁道2后6位)
		p+=5+len;
	   		
		szTusnCiphertextOut[0]=0;
		ret=SecCalTusnCiphertext(szBuf, pszRandom, tusnKey, szTusnCiphertextOut);
		if(ret==0 && szTusnCiphertextOut[0])
		{
			len=strlen(szTusnCiphertextOut);
			sprintf(p, "%2.2s%03d%s", "04", len, szTusnCiphertextOut); //硬件序列号密文数据	
			p+=5+len;
		}
	}
			
	len = 8;
	 _pszGetAppVer(tmp);
	sprintf(p, "%2.2s%03d%-8.8s", "05",len,tmp);  //
	p += 5+len;

        //填充A2实际长度
        len=p-pA2;
        sprintf(szBuf, "A2%03d", len-5);        //减去"A2"和长度3字节
        memcpy(pA2, szBuf, 5);

	//A5
        pA5 = p;
	sprintf(p, "A5%03d", 0);        //0占位,后面再做修改
	p+=5;

	memset(szBuf,0,sizeof(szBuf));
	gprs_at_GetCellInfo(szBuf);
       dbg("1:gprs_at_GetCellInfo:%s\r\n",szBuf);
	GetTLVFieldData(',', 0, szBuf, strlen(szBuf), ucMcc);
	if(ucMcc[0])
	{
		len=strlen(ucMcc);
		sprintf(p, "%2.2s%03d%s", "03",len,ucMcc);
		p += 5 + len;		
	}
	
	GetTLVFieldData(',', 1, szBuf, strlen(szBuf), ucMnc);
	if(ucMnc[0])
	{
		len=strlen(ucMnc);
		sprintf(p, "%2.2s%03d%s", "04",len,ucMnc);
		p += 5 + len;		
	}
			
	GetTLVFieldData(',', 2, szBuf, strlen(szBuf), ucLac);
	if(ucLac[0])
	{
		len=strlen(ucLac);
		sprintf(p, "%2.2s%03d%s", "06",len,ucLac);
		p += 5 + len;		
	}
			
	GetTLVFieldData(',', 3, szBuf, strlen(szBuf), ucCi);
           	if(ucCi[0])
	{
		len=strlen(ucCi);
		sprintf(p, "%2.2s%03d%s", "07",len,ucCi);
		p += 5 + len;		
	}

	val=gprs_at_GetSignalQuality();
	if(val != 0)
	{
	//	sprintf(tmp,"%x",val & 0xffff);	
	        sprintf(tmp,"%x",val);	
		len=strlen(tmp);
		sprintf(p, "%2.2s%03d%s", "08",len,tmp);
		p += 5 + len;		
	}	

       	{
		//	char cell1[30],cell2[30],cell3[30]; 
                uchar szTmp[50];	       
		int  i;
                dbg("gl_SysInfo.intervaltime1:%d\n",gl_SysInfo.intervaltime);
		if((gl_SysInfo.intervaltime > 0 && gl_SysInfo.intervaltime < 10 ) || gl_SysInfo.intervaltime > 120)
			gl_SysInfo.intervaltime = 120;		
		
		if(gl_SysInfo.intervaltime == 0)
		{
			gl_cell1[0] = 0;
			gl_cell2[0] = 0;
			gl_cell3[0] = 0;
		//	for(i = 0;i < 10;i++)
			{
				ret=gprs_at_NeighbourCell(gl_cell1, gl_cell2, gl_cell3);
		//		if((gl_cell1[0] && memcmp(gl_cell1,"0,0,0,0,0",9) != 0) 
		//			|| (gl_cell2[0] && memcmp(gl_cell2,"0,0,0,0,0",9) != 0 )
		//			|| (gl_cell3[0] &&memcmp(gl_cell3,"0,0,0,0,0",9) != 0))
		//			break;
		//		_vDelay(500);
			}	
			dbg("cell1:gprs_at_GetCellInfo:%s\r\n",gl_cell1);
                	dbg("cell2:gprs_at_GetCellInfo:%s\r\n",gl_cell2);
			dbg("cell3:gprs_at_GetCellInfo:%s\r\n",gl_cell3);	
		}else if(_uiTestTimer(gl_ulTimer))	
		{
			gl_cell1[0] = 0;
			gl_cell2[0] = 0;
			gl_cell3[0] = 0;
		//	for(i = 0;i < 10;i++)
			{
				ret=gprs_at_NeighbourCell(gl_cell1, gl_cell2, gl_cell3);
		//		if((gl_cell1[0] && memcmp(gl_cell1,"0,0,0,0,0",9) != 0) 
		//			|| (gl_cell2[0] && memcmp(gl_cell2,"0,0,0,0,0",9) != 0 )
		//			|| (gl_cell3[0] &&memcmp(gl_cell3,"0,0,0,0,0",9) != 0))
		//			break;
		//		_vDelay(500);
			}	
			dbg("cell1.1:gprs_at_GetCellInfo:%s\r\n",gl_cell1);
                	dbg("cell2.2:gprs_at_GetCellInfo:%s\r\n",gl_cell2);
			dbg("cell3.3:gprs_at_GetCellInfo:%s\r\n",gl_cell3);

			//_vSetTimer(&gl_ulTimer, 12000L); //120秒2分钟
			_vSetTimer(&gl_ulTimer, gl_SysInfo.intervaltime*6000L);  //默认120分钟* 60秒
		}

		//相邻基站1
		memset(ucval2,0,sizeof(ucval2));
		memset(ucval3,0,sizeof(ucval3));
		dbg("2:gprs_at_GetCellInfo:%s\r\n",gl_cell1);
		
		if(gl_cell1[0]) {
			GetTLVFieldData(',', 2, gl_cell1, strlen(gl_cell1), ucLac2);
		        if(strlen(ucLac2) > 1 || ucLac2[0] != '0')
		       {
				len=strlen(ucLac2);
				sprintf(p, "%2.2s%03d%s", "09",len,ucLac2);
				p += 5 + len;		
			}

			GetTLVFieldData(',', 3, gl_cell1, strlen(gl_cell1), ucCi2);			
		        if(strlen(ucCi2) > 1 ||ucCi2[0] != '0' )
		       {
				len=strlen(ucCi2);
				sprintf(p, "%2.2s%03d%s", "10",len,ucCi2);
				p += 5 + len;		
			}

			GetTLVFieldData(',', 4, gl_cell1, strlen(gl_cell1), ucval2);
		        if(atoi(ucval2) > 0)
		       {
				len=strlen(ucval2);
				sprintf(p, "%2.2s%03d%s", "11",len,ucval2);
				p += 5 + len;		
			}else if(ucLac2[0] != '0' && strlen(ucLac2) != 1 && ucCi2[0] != '0' && strlen(ucCi2) != 1)
			{
				sprintf(tmp,"%x",val-5);	
				len=strlen(tmp);
				sprintf(p, "%2.2s%03d%s", "11",len,tmp);
				p += 5 + len;	
			}
		}	
              
                //相邻基站2
  		dbg("3:gprs_at_GetCellInfo:%s\r\n",gl_cell2);
		if(gl_cell2[0]) {		
			GetTLVFieldData(',', 2, gl_cell2, strlen(gl_cell2), ucLac3);
		        if(strlen(ucLac3) > 1 || ucLac3[0] != '0')
		       {
				len=strlen(ucLac3);
				sprintf(p, "%2.2s%03d%s", "12",len,ucLac3);
				p += 5 + len;		
			}

			GetTLVFieldData(',', 3, gl_cell2, strlen(gl_cell2), ucCi3);
		        if(strlen(ucCi3) > 1 ||ucCi3[0] != '0' )
		       {
				len=strlen(ucCi3);
				sprintf(p, "%2.2s%03d%s", "13",len,ucCi3);
				p += 5 + len;		
			}

	                GetTLVFieldData(',', 4, gl_cell2, strlen(gl_cell2), ucval3);
		        if(atoi(ucval3) > 0)
		       {
				len=strlen(ucval3);
				sprintf(p, "%2.2s%03d%s", "14",len,ucval3);
				p += 5 + len;		
			}
			else if(ucLac3[0] != '0' && strlen(ucLac3) != 1 && ucCi3[0] != '0' && strlen(ucCi3) != 1)
			{
				sprintf(tmp,"%x",val-5-5);	
				len=strlen(tmp);
				sprintf(p, "%2.2s%03d%s", "14",len,tmp);
				p += 5 + len;	
			}
		}	
	
	}

	iGetICCID(iccid); 
	if(iccid[0])
	{
		len=strlen(iccid);
		sprintf(p, "%2.2s%03d%s", "19",len,iccid);
		p += 5 + len;		
	}	
        //填充A5实际长度
        len=p-pA5;
        sprintf(szBuf, "A5%03d", len-5);        //减去"A2"和长度3字节
        memcpy(pA5, szBuf, 5);
			
//扫码交易用到
	//A3
//        if(strcmp(gl_Send8583.Field23,"03"))
//	{
//        }	
	len=p-asOut-2;
	asOut[0]=len/256;
	asOut[1]=len%256;
	TraceHex("gl_Send8583.Field59",asOut+2,len);  
}

uint32 EncryptMagData(u8 ucTrackData[],u16 iLen,u8 lpOut[])
{
    u8 tmp[256], buf[128];
    uint32 slt = 1;
    u8 *pBuf=0;
    bool bIsEncryp = false;
    u8 tmpend = 0;
    int len= 0;
	
    if(ucTrackData==NULL||lpOut==NULL)return 1;
 	dbgHex("gl_SysData.sMagKey", gl_SysData.sMagKey, 16);
    if (iLen > 16)
    {
        dbg("ucTrackData=[%s]\r\n", ucTrackData);
		memset(buf,0,sizeof(buf));

	if(gl_SysInfo.ucSmFlag == 0)	
	{
	        vTwoOne(ucTrackData, iLen,buf);
		pBuf = &buf[(iLen + 1) / 2 - 9];
        //加密
		 _vDes(TRI_ENCRYPT, pBuf, gl_SysData.sMagKey, pBuf);
//		 _vDes(TRI_ENCRYPT, pBuf+8, gl_SysData.sMagKey, pBuf+8);
	}
	else if(gl_SysInfo.ucSmFlag == 1)
	{
	        len = iLen - 1;
		dbg("len:%d\n",len);	
	        if(len >= 32)
		{
			vTwoOne(ucTrackData, iLen,buf);
			pBuf = &buf[(iLen + 1) / 2 - 17];
	        }else 
	        {
	              tmpend  = ucTrackData[len];
	               while(len < 32)
	        	{
	                        ucTrackData[len] = 'F';
				len++;	
	               	}
		        dbg("ucTrackData2=[%s]\r\n", ucTrackData);
				   
			ucTrackData[len] = tmpend;
                        len++;
			iLen = len;
						
			ucTrackData[iLen] = 0;	
			dbg("ucTrackData1=[%s]\r\n", ucTrackData);
			vTwoOne(ucTrackData, iLen,buf);
			pBuf = &buf[(iLen + 1) / 2 - 17];	
	        }
			
		_iSm4(ENCRYPT, pBuf, gl_SysData.sMagKey, pBuf);
	}
		memset(tmp, 0, sizeof(tmp));
        vOneTwo(buf, (iLen + 1) / 2,tmp);
        dbg("EncryptMagData=[%s]\r\n", tmp);
		memcpy(lpOut,tmp,iLen);
        bIsEncryp = true;
    }


    return 0;
}


void AjustTrack2Data(u8 asTrack2Data[]){
	u16 i = 0;
	if(asTrack2Data==NULL)return;
	
	for(i=0;i<strlen(asTrack2Data);i++){
		if(asTrack2Data[i]=='='){
			asTrack2Data[i] = 'D';
			break;
		}
	}		

}

void DivPublicField056(u8 *pInData,int iLen, stQrTransRec *QrTrRec)
{
#if 1
	uchar tagLen[2+1];
        int len;
	
	while(iLen > 0)
	{
               if ((*pInData & 0x0F) == 0x0F)
              {
              		if(*pInData == 0x9F && *(pInData+1) == 0x01)
              		{
	              		memcpy(tagLen,pInData + 2,2);
				len = tagLen[0]*256 + tagLen[1];
				if(len > 2)
					memcpy(QrTrRec->orderType,pInData + 4,2);
				else
					memcpy(QrTrRec->orderType,pInData + 4,len);
				pInData += 4 + len;
				iLen  -= 4 + len;
				dbgHex("QrTrRec->orderType:", QrTrRec->orderType,len);
				continue;
              		}
			else if(*pInData == 0x9F && *(pInData+1) == 0x02)
			{
				memcpy(tagLen,pInData + 2,2);
				len = tagLen[0]*256 + tagLen[1];
				if(len > 35) 
					memcpy(QrTrRec->orderId,pInData + 4,35);
				else
					memcpy(QrTrRec->orderId,pInData + 4,len);
				pInData += 4 + len;
				iLen  -= 4 + len;
				if(len > 35)
					dbgHex("QrTrRec->orderId:", QrTrRec->orderId,35);
				else
					dbgHex("QrTrRec->orderId:", QrTrRec->orderId,len);
			//	strcpy(QrTrRec->orderId,"12345678901234567890123456789012345");//linshi
				continue;
			}
			else if(*pInData == 0x9F && *(pInData+1) == 0x07)
			{

				memcpy(tagLen,pInData + 2,2);
				len = tagLen[0]*256 + tagLen[1];
				if(len > 60)
					memcpy(QrTrRec->couponAmt,pInData + 4,60);
				else
					memcpy(QrTrRec->couponAmt,pInData + 4,len);
				pInData += 4 + len;
				iLen  -= 4 + len;
				dbgHex("QrTrRec->couponAmt:",QrTrRec->couponAmt,len);
				continue;
			}
			else 
			{
				memcpy(tagLen,pInData + 2,2);
				len = tagLen[0]*256 + tagLen[1];
				pInData += 4 + len;
				iLen  -= 4 + len;
				continue;
			}
               }else
               {
               			memcpy(tagLen,pInData + 1,2);
				len = tagLen[0]*256 + tagLen[1];
				pInData += 3 + len;
				iLen  -= 3 + len;
				continue;
               	}

/*
			   
		if (memcmp(pInData,"\x9f\x01",2) == 0)
		{
			memcpy(tagLen,pInData + 2,2);
			len = tagLen[0]*256 + tagLen[1];
			if(len > 2)
				memcpy(QrTrRec->orderType,pInData + 4,2);
			else
				memcpy(QrTrRec->orderType,pInData + 4,len);
			pInData += 4 + len;
			iLen  -= 4 + len;
			dbgHex("QrTrRec->orderType:", QrTrRec->orderType,len);
			continue;
		}
		else if (memcmp(pInData,"\x9f\x02",2) == 0)
		{
			memcpy(tagLen,pInData + 2,2);
			len = tagLen[0]*256 + tagLen[1];
			if(len > 35) 
				memcpy(QrTrRec->orderId,pInData + 4,35);
			else
				memcpy(QrTrRec->orderId,pInData + 4,len);
			pInData += 4 + len;
			iLen  -= 4 + len;
			if(len > 35)
				dbgHex("QrTrRec->orderId:", QrTrRec->orderId,35);
			else
				dbgHex("QrTrRec->orderId:", QrTrRec->orderId,len);
		//	strcpy(QrTrRec->orderId,"12345678901234567890123456789012345");//linshi
			continue;
		} 
		else if (memcmp(pInData,"\x9f\x03",2) == 0)
		{
			memcpy(tagLen,pInData + 2,2);
			len = tagLen[0]*256 + tagLen[1];
		//	memcpy(QrTrRec->orderDesc,pInData + 4,len);
			pInData += 4 + len;
			iLen  -= 4 + len;
		//	dbgHex("QrTrRec->orderDesc:", QrTrRec->orderDesc,len);
			continue;
		}
		else if (memcmp(pInData,"\x9f\x04",2) == 0)
		{
			memcpy(tagLen,pInData + 2,2);
			len = tagLen[0]*256 + tagLen[1];
		//	memcpy(QrTrRec->orderMsg,pInData + 4,len);
			pInData += 4 + len;
			iLen  -= 4 + len;
		//	dbgHex("QrTrRec->orderMsg:",QrTrRec->orderMsg,len);
			continue;
		}
		else if (memcmp(pInData,"\x9f\x05",2) == 0)
		{
			memcpy(tagLen,pInData + 2,2);
			len = tagLen[0]*256 + tagLen[1];
		//	memcpy(QrTrRec->orderRemark,pInData + 4,len);
			pInData += 4 + len;
			iLen  -= 4 + len;
		//	dbgHex("QrTrRec->orderRemark:",QrTrRec->orderRemark,len);
			continue;
		}
		else if (memcmp(pInData,"\x9f\x06",2) == 0)
		{
			memcpy(tagLen,pInData + 2,2);
			len = tagLen[0]*256 + tagLen[1];
		//	memcpy(QrTrRec->organName,pInData + 4,len);
			pInData += 4 + len;
			iLen  -= 4 + len;
		//	dbgHex("QrTrRec->organName:",QrTrRec->organName,len);
			continue;
		} 
		else if (memcmp(pInData,"\x9f\x07",2) == 0)
		{
			memcpy(tagLen,pInData + 2,2);
			len = tagLen[0]*256 + tagLen[1];
			if(len > 60)
				memcpy(QrTrRec->couponAmt,pInData + 4,60);
			else
				memcpy(QrTrRec->couponAmt,pInData + 4,len);
			pInData += 4 + len;
			iLen  -= 4 + len;
			dbgHex("QrTrRec->couponAmt:",QrTrRec->couponAmt,len);
			continue;
		}
		else if (memcmp(pInData,"\x9f\x08",2) == 0)
		{
			memcpy(tagLen,pInData + 2,2);
			len = tagLen[0]*256 + tagLen[1];
		//	memcpy(QrTrRec->issName,pInData + 4,len);
			pInData += 4 + len;
			iLen  -= 4 + len;
		//	dbgHex("QrTrRec->issName:",QrTrRec->issName,len);
			continue;
		}	
		
		pInData ++;
		iLen --;
		continue;
*/		
	}
#endif

#if 0
	int tagBinLen;
	TagList tmpTagList;
        signed long ret;
        unsigned short tag;

	if ((pInData==NULL)||(iLen==0))
	{
		vMessage("56域格式不对");
		return ;
	}	

	dbgHex("pInData", pInData,iLen);
	InitTagList(&tmpTagList);
	ret = BuildTagListOneLevel(pInData, iLen,&tmpTagList);
	dbg("ret:%d\n",ret);
        if(ret != 0)
        {
        	vMessage("TLV解析失败");
		FreeTagList(&tmpTagList);
		return ;
        }

	tag=0x9F01;
	if(TagIsExisted(&tmpTagList, tag) == TRUE)
        {
	            tagBinLen=GetTagValueSize(&tmpTagList, tag);	
		    if(tagBinLen > (sizeof(QrTrRec->orderType)-1))
		    {

		    	       	vMessage("9F01长度不对");
				FreeTagList(&tmpTagList);
				return ;
		    }
		     memcpy(QrTrRec->orderType, GetTagValue(&tmpTagList, tag), tagBinLen);
		     dbgHex("QrTrRec->orderType:", QrTrRec->orderType,tagBinLen);
         }

	tag=0x9F02;
       	if(TagIsExisted(&tmpTagList, tag) == TRUE)
        {
	            tagBinLen=GetTagValueSize(&tmpTagList, tag);	
	    	    if(tagBinLen > (sizeof(QrTrRec->orderId)-1))
		    {

		    	       	vMessage("9F02长度不对");
				FreeTagList(&tmpTagList);
				return ;
		    } 		
		    memcpy(QrTrRec->orderId, GetTagValue(&tmpTagList, tag), tagBinLen);	
		    dbgHex("QrTrRec->orderId:", QrTrRec->orderId,tagBinLen);	
        }

	tag=0x9F07;
       	if(TagIsExisted(&tmpTagList, tag) == TRUE)
        {
	            tagBinLen=GetTagValueSize(&tmpTagList, tag);	
		    if(tagBinLen > (sizeof(QrTrRec->couponAmt)-1))
	    	    {

	    	       	vMessage("9F07长度不对");
			FreeTagList(&tmpTagList);
			return ;
	    	     } 		
		     memcpy(QrTrRec->couponAmt, GetTagValue(&tmpTagList, tag), tagBinLen);	
	             dbgHex("QrTrRec->couponAmt:", QrTrRec->couponAmt,tagBinLen);	
        }

	FreeTagList(&tmpTagList);
	
	return;
#endif	
}

void GetPublicField058(uchar asOut[])
{
       uchar szBuf[200];
	uchar ucMcc[30+1]={0}, ucMnc[30+1]={0}, ucLac[30+1]={0}, ucCi[30+1]={0}; 
	uchar *p;
	int len;
	long n;
	char imsi[50] = {0},iccid[50] = {0};
	char length[2+1] = {0};
		
//基站信息
	memset(szBuf,0,sizeof(szBuf));
	gprs_at_GetCellInfo(szBuf);
	dbg("gprs_at_GetCellInfo:%s\r\n",szBuf);
//460,07,18CC,8646E92  lac和cid都为16进制，不存在在填0
        GetTLVFieldData(',', 0, szBuf, strlen(szBuf), ucMcc);
        GetTLVFieldData(',', 1, szBuf, strlen(szBuf), ucMnc);
		
	GetTLVFieldData(',', 2, szBuf, strlen(szBuf), ucLac);
	dbg("58ucLac1:%d    %s\n",strlen(ucLac),ucLac);
	if(ucLac[0])
	{
		n=ulHexToLong(ucLac,strlen(ucLac));
		dbg("ucLac_n:%d\n",n);
		memset(ucLac,0,sizeof(ucLac));
		sprintf(ucLac,"%ld",n);
	        dbg("58ucLac2:%d    %s\n",strlen(ucLac),ucLac);
	}		
	
	GetTLVFieldData(',', 3, szBuf, strlen(szBuf), ucCi);
	dbg("58ucCi1:%d    %s\n",strlen(ucCi),ucCi);
	if(ucCi[0])
	{
		n=ulHexToLong(ucCi,strlen(ucCi));
		dbg("ucCi_n:%d\n",n);
		memset(ucCi,0,sizeof(ucCi));
		sprintf(ucCi,"%ld",n);
		dbg("58ucCi2:%d    %s\n",strlen(ucCi),ucCi);
	}	
 //       GetTLVFieldData(',', 4, szBuf, strlen(szBuf), ucImsi);

        p = szBuf;

        p[0] = 0xA1;
	p += 1;

	memset(length,0x00,sizeof(length));  
	p[0] = 0xBD; ;
	sprintf(length,"%02d",strlen("01"));
        vTwoOne(length, 1,&p[1]);
        memcpy(p+2,"01",strlen("01"));
	p += 2 +  strlen("01");	

	memset(length,0x00,sizeof(length));       
        p[0] = 0xB1;
	sprintf(length,"%02d",strlen(ucMcc));
        vTwoOne(length, 1,&p[1]);		
        memcpy(p+2,ucMcc,strlen(ucMcc));  
	p += 2 + strlen(ucMcc);	

	memset(length,0x00,sizeof(length));    
        p[0] = 0xB2;
	if(strcmp(ucMnc,"00") == 0 ||strcmp(ucMnc,"02") == 0 ||
		strcmp(ucMnc,"04") == 0 || strcmp(ucMnc,"07") == 0 ||
		strcmp(ucMnc,"08") == 0)	
        {  
        	dbg("ucMnc:%s\n",ucMnc);
		strcpy(ucMnc,"00");
	}	
	else if(strcmp(ucMnc,"01") == 0 ||strcmp(ucMnc,"06") == 0 ||strcmp(ucMnc,"09") == 0)	
	{
		strcpy(ucMnc,"01");
	}else if(strcmp(ucMnc,"03") == 0 ||strcmp(ucMnc,"05") == 0 || strcmp(ucMnc,"11") == 0)
	{
		strcpy(ucMnc,"11");
        }else
        	strcpy(ucMnc,"00");
        
//	sprintf(length,"%02d",strlen("00"));
        sprintf(length,"%02d",strlen(ucMnc));       
        vTwoOne(length, 1,&p[1]);	
//	memcpy(p+2,"00",strlen("00"));   
	memcpy(p+2,ucMnc,strlen(ucMnc));  
	p += 2 + strlen(ucMnc);
//	p += 2 + strlen("00");	

	memset(length,0x00,sizeof(length)); 
        p[0] = 0xB3;
	sprintf(length,"%02d",strlen(ucLac));
        vTwoOne(length, 1,&p[1]);
	memcpy(p+2,ucLac,strlen(ucLac));  
	p += 2 + strlen(ucLac);	

	memset(length,0x00,sizeof(length)); 	
	p[0] = 0xB4;
	sprintf(length,"%02d",strlen(ucCi));
        vTwoOne(length, 1,&p[1]);
	memcpy(p+2,ucCi,strlen(ucCi));  
	p += 2 + strlen(ucCi);	

        memset(length,0x00,sizeof(length)); 
        iGetICCID(iccid); 
        p[0] = 0xD1;
        sprintf(length,"%02d",strlen(iccid));
        vTwoOne(length, 1,&p[1]);
	memcpy(p+2,iccid,strlen(iccid));  
	p += 2 + strlen(iccid);

	memset(length,0x00,sizeof(length)); 
	gprs_at_IMSI(imsi);	
	p[0] = 0xD2;	
        sprintf(length,"%02d",strlen(imsi));
        vTwoOne(length, 1,&p[1]);
	memcpy(p+2,imsi,strlen(imsi));  
	p += 2 + strlen(imsi);
  
	len = p - szBuf;	
	dbg("len:%d\n",len);	

	memcpy(asOut,szBuf,len);
	dbgHex("asOut", asOut,  len);
}
