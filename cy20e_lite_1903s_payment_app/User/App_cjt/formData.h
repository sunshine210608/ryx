#ifndef FORMDATA_H
#define FORMDATA_H


extern uint32 EncryptMagData(u8 ucTrackData[],u16 iLen,u8 lpOut[]);
extern u16 GetTLVFieldData(char sp,u16 sn,u8 *lpIn,u32 nLc,u8 *lpOut);
extern void GetPublicField056(void);
extern void GetPublicField058(uchar asOut[]);
extern void GetPublicField059(char *pszEncPin,uchar *pszRandom, uchar asOut[]);
extern void AjustTrack2Data(u8 asTrack2Data[]);
#endif
