#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "VposFace.h"
#include "pub.h"
#include "Pack8583.h"
#include "cJSON.h"
#include "debug.h"
#include "tag.h"
#include "myHttp.h"
#include "myBase64.h"
#include "utf2gb.h"

#include "AppGlobal_cjt.h"
#include "MemMana_cjt.h"
#include "St8583_cjt.h"
#include "tcpcomm.h"
#include "func.h"
#include "QrTrans_cjt.h"


extern int iHttpSendRecv8583(uchar ucRevFlag);
extern int iCheckTranStat(uint uiTransType, uchar ucMode);
extern void flushStatusBar(int iFlushNow);
extern int iPrintQrTrans(stQrTransRec *QrRec, int dup);
extern void vShowWaitEx(char *pszMessage, long lWaitTime, int keyFlag);

extern void vSetCommNoDisp(uchar disp);
extern int iTcpSendRecv(char *pszSvrIp, char *pszSvrPort, char *pszBakSvrIp, char *pszBakSvrPort,
        uchar *psSnd, uint uiSndLen, uchar *psRsp, uint uiRspSize, uint *puiRcvLen, uint iTimeOutMs,uchar ucRevFlag);
extern int iSetConnStatus(char cLinkFlag, char *pszUrl);
extern void vGenFiled63CellInfo(uchar *pszFd63);
extern uint Select_pay_way(void);
extern void TermRemindInfo(void);
extern void TermRemindUseInfo(void);
extern void vShowHostErrMsg(uchar *pszHostErrCode, uchar *pszHostErrMsg);
extern int DispTransQrCode(char *pszQrCodeStr);
extern uint _uiCheckTimer(ulong ulTimer);
extern void DivPublicField056(u8 *pInData,int iLen, stQrTransRec *QrTrRec);
extern void GetPublicField059_QR(uchar *pszRandom, uchar asOut[]);
extern void GetPublicField058(uchar asOut[]);
extern void vSetCommCallBack(void (*func)(void));
extern void vSetQrPreView(int flag);
extern int iGetStringGBK(unsigned char *pszStr, int iStrLen);
extern int iGetQrPreView(void);
extern void vDispWaitQrImg(int iInitFlag);

//static void vAppPackPinBlock(uchar *pszCardId, uchar *pszPin, uchar *psKey, int iKeyLen, uchar *psOut);
int iPackReq8583(st8583 *pSend8583, char *sCommBuf, uint *puiLen);
//static int iMagTrans(uint uiTransType, int iEntry, int iVoidRecIdx, ulong ulVoidTTC);
int iUnPackRsp8583(char *sCommBuf, uint uiRecvLen, st8583 *pRecv8583);

void vPrt8583Fields(char ucType, st8583 *pMsg8583);
void vPrtTransRecInfo(void);

uchar Keyflag;

//static int sg_iRevTry=0;
//static int sg_iVoidRecIndex;
u8 whitchAuthCode(u8 * authCode)
{
	u8 isweb = 0;
	
	if(strlen(authCode) == 18 && 
		(memcmp(authCode,"10",2) == 0||memcmp(authCode,"11",2) == 0||
	   memcmp(authCode,"12",2) == 0||memcmp(authCode,"13",2) == 0||
	   memcmp(authCode,"14",2) == 0||memcmp(authCode,"15",2) == 0))
	{
		isweb = MODE_WETCHAT;
	}
	else if((strlen(authCode) >= 16 && strlen(authCode) <= 24) &&
		(memcmp(authCode,"25",2) == 0 || memcmp(authCode,"26",2) == 0 ||
		memcmp(authCode,"27",2) == 0 || memcmp(authCode,"28",2) == 0 ||
		memcmp(authCode,"29",2) == 0 || memcmp(authCode,"30",2) == 0))
	{
		isweb = MODE_ALIPAY;
	}
	else if(strlen(authCode) == 19 && 
		(memcmp(authCode,"62",2) == 0))
	{
		isweb = MODE_UNIONPAY;
	}
	
	return isweb;

}

//减小存储空间
uchar ucGetQrPayMode(char *pszPayMode)
{
    if(pszPayMode==NULL || pszPayMode[0]==0)
        return 0;   //无效
    if( !strcmp("WX", pszPayMode) )
    {
        return 1;
    }
    if( !strcmp("ZFB", pszPayMode) )
    {
        return 2;
    }
    if( !strcmp("CUPQRC", pszPayMode) )
    {
        return 3;
    }
    return 0;
}


int iAddTLV2Str(char *psOut, char *pszTag, uint len, char *psVal, uint *outlen)
{
    char *psbak=psOut;

    strcpy(psOut, pszTag);
    psOut+=strlen(pszTag);
    if(len==0)
        len=strlen(psVal);
    if(len<128)
    {
        *psOut=len;
        psOut++;
    }else{
        if(len<256)
        {
            *psOut++=0x81;
            *psOut++=len;
        }else{
            *psOut++=0x82;
            *psOut++=(len/256);
            *psOut++=(len%256);
        }
    }
    memcpy(psOut, psVal, len);
    if(outlen)
        *outlen=psOut-psbak+len;
    return 0;
}

//获取51域tlv, v为字符串格式且v中不会包含tag字符串
int iGetTLVStr(char *pszStr, char *pszTag, char *pszVal, uint *outlen)
{
    uchar *p;
    int len;
    p=(uchar*)strstr(pszStr, pszTag);
    if(p)
    {
        p+=strlen(pszTag);
        if(p[0]>=0x80)
        {
            len=ulStrToLong(p+1, p[0]&0x0F);
            p+=(1+p[0]&0x0F);
        }else
        {
            len=p[0];
            p++;
        }
        if(p+len>(uchar*)pszStr+strlen(pszStr)) //长度超过原始字符串长度
            return 1;
        vMemcpy0(pszVal, p, len);
        if(outlen)
            *outlen=len;
        return 0;
    }
    return 1;
}
extern int iDispTransQrCode(uchar *pszPrompt, char *pszQrCodeStr);

int iGetQrCode(char *pszPrompt, char *pszQrCode, int size)
{ 
    int ret;
    ulong ulTimeOut;
    
    if(pszPrompt)
    {
        vDispCenter(3, "开始扫码...", 0);
        vDispCenter(_uiGetVLines()-1, pszPrompt, 0);

    #if 0
        vDispCenter(_uiGetVLines(), "[确认]扫码,[取消]退出)", 0);
        if(!bOK())
            return 1;
    #else
        vDispCenter(_uiGetVLines(), "(按取消键退出)", 0);
    #endif
        
    }
    pszQrCode[0]=0;
    ret=_uiQrStart();
    if(ret)
    {
        //_vQrEnd();
        vMessage("初始化扫码模块失败");
        return 1;
    }
    _vFlushKey();
    
    _vSetTimer(&ulTimeOut, 90*100);     //设置90秒超时
    while(_uiTestTimer(ulTimeOut)==0)
    {
        if(_uiKeyPressed() && _uiGetKey()==_KEY_ESC)
        {
            ret=-2;
            break;
        }
        ret=_uiGetQrCode(pszQrCode, size);
        if(ret)
            break;
        _vDelay(1);
    }
    _vQrEnd();
    
    _vCls();
#ifndef USE_LTE_4G
    flushStatusBar(1);
#endif    
    if(ret>0 && pszQrCode[0])
        return 0;
    else  
        return 1;
}

//获取扫码数据
int scanCode(u8 *OutBuff,int size)
{
    int iRet;
    ulong ulTimeOut;
    int first,iPreView;

    vClearLines(2);
	
   if(iPosHardwareModule(CHK_HAVE_CAM, NULL, 0)<=0)
    {
    	vMessage( "不支持扫码");
        return -1;
    }
   
//    vSetQrPreView(gl_SysInfo.ucQrPreViewFlag);
    vSetQrPreView(0);

    iRet=_uiQrStart();
    if(iRet)
    {           
        vMessage("初始化扫码模块失败");
        return -1;
    }
    _vFlushKey();
               
    _vSetTimer(&ulTimeOut, 90*100);     //设置90秒超时
    first=1;
    iPreView=iGetQrPreView();
    while(_uiTestTimer(ulTimeOut)==0)
    {
        if(_uiKeyPressed() && _uiGetKey()==_KEY_ESC)
        {
            break;
        }
        if(!iPreView)
        {
            vDispWaitQrImg(first);
            first=0;
        }
        iRet=_uiGetQrCode(OutBuff, size);
        if(iRet)
            break;
        _vDelay(1);
    }
    _vQrEnd();

    _vCls();
    #ifndef USE_LTE_4G
    flushStatusBar(1); 
    #endif
    if(OutBuff[0]==0)      //用户取消或超时
    {
        vMessage("扫码失败");
        return -1;
    }

   return iRet;
}

void vDispWarnInfoCallBack(void)
{
	uint uiKey;

	if(!Keyflag &&  _uiKeyPressed())
	{
		uiKey =_uiGetKey();
		if(uiKey ==_KEY_CANCEL )
		{
			   _vCls();
		           vDispMid(3, "终止自动查询请稍候...");
			   Keyflag=1;
		}
	}
   
    return;
}

void vGetPrtQrMerchInfo(uchar *psFields, stQrTransRec *rec)
{
    int len;
    uchar *p;
    uchar *CurPos;
	
    len=strlen(psFields);
    if(len==0)
        return;
    CurPos=psFields; 

     //商户号
     p = (unsigned char *)strchr((unsigned char *)CurPos,'|');
    if(p == NULL)
     {
	//	vMessage("格式不对Q1");
	 	return;
     }
      if((p-CurPos) > 	(sizeof(rec->sPrtMerchId) -1))
      {
      	//	vMessage("长度超限Q1");	
	//	return ;
	         memcpy(rec->sPrtMerchId, CurPos, 15);
      }
      else
    		 memcpy(rec->sPrtMerchId, CurPos, p-CurPos);
    CurPos = p+1;
	 
     //终端号
      p = (unsigned char *)strchr((unsigned char *)CurPos,'|');
	 if(p == NULL)
     {
	//	vMessage("格式不对Q2");
		return;
     }
      if((p-CurPos) > 	(sizeof(rec->sPrtPosId) -1))
      {
      	//	vMessage("长度超限Q2");	
	//	return ;
	        memcpy(rec->sPrtPosId, CurPos,8);
      }
      else	  
     		memcpy(rec->sPrtPosId, CurPos,p-CurPos);
     CurPos = p+1;
 
      //商户名
       if(strlen(CurPos) > (sizeof(rec->szPrtMerchName) -1))
      {
      	//	vMessage("长度超限Q3");	
	//	return ;
		rtrim(CurPos);
     		memcpy(rec->szPrtMerchName, CurPos,40);
      }
      else
     {
     		rtrim(CurPos);
      		strcpy(rec->szPrtMerchName,CurPos);  	
      }		
#ifdef REMOTE_GBK
    {
        char gbkstr[100];
        
        strcpy(gbkstr, (char*)rec->szPrtMerchName);        
        iGetStringGBK((uchar*)gbkstr, -1);
    }
#endif 
	  
    return;
}

unsigned char iQRCodePay(u8 nMode)
{
        uchar szQrCode[500] = {0};
        int iRet;
	char szTitle[20+1];
        ulong ulAmount;
	uchar szBuf[100];
	uchar PayWay;	
	ulong ulTTC;	
	ulong ulTimer,ulTimer1;
	uint uiKey;
	u16 len =0;
	uint time = 0,time1 = 0;
	uchar ucRet;
	u8 asBuf[128+1]={0};
	stQrTransRec QrTrRec;
	
	if(nMode == Z_phoneTopos)
		strcpy(szTitle,"用户扫码");
	else if(nMode == B_posTophone)
		strcpy(szTitle,"商户扫码");

	_vCls();	
	vDispCenter(1, szTitle, 1);
	
	if(iCheckTranStat( QRTRANS_TYPE_SALE,0))
        	return 1;
	
	if(gl_SysInfo.ucQrSaleSwitch == 0) 
	{
		TermRemindInfo();
		return 1;
        }

	gl_ucQrTransFlag = 1;
	
        if(nMode == Z_phoneTopos)
	{
		PayWay = Select_pay_way();
		if(PayWay==0)
		{
			return 1;
		}

		if(PayWay == MODE_WETCHAT)
		{				
			strcpy(szTitle,"微信支付");
		}
		else if(PayWay== MODE_ALIPAY)
		{
			strcpy(szTitle,"支付宝支付");
		}
		else if(PayWay== MODE_UNIONPAY)
		{
			strcpy(szTitle,"银联支付");
		}

		_vCls();
		vDispCenter(1, szTitle, 1);
        }
	 dbg("PayWay1:%d\n",PayWay);

	if(iGetAmount(NULL, "请输入金额",NULL, szBuf)<=0)
		return 1;	
	ulAmount=atol(szBuf);
		
	if(nMode == B_posTophone)
	{		
		sprintf(szBuf, "收款金额:%lu.%02lu", ulAmount/100, ulAmount%100);;
		vDispCenter(2, szBuf, 0);
		vDispCenter(3, "请出示付款码", 0);
                vDispCenter(_uiGetVLines(), "按[确认]键扫码", 0);

		_vSetTimer(&ulTimer, 6000L); //60秒		
		_vFlushKey();
		while(1)
		{
			if(_uiKeyPressed())
			{
				uiKey=_uiGetKey();
				if(uiKey==_KEY_ENTER)
					break;
				if(uiKey==_KEY_CANCEL)
					return 1;
			}	
			if(_uiTestTimer(ulTimer))
				return 1;
		}
		
		iRet = scanCode(szQrCode,sizeof(szQrCode));		
		if(iRet == -1)
               		 return 1;		
	
		dbg("qrcode=[%s]\n", szQrCode);

		PayWay= whitchAuthCode(szQrCode);
		if(PayWay == MODE_WETCHAT)
		{
			strcpy(szTitle,"微信支付");
		}	
		else if(PayWay == MODE_ALIPAY )
		{
			strcpy(szTitle,"支付宝支付");
		}	
		else if(PayWay == MODE_UNIONPAY)
		{
			strcpy(szTitle,"银联支付");
		}	
		else 
		{
			TermRemindInfo();
			return 1;
		}
		_vCls();
		vDispCenter(1, szTitle, 1);
		
	}
	
         memset(&gl_Send8583, 0, sizeof(gl_Send8583));
         memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
		 
        strcpy((char *)gl_Send8583.Msg01, "0200");
	strcpy((char *)gl_Send8583.Field03, "000011");
	sprintf((char *)gl_Send8583.Field04, "%012lu", ulAmount);
	
	vIncTTC();
	ulTTC=gl_SysData.ulTTC;
	sprintf((char *)gl_Send8583.Field11, "%06lu", ulTTC);
	strcpy((char *)gl_Send8583.Field25, "16");
	strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
	strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);
	
        //49域
        strcpy((char *)gl_Send8583.Field49, "156");

	//DE56
      if(PayWay == MODE_WETCHAT)
      	{
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x01",6);	
      	}
      else if(PayWay == MODE_ALIPAY)
	{
		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x02",6);	
      	}
       else if(PayWay == MODE_UNIONPAY)	  	
       {
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x03",6);	
      	}

	dbgHex("asBuf", asBuf, 6);
        vLongToStr(6, 2, gl_Send8583.Field56);
	memcpy(gl_Send8583.Field56+2, asBuf,6); 
       dbgHex("gl_Send8583.Field56", gl_Send8583.Field56, 8);

	//58域
	memset(asBuf,0,sizeof(asBuf));
    	GetPublicField058(asBuf);
	vLongToStr(strlen(asBuf), 2, gl_Send8583.Field58);
	memcpy(gl_Send8583.Field58+2, asBuf,strlen(asBuf));  
	dbgHex("gl_Send8583.Field58",gl_Send8583.Field58,strlen(asBuf));
	
	// 59 域
	if(nMode == Z_phoneTopos)
        	GetPublicField059_QR(NULL, gl_Send8583.Field59);
        else if(nMode == B_posTophone)
		GetPublicField059_QR(szQrCode+strlen(szQrCode)-6, gl_Send8583.Field59);				
	
        //60域
        if(nMode == Z_phoneTopos)
		sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s", 21, gl_SysData.ulBatchNo, "000");
	else if(nMode == B_posTophone)
		sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s", 22, gl_SysData.ulBatchNo, "000");

	if(nMode == B_posTophone)
	{
		//62域
		vLongToStr(strlen(szQrCode), 2, gl_Send8583.Field62);
		sprintf((char *)gl_Send8583.Field62+2, "%s", szQrCode);	
	}
	
	//63域
	sprintf((char *)gl_Send8583.Field63, "%-3.3s", gl_SysData.szCurOper);   //63.1

	 memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

	if(nMode == B_posTophone)
	{      
		memset(&QrTrRec, 0, sizeof(QrTrRec));			
		QrTrRec.uiTransType=QRTRANS_TYPE_SALE;

		QrTrRec.ucPayMode= B_posTophone;
		QrTrRec.ucProType = 0x02; //待确认
		QrTrRec.ulAmount=ulAmount;
		QrTrRec.ulBatchNo = gl_SysData.ulBatchNo;
		QrTrRec.ulTTC=ulTTC;
		dbg("QrTrRec.ulTTC12:%d\r\n",QrTrRec.ulTTC);
		strcpy(QrTrRec.szchnlQrCode, szQrCode);
		vTwoOne(gl_SysData.szCurOper, 2, QrTrRec.sOperNo);		
		_vGetTime(szBuf);
		vTwoOne(szBuf+2, 12, QrTrRec.sDateTime);		
		QrTrRec.ucPayWay = PayWay;	
                uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum, &QrTrRec);
				
		gl_SysData.uiQrTransNum ++;	
		 uiMemManaPutSysData();	
	}
	else if(nMode == Z_phoneTopos) 
	{	  
        	memset(&QrTrRec, 0, sizeof(QrTrRec));
		QrTrRec.uiTransType=QRTRANS_TYPE_SALE;
                QrTrRec.ucPayMode= Z_phoneTopos;
		QrTrRec.ucProType = 0x02; //待确认
		QrTrRec.ulAmount=ulAmount;
		QrTrRec.ulBatchNo = gl_SysData.ulBatchNo;
		QrTrRec.ulTTC=ulTTC;		
		vTwoOne(gl_SysData.szCurOper, 2, QrTrRec.sOperNo);
		_vGetTime(szBuf);
		vTwoOne(szBuf+2, 12, QrTrRec.sDateTime);
		QrTrRec.ucPayWay = PayWay;
	        uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum, &QrTrRec);
			
		gl_SysData.uiQrTransNum ++;
	        uiMemManaPutSysData();	
         }

   	 //打8583包-填充http-发送-接收-解http-解8583包
	iRet = iHttpSendRecv8583(0);
	if (iRet)
	{
	        return 1;
   	 }

	if (strcmp((char *)gl_Recv8583.Field39, "00") && strcmp((char *)gl_Recv8583.Field39, "IP"))
	{
		 if (strcmp((char *)gl_Recv8583.Field39, "IF") == 0)
		 {
		 //	QrTrRec.ucProType = 0x03;
		//	uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrRec);

			gl_SysData.uiQrTransNum --;
	                uiMemManaPutSysData();	
			
			vClearLines(2);
			vDispMid(3,gl_Recv8583.Field39);	
			vDispMid(4,"交易失败");
			vMessage(NULL);
		 }
		 else if(strcmp((char *)gl_Recv8583.Field39, "03") == 0 || strcmp((char *)gl_Recv8583.Field39, "12") == 0 
				||strcmp((char *)gl_Recv8583.Field39, "94") == 0 || strcmp((char *)gl_Recv8583.Field39, "96") == 0 
				||strcmp((char *)gl_Recv8583.Field39, "FF") == 0 ||  strcmp((char *)gl_Recv8583.Field39, "A0") == 0)
		{
		//	QrTrRec.ucProType = 0x03;
		//	uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrRec);

			gl_SysData.uiQrTransNum --;
	                uiMemManaPutSysData();
					
			vShowHostErrMsg(gl_Recv8583.Field39,NULL);			
		}
		 else
		 {
		 	gl_SysData.uiQrTransNum --;
	                uiMemManaPutSysData();
					
		 	vShowHostErrMsg(gl_Recv8583.Field39,NULL);
		 }	
		return 1;
	} 
		
	if(nMode == B_posTophone)
	{     
		_vGetTime(szBuf);
		if(gl_Recv8583.Field13[0])
			memcpy(szBuf+4, gl_Recv8583.Field13, 4);
		if(gl_Recv8583.Field12[0])
			memcpy(szBuf+8, gl_Recv8583.Field12, 6);
		vTwoOne(szBuf+2, 12, QrTrRec.sDateTime);
		if(gl_Recv8583.Field37[0])
			strcpy(QrTrRec.sReferenceNo, gl_Recv8583.Field37);	

		if(gl_Recv8583.Field43[0] && QrTrRec.ucPayWay == MODE_UNIONPAY)
		{
			vGetPrtQrMerchInfo(gl_Recv8583.Field43, &QrTrRec);
		}	
			
	        len = ulStrToLong(gl_Recv8583.Field56,2);
		if(len>0)	
		{	
			DivPublicField056(gl_Recv8583.Field56+2,len,&QrTrRec);	
	         	dbg("orderId:%s\n",QrTrRec.orderId);
		}		
		 uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrRec);		 

	 	_vCls();
		vDispCenter(1, szTitle, 1);			

		vDispCenter(2, "查询中,请稍候...", 0);
	//	vDispCenter(4, "[取消]退出",0);	
	}
	else if(nMode == Z_phoneTopos) 
	{	
		len = ulStrToLong(gl_Recv8583.Field62,2);				
		memcpy(QrTrRec.szchnlUrl,gl_Recv8583.Field62 + 2,len);	
		
		_vGetTime(szBuf);		
		if(gl_Recv8583.Field13[0])
			memcpy(szBuf+4, gl_Recv8583.Field13, 4);
		if(gl_Recv8583.Field12[0])
			memcpy(szBuf+8, gl_Recv8583.Field12, 6);
		vTwoOne(szBuf+2, 14, QrTrRec.sDateTime);			
		if(gl_Recv8583.Field37[0])
			strcpy(QrTrRec.sReferenceNo, gl_Recv8583.Field37);				

		if(gl_Recv8583.Field43[0] && QrTrRec.ucPayWay == MODE_UNIONPAY)
		{
			vGetPrtQrMerchInfo(gl_Recv8583.Field43, &QrTrRec);
		}	
					
		len = ulStrToLong(gl_Recv8583.Field56,2);
	         DivPublicField056(gl_Recv8583.Field56+2,len,&QrTrRec);	
		dbg("orderId:%s\n",QrTrRec.orderId);	 
	        uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrRec);	

	       _vCls();
		vDispCenter(1, szTitle, 1);
	//	_vSetBackLight(1);				
           		DispTransQrCode(QrTrRec.szchnlUrl);
	        
		_vDispAt(2, 11, "请扫码二");
		_vDispAt(3, 11, "维码支付");		
	//	_vDispAt(4, 11, "[取消]退出");	
		 vSetCommNoDisp(1);      //关闭通讯提示,避免提示信息破坏二维码	
         }
	
	 memset(&gl_Send8583, 0, sizeof(gl_Send8583));
         memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));

	strcpy((char *)gl_Send8583.Msg01, "0200");
	strcpy((char *)gl_Send8583.Field03, "310000");
//	vIncTTC();
//	ulTTC=gl_SysData.ulTTC;
//	sprintf((char *)gl_Send8583.Field11, "%06lu", ulTTC);

	strcpy((char *)gl_Send8583.Field25, "16");
	strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
	strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);

	//DE56
	memset(asBuf,0x00,sizeof(asBuf));
      if(QrTrRec.ucPayWay == MODE_WETCHAT)
      	{
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x01",6);	
      	}
      else if(QrTrRec.ucPayWay == MODE_ALIPAY)
	{
		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x02",6);	
      	}
       else if(QrTrRec.ucPayWay == MODE_UNIONPAY)	  	
       {
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x03",6);	
      	}

	dbgHex("asBuf", asBuf, 6);
        vLongToStr(6, 2, gl_Send8583.Field56);
	memcpy(gl_Send8583.Field56+2, asBuf,6); 
       dbgHex("gl_Send8583.Field56", gl_Send8583.Field56, 8);

        //60域
	sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s", 01, gl_SysData.ulBatchNo, "000");

	//61域
	vOneTwo0(QrTrRec.sDateTime+1, 2, szBuf);
        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", gl_SysData.ulBatchNo, QrTrRec.ulTTC, szBuf);

	//63域
	sprintf((char *)gl_Send8583.Field63, "%-3.3s", gl_SysData.szCurOper);   //63.1

	 memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

         _vSetTimer(&ulTimer, 18000L); //180秒	 
          _vSetTimer(&ulTimer1, 500L); //5秒	 
         _vFlushKey();
	Keyflag=0;	
	while (!_uiTestTimer(ulTimer))
	{			        
	        time = _uiCheckTimer(ulTimer); 
		time1 = _uiCheckTimer(ulTimer1); 	
		if(!time) break;
		if(time <= 150) 
			vSetCommCallBack(vDispWarnInfoCallBack);
		else 
			_vFlushKey();
		if(!time1 ) {
			vIncTTC();
			ulTTC=gl_SysData.ulTTC;
			sprintf((char *)gl_Send8583.Field11, "%06lu", ulTTC);	
		iRet = iHttpSendRecv8583(0);
		if (iRet)
		{
			 vSetCommNoDisp(0);
			 vSetCommCallBack(NULL);
			return 1;
		}
               
		if(memcmp(gl_Recv8583.Field39,"IP",2)==0x00)
		{			
		        if(Keyflag == 1)
		        {
		        	vDispCenter(1, szTitle, 1);
				vDispMid(2, "交易取消");
				vDispMid(3, "请在【支付查询】菜单");
				vDispMid(4, "手动发起查询");
				vMessage(NULL);
				
		             	if(nMode == B_posTophone)
				{
					vSetCommCallBack(NULL);
					return 1;
				}
				if(nMode == Z_phoneTopos)						    
				{
					vSetCommCallBack(NULL);
					vSetCommNoDisp(0);
					return 1;
				}
		        }
				
			 _vSetTimer(&ulTimer1, 500L); //5秒
			continue;
		}	
		
		if(memcmp(gl_Recv8583.Field39,"00",2)==0x00)
		{      
		         _vCls();
			vDispCenter(1, szTitle, 1);
			vSetCommNoDisp(0);
		       //成功
			
		       QrTrRec.ucProType = 0x01;

		      	if(gl_Recv8583.Field37[0])
				strcpy(QrTrRec.sReferenceNo, gl_Recv8583.Field37);	
			    
			if(gl_Recv8583.Field43[0] && QrTrRec.ucPayWay == MODE_UNIONPAY)
			{
				vGetPrtQrMerchInfo(gl_Recv8583.Field43, &QrTrRec);
			}	

			len = ulStrToLong(gl_Recv8583.Field56,2);
			if(len>0)
			{
				DivPublicField056(gl_Recv8583.Field56+2,len,&QrTrRec);	
				dbg("orderId_query:%s\n",QrTrRec.orderId);	
			}	
						
			uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrRec);			
			
			gl_SysData.ulLastTransTTC = QrTrRec.ulTTC;
			uiMemManaPutSysData();
				
#ifdef ENABLE_PRINTER 									 
			{
				vShowWaitEx("支付成功,打印票据...", -1, 0); 
				iPrintQrTrans(&QrTrRec, 0);
				vShowWaitEx(NULL, 0, 0x03); 
			}
#else    
			{				
				vMessage("支付成功");
			}
#endif 
			    vSetCommCallBack(NULL);
		             return 0;
		}	
		else
		{		
		//失败
		         vSetCommNoDisp(0);
		        _vCls();
			vDispCenter(1, szTitle, 1);
			if (strcmp((char *)gl_Recv8583.Field39, "IF") == 0)
			{
			//	QrTrRec.ucProType = 0x03;
			//	uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrRec);	

				gl_SysData.uiQrTransNum --;
	                	uiMemManaPutSysData();
					
				vClearLines(2);
				vDispMid(3,gl_Recv8583.Field39);	
				vDispMid(4,"交易失败");
				vMessage(NULL);
			}
			else if(strcmp((char *)gl_Recv8583.Field39, "03") == 0
				|| strcmp((char *)gl_Recv8583.Field39, "12") == 0 ||strcmp((char *)gl_Recv8583.Field39, "94") == 0
				|| strcmp((char *)gl_Recv8583.Field39, "96") == 0 ||strcmp((char *)gl_Recv8583.Field39, "FF") == 0)
			{
			//	QrTrRec.ucProType = 0x03;
			//	uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrRec);	

				gl_SysData.uiQrTransNum --;
	                	uiMemManaPutSysData();
					
				vShowHostErrMsg(gl_Recv8583.Field39,NULL);
			}
			else
			{
				gl_SysData.uiQrTransNum --;
	                	uiMemManaPutSysData();
						
		 		vShowHostErrMsg(gl_Recv8583.Field39,NULL);
			}
			vSetCommCallBack(NULL);
		        return 1;
		}			
	     }

        	if(_uiKeyPressed() && time <= 30)
		{
			uiKey =_uiGetKey();
			if(uiKey ==_KEY_CANCEL && nMode == B_posTophone)
			{
				vSetCommCallBack(NULL);
				return 1;
			}
			if(uiKey ==_KEY_CANCEL && nMode == Z_phoneTopos)						    
			{
				vSetCommCallBack(NULL);
				vSetCommNoDisp(0);
				return 1;
			}
		}		
	}
	
	vSetCommCallBack(NULL);
        dbg("QrTrRec.ucPayWay:%d\n",QrTrRec.ucPayWay);
	vSetCommNoDisp(0);
	ucRet =  iQRCodePay_Query(QrTrRec.ulTTC,0,0);
	return ucRet;
}

unsigned char iQRCodePay_Query(ulong ulTTC,int  dup,int flag)
{
	u8 timce=0;
        int iRet;
	uint uiKey;
	 int iIndex,iIndex1;
	 ulong ulTimer;
	 u8 asBuf[128+1]={0};
	 uchar szBuf[100];
	 u16 len =0;
	stQrTransRec QrTrRec,QrQueTrRec;

        _vCls();
	vDispCenter(1, "支付结果查询", 1);

	gl_ucQrTransFlag = 1;
	
        dbg("ulTTC=%d\r\n",ulTTC);
	for(iIndex=0; iIndex<gl_SysData.uiQrTransNum; iIndex++)
        {
		memset(&QrQueTrRec,0,sizeof(QrQueTrRec));
		if(uiMemManaGetQrTransRec(iIndex, &QrQueTrRec))
		{
			vMessage("读取交易记录失败");
			return 1;
		}
		dbg("QrQueTrRec.ulTTC =%d\r\n",QrQueTrRec.ulTTC);
		if(QrQueTrRec.ulTTC==ulTTC)
		{
		      dbg("iIndex:%d\r\n",iIndex);
			break;	
			}
        }

	 if(iIndex>=gl_SysData.uiQrTransNum)
        {
            vMessage("无此交易记录");
            return 1;
        }

	 memset(&gl_Send8583, 0, sizeof(gl_Send8583));
         memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));

	strcpy((char *)gl_Send8583.Msg01, "0200");
	strcpy((char *)gl_Send8583.Field03, "310000");
	
	vIncTTC();
	sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);	
	
	strcpy((char *)gl_Send8583.Field25, "16");
	strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
	strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);

	//DE56
	memset(asBuf,0x00,sizeof(asBuf));
      if(QrQueTrRec.ucPayWay == MODE_WETCHAT)
      	{
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x01",6);	
      	}
      else if(QrQueTrRec.ucPayWay == MODE_ALIPAY)
	{
		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x02",6);	
      	}
       else if(QrQueTrRec.ucPayWay == MODE_UNIONPAY)	  	
       {
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x03",6);	
      	}

	dbgHex("asBuf", asBuf, 6);
        vLongToStr(6, 2, gl_Send8583.Field56);
	memcpy(gl_Send8583.Field56+2, asBuf,6); 
       dbgHex("gl_Send8583.Field56", gl_Send8583.Field56, 8);

        //60域
	sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s", 01, gl_SysData.ulBatchNo, "000");

	//61域
	vOneTwo0(QrQueTrRec.sDateTime+1, 2, szBuf);
        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", gl_SysData.ulBatchNo, ulTTC, szBuf);

	//63域
	sprintf((char *)gl_Send8583.Field63, "%-3.3s", gl_SysData.szCurOper);   //63.1

	 memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

doQueryOnce:
	
	iRet = iHttpSendRecv8583(0);
	if (iRet)
	{
                //显示错误信息
	        return 1;
   	 }
	
 	if(memcmp(gl_Recv8583.Field39,"IP",2)==0)
        {
                timce++;							                  

                    if(timce == 3)
                    {
			//失败
			vClearLines(2);
			vDispMid(3,gl_Recv8583.Field39);	
			vDispMid(4,"交易处理中");	
			vMessage(NULL);
			
			return 1;
                    }

                   	vClearLines(2);
		vDispMid(3,gl_Recv8583.Field39);	
		vDispMid(4,"交易处理中");	
		_vDisp(5,"退出            查询");  
		
		_vSetTimer(&ulTimer, 500L); //5秒
                   _vFlushKey();
		while(1)
		{		
                        if(_uiTestTimer(ulTimer))	
			{
				vClearLines(2);
				vIncTTC();
				sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);
				goto doQueryOnce;
			}else if(_uiKeyPressed())
			{
				uiKey =_uiGetKey();
				if(uiKey ==_KEY_ENTER)
				{
					vClearLines(2);
					vIncTTC();
					sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);
					goto doQueryOnce;
				}
				else if(uiKey ==_KEY_CANCEL)						    
				{
					return	1;	
				}
			}
		}	 				   	
         }
	
	if(memcmp(gl_Recv8583.Field39,"00",2)==0x00)
	{	   
		if(QrQueTrRec.uiTransType == QRTRANS_TYPE_VOID)
		{
		        for(iIndex1=0; iIndex1 < gl_SysData.uiQrTransNum; iIndex1++)
		        {
				memset(&QrTrRec,0,sizeof(stQrTransRec));
				if(uiMemManaGetQrTransRec(iIndex1, &QrTrRec))
				{
					vMessage("读取交易记录失败");
					return 1;
				}
				dbg("ulTTC=%d\r\n",QrTrRec.ulTTC);
				if(QrTrRec.ulTTC == QrQueTrRec.ulVoidTTC)
					break;
		        }
				
			QrTrRec.ucVoidFlag = 0x01;
			uiMemManaPutQrTransRec(iIndex1, &QrTrRec);
			
		}
/*		
		else if(QrQueTrRec.uiTransType == QRTRANS_TYPE_REFUND)
		{
			_vGetTime(szBuf);
		      vOneTwo0(QrQueTrRec.sVoidDateTime, 2, szBuf+4);
		      for(iIndex1=0; iIndex1 < gl_SysData.uiQrTransNum; iIndex1++)
	       	      {
				memset(&QrTrRec,0,sizeof(stQrTransRec));
				uiMemManaGetQrTransRec(iIndex1, &QrTrRec);
				dbg("ulTTC=%d\r\n",QrTrRec.ulTTC);
				vOneTwo0(QrTrRec.sDateTime, 6, szBuf1);
				dbg("szBuf+2:%s\n",szBuf+2);
				dbg("szBuf1:%s\n",szBuf1);
				if(QrTrRec.ulBatchNo == QrQueTrRec.ulVoidBatchNo && QrTrRec.ulTTC == QrQueTrRec.ulVoidTTC
					&& memcmp(szBuf+2,szBuf1,6) == 0)
				{
				        dbg("12345678\n");
					QrTrRec.ucVoidFlag = 0x03;
					uiMemManaPutQrTransRec(iIndex1, &QrTrRec);
					break;
				}	
	               }
		}
*/		
			
           //成功			
        	QrQueTrRec.ucProType = 0x01;

		if(gl_Recv8583.Field37[0])
			strcpy(QrQueTrRec.sReferenceNo, gl_Recv8583.Field37);	
							
		if(gl_Recv8583.Field43[0] && QrQueTrRec.ucPayWay == MODE_UNIONPAY)
		{
			vGetPrtQrMerchInfo(gl_Recv8583.Field43, &QrQueTrRec);
		}	

               	len = ulStrToLong(gl_Recv8583.Field56,2);
		if(len>0)
		{
			DivPublicField056(gl_Recv8583.Field56+2,len,&QrQueTrRec);	
			dbg("orderId_query:%s\n",QrQueTrRec.orderId);	
		}	
		
		uiMemManaPutQrTransRec(iIndex, &QrQueTrRec);
		
		gl_SysData.ulLastTransTTC = QrQueTrRec.ulTTC;
		uiMemManaPutSysData();

#ifdef ENABLE_PRINTER     
	        { 						
			vShowWaitEx("支付成功,打印票据...", -1, 0); 			
			iPrintQrTrans(&QrQueTrRec, dup);
			vShowWaitEx(NULL, 0, 0x03); 	
		}	
#else    			
                {
			vMessage("支付成功");
		}	
		
#endif 
                 return 0;
	}	
	else
	{		
	//失败
		if (strcmp((char *)gl_Recv8583.Field39, "IP") == 0)
		{
			vClearLines(2);
			vDispMid(3,gl_Recv8583.Field39);	
			vDispMid(4,"交易处理中");
			vMessage(NULL);
		}	
		else if (strcmp((char *)gl_Recv8583.Field39, "IF") == 0 )
		{
			if(flag == 1)
			{
				QrQueTrRec.ucProType = 0x03;
				uiMemManaPutQrTransRec(iIndex, &QrQueTrRec);
			}else if(flag == 0)
			{
				gl_SysData.uiQrTransNum --;
	                	uiMemManaPutSysData();
			}
			
			vClearLines(2);
			vDispMid(3,gl_Recv8583.Field39);	
			vDispMid(4,"交易失败");	
			vMessage(NULL);
		}
		else if(strcmp((char *)gl_Recv8583.Field39, "03") == 0 || strcmp((char *)gl_Recv8583.Field39, "12") == 0 
				||strcmp((char *)gl_Recv8583.Field39, "94") == 0 || strcmp((char *)gl_Recv8583.Field39, "96") == 0 
				||strcmp((char *)gl_Recv8583.Field39, "FF") == 0)
		{
			if(flag == 1)
			{
				QrQueTrRec.ucProType = 0x03;
				uiMemManaPutQrTransRec(iIndex, &QrQueTrRec);
			}else if(flag == 0)
			{
				gl_SysData.uiQrTransNum --;
	                	uiMemManaPutSysData();
			}

			vShowHostErrMsg(gl_Recv8583.Field39,NULL);			
		}
		else if(strcmp((char *)gl_Recv8583.Field39, "A0") == 0)
		{
			vShowHostErrMsg(gl_Recv8583.Field39,NULL);	
		}
		else
		{
			if(flag == 1)
			{
				QrQueTrRec.ucProType = 0x03;
				uiMemManaPutQrTransRec(iIndex, &QrQueTrRec);
			}else if(flag == 0)
			{
				gl_SysData.uiQrTransNum --;
	                	uiMemManaPutSysData();
			}

			vShowHostErrMsg(gl_Recv8583.Field39,NULL);	
		}	

	        return 1;
	} 

}

unsigned char iQRCodeVoid(uint uiTransType)
{
	int iRet;
	char szBuf[100] = {0},szTitle[20+1] = {0};
	ulong ulVoidTTC;	
	int iIndex;
	ulong  ulAmount;
	int page = 0;
	uint uiKey;
	char temp[100] = {0};
	uchar isexit = 0;
	stQrTransRec QrTrRec,QrVoidTraRec;
	uchar PayWay;
	int col;
	 uchar  orderId[60+1] = {0};      	      // 支付渠道订单号 DF27    
	 u8 asBuf[128+1]={0};
	 int len  = 0;
	 int line;
	 
	_vCls();	
	strcpy(szTitle, "扫码撤销");
	vDispCenter(1, szTitle, 1); 	

	if(iCheckTranStat(uiTransType, 0))
		return 1;

	if(iMainOperCheck((char*)gl_SysInfo.szMainOperPwd))
		return 1;

        gl_ucQrTransFlag = 1;
		
	ulVoidTTC=0L;
	iIndex=0;

	vClearLines(2);
	col=(_uiGetVCols()-6)/2;
	_vDisp(2, "请输入原凭证号:");
	vDispCenter(_uiGetVLines(), "输错请按[清除]", 0);
	iRet=iInput(INPUT_NORMAL|INPUT_CENTER_SPACE|INPUT_DOT_SPACE, 3, col+1, szBuf, 6, 1, 6, 30);
	if(iRet<0)
		return 1;
	ulVoidTTC=atol(szBuf);
	if(ulVoidTTC==0)
	{
		vMessage("无效凭证号");
		return 1;
	}
        
        vClearLines(2);
	dbg("uiQrTransNum=%d\r\n",gl_SysData.uiQrTransNum);
	dbg("ulVoidTTC=%d\r\n",ulVoidTTC);
        for(iIndex=0; iIndex<gl_SysData.uiQrTransNum; iIndex++)
        {
		memset(&QrTrRec,0,sizeof(stQrTransRec));
		if(uiMemManaGetQrTransRec(iIndex, &QrTrRec))
		{
			vMessage("读取交易记录失败");
			return 1;
		}
		dbg("ulTTC=%d\r\n",QrTrRec.ulTTC);
		if(QrTrRec.ulTTC == ulVoidTTC)
			break;
        }
        dbg("iIndex=%d\r\n",iIndex);
        if(iIndex>=gl_SysData.uiQrTransNum)
        {
            vMessage("无此交易记录");
            return 1;
        }

         if(QrTrRec.uiTransType!= QRTRANS_TYPE_SALE)
        {
            vMessage("交易类型不匹配");
            return 1;
        }
			   
        if(QrTrRec.ucVoidFlag)
        {
            vMessageEx("该交易已撤销", 800);
            return 1;
        }

	if(QrTrRec.ucProType == 0x02)
        {
            vMessageEx("该交易未支付", 800);
            return 1;
        }
			
        page = 1;
	strcpy(orderId,QrTrRec.orderId);
	memset(szBuf,0x00,sizeof(szBuf));
	vOneTwo0(QrTrRec.sDateTime, 6, szBuf);
	dbg("chencheng123\n");
        while(isexit != 1)
        {      
                _vCls();
                vDispCenterEx(1, szTitle, 1,1,page,2); 
                if(page == 1)  
		{		        
		        if(QrTrRec.ucPayMode== B_posTophone)
				_vDisp(2, "商户扫码");
		        else if(QrTrRec.ucPayMode == Z_phoneTopos)
				_vDisp(2, "用户扫码");	
				
			if(QrTrRec.ucPayWay == MODE_WETCHAT)
			{
				vDispVarArg(3,"支付类型:%s","微信扫码");
			        PayWay = MODE_WETCHAT;
			}		
			else if(QrTrRec.ucPayWay == MODE_ALIPAY )
			{
				vDispVarArg(3,"支付类型:%s","支付宝扫码");
			        PayWay = MODE_ALIPAY; 
			}		
			else if(QrTrRec.ucPayWay == MODE_UNIONPAY)
			{
				vDispVarArg(3,"支付类型:%s","银联扫码");	
			         PayWay = MODE_UNIONPAY;    
			}		 
			 vDispVarArg(4, "凭证号:%06lu", QrTrRec.ulTTC);
		         vDispVarArg(5, "金额:%lu.%02lu", QrTrRec.ulAmount/100, QrTrRec.ulAmount%100);
        	}
		else if(page ==2)
		{	
		       line = 2;
	               vDispVarArg(line++, "参考号:%.12s", QrTrRec.sReferenceNo);
			sprintf(temp,"订单号:%s",QrTrRec.orderId);		
			_vDisp(line++, temp);		
			if(strlen(QrTrRec.orderId) > _uiGetVCols() - 7)
				_vDisp(line++, QrTrRec.orderId+_uiGetVCols() - 7);
	                vDispVarArg(line++, "20%.2s/%.2s/%.2s  %.2s:%.2s:%.2s", szBuf, szBuf+2, szBuf+4, szBuf+6, szBuf+8, szBuf+10);	
		}   	                

		_vFlushKey();	
		while(1)
		{
			if(_uiKeyPressed())
			{
				uiKey =_uiGetKey();
				if(uiKey==_KEY_ESC)
					return 1;		
			        if(uiKey == _KEY_DOWN  &&  page != 2)
			        {
			        	page ++;
					break;
			        }				 	
		                if(uiKey == _KEY_UP  &&  page != 1)
			        {
			        	page --;
					break;
			        }	
				if(uiKey == _KEY_ENTER)
				{
					 isexit = 1;
					break;
				}
		 	 }
		}		
	}

	_vCls();
	vDispCenter(1, szTitle, 1); 	
	
	memset(&gl_Send8583, 0, sizeof(gl_Send8583));
	memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));

	strcpy((char *)gl_Send8583.Msg01, "0200");
	strcpy((char *)gl_Send8583.Field03, "200000");
	sprintf((char *)gl_Send8583.Field04, "%012lu", QrTrRec.ulAmount);
	ulAmount = QrTrRec.ulAmount;

	vIncTTC();
	sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);
	
	strcpy((char *)gl_Send8583.Field25, "16");
	
//	strcpy((char *)gl_Send8583.Field37, (char *)QrTrRec.sReferenceNo);
	strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
	strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);

	strcpy((char *)gl_Send8583.Field49, "156");	

	//DE56	
	memset(asBuf,0x00,sizeof(asBuf));
      if(QrTrRec.ucPayWay == MODE_WETCHAT)
      	{
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x01",6);	
      	}
      else if(QrTrRec.ucPayWay == MODE_ALIPAY)
	{
		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x02",6);	
      	}
       else if(QrTrRec.ucPayWay == MODE_UNIONPAY)	  	
       {
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x03",6);	
      	}

	dbgHex("asBuf", asBuf, 6);
        vLongToStr(6, 2, gl_Send8583.Field56);
	memcpy(gl_Send8583.Field56+2, asBuf,6); 
       dbgHex("gl_Send8583.Field56", gl_Send8583.Field56, 8);

/*	
	memset(asBuf,0x00,sizeof(asBuf));
      if(QrTrRec.ucPayWay == MODE_WETCHAT)
      	{
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x01",6);

	        memcpy(asBuf+6,"\x9f\x02",2);
		len = strlen(QrTrRec.orderId);    
		asBuf[8]=len/256;
		asBuf[9]=len%256;
		memcpy(&asBuf[10],QrTrRec.orderId,len);
      	}
      else if(QrTrRec.ucPayWay == MODE_ALIPAY)
	{
		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x02",6);	

                memcpy(asBuf+6,"\x9f\x02",2);
		len = strlen(QrTrRec.orderId);    
		asBuf[8]=len/256;
		asBuf[9]=len%256;
		memcpy(&asBuf[10],QrTrRec.orderId,len);
      	}
       else if(QrTrRec.ucPayWay == MODE_UNIONPAY)	  	
       {
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x03",6);	

	         memcpy(asBuf+6,"\x9f\x02",2);
		len = strlen(QrTrRec.orderId);    
		asBuf[8]=len/256;
		asBuf[9]=len%256;
		memcpy(&asBuf[10],QrTrRec.orderId,len);	
      	}

	dbgHex("asBuf", asBuf, 6+4+len);
        vLongToStr(6+4+len, 2, gl_Send8583.Field56);
	memcpy(gl_Send8583.Field56+2, asBuf,6+4 + len); 
 	dbgHex("gl_Send8583.Field56", gl_Send8583.Field56, 6 + 4+len);
*/

	//58域
	memset(asBuf,0,sizeof(asBuf));
    	GetPublicField058(asBuf);
	vLongToStr(strlen(asBuf), 2, gl_Send8583.Field58);
	memcpy(gl_Send8583.Field58+2, asBuf,strlen(asBuf));  
	dbgHex("gl_Send8583.Field58",gl_Send8583.Field58,strlen(asBuf));
	
	// 59 域
	memset(asBuf,0,sizeof(asBuf));
	GetPublicField059_QR(QrTrRec.szchnlQrCode+strlen(QrTrRec.szchnlQrCode)-6, gl_Send8583.Field59);	
		
        //60域
//	sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s", 23, gl_SysData.ulBatchNo, "000");
        sprintf((char *)gl_Send8583.Field60, "%02u%06lu", 23, gl_SysData.ulBatchNo);

	//61域
	vOneTwo0(QrTrRec.sDateTime+1, 2, szBuf);
        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", gl_SysData.ulBatchNo, ulVoidTTC, szBuf);

	//63域
//	sprintf((char *)gl_Send8583.Field63, "%-3.3s", gl_SysData.szCurOper);   //63.1

	 memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

   	memset(&QrVoidTraRec, 0, sizeof(QrVoidTraRec));
	QrVoidTraRec.ucProType = 0x02; //待确认
	QrVoidTraRec.ucPayWay = PayWay;
	QrVoidTraRec.ulVoidTTC = ulVoidTTC;
	strcpy(QrVoidTraRec.szVoidOrderId,orderId);
	QrVoidTraRec.uiTransType=QRTRANS_TYPE_VOID;
	QrVoidTraRec.ulAmount=ulAmount;
	QrVoidTraRec.ulTTC=gl_SysData.ulTTC;
	vTwoOne(gl_SysData.szCurOper, 2, QrVoidTraRec.sOperNo);
         memset(szBuf,0x00,sizeof(szBuf));
	_vGetTime(szBuf);
	vTwoOne(szBuf+2, 12, QrVoidTraRec.sDateTime);
        uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum, &QrVoidTraRec);

	gl_SysData.uiQrTransNum ++;	
	 uiMemManaPutSysData();
		 
	//打8583包-填充http-发送-接收-解http-解8583包	
	iRet = iHttpSendRecv8583(0);
	if (iRet)
	{
		return 1;
	}	
	  	
	vClearLines(2);	
	if(memcmp(gl_Recv8583.Field39,"00",2)==0x00)
	{
		QrTrRec.ucVoidFlag = 0x01;
		uiMemManaPutQrTransRec(iIndex, &QrTrRec);

                QrVoidTraRec.ucProType = 0x01;
                memset(szBuf,0x00,sizeof(szBuf));
		_vGetTime(szBuf);
		if(gl_Recv8583.Field13[0])
			memcpy(szBuf+4, gl_Recv8583.Field13, 4);
		if(gl_Recv8583.Field12[0])
			memcpy(szBuf+8, gl_Recv8583.Field12, 6);
		vTwoOne(szBuf+2, 12, QrVoidTraRec.sDateTime);

		if(gl_Recv8583.Field43[0] && QrVoidTraRec.ucPayWay == MODE_UNIONPAY)
		{
			vGetPrtQrMerchInfo(gl_Recv8583.Field43, &QrVoidTraRec);
		}	

		if(gl_Recv8583.Field37[0])
			strcpy(QrVoidTraRec.sReferenceNo, gl_Recv8583.Field37);	

		len = ulStrToLong(gl_Recv8583.Field56,2);
		if(len>0)
	        {
	        	DivPublicField056(gl_Recv8583.Field56+2,len,&QrVoidTraRec);	
			dbg("orderId:%s\n",QrVoidTraRec.orderId);	
		}	
		
		uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrVoidTraRec);
		gl_SysData.ulLastTransTTC = QrVoidTraRec.ulTTC;
		uiMemManaPutSysData();
				
#ifdef ENABLE_PRINTER                 
		vShowWaitEx("交易成功,打印票据...", -1, 0); 
		iPrintQrTrans(&QrVoidTraRec, 0);
		vShowWaitEx(NULL, 0, 0x03); 
	
#else        	
		vMessage("交易成功");
		
#endif
	}		
	else
	{
		QrVoidTraRec.ucProType = 0x03;
		uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrVoidTraRec);
		
		vShowHostErrMsg(gl_Recv8583.Field39,NULL);
		return 1;
	}

	return 0;

}

unsigned char iQRCodeRefund(uint uiTransType)
{
	int iRet;
	char szBuf[100] = {0},szTitle[20+1] = {0},szBuf1[12+1] = {0};
	ulong ulVoidTTC;	
	ulong ulVoidBatchNo;
	uchar VoidDateTime[2+1];
	int i;
	int iIndex;
	ulong  ulAmount;
	stQrTransRec QrTrRec,QrRefundTraRec;
	uchar PayWay;
	int col; 
	 u8 asBuf[128+1]={0};
	  int len  = 0;
	  uchar  szchnlQrCode[30+1] ={0};      	      // 二维码           
	  uchar ucoriginal = 0;
	  
	_vCls();	
	strcpy(szTitle, "扫码退款");
	vDispCenter(1, szTitle, 1); 	

	if(iCheckTranStat(uiTransType, 0))
		return 1;

	if(iMainOperCheck((char*)gl_SysInfo.szMainOperPwd))
		return 1;

        gl_ucQrTransFlag = 1;
/*
	vClearLines(2);
        _vDisp(2, "请输原交易订单号:");
	ucPinPos = _uiGetVCols();
        if(iInput(INPUT_NORMAL|INPUT_DOT_SPACE|INPUT_TWO_LINE, 3, ucPinPos, szBuf, 40, 0, 40, 30) <= 0)
            return -1;
        strcpy(VoidOrderId,szBuf);
*/
	vClearLines(2);
	col=(_uiGetVCols()-6)/2;
	_vDisp(2, "请输入原批次号:");
	vDispCenter(_uiGetVLines(), "输错请按[清除]", 0);
	iRet=iInput(INPUT_NORMAL|INPUT_CENTER_SPACE|INPUT_DOT_SPACE, 3, col+1, szBuf, 6, 1, 6, 30);
	if(iRet<0)
		return 1;
	ulVoidBatchNo=atol(szBuf);
	if(ulVoidBatchNo==0)
	{
		vMessage("无效批次号");
		return 1;
	}

	ulVoidTTC=0L;
	
	vClearLines(2);
	col=(_uiGetVCols()-6)/2;
	_vDisp(2, "请输入原凭证号:");
	vDispCenter(_uiGetVLines(), "输错请按[清除]", 0);
	iRet=iInput(INPUT_NORMAL|INPUT_CENTER_SPACE|INPUT_DOT_SPACE, 3, col+1, szBuf, 6, 1, 6, 30);
	if(iRet<0)
		return 1;
	ulVoidTTC=atol(szBuf);
	if(ulVoidTTC==0)
	{
		vMessage("无效凭证号");
		return 1;
	}
	
         vClearLines(2);
	_vDisp(2, "请输入原交易日期:");
	_vDisp(3, "      (月月日日)");
	while(1)	
	{	 
		iRet=iInput(INPUT_NORMAL|INPUT_DOT_SPACE|INPUT_CENTER_SPACE, 4, 9, szBuf, 4, 4, 4, 30);
			if(iRet<0)
		return 1;
		i=ulA2L(szBuf, 2);
		if(i==0 || i>12)
		{
			vMessageLine(4,"无效日期");
			_vDisp(4, " ");
		continue;
	}
	i=ulA2L(szBuf+2, 2);
	if(i==0 || i>31)
	{
	vMessageLine(4,"无效日期");
	_vDisp(4, " ");
	continue;
	}
	if (iRet == 4)
	break;	
	}	
	vTwoOne(szBuf, 4, VoidDateTime);
	
	
	    vClearLines(2);
	    if(iGetAmount(NULL, "请输入金额", NULL,szBuf)<=0)
	        return 1;
	    ulAmount = atol(szBuf);

	PayWay = Select_pay_way();
	if(PayWay==0)
	{
		return 1;
	}
	
	_vCls();
	vDispCenter(1, szTitle, 1); 	
	
	memset(&gl_Send8583, 0, sizeof(gl_Send8583));
	memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));

	strcpy((char *)gl_Send8583.Msg01, "0220");
	strcpy((char *)gl_Send8583.Field03, "200011");
	sprintf((char *)gl_Send8583.Field04, "%012lu",ulAmount);

	vIncTTC();
	sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);
	
	strcpy((char *)gl_Send8583.Field25, "16");
	
//	strcpy((char *)gl_Send8583.Field37, (char *)QrTrRec.sReferenceNo);
	strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
	strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);

	strcpy((char *)gl_Send8583.Field49, "156");	

	//DE56		
      memset(asBuf,0x00,sizeof(asBuf));
      if(PayWay == MODE_WETCHAT)
      	{
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x01",6);	
      	}
      else if(PayWay == MODE_ALIPAY)
	{
		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x02",6);	
      	}
       else if(PayWay == MODE_UNIONPAY)	  	
       {
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x03",6);	
      	}

	dbgHex("asBuf", asBuf, 6);
        vLongToStr(6, 2, gl_Send8583.Field56);
	memcpy(gl_Send8583.Field56+2, asBuf,6); 
       dbgHex("gl_Send8583.Field56", gl_Send8583.Field56, 8);		   

/*
	memset(asBuf,0x00,sizeof(asBuf));
      if(PayWay == MODE_WETCHAT)
      	{
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x01",6);	

		memcpy(asBuf+6,"\x9f\x02",2);
		len = strlen(VoidOrderId);    
		asBuf[8]=len/256;
		asBuf[9]=len%256;
		memcpy(&asBuf[10],VoidOrderId,len);	
      	}
      else if(PayWay == MODE_ALIPAY)
	{
		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x02",6);

		memcpy(asBuf+6,"\x9f\x02",2);
		len = strlen(VoidOrderId);    
		asBuf[8]=len/256;
		asBuf[9]=len%256;
		memcpy(&asBuf[10],VoidOrderId,len);
      	}
       else if(PayWay == MODE_UNIONPAY)	  	
       {
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x03",6);	

		memcpy(asBuf+6,"\x9f\x02",2);
		len = strlen(VoidOrderId);    
		asBuf[8]=len/256;
		asBuf[9]=len%256;
		memcpy(&asBuf[10],VoidOrderId,len);	
      	}

	dbgHex("asBuf", asBuf, 6+4+len);
        vLongToStr(6+4+len, 2, gl_Send8583.Field56);
	memcpy(gl_Send8583.Field56+2, asBuf,6+4 + len); 
 	dbgHex("gl_Send8583.Field56", gl_Send8583.Field56, 6 + 4+len);
*/

	//58域
	memset(asBuf,0,sizeof(asBuf));
    	GetPublicField058(asBuf);
	vLongToStr(strlen(asBuf), 2, gl_Send8583.Field58);
	memcpy(gl_Send8583.Field58+2, asBuf,strlen(asBuf));  
	dbgHex("gl_Send8583.Field58",gl_Send8583.Field58,strlen(asBuf));
	
	// 59 域
	 _vGetTime(szBuf);
        vOneTwo0(VoidDateTime, 2, szBuf+4);
        for(iIndex=0; iIndex<gl_SysData.uiQrTransNum; iIndex++)
	{
		memset(&QrTrRec,0,sizeof(stQrTransRec));
		uiMemManaGetQrTransRec(iIndex, &QrTrRec);
		dbg("ulTTC=%d\r\n",QrTrRec.ulTTC);
		vOneTwo0(QrTrRec.sDateTime, 6, szBuf1);
		dbg("szBuf+2:%s\n",szBuf+2);
		dbg("szBuf1:%s\n",szBuf1);
		if(QrTrRec.ulBatchNo == ulVoidBatchNo && QrTrRec.ulTTC == ulVoidTTC
			&& memcmp(szBuf+2,szBuf1,6) == 0)
		{
			dbg("87654321\n");
			ucoriginal = 1;
			strcpy(szchnlQrCode,QrTrRec.szchnlQrCode);
			break;
		}	
	}
	 if(ucoriginal == 1)
		GetPublicField059_QR(szchnlQrCode+strlen(szchnlQrCode)-6, gl_Send8583.Field59);	
	else
		GetPublicField059_QR(NULL, gl_Send8583.Field59);
	
        //60域
//	sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s", 25, gl_SysData.ulBatchNo, "000");
	sprintf((char *)gl_Send8583.Field60, "%02u%06lu", 25, gl_SysData.ulBatchNo);

	//61域
	vOneTwo0(VoidDateTime, 2, szBuf);
        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", ulVoidBatchNo, ulVoidTTC, szBuf);

	//63域
//	sprintf((char *)gl_Send8583.Field63, "%-3.3s", gl_SysData.szCurOper);   //63.1

	 memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

   	memset(&QrRefundTraRec, 0, sizeof(QrRefundTraRec));
	QrRefundTraRec.ucProType = 0x02; //待确认
	QrRefundTraRec.ucPayWay = PayWay;
	QrRefundTraRec.ulVoidBatchNo = ulVoidBatchNo;
	QrRefundTraRec.ulVoidTTC = ulVoidTTC;
	strcpy(QrRefundTraRec.sVoidDateTime,VoidDateTime);	
	QrRefundTraRec.uiTransType=QRTRANS_TYPE_REFUND;
	QrRefundTraRec.ulAmount=ulAmount;
	QrRefundTraRec.ulTTC=gl_SysData.ulTTC;
	vTwoOne(gl_SysData.szCurOper, 2, QrRefundTraRec.sOperNo);
         memset(szBuf,0x00,sizeof(szBuf));
	_vGetTime(szBuf);
	vTwoOne(szBuf+2, 12, QrRefundTraRec.sDateTime);
        uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum, &QrRefundTraRec);

	gl_SysData.uiQrTransNum ++;	
	 uiMemManaPutSysData();
	
	//打8583包-填充http-发送-接收-解http-解8583包	
	iRet = iHttpSendRecv8583(0);
	if (iRet)
	{
		return 1;
	}	
	  	
	vClearLines(2);	
	if(memcmp(gl_Recv8583.Field39,"00",2)==0x00)
	{
/*	
	     _vGetTime(szBuf);
	      vOneTwo0(VoidDateTime, 2, szBuf+4);
	      for(iIndex=0; iIndex<gl_SysData.uiQrTransNum; iIndex++)
       	      {
			memset(&QrTrRec,0,sizeof(stQrTransRec));
			uiMemManaGetQrTransRec(iIndex, &QrTrRec);
			dbg("ulTTC=%d\r\n",QrTrRec.ulTTC);
			vOneTwo0(QrTrRec.sDateTime, 6, szBuf1);
			dbg("szBuf+2:%s\n",szBuf+2);
			dbg("szBuf1:%s\n",szBuf1);
			if(QrTrRec.ulBatchNo == ulVoidBatchNo && QrTrRec.ulTTC == ulVoidTTC
				&& memcmp(szBuf+2,szBuf1,6) == 0)
			{
			        dbg("12345678\n");
				QrTrRec.ucVoidFlag = 0x03;
				uiMemManaPutQrTransRec(iIndex, &QrTrRec);
				break;
			}	
               }
*/	  			
		QrRefundTraRec.ucProType = 0x01;
                memset(szBuf,0x00,sizeof(szBuf));
		_vGetTime(szBuf);
		if(gl_Recv8583.Field13[0])
			memcpy(szBuf+4, gl_Recv8583.Field13, 4);
		if(gl_Recv8583.Field12[0])
			memcpy(szBuf+8, gl_Recv8583.Field12, 6);
		vTwoOne(szBuf+2, 12, QrRefundTraRec.sDateTime);

		if(gl_Recv8583.Field43[0] && QrRefundTraRec.ucPayWay == MODE_UNIONPAY)
		{
			vGetPrtQrMerchInfo(gl_Recv8583.Field43, &QrRefundTraRec);
		}	

		if(gl_Recv8583.Field37[0])
			strcpy(QrRefundTraRec.sReferenceNo, gl_Recv8583.Field37);	

		len = ulStrToLong(gl_Recv8583.Field56,2);
		if(len>0)
	       {
	       		DivPublicField056(gl_Recv8583.Field56+2,len,&QrRefundTraRec);	
			dbg("orderId:%s\n",QrRefundTraRec.orderId);	
		}	

		uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrRefundTraRec);
		gl_SysData.ulLastTransTTC = QrRefundTraRec.ulTTC;
		uiMemManaPutSysData();
				
#ifdef ENABLE_PRINTER                 
		vShowWaitEx("交易成功,打印票据...", -1, 0); 
		iPrintQrTrans(&QrRefundTraRec, 0);
		vShowWaitEx(NULL, 0, 0x03); 
	
#else        
		vMessage("交易成功");
		
#endif
	}
	else
	{
		QrRefundTraRec.ucProType = 0x03;
		uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrRefundTraRec);
		
		vShowHostErrMsg(gl_Recv8583.Field39,NULL);
		return 1;
	}
	
	return 0;

}

unsigned char iQRCodePreAuth(u8 nMode)
{
        uchar szQrCode[500] = {0};
        int iRet;
	char szTitle[20+1];
        ulong ulAmount;
	uchar szBuf[100];
	uchar PayWay;	
	ulong ulTTC;	
	ulong ulTimer,ulTimer1;
	uint uiKey;
	u16 len =0;
	uint time =0,time1 = 0;
	uchar ucRet;
	u8 asBuf[128+1]={0};
	stQrTransRec QrTrRec;
	
	if(nMode == Z_phoneTopos)
		strcpy(szTitle,"用户扫码");
	else if(nMode == B_posTophone)
		strcpy(szTitle,"商户扫码");

	_vCls();
	vDispCenter(1, szTitle, 1);
	
	if(iCheckTranStat( SCAN_TRANS_TYPE_PREAUTH,0))
        	return 1;
	
	if(gl_SysInfo.ucQrPreAuthSwitch == 0) 
	{
		TermRemindInfo();
		return 1;
        }
	
	gl_ucQrTransFlag = 1;
		
        if(nMode == Z_phoneTopos)
	{
		PayWay = Select_pay_way();
		if(PayWay==0)
		{
			return 1;
		}

		if(PayWay == MODE_WETCHAT)
		{			
			strcpy(szTitle,"微信预授权");
		}
		else if(PayWay== MODE_ALIPAY)
		{
			strcpy(szTitle,"支付宝预授权");
		}
		else if(PayWay== MODE_UNIONPAY)
		{
		//	strcpy(szTitle,"银联预授权");
		
			TermRemindInfo();
			return 1;
		}

		_vCls();
		vDispCenter(1, szTitle, 1);
        }
	 dbg("PayWay1:%d\n",PayWay);

	if(iGetAmount(NULL, "请输入金额",NULL, szBuf)<=0)
		return 1;	
	ulAmount=atol(szBuf);
		
	if(nMode == B_posTophone)
	{		
		sprintf(szBuf, "收款金额:%lu.%02lu", ulAmount/100, ulAmount%100);;
		vDispCenter(2, szBuf, 0);
		vDispCenter(3, "请出示付款码", 0);
                vDispCenter(_uiGetVLines(), "按[确认]键扫码", 0);

		_vSetTimer(&ulTimer, 6000L); //60秒		
		_vFlushKey();
		while(1)
		{
			if(_uiKeyPressed())
			{
				uiKey=_uiGetKey();
				if(uiKey==_KEY_ENTER)
					break;
				if(uiKey==_KEY_CANCEL)
					return 1;
			}	
			if(_uiTestTimer(ulTimer))
				return 1;
		}
		
		iRet = scanCode(szQrCode,sizeof(szQrCode));		
		if(iRet == -1)
               		 return 1;		
	
		dbg("qrcode=[%s]\n", szQrCode);

		PayWay= whitchAuthCode(szQrCode);
		if(PayWay == MODE_WETCHAT)
		{
			strcpy(szTitle,"微信预授权");
		}	
		else if(PayWay == MODE_ALIPAY )
		{
			strcpy(szTitle,"支付宝预授权");
		}	
		else if(PayWay == MODE_UNIONPAY)
		{
		//	strcpy(szTitle,"银联预授权");

			TermRemindInfo();
			return 1;
		}	
		else 
		{
			TermRemindInfo();
			return 1;
		}
		_vCls();
		vDispCenter(1, szTitle, 1);
		
	}
	
         memset(&gl_Send8583, 0, sizeof(gl_Send8583));
         memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
		 
        strcpy((char *)gl_Send8583.Msg01, "0100");
	strcpy((char *)gl_Send8583.Field03, "030001");
	sprintf((char *)gl_Send8583.Field04, "%012lu", ulAmount);
	
	vIncTTC();
	ulTTC=gl_SysData.ulTTC;
	sprintf((char *)gl_Send8583.Field11, "%06lu", ulTTC);
	strcpy((char *)gl_Send8583.Field25, "06");
	strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
	strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);
	
        //49域
        strcpy((char *)gl_Send8583.Field49, "156");

	//DE56
      if(PayWay == MODE_WETCHAT)
      	{
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x01",6);	
      	}
      else if(PayWay == MODE_ALIPAY)
	{
		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x02",6);	
      	}
       else if(PayWay == MODE_UNIONPAY)	  	
       {
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x03",6);	
      	}

	dbgHex("asBuf", asBuf, 6);
        vLongToStr(6, 2, gl_Send8583.Field56);
	memcpy(gl_Send8583.Field56+2, asBuf,6); 
       dbgHex("gl_Send8583.Field56", gl_Send8583.Field56, 8);

	//58域
	memset(asBuf,0,sizeof(asBuf));
    	GetPublicField058(asBuf);
	vLongToStr(strlen(asBuf), 2, gl_Send8583.Field58);
	memcpy(gl_Send8583.Field58+2, asBuf,strlen(asBuf));  
	dbgHex("gl_Send8583.Field58",gl_Send8583.Field58,strlen(asBuf));
	   
	// 59 域
	if(nMode == Z_phoneTopos)
        	GetPublicField059_QR(NULL, gl_Send8583.Field59);		
        else if(nMode == B_posTophone)
		GetPublicField059_QR(szQrCode+strlen(szQrCode)-6, gl_Send8583.Field59);		
	
        //60域
        if(nMode == Z_phoneTopos)
	{
		sprintf((char *)gl_Send8583.Field60, "%02u%06lu", 31, gl_SysData.ulBatchNo);
        }	
	else if(nMode == B_posTophone)
	{
	//	sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s", 32, gl_SysData.ulBatchNo, "000");
		sprintf((char *)gl_Send8583.Field60, "%02u%06lu", 32, gl_SysData.ulBatchNo);
	}
	
	if(nMode == B_posTophone)
	{
		//62域
		vLongToStr(strlen(szQrCode), 2, gl_Send8583.Field62);
		sprintf((char *)gl_Send8583.Field62+2, "%s", szQrCode);	
	}
	
	//63域
	sprintf((char *)gl_Send8583.Field63, "%-3.3s", gl_SysData.szCurOper);   //63.1

	 memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

	if(nMode == B_posTophone)
	{      
		memset(&QrTrRec, 0, sizeof(QrTrRec));			
		QrTrRec.uiTransType=SCAN_TRANS_TYPE_PREAUTH;

		QrTrRec.ucPayMode= B_posTophone;
		QrTrRec.ucProType = 0x02; //待确认
		QrTrRec.ulAmount=ulAmount;
		QrTrRec.ulBatchNo = gl_SysData.ulBatchNo;
		QrTrRec.ulTTC=ulTTC;
		dbg("QrTrRec.ulTTC12:%d\r\n",QrTrRec.ulTTC);
		strcpy(QrTrRec.szchnlQrCode, szQrCode);		
		vTwoOne(gl_SysData.szCurOper, 2, QrTrRec.sOperNo);
		_vGetTime(szBuf);
		vTwoOne(szBuf+2, 12, QrTrRec.sDateTime);	
		QrTrRec.ucPayWay = PayWay;
		uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum, &QrTrRec);
		 
		 gl_SysData.uiQrTransNum ++;	
		 uiMemManaPutSysData(); 	
	}
	else if(nMode == Z_phoneTopos) 
	{	  
        	memset(&QrTrRec, 0, sizeof(QrTrRec));
		QrTrRec.uiTransType=SCAN_TRANS_TYPE_PREAUTH;
                QrTrRec.ucPayMode= Z_phoneTopos;
		QrTrRec.ucProType = 0x02; //待确认
		QrTrRec.ulAmount=ulAmount;
		QrTrRec.ulBatchNo = gl_SysData.ulBatchNo;
		QrTrRec.ulTTC=ulTTC;		
		vTwoOne(gl_SysData.szCurOper, 2, QrTrRec.sOperNo);
		_vGetTime(szBuf);
		vTwoOne(szBuf+2, 12, QrTrRec.sDateTime);	
		QrTrRec.ucPayWay = PayWay;
		uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum, &QrTrRec);
		 
		 gl_SysData.uiQrTransNum ++;	
		 uiMemManaPutSysData();		 		 
         }
	
   	 //打8583包-填充http-发送-接收-解http-解8583包
	iRet = iHttpSendRecv8583(0);
	if (iRet)
	{
	        return 1;
   	 }

	if (strcmp((char *)gl_Recv8583.Field39, "00") && strcmp((char *)gl_Recv8583.Field39, "IP"))
	{
		 if (strcmp((char *)gl_Recv8583.Field39, "IF") == 0)
		 {
		 //	QrTrRec.ucProType = 0x03;
		//	uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrRec);

		 	 gl_SysData.uiQrTransNum --;	
			 uiMemManaPutSysData();
		 
			vClearLines(2);
			vDispMid(3,gl_Recv8583.Field39);	
			vDispMid(4,"交易失败");
			vMessage(NULL);
		 }
		 else if(strcmp((char *)gl_Recv8583.Field39, "03") == 0 || strcmp((char *)gl_Recv8583.Field39, "12") == 0 
				||strcmp((char *)gl_Recv8583.Field39, "94") == 0 || strcmp((char *)gl_Recv8583.Field39, "96") == 0 
				||strcmp((char *)gl_Recv8583.Field39, "FF") == 0 ||  strcmp((char *)gl_Recv8583.Field39, "A0") == 0)
		{
		//	QrTrRec.ucProType = 0x03;
		//	uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrRec);

			gl_SysData.uiQrTransNum --;	
			 uiMemManaPutSysData();
			 
			vShowHostErrMsg(gl_Recv8583.Field39,NULL);			
		}
		else
		{
			gl_SysData.uiQrTransNum --;	
			 uiMemManaPutSysData();
			 
			vShowHostErrMsg(gl_Recv8583.Field39,NULL);
		}
		
		return 1;
	} 
		
	if(nMode == B_posTophone)
	{   
		_vGetTime(szBuf);
		if(gl_Recv8583.Field13[0])
			memcpy(szBuf+4, gl_Recv8583.Field13, 4);
		if(gl_Recv8583.Field12[0])
			memcpy(szBuf+8, gl_Recv8583.Field12, 6);
		vTwoOne(szBuf+2, 12, QrTrRec.sDateTime);
		if(gl_Recv8583.Field37[0])
			strcpy(QrTrRec.sReferenceNo, gl_Recv8583.Field37);	

		if(gl_Recv8583.Field43[0] && QrTrRec.ucPayWay == MODE_UNIONPAY)
		{
			vGetPrtQrMerchInfo(gl_Recv8583.Field43, &QrTrRec);
		}	
		
	        len = ulStrToLong(gl_Recv8583.Field56,2);
		if(len>0)	
	       {
	       		DivPublicField056(gl_Recv8583.Field56+2,len,&QrTrRec);	
			dbg("orderId:%s\n",QrTrRec.orderId);
		}	
		 uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrRec);
		 
	 	_vCls();
		vDispCenter(1, szTitle, 1);			

		vDispCenter(2, "查询中,请稍候...", 0);
	//	vDispCenter(4, "[取消]退出",0);	
	}
	else if(nMode == Z_phoneTopos) 
	{
		len = ulStrToLong(gl_Recv8583.Field62,2);				
		memcpy(QrTrRec.szchnlUrl,gl_Recv8583.Field62 + 2,len);	
		
		_vGetTime(szBuf);		
		if(gl_Recv8583.Field13[0])
			memcpy(szBuf+4, gl_Recv8583.Field13, 4);
		if(gl_Recv8583.Field12[0])
			memcpy(szBuf+8, gl_Recv8583.Field12, 6);
		vTwoOne(szBuf+2, 14, QrTrRec.sDateTime);		
		if(gl_Recv8583.Field37[0])
			strcpy(QrTrRec.sReferenceNo, gl_Recv8583.Field37);				

		if(gl_Recv8583.Field43[0] && QrTrRec.ucPayWay == MODE_UNIONPAY)
		{
			vGetPrtQrMerchInfo(gl_Recv8583.Field43, &QrTrRec);
		}	
		
		 len = ulStrToLong(gl_Recv8583.Field56,2);
	         DivPublicField056(gl_Recv8583.Field56+2,len,&QrTrRec);	
		dbg("orderId:%s\n",QrTrRec.orderId);	 
	        uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrRec);			

	       _vCls();
		vDispCenter(1, szTitle, 1);
	//	_vSetBackLight(1);				
           		DispTransQrCode(QrTrRec.szchnlUrl);
	        
		_vDispAt(2, 11, "请扫码二");
		_vDispAt(3, 11, "维码支付");		
	//	_vDispAt(4, 11, "[取消]退出");	
		 vSetCommNoDisp(1);      //关闭通讯提示,避免提示信息破坏二维码		
         }
	
	 memset(&gl_Send8583, 0, sizeof(gl_Send8583));
         memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));

	strcpy((char *)gl_Send8583.Msg01, "0200");
	strcpy((char *)gl_Send8583.Field03, "310000");
//	vIncTTC();
//	ulTTC=gl_SysData.ulTTC;
//	sprintf((char *)gl_Send8583.Field11, "%06lu", ulTTC);

	strcpy((char *)gl_Send8583.Field25, "06");
	strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
	strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);

	//DE56
	memset(asBuf,0x00,sizeof(asBuf));
      if(QrTrRec.ucPayWay == MODE_WETCHAT)
      	{
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x01",6);	
      	}
      else if(QrTrRec.ucPayWay == MODE_ALIPAY)
	{
		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x02",6);	
      	}
       else if(QrTrRec.ucPayWay == MODE_UNIONPAY)	  	
       {
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x03",6);	
      	}

	dbgHex("asBuf", asBuf, 6);
        vLongToStr(6, 2, gl_Send8583.Field56);
	memcpy(gl_Send8583.Field56+2, asBuf,6); 
       dbgHex("gl_Send8583.Field56", gl_Send8583.Field56, 8);

        //60域
	sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s", 01, gl_SysData.ulBatchNo, "000");

	//61域
	vOneTwo0(QrTrRec.sDateTime+1, 2, szBuf);
        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", gl_SysData.ulBatchNo, QrTrRec.ulTTC, szBuf);

	//63域
	sprintf((char *)gl_Send8583.Field63, "%-3.3s", gl_SysData.szCurOper);   //63.1

	 memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

        _vSetTimer(&ulTimer, 18000L); //180秒	 
          _vSetTimer(&ulTimer1, 500L); //5秒	 
         _vFlushKey();
	Keyflag=0;	
	while (!_uiTestTimer(ulTimer))
	{			        
	        time = _uiCheckTimer(ulTimer); 
		time1 = _uiCheckTimer(ulTimer1); 	
		if(!time) break;
		if(time <= 150) 
			vSetCommCallBack(vDispWarnInfoCallBack);
		else 
			_vFlushKey();
		if(!time1) {
			vIncTTC();
			ulTTC=gl_SysData.ulTTC;
			sprintf((char *)gl_Send8583.Field11, "%06lu", ulTTC);	
		iRet = iHttpSendRecv8583(0);
		if (iRet)
		{
			 vSetCommNoDisp(0);
			 vSetCommCallBack(NULL);
			return 1;
		}
		
		if(memcmp(gl_Recv8583.Field39,"IP",2)==0x00)
		{
			 if(Keyflag == 1)
		        {
		        	vDispCenter(1, szTitle, 1);
		               	vDispMid(2, "交易取消");
				vDispMid(3, "请在【支付查询】菜单");
				vDispMid(4, "手动发起查询");
				vMessage(NULL);
				
		             	if(nMode == B_posTophone)
				{
					vSetCommCallBack(NULL);
					return 1;
				}
				if(nMode == Z_phoneTopos)						    
				{
					vSetCommCallBack(NULL);
					vSetCommNoDisp(0);
					return 1;
				}
		        }
				
			 _vSetTimer(&ulTimer1, 500L); //5秒
			continue;
		}	

		if(memcmp(gl_Recv8583.Field39,"00",2)==0x00)
		{      
		         _vCls();
			vDispCenter(1, szTitle, 1);
			vSetCommNoDisp(0);
			
		       //成功		
		       QrTrRec.ucProType = 0x01;

			if(gl_Recv8583.Field37[0])
				strcpy(QrTrRec.sReferenceNo, gl_Recv8583.Field37);	
								
			if(gl_Recv8583.Field43[0] && QrTrRec.ucPayWay == MODE_UNIONPAY)
			{
				vGetPrtQrMerchInfo(gl_Recv8583.Field43, &QrTrRec);
			}	

			len = ulStrToLong(gl_Recv8583.Field56,2);
			if(len>0)
			{
				DivPublicField056(gl_Recv8583.Field56+2,len,&QrTrRec);	
				dbg("orderId_query:%s\n",QrTrRec.orderId);	
			}	
		
			uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrRec);			
			
			gl_SysData.ulLastTransTTC = QrTrRec.ulTTC;
			uiMemManaPutSysData();

#ifdef ENABLE_PRINTER 
			{									 
				vShowWaitEx("支付成功,打印票据...", -1, 0); 
				iPrintQrTrans(&QrTrRec, 0);
				vShowWaitEx(NULL, 0, 0x03); 
			}
#else    
			{
				vMessage("支付成功");
			}
#endif 
			     vSetCommCallBack(NULL);
		             return 0;
		}	
		else
		{		
		//失败
		         vSetCommNoDisp(0);
		        _vCls();
			vDispCenter(1, szTitle, 1);
			if (strcmp((char *)gl_Recv8583.Field39, "IF") == 0)
			{
			//	QrTrRec.ucProType = 0x03;
			//	uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrRec);	

				gl_SysData.uiQrTransNum --;	
			 	uiMemManaPutSysData();
			 
				vClearLines(2);
				vDispMid(3,gl_Recv8583.Field39);	
				vDispMid(4,"交易失败");
				vMessage(NULL);
			}
			else if(strcmp((char *)gl_Recv8583.Field39, "03") == 0
				|| strcmp((char *)gl_Recv8583.Field39, "12") == 0 ||strcmp((char *)gl_Recv8583.Field39, "94") == 0
				|| strcmp((char *)gl_Recv8583.Field39, "96") == 0 ||strcmp((char *)gl_Recv8583.Field39, "FF") == 0)
			{
			//	QrTrRec.ucProType = 0x03;
			//	uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrRec);	

				gl_SysData.uiQrTransNum --;	
			 	uiMemManaPutSysData();
				
				vShowHostErrMsg(gl_Recv8583.Field39, NULL);
			}
			else
			{	
				gl_SysData.uiQrTransNum --;	
			 	uiMemManaPutSysData();
			 
				vShowHostErrMsg(gl_Recv8583.Field39,NULL);
			}
			
			vSetCommCallBack(NULL);
		        return 1;
		}			
	     }
		
	        if(_uiKeyPressed() && time <= 30)
		{
			uiKey =_uiGetKey();
			if(uiKey ==_KEY_CANCEL && nMode == B_posTophone)
			{
				vSetCommCallBack(NULL);
				return 1;
			}
			if(uiKey ==_KEY_CANCEL && nMode == Z_phoneTopos)						    
			{
				vSetCommCallBack(NULL);
				vSetCommNoDisp(0);
				return 1;
			}
		}
	}

        vSetCommCallBack(NULL);
        dbg("QrTrRec.ucPayWay:%d\n",QrTrRec.ucPayWay);
	vSetCommNoDisp(0);
	ucRet =  iQRCodePreAuth_Query(QrTrRec.ulTTC,0,0);	
        return ucRet;
}

unsigned char iQRCodePreAuth_Query(ulong ulTTC,int  dup,int flag)
{
	u8 timce=0;
        int iRet;
	uint uiKey;
	 int iIndex;
	 ulong ulTimer;
	  u8 asBuf[128+1]={0};
	  uchar szBuf[100];
	  u16 len =0;
	stQrTransRec QrQueTrRec;

        _vCls();
	vDispCenter(1, "支付结果查询", 1);

	gl_ucQrTransFlag = 1;
	
        dbg("ulTTC=%d\r\n",ulTTC);
	for(iIndex=0; iIndex<gl_SysData.uiQrTransNum; iIndex++)
        {
		memset(&QrQueTrRec,0,sizeof(QrQueTrRec));
		if(uiMemManaGetQrTransRec(iIndex, &QrQueTrRec))
		{
			vMessage("读取交易记录失败");
			return 1;
		}
		dbg("QrQueTrRec.ulTTC =%d\r\n",QrQueTrRec.ulTTC);
		if(QrQueTrRec.ulTTC==ulTTC)
		{
		      dbg("iIndex:%d\r\n",iIndex);
			break;	
			}
        }

	 if(iIndex>=gl_SysData.uiQrTransNum)
        {
            vMessage("无此交易记录");
            return 1;
        }

	 memset(&gl_Send8583, 0, sizeof(gl_Send8583));
         memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));

	strcpy((char *)gl_Send8583.Msg01, "0200");
	strcpy((char *)gl_Send8583.Field03, "310000");
	
	vIncTTC();
	sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);
	
	strcpy((char *)gl_Send8583.Field25, "06");
	strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
	strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);

	//DE56
	memset(asBuf,0x00,sizeof(asBuf));
      if(QrQueTrRec.ucPayWay == MODE_WETCHAT)
      	{
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x01",6);	
      	}
      else if(QrQueTrRec.ucPayWay == MODE_ALIPAY)
	{
		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x02",6);	
      	}
       else if(QrQueTrRec.ucPayWay == MODE_UNIONPAY)	  	
       {
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x03",6);	
      	}

	dbgHex("asBuf", asBuf, 6);
        vLongToStr(6, 2, gl_Send8583.Field56);
	memcpy(gl_Send8583.Field56+2, asBuf,6); 
       dbgHex("gl_Send8583.Field56", gl_Send8583.Field56, 8);

        //60域
	sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s", 01, gl_SysData.ulBatchNo, "000");

	//61域
	vOneTwo0(QrQueTrRec.sDateTime+1, 2, szBuf);
        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", gl_SysData.ulBatchNo, ulTTC, szBuf);

	//63域
	sprintf((char *)gl_Send8583.Field63, "%-3.3s", gl_SysData.szCurOper);   //63.1

	 memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

  doQueryOnce:
	
	iRet = iHttpSendRecv8583(0);
	if (iRet)
	{
                //显示错误信息
	        return 1;
   	 }

	strcpy(gl_Recv8583.Field39,"IF");//ceshi
 	if(memcmp(gl_Recv8583.Field39,"IP",2)==0)
        {
                timce++;							                  

                    if(timce == 3)
                    {
			//失败
			vClearLines(2);
			vDispMid(3,gl_Recv8583.Field39);	
			vDispMid(4,"交易处理中");	
			vMessage(NULL);
			
			return 1;
                    }

                   	vClearLines(2);
		vDispMid(3,gl_Recv8583.Field39);	
		vDispMid(4,"交易处理中");	
		_vDisp(5,"退出            查询");  
		
		_vSetTimer(&ulTimer, 500L); //5秒
                   _vFlushKey();
		while(1)
		{		
                        if(_uiTestTimer(ulTimer))	
			{
				vClearLines(2);
				vIncTTC();
				sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);
				goto doQueryOnce;
			}else if(_uiKeyPressed())
			{
				uiKey =_uiGetKey();
				if(uiKey ==_KEY_ENTER)
				{
					vClearLines(2);
					vIncTTC();
					sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);
					goto doQueryOnce;
				}
				else if(uiKey ==_KEY_CANCEL)						    
				{
					return	1;	
				}
			}
		}	 				   	
         }

	
	if(memcmp(gl_Recv8583.Field39,"00",2)==0x00)
	{
/*	
		if(QrQueTrRec.uiTransType == SCAN_TRANS_TYPE_PREAUTHVOID)
		{		
		      _vGetTime(szBuf);
		      vOneTwo0(QrQueTrRec.sVoidDateTime, 2, szBuf+4);
		      for(iIndex1 = 0; iIndex1 < gl_SysData.uiQrTransNum; iIndex1++)
	       	      {
				memset(&QrTrRec,0,sizeof(stQrTransRec));
				uiMemManaGetQrTransRec(iIndex1, &QrTrRec);
				dbg("ulTTC=%d\r\n",QrTrRec.ulTTC);
				vOneTwo0(QrTrRec.sDateTime, 6, szBuf1);
				dbg("szBuf+2:%s\n",szBuf+2);
				dbg("szBuf1:%s\n",szBuf1);
				if(QrTrRec.ulBatchNo == QrQueTrRec.ulVoidBatchNo && QrTrRec.ulTTC == QrQueTrRec.ulVoidTTC
					&& memcmp(szBuf+2,szBuf1,6) == 0)
				{
					dbg("12345678\n");
					QrTrRec.ucVoidFlag = 0x01;
					uiMemManaPutQrTransRec(iIndex1, &QrTrRec);
					break;
				}	
	               }			
		}else if(QrQueTrRec.uiTransType == SCAN_TRANS_TYPE_PREAUTH_COMP)
		{
		      _vGetTime(szBuf);
		      vOneTwo0(QrQueTrRec.sVoidDateTime, 2, szBuf+4);
		       for(iIndex1=0; iIndex1<gl_SysData.uiQrTransNum; iIndex1++)
	       	      {
				memset(&QrTrRec,0,sizeof(stQrTransRec));
				uiMemManaGetQrTransRec(iIndex1, &QrTrRec);
				dbg("ulTTC=%d\r\n",QrTrRec.ulTTC);
				vOneTwo0(QrTrRec.sDateTime, 6, szBuf1);
				dbg("szBuf+2:%s\n",szBuf+2);
				dbg("szBuf1:%s\n",szBuf1);
				if(QrTrRec.ulBatchNo == QrQueTrRec.ulVoidBatchNo && QrTrRec.ulTTC == QrQueTrRec.ulVoidTTC
					&& memcmp(szBuf+2,szBuf1,6) == 0)
				{
					dbg("12345678\n");
					QrTrRec.ucVoidFlag = 0x02;
					uiMemManaPutQrTransRec(iIndex1, &QrTrRec);
					break;
				}	
	               }
		}
*/
	
           //成功
	    	QrQueTrRec.ucProType = 0x01;

		if(gl_Recv8583.Field37[0])
				strcpy(QrQueTrRec.sReferenceNo, gl_Recv8583.Field37);	
							
		if(gl_Recv8583.Field43[0] && QrQueTrRec.ucPayWay == MODE_UNIONPAY)
		{
			vGetPrtQrMerchInfo(gl_Recv8583.Field43, &QrQueTrRec);
		}	

	           	len = ulStrToLong(gl_Recv8583.Field56,2);
		if(len>0)
		{
			DivPublicField056(gl_Recv8583.Field56+2,len,&QrQueTrRec);	
			dbg("orderId_query:%s\n",QrQueTrRec.orderId);	
		}	
		
		uiMemManaPutQrTransRec(iIndex, &QrQueTrRec);
		
		gl_SysData.ulLastTransTTC = QrQueTrRec.ulTTC;
		uiMemManaPutSysData();

#ifdef ENABLE_PRINTER     
	        {						
			vShowWaitEx("支付成功,打印票据...", -1, 0); 			
			iPrintQrTrans(&QrQueTrRec, dup);
			vShowWaitEx(NULL, 0, 0x03); 	
		}	
#else    			
                {
			vMessage("支付成功");
		}	
		
#endif 
                 return 0;
	}	
	else
	{		
	//失败
		vClearLines(2);
		if (strcmp((char *)gl_Recv8583.Field39, "IP") == 0)
		{
			vClearLines(2);
			vDispMid(3,gl_Recv8583.Field39);	
			vDispMid(4,"交易处理中");
			vMessage(NULL);
		}
		else if (strcmp((char *)gl_Recv8583.Field39, "IF") == 0 )
		{
			if(flag == 1)
			{
				QrQueTrRec.ucProType = 0x03;
				uiMemManaPutQrTransRec(iIndex, &QrQueTrRec);
			}else if(flag == 0)
			{
				gl_SysData.uiQrTransNum --;	
			 	uiMemManaPutSysData();
			}
			
			vClearLines(2);
			vDispMid(3,gl_Recv8583.Field39);	
			vDispMid(4,"交易失败");	
			vMessage(NULL);
		}
		else if (strcmp((char *)gl_Recv8583.Field39, "03") == 0 || strcmp((char *)gl_Recv8583.Field39, "12") == 0 
						||strcmp((char *)gl_Recv8583.Field39, "94") == 0 || strcmp((char *)gl_Recv8583.Field39, "96") == 0
						||strcmp((char *)gl_Recv8583.Field39, "FF") == 0)
		{
			if(flag == 1)
			{
				QrQueTrRec.ucProType = 0x03;
				uiMemManaPutQrTransRec(iIndex, &QrQueTrRec);
			}else if(flag == 0)
			{
				gl_SysData.uiQrTransNum --;	
			 	uiMemManaPutSysData();
			}
			
			vShowHostErrMsg(gl_Recv8583.Field39,NULL);
		}
		else if(strcmp((char *)gl_Recv8583.Field39, "A0") == 0)
		{
			vShowHostErrMsg(gl_Recv8583.Field39,NULL);	
		}
		else
		{
			if(flag == 1)
			{
				QrQueTrRec.ucProType = 0x03;
				uiMemManaPutQrTransRec(iIndex, &QrQueTrRec);
			}else if(flag == 0)
			{
				gl_SysData.uiQrTransNum --;	
			 	uiMemManaPutSysData();
			}
			
			vShowHostErrMsg(gl_Recv8583.Field39,NULL);
		}	
		
	        return 1;
	} 
}

unsigned char iQRCodePreAuthVoid(uint uiTransType)
{
	int iRet;
	char szBuf[100] = {0},szTitle[20+1] = {0},szBuf1[12+1] = {0};
	ulong ulVoidTTC;
	ulong ulVoidBatchNo;
	uchar VoidDateTime[2+1];
	int i;
	int iIndex;
	ulong  ulAmount;
	stQrTransRec QrTrRec,QrTrVoidRec;
	uchar PayWay;
	int col;  
	 u8 asBuf[128+1]={0};
	  int len  = 0;
	  uchar  szchnlQrCode[30+1] ={0};      	      // 二维码           
	  uchar ucoriginal = 0;
	  
	_vCls();	
	strcpy(szTitle, "扫码预授权撤销");
	vDispCenter(1, szTitle, 1); 	

	if(iCheckTranStat(uiTransType, 0))
		return 1;

	if(iMainOperCheck((char*)gl_SysInfo.szMainOperPwd))
		return 1;

       gl_ucQrTransFlag = 1;
	   
/*
       	vClearLines(2);
        _vDisp(2, "请输原交易订单号:");
	ucPinPos = _uiGetVCols();
        if(iInput(INPUT_NORMAL|INPUT_DOT_SPACE|INPUT_TWO_LINE, 3, ucPinPos, szBuf, 40, 0, 40, 30) <= 0)
            return -1;
        strcpy(VoidOrderId,szBuf); 
*/
	
	vClearLines(2);
	col=(_uiGetVCols()-6)/2;
	_vDisp(2, "请输入原批次号:");
	vDispCenter(_uiGetVLines(), "输错请按[清除]", 0);
	iRet=iInput(INPUT_NORMAL|INPUT_CENTER_SPACE|INPUT_DOT_SPACE, 3, col+1, szBuf, 6, 1, 6, 30);
	if(iRet<0)
		return 1;
	ulVoidBatchNo=atol(szBuf);
	if(ulVoidBatchNo==0)
	{
		vMessage("无效批次号");
		return 1;
	}

	ulVoidTTC=0L;
	
	vClearLines(2);
	col=(_uiGetVCols()-6)/2;
	_vDisp(2, "请输入原凭证号:");
	vDispCenter(_uiGetVLines(), "输错请按[清除]", 0);
	iRet=iInput(INPUT_NORMAL|INPUT_CENTER_SPACE|INPUT_DOT_SPACE, 3, col+1, szBuf, 6, 1, 6, 30);
	if(iRet<0)
		return 1;
	ulVoidTTC=atol(szBuf);
	if(ulVoidTTC==0)
	{
		vMessage("无效凭证号");
		return 1;
	}

	vClearLines(2);
	_vDisp(2, "请输入原交易日期:");
	_vDisp(3, "      (月月日日)");
	while(1)	
	{	 
		iRet=iInput(INPUT_NORMAL|INPUT_DOT_SPACE|INPUT_CENTER_SPACE, 4, 9, szBuf, 4, 4, 4, 30);
			if(iRet<0)
		return 1;
		i=ulA2L(szBuf, 2);
		if(i==0 || i>12)
		{
			vMessageLine(4,"无效日期");
			_vDisp(4, " ");
		continue;
	}
	i=ulA2L(szBuf+2, 2);
	if(i==0 || i>31)
	{
	vMessageLine(4,"无效日期");
	_vDisp(4, " ");
	continue;
	}
	if (iRet == 4)
	break;	
	}	
	vTwoOne(szBuf, 4, VoidDateTime);
	
	
	    vClearLines(2);
	    if(iGetAmount(NULL, "请输入金额", NULL,szBuf)<=0)
	        return 1;
	    ulAmount = atol(szBuf);

	PayWay = Select_pay_way();
	if(PayWay==0)
	{
		return 1;
	}

	_vCls();
	vDispCenter(1, szTitle, 1); 	
	
	memset(&gl_Send8583, 0, sizeof(gl_Send8583));
	memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));

	strcpy((char *)gl_Send8583.Msg01, "0100");
	strcpy((char *)gl_Send8583.Field03, "200000");
	sprintf((char *)gl_Send8583.Field04, "%012lu", ulAmount);

	vIncTTC();
	sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);
	
	strcpy((char *)gl_Send8583.Field25, "06");
	
//	strcpy((char *)gl_Send8583.Field37, (char *)QrTrRec.sReferenceNo);
	strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
	strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);

	strcpy((char *)gl_Send8583.Field49, "156");	

	//DE56	
	memset(asBuf,0x00,sizeof(asBuf));
      if(PayWay == MODE_WETCHAT)
      	{
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x01",6);	
      	}
      else if(PayWay == MODE_ALIPAY)
	{
		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x02",6);	
      	}
       else if(PayWay == MODE_UNIONPAY)	  	
       {
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x03",6);	
      	}

	dbgHex("asBuf", asBuf, 6);
        vLongToStr(6, 2, gl_Send8583.Field56);
	memcpy(gl_Send8583.Field56+2, asBuf,6); 
       dbgHex("gl_Send8583.Field56", gl_Send8583.Field56, 8);   

/*		
	memset(asBuf,0x00,sizeof(asBuf));
      if(PayWay == MODE_WETCHAT)
      	{
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x01",6);	

		memcpy(asBuf+6,"\x9f\x02",2);
		len = strlen(VoidOrderId);    
		asBuf[8]=len/256;
		asBuf[9]=len%256;
		memcpy(&asBuf[10],VoidOrderId,len);		
      	}
      else if(PayWay == MODE_ALIPAY)
	{
		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x02",6);	

		memcpy(asBuf+6,"\x9f\x02",2);
		len = strlen(VoidOrderId);    
		asBuf[8]=len/256;
		asBuf[9]=len%256;
		memcpy(&asBuf[10],VoidOrderId,len);	
      	}
       else if(PayWay == MODE_UNIONPAY)	  	
       {
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x03",6);	

		memcpy(asBuf+6,"\x9f\x02",2);
		len = strlen(VoidOrderId);    
		asBuf[8]=len/256;
		asBuf[9]=len%256;
		memcpy(&asBuf[10],VoidOrderId,len);	
      	}

	dbgHex("asBuf", asBuf, 6+4+len);
        vLongToStr(6+4+len, 2, gl_Send8583.Field56);
	memcpy(gl_Send8583.Field56+2, asBuf,6+4 + len); 
	 dbgHex("gl_Send8583.Field56", gl_Send8583.Field56, 6 + 4+len);	   
*/

	//58域
	memset(asBuf,0,sizeof(asBuf));
    	GetPublicField058(asBuf);
	vLongToStr(strlen(asBuf), 2, gl_Send8583.Field58);
	memcpy(gl_Send8583.Field58+2, asBuf,strlen(asBuf));  
	dbgHex("gl_Send8583.Field58",gl_Send8583.Field58,strlen(asBuf));
	
        // 59 域
	 _vGetTime(szBuf);
        vOneTwo0(VoidDateTime, 2, szBuf+4);
        for(iIndex=0; iIndex<gl_SysData.uiQrTransNum; iIndex++)
	{
		memset(&QrTrRec,0,sizeof(stQrTransRec));
		uiMemManaGetQrTransRec(iIndex, &QrTrRec);
		dbg("ulTTC=%d\r\n",QrTrRec.ulTTC);
		vOneTwo0(QrTrRec.sDateTime, 6, szBuf1);
		dbg("szBuf+2:%s\n",szBuf+2);
		dbg("szBuf1:%s\n",szBuf1);
		if(QrTrRec.ulBatchNo == ulVoidBatchNo && QrTrRec.ulTTC == ulVoidTTC
			&& memcmp(szBuf+2,szBuf1,6) == 0)
		{
			dbg("87654321\n");
			ucoriginal = 1;
			strcpy(szchnlQrCode,QrTrRec.szchnlQrCode);
			break;
		}	
	}
	 if(ucoriginal == 1)
		GetPublicField059_QR(szchnlQrCode+strlen(szchnlQrCode)-6, gl_Send8583.Field59);	
	else
		GetPublicField059_QR(NULL, gl_Send8583.Field59);
	
        //60域
	sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s", 33, gl_SysData.ulBatchNo, "000");

	//61域
	vOneTwo0(VoidDateTime, 2, szBuf);
        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", ulVoidBatchNo, ulVoidTTC, szBuf);

	//63域
	sprintf((char *)gl_Send8583.Field63, "%-3.3s", gl_SysData.szCurOper);   //63.1

	 memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

	memset(&QrTrVoidRec, 0, sizeof(QrTrVoidRec));
	QrTrVoidRec.ucProType = 0x02; //待确认
	QrTrVoidRec.uiTransType=uiTransType;
	QrTrVoidRec.ucPayWay = PayWay;
	QrTrVoidRec.ulVoidTTC = ulVoidTTC;
	strcpy(QrTrVoidRec.sVoidDateTime,VoidDateTime);	
	QrTrVoidRec.uiTransType=SCAN_TRANS_TYPE_PREAUTHVOID;
	QrTrVoidRec.ulAmount=ulAmount;
	QrTrVoidRec.ulTTC=gl_SysData.ulTTC;
	memset(szBuf,0x00,sizeof(szBuf));
	_vGetTime(szBuf);
	vTwoOne(szBuf+2, 12, QrTrVoidRec.sDateTime);
	vTwoOne(gl_SysData.szCurOper, 2, QrTrVoidRec.sOperNo);

	uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum, &QrTrVoidRec);

	gl_SysData.uiQrTransNum ++;	
	uiMemManaPutSysData();
		
	//打8583包-填充http-发送-接收-解http-解8583包	
	iRet = iHttpSendRecv8583(0);
	if (iRet)
	{
		return 1;
	}	
	  	
	vClearLines(2);	
	if(memcmp(gl_Recv8583.Field39,"00",2)==0x00)
	{	
/*	
	       _vGetTime(szBuf);
	      vOneTwo0(VoidDateTime, 2, szBuf+4);
	      for(iIndex=0; iIndex<gl_SysData.uiQrTransNum; iIndex++)
       	      {
			memset(&QrTrRec,0,sizeof(stQrTransRec));
			uiMemManaGetQrTransRec(iIndex, &QrTrRec);
			dbg("ulTTC=%d\r\n",QrTrRec.ulTTC);
			vOneTwo0(QrTrRec.sDateTime, 6, szBuf1);
			dbg("szBuf+2:%s\n",szBuf+2);
			dbg("szBuf1:%s\n",szBuf1);
			if(QrTrRec.ulBatchNo == ulVoidBatchNo && QrTrRec.ulTTC == ulVoidTTC
				&& memcmp(szBuf+2,szBuf1,6) == 0)
			{
				dbg("12345678\n");
				QrTrRec.ucVoidFlag = 0x01;
				uiMemManaPutQrTransRec(iIndex, &QrTrRec);
				break;
			}	
               }				  
*/
		QrTrVoidRec.ucProType = 0x01;
                memset(szBuf,0x00,sizeof(szBuf));
		_vGetTime(szBuf);
		if(gl_Recv8583.Field13[0])
			memcpy(szBuf+4, gl_Recv8583.Field13, 4);
		if(gl_Recv8583.Field12[0])
			memcpy(szBuf+8, gl_Recv8583.Field12, 6);
		vTwoOne(szBuf+2, 12, QrTrVoidRec.sDateTime);

		if(gl_Recv8583.Field43[0] && QrTrVoidRec.ucPayWay == MODE_UNIONPAY)
		{
			vGetPrtQrMerchInfo(gl_Recv8583.Field43, &QrTrVoidRec);
		}	

		if(gl_Recv8583.Field37[0])
			strcpy(QrTrVoidRec.sReferenceNo, gl_Recv8583.Field37);	
		
		len = ulStrToLong(gl_Recv8583.Field56,2);
		if(len>0)
	       {
	       		DivPublicField056(gl_Recv8583.Field56+2,len,&QrTrVoidRec);	
			dbg("orderId:%s\n",QrTrVoidRec.orderId);	
		}	
		
		uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrVoidRec);
		gl_SysData.ulLastTransTTC = QrTrVoidRec.ulTTC;
		uiMemManaPutSysData();
				
#ifdef ENABLE_PRINTER                 
		vShowWaitEx("交易成功,打印票据...", -1, 0); 
		iPrintQrTrans(&QrTrVoidRec, 0);
		vShowWaitEx(NULL, 0, 0x03); 	
#else        	
		vMessage("交易成功");
		
#endif
	}
	else
	{
		QrTrVoidRec.ucProType = 0x03;
		uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrVoidRec);
		
		vShowHostErrMsg(gl_Recv8583.Field39,NULL);
		return 1;
	}
	
	return 0;

}

unsigned char iQRCodePreAuthComp(uint uiTransType)
{
	int iRet;
	char szBuf[100] = {0},szTitle[20+1] = {0},szBuf1[12+1] = {0};
	ulong ulVoidTTC;
	ulong ulVoidBatchNo;
	uchar VoidDateTime[2+1];
	int i;
	int iIndex;
	ulong  ulAmount;
	stQrTransRec QrTrRec,QrTrCompRec;
	uchar PayWay;
	int col;   
	 u8 asBuf[128+1]={0};
	  int len  = 0;
	 uchar  szchnlQrCode[30+1] ={0};      	      // 二维码           
	  uchar ucoriginal = 0;
	  
	_vCls();	
	strcpy(szTitle, "扫码预授权完成");
	vDispCenter(1, szTitle, 1); 	

	if(iCheckTranStat(uiTransType, 0))
		return 1;

	if(iMainOperCheck((char*)gl_SysInfo.szMainOperPwd))
		return 1;
	
	gl_ucQrTransFlag = 1;
	
/*
       	vClearLines(2);
        _vDisp(2, "请输原交易订单号:");
	ucPinPos = _uiGetVCols();
        if(iInput(INPUT_NORMAL|INPUT_DOT_SPACE|INPUT_TWO_LINE, 3, ucPinPos, szBuf, 40, 0, 40, 30) <= 0)
            return -1;
        strcpy(VoidOrderId,szBuf); 
*/
	
	vClearLines(2);
	col=(_uiGetVCols()-6)/2;
	_vDisp(2, "请输入原批次号:");
	vDispCenter(_uiGetVLines(), "输错请按[清除]", 0);
	iRet=iInput(INPUT_NORMAL|INPUT_CENTER_SPACE|INPUT_DOT_SPACE, 3, col+1, szBuf, 6, 1, 6, 30);
	if(iRet<0)
		return 1;
	ulVoidBatchNo=atol(szBuf);
	if(ulVoidBatchNo==0)
	{
		vMessage("无效批次号");
		return 1;
	}
	
	ulVoidTTC=0L;

	vClearLines(2);
	col=(_uiGetVCols()-6)/2;
	_vDisp(2, "请输入原凭证号:");
	vDispCenter(_uiGetVLines(), "输错请按[清除]", 0);
	iRet=iInput(INPUT_NORMAL|INPUT_CENTER_SPACE|INPUT_DOT_SPACE, 3, col+1, szBuf, 6, 1, 6, 30);
	if(iRet<0)
		return 1;
	ulVoidTTC=atol(szBuf);
	if(ulVoidTTC==0)
	{
		vMessage("无效凭证号");
		return 1;
	}

		
	vClearLines(2);
	_vDisp(2, "请输入原交易日期:");
	_vDisp(3, "      (月月日日)");
	while(1)	
	{	 
		iRet=iInput(INPUT_NORMAL|INPUT_DOT_SPACE|INPUT_CENTER_SPACE, 4, 9, szBuf, 4, 4, 4, 30);
			if(iRet<0)
		return 1;
		i=ulA2L(szBuf, 2);
		if(i==0 || i>12)
		{
			vMessageLine(4,"无效日期");
			_vDisp(4, " ");
		continue;
	}
	i=ulA2L(szBuf+2, 2);
	if(i==0 || i>31)
	{
	vMessageLine(4,"无效日期");
	_vDisp(4, " ");
	continue;
	}
	if (iRet == 4)
	break;	
	}	
	vTwoOne(szBuf, 4, VoidDateTime);
	
	
	    vClearLines(2);
	    if(iGetAmount(NULL, "请输入金额", NULL,szBuf)<=0)
	        return 1;
	    ulAmount = atol(szBuf);

	PayWay = Select_pay_way();
	if(PayWay==0)
	{
		return 1;
	}
	
	_vCls();
	vDispCenter(1, szTitle, 1); 	
	
	memset(&gl_Send8583, 0, sizeof(gl_Send8583));
	memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));

	strcpy((char *)gl_Send8583.Msg01, "0100");
	strcpy((char *)gl_Send8583.Field03, "200030");
	sprintf((char *)gl_Send8583.Field04, "%012lu", ulAmount);

	vIncTTC();
	sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);
	
	strcpy((char *)gl_Send8583.Field25, "06");
	
//	strcpy((char *)gl_Send8583.Field37, (char *)QrTrRec.sReferenceNo);
	strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
	strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);

	strcpy((char *)gl_Send8583.Field49, "156");	

	//DE56	
	memset(asBuf,0x00,sizeof(asBuf));
      if(PayWay == MODE_WETCHAT)
      	{
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x01",6);	
      	}
      else if(PayWay == MODE_ALIPAY)
	{
		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x02",6);	
      	}
       else if(PayWay == MODE_UNIONPAY)	  	
       {
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x03",6);	
      	}

	dbgHex("asBuf", asBuf, 6);
        vLongToStr(6, 2, gl_Send8583.Field56);
	memcpy(gl_Send8583.Field56+2, asBuf,6); 
       dbgHex("gl_Send8583.Field56", gl_Send8583.Field56, 8);	   

/*	   	
	memset(asBuf,0x00,sizeof(asBuf));
      if(PayWay == MODE_WETCHAT)
      	{
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x01",6);	
			
	        memcpy(asBuf+6,"\x9f\x02",2);
		len = strlen(VoidOrderId);    
		asBuf[8]=len/256;
		asBuf[9]=len%256;
		memcpy(&asBuf[10],VoidOrderId,len);			
      	}
      else if(PayWay == MODE_ALIPAY)
	{
		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x02",6);	
		
	        memcpy(asBuf+6,"\x9f\x02",2);
		len = strlen(VoidOrderId);    
		asBuf[8]=len/256;
		asBuf[9]=len%256;
		memcpy(&asBuf[10],VoidOrderId,len);		
      	}
       else if(PayWay == MODE_UNIONPAY)	  	
       {
      		memcpy(asBuf,"\x9f\x01\x00\x02\x00\x03",6);	
			
	        memcpy(asBuf+6,"\x9f\x02",2);
		len = strlen(VoidOrderId);    
		asBuf[8]=len/256;
		asBuf[9]=len%256;
		memcpy(&asBuf[10],VoidOrderId,len);			
      	}

	dbgHex("asBuf", asBuf, 6+4+len);
        vLongToStr(6+4+len, 2, gl_Send8583.Field56);
	memcpy(gl_Send8583.Field56+2, asBuf,6+4 + len); 
 	dbgHex("gl_Send8583.Field56", gl_Send8583.Field56, 6 + 4+len);
*/

	//58域
	memset(asBuf,0,sizeof(asBuf));
    	GetPublicField058(asBuf);
	vLongToStr(strlen(asBuf), 2, gl_Send8583.Field58);
	memcpy(gl_Send8583.Field58+2, asBuf,strlen(asBuf));  
	dbgHex("gl_Send8583.Field58",gl_Send8583.Field58,strlen(asBuf));
	
	// 59 域
	 _vGetTime(szBuf);
        vOneTwo0(VoidDateTime, 2, szBuf+4);
        for(iIndex=0; iIndex<gl_SysData.uiQrTransNum; iIndex++)
	{
		memset(&QrTrRec,0,sizeof(stQrTransRec));
		uiMemManaGetQrTransRec(iIndex, &QrTrRec);
		dbg("ulTTC=%d\r\n",QrTrRec.ulTTC);
		vOneTwo0(QrTrRec.sDateTime, 6, szBuf1);
		dbg("szBuf+2:%s\n",szBuf+2);
		dbg("szBuf1:%s\n",szBuf1);
		if(QrTrRec.ulBatchNo == ulVoidBatchNo && QrTrRec.ulTTC == ulVoidTTC
			&& memcmp(szBuf+2,szBuf1,6) == 0)
		{
			dbg("87654321\n");
			ucoriginal = 1;
			strcpy(szchnlQrCode,QrTrRec.szchnlQrCode);
			break;
		}	
	}
	 if(ucoriginal == 1)
		GetPublicField059_QR(szchnlQrCode+strlen(szchnlQrCode)-6, gl_Send8583.Field59);	
	else
		GetPublicField059_QR(NULL, gl_Send8583.Field59);
	
        //60域
	sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s", 35, gl_SysData.ulBatchNo, "000");

	//61域
	vOneTwo0(VoidDateTime, 2, szBuf);
        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", ulVoidBatchNo, ulVoidTTC, szBuf);

	//63域
	sprintf((char *)gl_Send8583.Field63, "%-3.3s", gl_SysData.szCurOper);   //63.1

	 memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

	memset(&QrTrCompRec, 0, sizeof(QrTrCompRec));
	QrTrCompRec.ucProType = 0x02; //待确认
	QrTrCompRec.uiTransType=uiTransType;
	QrTrCompRec.ucPayWay = PayWay;
	QrTrCompRec.ulVoidTTC = ulVoidTTC;
	strcpy(QrTrCompRec.sVoidDateTime,VoidDateTime);	
	QrTrCompRec.uiTransType=SCAN_TRANS_TYPE_PREAUTH_COMP;
	QrTrCompRec.ulAmount=ulAmount;
	QrTrCompRec.ulTTC=gl_SysData.ulTTC;
	memset(szBuf,0x00,sizeof(szBuf));
	_vGetTime(szBuf);
	vTwoOne(szBuf+2, 12, QrTrCompRec.sDateTime);
	vTwoOne(gl_SysData.szCurOper, 2, QrTrCompRec.sOperNo);

	uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum, &QrTrCompRec);

	gl_SysData.uiQrTransNum ++;	
	uiMemManaPutSysData();
	
	//打8583包-填充http-发送-接收-解http-解8583包	
	iRet = iHttpSendRecv8583(0);
	if (iRet)
	{
		return 1;
	}	
	  	
	vClearLines(2);	
	if(memcmp(gl_Recv8583.Field39,"00",2)==0x00)
	{	
/*	
	      _vGetTime(szBuf);
	      vOneTwo0(VoidDateTime, 2, szBuf+4);
	       for(iIndex=0; iIndex<gl_SysData.uiQrTransNum; iIndex++)
       	      {
			memset(&QrTrRec,0,sizeof(stQrTransRec));
			uiMemManaGetQrTransRec(iIndex, &QrTrRec);
			dbg("ulTTC=%d\r\n",QrTrRec.ulTTC);
			vOneTwo0(QrTrRec.sDateTime, 6, szBuf1);
			dbg("szBuf+2:%s\n",szBuf+2);
			dbg("szBuf1:%s\n",szBuf1);
			if(QrTrRec.ulBatchNo == ulVoidBatchNo && QrTrRec.ulTTC == ulVoidTTC
				&& memcmp(szBuf+2,szBuf1,6) == 0)
			{
				dbg("12345678\n");
				QrTrRec.ucVoidFlag = 0x02;
				uiMemManaPutQrTransRec(iIndex, &QrTrRec);
				break;
			}	
               }
*/
		QrTrCompRec.ucProType = 0x01;   
                memset(szBuf,0x00,sizeof(szBuf));
		_vGetTime(szBuf);
		if(gl_Recv8583.Field13[0])
			memcpy(szBuf+4, gl_Recv8583.Field13, 4);
		if(gl_Recv8583.Field12[0])
			memcpy(szBuf+8, gl_Recv8583.Field12, 6);
		vTwoOne(szBuf+2, 12, QrTrCompRec.sDateTime);

		if(gl_Recv8583.Field43[0] && QrTrCompRec.ucPayWay == MODE_UNIONPAY)
		{
			vGetPrtQrMerchInfo(gl_Recv8583.Field43, &QrTrCompRec);
		}	

		if(gl_Recv8583.Field37[0])
			strcpy(QrTrCompRec.sReferenceNo, gl_Recv8583.Field37);	

		len = ulStrToLong(gl_Recv8583.Field56,2);
		if(len>0)
	        {
	        	DivPublicField056(gl_Recv8583.Field56+2,len,&QrTrCompRec);	
			dbg("orderId:%s\n",QrTrCompRec.orderId);	
		}
		
		uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrCompRec);
		gl_SysData.ulLastTransTTC = QrTrCompRec.ulTTC;
		uiMemManaPutSysData();
				
#ifdef ENABLE_PRINTER                 
		vShowWaitEx("交易成功,打印票据...", -1, 0); 
		iPrintQrTrans(&QrTrCompRec, 0);
		vShowWaitEx(NULL, 0, 0x03); 	
#else        	
		vMessage("交易成功");
		
#endif
	}
	else
	{
		QrTrCompRec.ucProType = 0x03;
		uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrCompRec);
		
		vShowHostErrMsg(gl_Recv8583.Field39,NULL);
		return 1;
	}
	
	return 0;

}

