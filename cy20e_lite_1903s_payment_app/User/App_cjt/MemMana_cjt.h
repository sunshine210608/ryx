// 外部存储管理
// 目标: 将外部存储操作与应用隔离, 以适应各种存储方式
#ifndef _MEMMANA_H
#define _MEMMANA_H
//#include "EmvProc.h"
#include "VposFace.h"
#include "St8583_cjt.h"
#include "AppGlobal_cjt.h"

#define MSG8583_TYPE_REV    0
#define MSG8583_TYPE_SCR    1

#define AID_REC_SIZE    300
#define PUBK_REC_SIZE   350

// 建立扩展内存管理环境
// ret : 0 : OK
//       1 : 未初始化或扩展内存错误
uint uiMemManaSetEnv(void);

// 首次扩展内存管理初始化
// ret : 0 : OK
//       1 : error
// Note: 只能在终端完全初始化时执行一次
//       gl_SysInfo, gl_SysData必须填好, 初始化后要建立初始环境
uint uiMemManaInit(void);

// 获取存储类型
// ret : 0 : 正常存储类型
//       1 : Flash存储类型
uint uiMemGetMemType(void);

////////// 系统参数 //////////
// 保存系统参数
// ret : 0 : OK
//       1 : 出错
uint uiMemManaPutSysInfo(void);

////////// 系统数据 //////////
// 保存系统数据
// ret : 0 : OK
//       1 : 出错
uint uiMemManaPutSysData(void);

////////// 保留8583 //////////
// 保存保留8583数据
// in  : iFlag : 0 : 冲正/重发8583
//               1 : 脚本上送8583
// ret : 0 : OK
//       1 : 出错
uint uiMemManaPut8583(int iFlag, st8583 *p8583);
// 读取保留8583数据
// in  : iFlag : 0 : 冲正/重发8583
//               1 : 脚本上送8583
// ret : 0 : OK
//       1 : 无冲正
uint uiMemManaGet8583(int iFlag, st8583 *p8583);
// 判断是否存在报文
// in  : iFlag : 0 : 冲正/重发8583
//               1 : 脚本上送8583
// ret : 0 : 无
//       1 : 有
uint uiMemManaIfExist8583(int iFlag);
// 清除8583报文
// in  : iFlag : 0 : 冲正/重发8583
//               1 : 脚本上送8583
// ret : 0 : OK
uint uiMemManaErase8583(int iFlag);

/*
////////// 卡BIN //////////
// 擦除卡BIN
// 同时擦除卡BIN黑/白名单
// ret : 0 : OK
//       1 : 出错
uint uiMemManaEraseCardBin(void);
// 保存卡BIN 黑/白 名单
// in  : iFlag      : 0:保存白名单 1:保存黑名单
//       iIndex     : 索引, 0开始
// out : pszCardBin : 卡BIN
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaPutCardBin(int iFlag, int iIndex, char *pszCardBin);
// 读取卡BIN 黑/白 名单
// in  : iFlag      : 0:读白名单 1:读黑名单
//       iIndex     : 索引, 0开始
// out : pszCardBin : 卡BIN
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaGetCardBin(int iFlag, int iIndex, char *pszCardBin);
*/

////////// 结算列表 //////////
// 擦除结算列表
// ret : 0 : OK
//       1 : 出错
uint uiMemManaEraseSettleList(void);
// 保存结算记录
// in  : iIndex : 位置, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
//uint uiMemManaPutSettleRec(int iIndex, stSettle *pSettle);
// 读取结算记录
// in  : iIndex : 位置, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
//uint uiMemManaGetSettleRec(int iIndex, stSettle *pSettle);

////////// 交易记录 //////////
// 清除交易记录
// ret : 0 : OK
//       1 : 出错
uint uiMemManaTransRecErase(void);
// 保存交易记录
// in  : iIndex : 记录索引, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaPutTransRec(int iIndex, stTransRec *rec);

// 读取交易记录
// in  : iIndex : 记录索引, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaGetTransRec(int iIndex, stTransRec *rec);


// 保存扫码交易记录
// in  : iIndex : 记录索引, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaPutQrTransRec(int iIndex, stQrTransRec *rec);

// 读取扫码交易记录
// in  : iIndex : 记录索引
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaGetQrTransRec(int iIndex, stQrTransRec *rec);

////////// 操作员 //////////
// 擦除操作员列表
// ret : 0 : OK
//       1 : 出错
uint uiMemManaEraseOperList(void);
// 保存操作员数据
// in  : iIndex : 操作员索引, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaPutOper(int iIndex, void *pOperRec);
// 读取操作员数据
// in  : iIndex : 操作员索引, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaGetOper(int iIndex, void *pOperRec);

////////// EMV终端参数 //////////
// 保存EMV终端参数
// ret : 0 : OK
//       1 : 出错
uint uiMemManaPutEmvPara(stHxTermParam *pHxTermParam);

// 读取EMV终端参数
// ret : 0 : OK
//       1 : 出错
uint uiMemManaGetEmvPara(stHxTermParam *pHxTermParam);

////////// AID //////////
// 擦除AID存储区
// ret : 0 : OK
//       1 : 出错
uint uiMemManaEraseAid(void);
// 保存AID
// in  : iIndex : AID索引, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaPutAid(int iIndex, uchar *pszTermAid, uint len);
uint uiMemManaPutAidStr(int iIndex, uchar *pszTermAid);
// 读取AID
// in  : iIndex : AID索引, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaGetAid(int iIndex, uchar *psTermAid, uint *puiLen);
uint uiMemManaGetAidStr(int iIndex, uchar *pszTermAid);

////////// CA公钥 //////////
// 擦除CA公钥存储区
// ret : 0 : OK
//       1 : 出错
uint uiMemManaEraseCaPubKey(void);
// 保存CA公钥
// in  : iIndex : CA公钥索引, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaPutCaPubKey(int iIndex, uchar *psCaPublicKey, int len);
uint uiMemManaPutCaPubKeystr(int iIndex, uchar *pszCaPublicKey);
// 读取CA公钥
// in  : iIndex : CA公钥索引, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaGetCaPubKey(int iIndex, uchar *psCaPublicKey, uint *puiLen);
uint uiMemManaGetCaPubKeyStr(int iIndex, uchar *pszCaPublicKey);

//签字文件保存，读取，删除，清空目录
uint uiMemPutJbg(char *pszName, uchar *psData, int iLen);
uint uiMemGetJbg(char *pszName, uchar *psData, int size, int *piLen);
uint uiMemDelJbg(char *pszName);
uint uiMemClearAllJbg(void);

#endif
