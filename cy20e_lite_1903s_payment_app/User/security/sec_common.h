/********************************************************************************

 *************************Copyright (C), 2017, ScienTech Inc.**************************

 ********************************************************************************
 * File Name     : common.h
 * Author        : Jay
 * Date          : 2017-02-08
 * Description   : .C file function description
 * Version       : 1.0
 * Function List :
 * 
 * Record        :
 * 1.Date        : 2017-02-08
 *   Author      : Jay
 *   Modification: Created file

********************************************************************************/
#ifndef __SEC_COMMON_H__
#define __SEC_COMMON_H__

void delayms(void);

void delayms_ex(int ms);

void hexdumps(unsigned char *buf, int len);

int delayus_exact(int us);

unsigned char CalculateLRC(unsigned char *buf, int len);

unsigned char* Xor_ex(unsigned char *Dest, const unsigned char *Src, int len);

unsigned char* Xor(unsigned char *Dest, unsigned char *Src);

int memcmp_ex(const void *buffer1, const void *buffer2, unsigned int count);

int HextoA(char *Dest, const unsigned char *Src, int SrcLen);

int AtoHex(unsigned char *Dest, char *Src, int SrcLen);




#endif

