/********************************************************************************

 *************************Copyright (C), 2019, KingShiDun Inc.**************************

 ********************************************************************************
 * @file     	 : comm.h
 * @brief   		  : comm.c header file
 * @author       : Jay
 * @version        : 1.0
 * @date          : 2019-11-23
 * 
 * 
 * @note History:
 * @note        : Jay 2019-11-23
 * @note        : 
 *   Modification: Created file

********************************************************************************/

#ifndef __COMM_H__
#define __COMM_H__


#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

typedef void (*CMDFUNC)(unsigned char *, int, unsigned char *, int *);

typedef struct {
	unsigned char id;
	CMDFUNC func;
} SCommand;


#define TLHEADLEN           4  //transport layer head len = stx cmdid len0 len1
#define ETBHEADLEN          4  //etb first packet head len = RES/STA LRC TTLEN0 TTLEN1
#define MAXIDLEMS		10000 //10000/6
#define MAXPACKETDATALEN        1024
#define MAXPACKETLEN            1030


#define DPA_TEST				 0x10
#define GETVERSION  			 0x11
#define OUTSIDE_PINPAD 			 0x2E
#define INTERNAL_PINPAD 		 0x2F
#define GET_TUSN 				 0x50
#define GET_KEY_STATUS           0x51
#define GET_HARDWARE_TEST_RESULT 0x52

int CheckTamperStatus(void);
int endlessloop(void);
int Execute_Cmd(unsigned char byCmdID, unsigned char *byArrCmdData, int iCmdlen, unsigned char *byArrRspData, int *piRspLen);
void freeETBBuffer(void);
int getack(void);
unsigned char *getETBBuffer(int length);
unsigned char *getETBBufferPoint(void);
int getHostcommand(unsigned char *cmdid, unsigned char *data, int *datalen);
void ReStartIdleTimer(void);
void sendack(void);
void sendHostresponse(unsigned char cmdid, unsigned char *rsp, int rsplen);
void sendresponse(char cmd,char sta,unsigned char data);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __COMM_H__ */
