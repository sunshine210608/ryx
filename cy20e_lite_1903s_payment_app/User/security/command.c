#include <stdint.h>
#include <string.h>
#include "debug.h"
#include"command.h"
//#include"bq24296.h"
//#include "tft_ili9488.h"
//#include "gt911.h"
//#include "key.h"
//#include "buzzer.h"
//#include "screen.h"
//#include "screensign.h"
//#include "scanner.h"
//#include "pinpad.h"
//#include"security/pinpad.h"

#include "sec_common.h"
#include "comm.h"
#include "mhscpu_gpio.h"
#include "pinpad.h"
#include "des_sec.h"
#include "tms_flow.h"

/*****************************************************************************
 * Function      : Cmd_GetVersion
 * Description   : get firmware infomation.
 * Input         : unsigned char *byArrCmdData
                int iCmdlen
                unsigned char *byArrRspData
                int *piRspLen
 * Output        : None
 * Return        : void
 * Others        : 
 * Record
 * 1.Date        : 20161229
 *   Author      : Jay
 *   Modification: Created function

*****************************************************************************/
void Cmd_GetVersion( unsigned char *byArrCmdData, int iCmdlen, unsigned char *byArrRspData, int *piRspLen )
{
	strcpy((char *)byArrRspData, "1.0.0");
	*piRspLen = 5;
	
	return;
}

void Cmd_Outside_PINPad(unsigned char *byArrCmdData, int iCmdlen, unsigned char *byArrRspData, int *piRspLen )
{
	OutsidePinpadCmd(byArrCmdData, iCmdlen, byArrRspData, piRspLen);

	return;
}

void Cmd_Internal_PINPad(unsigned char *byArrCmdData, int iCmdlen, unsigned char *byArrRspData, int *piRspLen )
{
	InternalPinpadCmd(byArrCmdData, iCmdlen, byArrRspData, piRspLen);

	return;
}

extern int Sys_GetUsn(unsigned char *uSN, int *uSNLen);

void Cmd_Get_TUSN(unsigned char *byArrCmdData, int iCmdlen, unsigned char *byArrRspData, int *piRspLen )
{

	byte tusn[40];
	int tusnLen;
	int ret;

	ret = Sys_GetUsn(tusn, &tusnLen);
	if (ret != 0)
	{
		byArrRspData[0] = 0x01;
		*piRspLen = 1;
	}else
	{
		byArrRspData[0] = 0x00;
		memcpy(byArrRspData + 1, tusn, 20);
		*piRspLen = 20 + 1;
	}

/*
	byArrRspData[0] = 0x00;
	memcpy(byArrRspData + 1, "00007302220001000077", 20);
	*piRspLen = 20 + 1;
*/	
	return;

}

void Cmd_Get_KeyStatus(unsigned char *byArrCmdData, int iCmdlen, unsigned char *byArrRspData, int *piRspLen )
{
	int ret;

	ret = SecCheckKeyStatus();
	if (ret == 0)
	{
		////Line;
		byArrRspData[0] = 0x00;
	}else
	{
		////Line;
		byArrRspData[0] = 0x01;
	}
	*piRspLen = 1;
	
	return;
}

void Cmd_Get_HardwareTestResult(unsigned char *byArrCmdData, int iCmdlen, unsigned char *byArrRspData, int *piRspLen )
{
	memcpy(byArrRspData , "1234567890123456", 16);
	//memcpy(byArrRspData + 16, "x", 4);
	byArrRspData[16] = 0x11;
	byArrRspData[17] = 0xA3;
	byArrRspData[18] = 0x11;
	byArrRspData[19] = 0x00;
	memcpy(byArrRspData + 16 + 4, "12345678", 8);
	
	*piRspLen = 28;
	
	return;
}

#define DPA_GPIO        GPIOG
#define DPA_GPIO_PIN    GPIO_Pin_4

#define DPA_LOW()         GPIO_ResetBits(DPA_GPIO,DPA_GPIO_PIN)
#define DPA_HIGH()        GPIO_SetBits(DPA_GPIO,DPA_GPIO_PIN)


//PG4
/*
void DPAGPIOInit(void){
    GPIO_InitTypeDef GPIO_InitStruct;
    
	GPIO_InitStruct.GPIO_Pin = DPA_GPIO_PIN;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Remap = GPIO_Remap_1;
	//GPIO_InitDefault(DPA_GPIO, &GPIO_InitStruct,Bit_RESET);    
	GPIO_Init(DPA_GPIO, &GPIO_InitStruct);
	DPA_LOW();
}
*/

void Cmd_DPA_Test(unsigned char *byArrCmdData, int iCmdlen, 
	unsigned char *byArrRspData, int *piRspLen )
{
	unsigned char encryptOrDecrypt;
	unsigned char tdesKey[16];
	unsigned char tdesTestData[8];
	unsigned char aesKey[16];
	unsigned char aesTestData[16];
	unsigned char result[16];

	if (iCmdlen == 25)
	{
		//TDES 
		encryptOrDecrypt = byArrCmdData[0];
		memcpy(tdesKey, byArrCmdData + 1, 16);
		memcpy(tdesTestData, byArrCmdData + 1 + 16, 8);
		
		//DPA_HIGH();
		ucl_tdes_ecb(tdesTestData, 8, tdesKey, result, encryptOrDecrypt - '0');
		//DPA_LOW();
		
		byArrRspData[0] = '0';
		memcpy(byArrRspData + 1, result, 8);
		*piRspLen = 9;
	}else if (iCmdlen == 33)
	{
		//AES
		encryptOrDecrypt = byArrCmdData[0];
		memcpy(aesKey, byArrCmdData + 1, 16);
		memcpy(aesTestData, byArrCmdData + 1 + 16, 16);

		//DPA_HIGH();
		ucl_aes_ecb(aesTestData, 16, aesKey, result, encryptOrDecrypt - '0');
		//DPA_LOW();
		
		byArrRspData[0] = '0';
		memcpy(byArrRspData + 1, result, 16);
		*piRspLen = 17;
	}else
	{
		byArrRspData[0] = '1';
		*piRspLen = 1;
	}
	
}	
