#include "protocol_kisPos_lkl.h"




static int iPackKisPosData(KisPosData *pstKisPosDataIn, byte *pKisPosDataOut, int *piKisPosDataOutLen)
{
	int iFieldDataLen;
	int iTotalDataLen = 0;
	int iTmpLen;
	byte tmpBuffer[1024];
	

	//BitMap
	iFieldDataLen = 2;
	memcpy(pKisPosDataOut + iTotalDataLen, pstKisPosDataIn->bitMap, iFieldDataLen);
	iTotalDataLen += iFieldDataLen;

	//Field 1
	if (((pstKisPosDataIn->bitMap[1])&0x01) != 0)
	{
		iFieldDataLen = 1;
		memcpy(pKisPosDataOut + iTotalDataLen, pstKisPosDataIn->Field1_Request, iFieldDataLen);
		iTotalDataLen += iFieldDataLen;
	}

	//Field 2
	if (((pstKisPosDataIn->bitMap[1])&0x02) != 0)
	{
		iFieldDataLen = 1;
		memcpy(pKisPosDataOut + iTotalDataLen, pstKisPosDataIn->Field2_Response, iFieldDataLen);
		iTotalDataLen += iFieldDataLen;
	}

	//Field 3
	if (((pstKisPosDataIn->bitMap[1])&0x04) != 0)
	{
		iFieldDataLen = 2;
		iTmpLen = strlen(pstKisPosDataIn->Field3_KisPosTerminalSN);
		if (iTmpLen > 100)
		{
			err("Field 3 length error\n");
			return -1;
		}
		sprintf(tmpBuffer,"%02d", iTmpLen);
		memcpy(pKisPosDataOut + iTotalDataLen, tmpBuffer, iFieldDataLen);
		iTotalDataLen += iFieldDataLen;

		iFieldDataLen = iTmpLen;
		memcpy(pKisPosDataOut + iTotalDataLen, pstKisPosDataIn->Field3_KisPosTerminalSN, iFieldDataLen);
		iTotalDataLen += iFieldDataLen;
	}

	//Field 4
	if (((pstKisPosDataIn->bitMap[1])&0x08) != 0)
	{
		iFieldDataLen = 2;
		iTmpLen = strlen(pstKisPosDataIn->Field4_NormalPosTerminalSN);
		if (iTmpLen > 100)
		{
			err("Field 3 length error\n");
			return -1;
		}
		sprintf(tmpBuffer,"%02d", iTmpLen);
		memcpy(pKisPosDataOut + iTotalDataLen, tmpBuffer, iFieldDataLen);
		iTotalDataLen += iFieldDataLen;

		iFieldDataLen = iTmpLen;
		memcpy(pKisPosDataOut + iTotalDataLen, pstKisPosDataIn->Field4_NormalPosTerminalSN, iFieldDataLen);
		iTotalDataLen += iFieldDataLen;
	}

	//Field 5
	if (((pstKisPosDataIn->bitMap[1])&0x10) != 0)
	{
		iFieldDataLen = 2;
		memcpy(pKisPosDataOut + iTotalDataLen, pstKisPosDataIn->Field5_VendorID, iFieldDataLen);
		iTotalDataLen += iFieldDataLen;
	}

	//Field 6
	if (((pstKisPosDataIn->bitMap[1])&0x20) != 0)
	{
		iFieldDataLen = 2;
		memcpy(pKisPosDataOut + iTotalDataLen, pstKisPosDataIn->Field6_ApplicationID, iFieldDataLen);
		iTotalDataLen += iFieldDataLen;
	}

	//Field 7
	if (((pstKisPosDataIn->bitMap[1])&0x40) != 0)
	{
		iFieldDataLen = 2;
		memcpy(pKisPosDataOut + iTotalDataLen, pstKisPosDataIn->Field5_VendorID, iFieldDataLen);
		iTotalDataLen += iFieldDataLen;
	}

	//Field 8
	if (((pstKisPosDataIn->bitMap[1])&0x80) != 0)
	{
		iFieldDataLen = 2;
		iTmpLen = strlen(pstKisPosDataIn->Field8_ICCardNO);
		if (iTmpLen > 100)
		{
			err("Field 8 length error\n");
			return -1;
		}
		sprintf(tmpBuffer,"%02d", iTmpLen);
		memcpy(pKisPosDataOut + iTotalDataLen, tmpBuffer, iFieldDataLen);
		iTotalDataLen += iFieldDataLen;

		iFieldDataLen = iTmpLen;
		memcpy(pKisPosDataOut + iTotalDataLen, pstKisPosDataIn->Field8_ICCardNO, iFieldDataLen);
		iTotalDataLen += iFieldDataLen;
	}	

	//Field 9
	if (((pstKisPosDataIn->bitMap[0])&0x01) != 0)
	{
		iFieldDataLen = 8;
		memcpy(pKisPosDataOut + iTotalDataLen, pstKisPosDataIn->Field9_ICCardExpDate, iFieldDataLen);
		iTotalDataLen += iFieldDataLen;
	}

	*piKisPosDataOutLen = iTotalDataLen;
	return 0;
}

static int iUnpackKisPosData(KisPosData *pstKisPosDataOut, byte *pKisPosDataIn, 
	int iKisPosDataLenIn)
{
	int iFieldDataLen;
	int iTotalDataLen = 0;
	int iTmpLen;
	byte tmpBuffer[1024];

	//Bit map field
	iFieldDataLen = 2;
	memcpy(pstKisPosDataOut->bitMap, pKisPosDataIn + iTotalDataLen, iFieldDataLen);
	iTotalDataLen += iFieldDataLen;
	
	//Filed 1
	if (((pstKisPosDataOut->bitMap[1])&0x01) != 0)
	{
		iFieldDataLen = 1;
		memcpy(pstKisPosDataOut->Field1_Request, pKisPosDataIn + iTotalDataLen, iFieldDataLen);
		iTotalDataLen += iFieldDataLen;
	}

	//Filed 2
	if (((pstKisPosDataOut->bitMap[1])&0x02) != 0)
	{
		iFieldDataLen = 1;
		memcpy(pstKisPosDataOut->Field2_Response, pKisPosDataIn + iTotalDataLen, iFieldDataLen);
		iTotalDataLen += iFieldDataLen;
	}

	//Field 3
	if (((pstKisPosDataOut->bitMap[1])&0x04) != 0)
	{
		//Length
		iFieldDataLen = 2;
		memcpy(tmpBuffer, pKisPosDataIn + iTotalDataLen, iFieldDataLen);
		tmpBuffer[2] = 0x00;
		iTmpLen = atoi(tmpBuffer);
		iTotalDataLen += iFieldDataLen;

		//Data
		iFieldDataLen = iTmpLen;
		memcpy(pstKisPosDataOut->Field3_KisPosTerminalSN, pKisPosDataIn + iTotalDataLen, iFieldDataLen);
		pstKisPosDataOut->Field3_KisPosTerminalSN[iFieldDataLen] = 0x00;
		iTotalDataLen += iFieldDataLen;
	}

	//Field 4
	if (((pstKisPosDataOut->bitMap[1])&0x04) != 0)
	{
		//Length
		iFieldDataLen = 2;
		memcpy(tmpBuffer, pKisPosDataIn + iTotalDataLen, iFieldDataLen);
		tmpBuffer[2] = 0x00;
		iTmpLen = atoi(tmpBuffer);
		iTotalDataLen += iFieldDataLen;

		//Data
		iFieldDataLen = iTmpLen;
		memcpy(pstKisPosDataOut->Field4_NormalPosTerminalSN, pKisPosDataIn + iTotalDataLen, iFieldDataLen);
		pstKisPosDataOut->Field4_NormalPosTerminalSN[iFieldDataLen] = 0x00;
		iTotalDataLen += iFieldDataLen;
	}

	//Field 5
	if (((pstKisPosDataOut->bitMap[1])&0x10) != 0)
	{
		iFieldDataLen = 2;
		memcpy(pstKisPosDataOut->Field5_VendorID, pKisPosDataIn + iTotalDataLen, iFieldDataLen);
		iTotalDataLen += iFieldDataLen;
	}

	//Field 6
	if (((pstKisPosDataOut->bitMap[1])&0x20) != 0)
	{
		iFieldDataLen = 2;
		memcpy(pstKisPosDataOut->Field6_ApplicationID, pKisPosDataIn + iTotalDataLen, iFieldDataLen);
		iTotalDataLen += iFieldDataLen;
	}

	//Field 7
	if (((pstKisPosDataOut->bitMap[1])&0x40) != 0)
	{
		iFieldDataLen = 2;
		memcpy(pstKisPosDataOut->Field7_TMK, pKisPosDataIn + iTotalDataLen, iFieldDataLen);
		iTotalDataLen += iFieldDataLen;
	}

	//Field 8
	if (((pstKisPosDataOut->bitMap[1])&0x80) != 0)
	{
		//Length
		iFieldDataLen = 2;
		memcpy(tmpBuffer, pKisPosDataIn + iTotalDataLen, iFieldDataLen);
		tmpBuffer[2] = 0x00;
		iTmpLen = atoi(tmpBuffer);
		iTotalDataLen += iFieldDataLen;

		//Data
		iFieldDataLen = iTmpLen;
		memcpy(pstKisPosDataOut->Field8_ICCardNO, pKisPosDataIn + iTotalDataLen, iFieldDataLen);
		pstKisPosDataOut->Field8_ICCardNO[iFieldDataLen] = 0x00;
		iTotalDataLen += iFieldDataLen;
	}

	//Field 9
	if (((pstKisPosDataOut->bitMap[0])&0x01) != 0)
	{
		iFieldDataLen = 8;
		memcpy(pstKisPosDataOut->Field9_ICCardExpDate, pKisPosDataIn + iTotalDataLen, iFieldDataLen);
		iTotalDataLen += iFieldDataLen;
	}

	if (iKisPosDataLenIn != iTotalDataLen)
	{
		err("iTotalDataLen error\n");
		return -1;
	}
	
	return 0;
}


static int Protocol_ReuqstOrResponse_Package(KisPosData *pKisPosData, 
	byte *pbyReuqstOrResponse, short *pnRequestOrResponseLen)
{
	int iOffset = 0;
	int iFieldLen;
	byte byLengthField[2];
	byte byPackDataOut[1024*5];
	int iPackDataOutLen;

	//STX
	iFieldLen = 1;
	pbyReuqstOrResponse[iOffset] = KISPOS_STX;
	iOffset+= iFieldLen; 

	//Head + Data
	iPackKisPosData(pKisPosData, byPackDataOut, &iPackDataOutLen);
	memcpy(pbyReuqstOrResponse + iOffset, byPackDataOut, iPackDataOutLen);


	//ETX	
	iFieldLen = 1;
	pbyReuqstOrResponse[iOffset] = KISPOS_ETX;
	iOffset+= iFieldLen; 


	//LRC	
	iFieldLen = 1;
	pbyReuqstOrResponse[iOffset] = CalculateLRC(pbyReuqstOrResponse, iOffset);
	iOffset+= iFieldLen; 

	*pnRequestOrResponseLen = iOffset;
	
	return 0;
}


static int Protocol_ReuqstOrResponse_Unpackage(KisPosData *pKisPosData, 
	const byte *pbyReuqstOrResponse, short nRequestOrResponseLen)
{
	int iOffset = 0;
	int iFieldLen;
	byte byArrLengthField[2];
	byte byCalLRC;

	//STX
	iFieldLen = 1;
	if (pbyReuqstOrResponse[iOffset] != STX)
	{
		return -1;
	}	
	iOffset+= iFieldLen; 
		

	//Bitmap and Data field
	iFieldLen = nRequestOrResponseLen - 1 - 2 - 1;
	iUnpackKisPosData(pKisPosData, pbyReuqstOrResponse + iOffset, iFieldLen);
	iOffset += iFieldLen;

	//ETX
	iFieldLen = 1;
	if (pbyReuqstOrResponse[iOffset] != ETX)
	{
		return -1;
	}	
	iOffset+= iFieldLen; 

	//LRC
	iFieldLen = 1;
	byCalLRC = CalculateLRC(pbyReuqstOrResponse, iOffset);
	if (byCalLRC != pbyReuqstOrResponse[iOffset])
	{
		return -2;
	}	
	iOffset+= iFieldLen; 
	
	//Check length
	if (iOffset != nRequestOrResponseLen)
	{
		return -3;
	}

	return 0;
}


int KisPos_Protocol_RcvRqst(int iProtocolType, byte *pbyCmdID, byte *pbyDataField, short *pnDataFieldLength, int iTimeoutMs)
{
	byte byArrRequest[PROTOCOL_PACKAGE_LENGTH_MAX] = {0};
	short nRequestLen;
	byte byArrResponse[PROTOCOL_PACKAGE_LENGTH_MAX] = {0};
	short nResponseLen;
	byte byRequestSeqID;
	byte byResponseStatusCode;
	int iRet;
	int iRetryTime = 0;
	
	//Receive
	iRet = Rcv_WholeRequestOrResponse(iProtocolType, byArrResponse, &nResponseLen, iTimeoutMs);
	if (iRet != 0)
	{
		return -1;
	}
	
	//Unpackage
	iRet = Protocol_ReuqstOrResponse_Unpackage(pbyCmdID, &byRequestSeqID, NULL, 
		pbyDataField, pnDataFieldLength, byArrResponse, nResponseLen);
	if (iRet != 0)
	{
		if (iRet == -2)
		{
			byResponseStatusCode = STATUS_CODE_ERR_LRC;
		}else
		{
			byResponseStatusCode = STATUS_CODE_ERR_OTHER;
		}

		Protocol_ReuqstOrResponse_Package(pbyCmdID, byRequestSeqID, byResponseStatusCode, NULL, 0, 
			 byArrResponse, &nResponseLen);

		//Send
		Snd_WholeRequestOrResponse(iProtocolType, byArrResponse, nResponseLen);
		return -2;
	}

	//Same Server sequence ID, send last response again
	if (byRequestSeqID == PowaPIN_SeqID_Get(iProtocolType, 0))
	{
		Snd_WholeRequestOrResponse(iProtocolType, gbyArrLastSndResponse, gnLastSndResponseLen);
		return -3;
	}

	//Update Server sequence ID
	PowaPIN_SeqID_Set(iProtocolType, 0, byRequestSeqID);	
	return 0;	
}


int KisPos_Protocol_SndRsp(int iProtocolType, byte byCmdID, byte *pbyDataField, short nDataFieldLength)
{
	byte byArrResponse[PROTOCOL_PACKAGE_LENGTH_MAX] = {0};
	short nResponseLen;
	int iRet;

	//Get Sever sequence ID
	Protocol_ReuqstOrResponse_Package(byCmdID, PowaPIN_SeqID_Get(iProtocolType, 0), STATUS_CODE_SUCCESS,
		pbyDataField, nDataFieldLength, byArrResponse, &nResponseLen);
	

	//Send
	iRet = Snd_WholeRequestOrResponse(iProtocolType, byArrResponse, nResponseLen);
	if (iRet != 0)
	{
		return -1;
	}

	//Back up last send response
	memcpy(gbyArrLastSndResponse, byArrResponse, nResponseLen);
	gnLastSndResponseLen = nResponseLen;
	return 0;	
}


