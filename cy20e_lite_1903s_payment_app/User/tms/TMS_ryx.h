#ifndef _TMS_H_
#define _TMS_H_

//终端注册
int iTmsTermReg(void);

//终端注销
int iTmsTermUnReg(void);

//心跳上送
int iTmsHeartBeat(char cDispFlag);

//任务查询
int iTmsTaskQuery(void);

//执行tms指令
int iExecTmsTask(void);

//任务执行结果上送
int iTmsTaskResultUploadOne(char *psTaskId);

int iTmsDownloadParam(uchar *psTaskId);

int iTmsDownloadApp(void);

#endif
