#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "projectconfig.h"
#include "user_projectconfig.h"
#include "common.h"
#include "VposFace.h"
#include "Pub.h"

#include "gprs_at.h"
#include "define.h"
#include "sys_littlefs.h"
#include "debug.h"
#include "tcpcomm.h"
#include "myHttp.h"

#include "AppGlobal_cjt.h"
#include "MemMana_cjt.h"
#include "TMS_ryx.h"

#include "mbedtls/md5.h"

#define UPDATE_FLAG_NO_UPDATE		"0"
#define UPDATE_FLAG_NEED_UPDATE		"1"
#define UPDATE_FLAG_UPDATE_ALREADY	"2"

#if 0//def JTAG_DEBUG
#undef JTAG_DEBUG
#define dbg(x...)		do{}while(0)
static void dbgLTxt(char *pszFormat, ...)
{
}
static void dbgHex(uchar *head, uchar *in, uint len)
{
}
    
#endif

/*
#ifndef __BATTERY_H__
typedef enum{
    VOL_PHASE_0,        //0   <=vol<3721
    VOL_PHASE_1,        //3721<=vol<3800
    VOL_PHASE_2,        //3721<=vol<3800
    VOL_PHASE_3,        //3800<=vol<3890
    VOL_PHASE_4,        //3890<=vol<4010
    VOL_PHASE_5,        //4010<=vol<4100
} Battery_vol;
#define UPDATE_VOL_PHASE         VOL_PHASE_3
#endif
*/
extern uint _uiCheckBattery(void);

//ryx KEK
#define TMK_KEK_RYX		"\x1F\xC1\x22\x2D\xE3\x7E\x36\xEF\xF2\xFD\x4E\xE8\xE5\x48\x70\xA9"

#define TMK_KEK_SM4_RYX "\x4A\x1C\x82\xB9\x4D\x1D\x80\x43\x77\xD6\x5F\x88\x43\xBB\x46\x6D"

#define RCV_MAX_LEN    8*1024
#define DOWNLOAD_PACKSIZE 8000

//支持断点续传
#define BREAKPOINT_TRANS

typedef struct
{
	uchar sTaskId[8];
	ushort usTaskCmd;
	uchar szResUrl[256];
	uchar sResSize[8];
	uchar sResMd5[16];
	uchar ucForceInsFlag;
	ushort usOperaCmd;

	uchar ucExecFlag;	  //0:无效 1:待执行 2:执行成功 3:执行失败
	uchar ucResultUpFlag; //上送结果标志:1待上送 0无需上送
	ushort usFailCode;	  //失败原因
} stTMSTASK;

stTMSTASK sg_TMSTasks[5];
/*
static void vCalCheckCode(uchar *psBuf, int iLen, uchar *pucOut)
{
	int i;
  uchar ucCrc;
  ucCrc = psBuf[0];
  for (i = 1; i < iLen; i++)
    ucCrc ^= psBuf[i];
  *pucOut = ucCrc;
}
*/
typedef struct{
	char *pszName;
	char type;
	void *val;
	int  size;
}stTmsParam;

// 's'-字符串 'l''i'-数字 'b'-二进制,需压缩 't'-特殊处理
// size在s或b的时候为最大长度，在数字的表示最大数，0表示不检查
stTmsParam TmsParamTable[]=
{
	{ "商户号=", 			's', 	gl_SysInfo.szMerchId, sizeof(gl_SysInfo.szMerchId)-1 },
	{ "终端号=", 			's', 	gl_SysInfo.szPosId, sizeof(gl_SysInfo.szPosId)-1 },
	{ "商户名称=", 			's', 	gl_SysInfo.szMerchName, sizeof(gl_SysInfo.szMerchName)-1 },
//    { "英文商户名称=",      's', 	gl_SysInfo.szEngMerchName, sizeof(gl_SysInfo.szEngMerchName)-1 },
	{ "POS初始流水号=", 	'l', 	&gl_SysData.ulTTC, 999999L }, 
	{ "POS初始批次号=", 	'l', 	&gl_SysData.ulBatchNo, 999999L },
	{ "参数设置密码=", 		's', 	gl_SysInfo.szSysOperPwd, sizeof(gl_SysInfo.szSysOperPwd)-1 },
//	{ "安全设置密码=", 		's', 	gl_SysInfo.szSafeSetPwd, sizeof(gl_SysInfo.szSafeSetPwd)-1 },
	{"是否输入主管密码="	,'c',	&gl_SysInfo.ucMainOperPwd,1},
    { "主管管理员密码=",    's',    gl_SysInfo.szMainOperPwd, sizeof(gl_SysInfo.szMainOperPwd)-1 },
    { "是否国密=",          'c',    &gl_SysInfo.ucSmFlag, 1},
    { "终端主密钥密文=",    't',    &gl_SysInfo.sMasterKeyCiphertext, 0},
    
    { "是否支持消费=",          'c',    &gl_SysInfo.ucSaleSwitch, 1},
    { "是否支持撤销=",          'c',    &gl_SysInfo.ucVoidSwitch, 1},
    { "是否支持退货=",          'c',    &gl_SysInfo.ucRefundSwitch, 1},
    { "是否支持余额查询=",		'c',    &gl_SysInfo.ucBalanceSwitch, 1},
    
#ifdef ENABLE_PRINTER    
    { "是否支持预授权=",		'c',    &gl_SysInfo.ucAuthSwitch, 1},
    { "是否支持预授权撤销=",	'c',    &gl_SysInfo.ucAuthCancelSwitch, 1},
    { "是否支持预授权完成=",	'c',    &gl_SysInfo.ucAuthCompleteSwitch, 1},
    { "是否支持预授权完成撤销=",'c',    &gl_SysInfo.ucAuthCompleteVoidSwitch, 1},
#endif 

    { "是否支持扫码交易=",		'c',    &gl_SysInfo.ucQrSaleSwitch, 1},
    { "是否支持扫码退货=",		'c',    &gl_SysInfo.ucQrRefundSwitch, 1},
    { "是否支持扫码撤销=",		'c',    &gl_SysInfo.ucQrVoidSwitch, 1},
    
#ifdef ENABLE_PRINTER    
    { "是否支持扫码预授权=",	'c',    &gl_SysInfo.ucQrPreAuthSwitch, 1},
    { "是否支持扫码预授权撤销=",'c',    &gl_SysInfo.ucQrPreAuthCancelSwitch, 1},
    { "是否支持扫码预授权完成=",'c',    &gl_SysInfo.ucQrAuthCompleteSwitch, 1},
//    { "是否支持扫码预授权完成撤销=",	'c',    &gl_SysInfo.ucCanQrAuthCmpVoid, 1},
#endif    

    { "是否开通手输卡号=",		'c',    &gl_SysInfo.ucHandCard, 1},
    { "是否允许bypass=",		'c',    &gl_SysInfo.ucByPass, 1},
    
    { "交易最大笔数=",		'i', 	&gl_SysInfo.ucMaxTradeCnt, 300},
    { "最大退货限额=",		'l', 	&gl_SysInfo.ulRefund_limit, 		0 }, 
    { "操作超时时间=",		'i', 	&gl_SysInfo.uiUiTimeOut, 0},                  
    
    { "是否免密=",			'c',    &gl_SysInfo.ucNoPinFlag, 1},
    { "是否免签=",			'c',    &gl_SysInfo.ucNoSignFlag, 1},
    { "BIN表A标识=",		'c',    &gl_SysInfo.ucBINAFlag, 1},
    { "BIN表B标识=",		'c',    &gl_SysInfo.ucBINBFlag, 1},
    { "CDCVM标识=",			'c',    &gl_SysInfo.ucCDCVMFlag, 1},
    
    { "免密限额=",			'l', 	&gl_SysInfo.ulNoPinLimit, 		0 },
    { "免签限额=",			'l', 	&gl_SysInfo.ulNoSignLimit, 		0 },
    { "优先挥卡交易=",		'c',    &gl_SysInfo.ucNFCTransPrior, 1},
    { "加密磁道信息=",		'c',    &gl_SysInfo.magEncrypt, 1},
    { "是否二维码扫码预览=",'c',	&gl_SysInfo.ucQrPreViewFlag, 1},
    
#ifdef ENABLE_PRINTER       
    { "支持电子签名=",		'c',    &gl_SysInfo.ucSupportESign, 1},
#endif    

    { "签名重发次数=",		'c',    &gl_SysInfo.uiESignSendCount, 5},
    { "签名输入超时时间=",	'i', 	&gl_SysInfo.uiESignTimeout, 0},

    { "消费撤销交易是否输入密码=",		'c', 	&gl_SysInfo.ucSaleVoidPinFlag, 1},
    
#ifdef ENABLE_PRINTER    
    { "预授权撤销交易是否输入密码=",	'c', 	&gl_SysInfo.ucAuthVoidPinFlag, 1},
    { "预授权完成撤销是否输入密码=",	'c', 	&gl_SysInfo.ucAuthCompVoidPinFlag, 1},
    { "预授权完成请求是否输入密码=",	'c', 	&gl_SysInfo.ucAuthCompPinFlag, 1},    
    { "消费撤销刷卡标志=",				'c', 	&gl_SysInfo.ucSaleVoidCardFlag, 1},  
    { "预授权完成撤销刷卡标志=",		'c', 	&gl_SysInfo.ucAuthCompleteVoidCardFlag, 1},
#endif    

    { "自动签退=",			'c', 	&gl_SysInfo.ucAutoLogoutFlag, 1},
    
#ifdef ENABLE_PRINTER     
    { "是否打印明细=",		'c', 	&gl_SysInfo.ucSettlePrintDetail, 1},
    { "打印凭单数设置=",	'c', 	&gl_SysInfo.ucPrinterAttr, 3},
    { "是否打印英文=",		'c', 	&gl_SysInfo.ucPrintEN, 1},    
    { "是否打印中文发卡行=",		'c',	&gl_SysInfo.ucPrintIssuer, 1},
    { "默认交易设置=",		'c',	&gl_SysInfo.ucDefaultTrans, 1},
    { "预授权屏蔽卡号=",	'c',	&gl_SysInfo.ucAuthCardMask ,1},
    //{ "签购单字体打印=",		'c', 	NULL, 0},
#endif

    { "通讯方式=",			't', 	NULL, 0},
    { "用户名=",			's', 	gl_SysInfo.szApnUser, sizeof(gl_SysInfo.szApnUser)-1},
    { "网络密码=",			's', 	gl_SysInfo.szApnPwd, sizeof(gl_SysInfo.szApnPwd)-1}, 
    { "APN名称=",			's', 	gl_SysInfo.szApn, sizeof(gl_SysInfo.szApn)-1},
    //{ "接入点号码=",		'c', 	NULL, 0},
    //{ "无线是否为长连接=",		'c', 	NULL, 0},
    
    { "连接超时=",          'i', 	&gl_SysInfo.uiConnectTimeout, 0},     
    { "交易重发次数=",		'i', 	&gl_SysInfo.ucCommRetryCount, 0},
    { "通讯最长等待时间=",  'i', 	&gl_SysInfo.uiCommTimeout, 0},
    { "是否支持立即冲正=",  'c', 	&gl_SysInfo.ucDoReverseFlag, 1},
    { "最大冲正次数=",		'i', 	&gl_SysInfo.ucMaxResendCount, 3},
    { "是否开启隔日删除冲正=",    'c',	&gl_SysInfo.ucCleanReverseFlag, 1},
    { "隔日删除冲正时间=",	's', 	&gl_SysInfo.ucTimeCleanReverse, sizeof(gl_SysInfo.ucTimeCleanReverse)-1 },

    { "银行卡TPDU=",		'b',	gl_SysInfo.sTPDU, sizeof(gl_SysInfo.sTPDU) },
    { "扫码TPDU=",			'b', 	gl_SysInfo.sQrTPDU, sizeof(gl_SysInfo.sQrTPDU) },

      { "服务器访问方式=",		'c',    &gl_SysInfo.ucDomainOrIp, 1 },
	{ "银行卡主域名地址=",		's',	gl_SysInfo.szSvrDomain, sizeof(gl_SysInfo.szSvrDomain)-1 },
	{ "银行卡主域名端口=",		'i',	&gl_SysInfo.uiSvrPort, 65535 },
	{ "银行卡备域名地址=",		's',	gl_SysInfo.szBakSvrDomain, sizeof(gl_SysInfo.szBakSvrDomain)-1 },
	{ "银行卡备域名端口=",		'i',	&gl_SysInfo.uiBakSvrPort, 65535 },
	
	{ "服务器远端IP地址一=",	's',	gl_SysInfo.szSvrIp, sizeof(gl_SysInfo.szSvrIp)-1 },
	{ "服务器端口号一=",		'i',	&gl_SysInfo.uiSvrPort, 65535 },
	{ "服务器远端IP地址二=",	's',	gl_SysInfo.szBakSvrIp, sizeof(gl_SysInfo.szBakSvrIp)-1 },
	{ "服务器端口号二=",		'i',	&gl_SysInfo.uiBakSvrPort,	65535 },
    
    { "远程下载服务器访问方式=",'c',	&gl_SysInfo.ucTmsDomainOrIp, 1 },
    { "远程下载主域名地址=",	's',	gl_SysInfo.szTmsHost, sizeof(gl_SysInfo.szTmsHost)-1 },
    { "远程下载主域名端口=",	'i',	&gl_SysInfo.uiTmsHostPort, 65535 },    
    { "远程下载备域名地址=",	's',	gl_SysInfo.szTmsHostBak, sizeof(gl_SysInfo.szTmsHostBak)-1 },
    { "远程下载备域名端口=",	'i',	&gl_SysInfo.uiTmsHostPortBak, 65535 },
    { "远程更新IP1=",			's',	gl_SysInfo.szTmsIP1, sizeof(gl_SysInfo.szTmsIP1)-1 },
    { "远程更新端口1=",			'i',	&gl_SysInfo.uiTmsHostPort, 65535 },    
    { "远程更新IP2=",		    's',	gl_SysInfo.szTmsIP2, sizeof(gl_SysInfo.szTmsIP2)-1 },
    { "远程更新端口2=",			'i',	&gl_SysInfo.uiTmsHostPortBak, 65535 },
    { "远程更新TPDU=",			'b',	gl_SysInfo.sTmsTPDU, sizeof(gl_SysInfo.sTmsTPDU) },
    
    { "参数版本号=",			's',	gl_SysInfo.szParamVer, sizeof(gl_SysInfo.szParamVer)-1 },
    { "附近基站间隔时间=",		'i', 	&gl_SysInfo.intervaltime, 0},   
    //{ "软件版本号="
    //{ "远程下载块大小="
#ifdef ENABLE_PRINTER     
    { "是否打印商户联广告二维码=",	'c',	&gl_SysInfo.ucPrintMerchantQrFlag, 1},
    { "是否打印商户联广告信息=",	'c',	&gl_SysInfo.ucPrintMerchantMsgFlag,1},
    { "商户联打印广告二维码=",		's',	gl_SysInfo.ucPrintMerchantQr, sizeof(gl_SysInfo.ucPrintMerchantQr)-1 },
    { "商户联打印广告文字=",		's',	gl_SysInfo.ucPrintMerchantMsg, sizeof(gl_SysInfo.ucPrintMerchantMsg)-1 },
    
    { "是否打印持卡人联广告二维码=",'c',	&gl_SysInfo.ucPrintCardholderQrFlag, 1},
    { "是否打印持卡人联广告信息=",	'c',	&gl_SysInfo.ucPrintCardholderMsgFlag, 1},
    { "持卡人联打印广告二维码=",	's',	gl_SysInfo.ucPrintCardholderQr, sizeof(gl_SysInfo.ucPrintCardholderQr)-1 },
    { "持卡人联打印广告文字=",		's',	gl_SysInfo.ucPrintCardholderMsg, sizeof(gl_SysInfo.ucPrintCardholderMsg)-1 },
    
	{ "服务热线=",					's',	gl_SysInfo.ucServiceHotline, sizeof(gl_SysInfo.ucServiceHotline)-1 },
#endif	

	{ NULL, 0, NULL}
};

extern void vPrtParam(void);
extern int SaveTMK(byte *TMK);

extern void vRebootforUpdate(uchar ucForceInsFlag);
int iTmsUpdateEnd(ulong ulParamTaskId, char *pszParamVer, ulong ulAppTaskId, char *pszAppVer);
int iDownloadApp(ulong ulTaskId, uchar *pszAppVer, ulong start, ulong ulAppLen);
int iDownloadParam(ulong ulTaskId, uchar *pszParamVer, uint uiParamLen);

//#define PRT_TMS_PARAM
int iParseParam(char *pszParams)
{
	int i;
	uchar *p0, *p1;
	char szLnEnd[2+1];
	ulong *pulTmp;
    ushort *puiTmp;
    uchar *pucTmp;
	int len;
	uchar szTmp[320];
	
#ifdef PRT_TMS_PARAM
	uchar szPrtBuf[100];
	_uiPrintOpen();
#endif

	strcpy(szLnEnd, "\x0D\x0A");
	if(strstr(pszParams, szLnEnd)==NULL)
		strcpy(szLnEnd, "\x0A");
	for(i=0; TmsParamTable[i].pszName; i++)
	{
		p0=(uchar*)strstr(pszParams, TmsParamTable[i].pszName);
		if(p0==NULL)
			continue;
		p0+=strlen(TmsParamTable[i].pszName);
		p1=(uchar*)strstr((char*)p0, szLnEnd);
		if(p1==NULL)
		{
			dbg("not find LF/CR end.[%s]\n", TmsParamTable[i].pszName);
		#ifdef PRT_TMS_PARAM
			_uiPrint("\n\n\n", 1);
			_uiPrintClose();
		#endif
			return 1;
		}
		switch (TmsParamTable[i].type)
		{
		case 'l' :
            pulTmp=(ulong *)TmsParamTable[i].val;
			*pulTmp=ulA2L(p0, p1-p0);
			if(TmsParamTable[i].size!=0 && *pulTmp>TmsParamTable[i].size)
			{
                dbgHex("name", (uchar*)TmsParamTable[i].pszName, strlen(TmsParamTable[i].pszName));
				dbg("size err.[%d],[%lu]\n", TmsParamTable[i].size, *pulTmp);                
				*pulTmp=TmsParamTable[i].size;
			}
			#ifdef PRT_TMS_PARAM
			sprintf(szPrtBuf, "%s=[%lu] [%.12s]", TmsParamTable[i].pszName, *pulTmp, p0);
			_uiPrint(szPrtBuf, 0);
			#endif
            #ifdef JTAG_DEBUG
            vMemcpy0(szTmp, p0, p1-p0);
            dbg("[%s]=[%lu], [%s]\n", TmsParamTable[i].pszName, *pulTmp, szTmp);
            #endif
			break;
		case 'i' :
			puiTmp=(ushort *)TmsParamTable[i].val;
			*puiTmp=(ushort)ulA2L(p0, p1-p0);
			if(TmsParamTable[i].size!=0 && *puiTmp>TmsParamTable[i].size)
			{
                dbgHex("name", (uchar*)TmsParamTable[i].pszName, strlen(TmsParamTable[i].pszName));
				dbg("size err.[%d],[%d]\n", TmsParamTable[i].size, *puiTmp);
				*puiTmp=(ushort)TmsParamTable[i].size;
			}
			#ifdef PRT_TMS_PARAM
			sprintf(szPrtBuf, "%s=[%u], [%.6s]", TmsParamTable[i].pszName, *puiTmp, p0);
			_uiPrint(szPrtBuf, 0);
			#endif
            #ifdef JTAG_DEBUG
            vMemcpy0(szTmp, p0, p1-p0);
            dbg("[%s]=[%u], [%s]\n", TmsParamTable[i].pszName, *puiTmp, szTmp);
            #endif
			break;
		case 'c' :
			pucTmp=(uchar *)TmsParamTable[i].val;
			*pucTmp=(uchar)ulA2L(p0, p1-p0);
			if(TmsParamTable[i].size!=0 && *pucTmp>TmsParamTable[i].size)
			{
				dbg("size err.[%d],[%d]\n", TmsParamTable[i].size, *pucTmp);
				*pucTmp=(uchar)TmsParamTable[i].size;
			}
			#ifdef PRT_TMS_PARAM
			sprintf(szPrtBuf, "%s=[%u],[%.3s]", TmsParamTable[i].pszName, *pucTmp, p0);
			_uiPrint(szPrtBuf, 0);
			#endif
            #ifdef JTAG_DEBUG
            vMemcpy0(szTmp, p0, p1-p0);
            dbg("[%s]=[%u], [%s]\n", TmsParamTable[i].pszName, *pucTmp, szTmp);
            _vDelay(5);
            #endif
			break;
		case 's' :
			len=p1-p0;
			if(TmsParamTable[i].size)
			{
				memset(TmsParamTable[i].val, 0, TmsParamTable[i].size);
				if(len>TmsParamTable[i].size)
					len=TmsParamTable[i].size;
			}
			memcpy((uchar*)TmsParamTable[i].val, p0, len);
			#ifdef PRT_TMS_PARAM
			sprintf(szPrtBuf, "%s=[%.20s],[%.20s]", TmsParamTable[i].pszName, (char *)TmsParamTable[i].val, p0);
			_uiPrint(szPrtBuf, 0);
			#endif
            #ifdef JTAG_DEBUG
            vMemcpy0(szTmp, p0, len);
            dbg("[%s]=[%s], [%s]\n", TmsParamTable[i].pszName, TmsParamTable[i].val, szTmp);
            _vDelay(10);
            #endif
			break;
		case 'b' :
			len=p1-p0;
			if(TmsParamTable[i].size)
			{
				memset((uchar*)TmsParamTable[i].val, 0, TmsParamTable[i].size);
				if(len>TmsParamTable[i].size*2)
					len=TmsParamTable[i].size*2;
			}
			vTwoOne(p0, len, (uchar*)TmsParamTable[i].val);
			#ifdef PRT_TMS_PARAM
			vOneTwo0((uchar*)TmsParamTable[i].val, len/2, szTmp);
			sprintf(szPrtBuf, "%s=[%.20s], [%.20s]", TmsParamTable[i].pszName, szTmp, p0);
			_uiPrint(szPrtBuf, 0);
			#endif
            #ifdef JTAG_DEBUG
            vMemcpy0(szTmp, p0, len);
            dbg("[%s]=[...], [%s]\n", TmsParamTable[i].pszName, szTmp);
            dbgHex("...", (uchar*)TmsParamTable[i].val, len/2);
            #endif
            break;
		case 't' :
			if(strcmp(TmsParamTable[i].pszName, "终端主密钥密文=")==0)
			{
				len=p1-p0;
				if(len>sizeof(gl_SysInfo.sMasterKeyCiphertext)*2)
				{
					dbg("TMK len err.\n");
					#ifdef PRT_TMS_PARAM
						_uiPrint("\n\n\n", 1);
						_uiPrintClose();
					#endif
					return 1;
				}
                vMemcpy0(szTmp, p0, len);
                dbg("TMK CipherText:%s\n", szTmp);
                if(len!=32 && len!=16)
                    dbg("tmk len err!\n");
				vTwoOne(p0, len, szTmp);
                if(gl_SysInfo.ucSmFlag==0)
                {
                    dbg("tmk des dec.\n");
                    _vDesPlus(TRI_DECRYPT, szTmp, len/2, TMK_KEK_RYX, gl_SysInfo.sMasterKey);
                }else
                {
                    dbg("tmk sm4 dec.\n");
                    if(len/2==16)
                        _iSm4(DECRYPT, szTmp, (uchar*)TMK_KEK_SM4_RYX, gl_SysInfo.sMasterKey);
                }
				
				SaveTMK(gl_SysInfo.sMasterKey);	//存储至安全模块
				dbgHex("Tmk:", gl_SysInfo.sMasterKey, 16);
			}else if(strcmp(TmsParamTable[i].pszName, "通讯方式=")==0)
            {
                len=p1-p0;
                if(p0[0]=='G')
                    gl_SysInfo.iCommType=VPOSCOMM_GPRS;
                else if(p0[0]=='W')
                    gl_SysInfo.iCommType=VPOSCOMM_WIFI;
                #ifdef JTAG_DEBUG
                vMemcpy0(szTmp, p0, len);
                dbg("CommType=[%d],[%s]", gl_SysInfo.iCommType, szTmp);
                #endif
            }
			break;		
		default :
			break;
		}
	}
#ifdef PRT_TMS_PARAM
	_uiPrint("\n\n\n", 1);
	_uiPrintClose();
#endif

	//测试环境签名上送地址参数有误,调整成交易地址,两者端口不同
#ifdef TEST_ENV_APP
	//if(strcmp(gl_SysInfo.szESignSvrIp, "183.62.138.234")==0 && strcmp(gl_SysInfo.szSvrIp,"218.17.158.202")==0)
	//	strcpy(gl_SysInfo.szESignSvrIp, gl_SysInfo.szSvrIp);
#endif
    
	//如果没有下发备用地址,则用主地址填充备用地址
	{
		if(gl_SysInfo.szBakSvrIp[0]==0 || gl_SysInfo.uiBakSvrPort==0)
		{
			strcpy(gl_SysInfo.szBakSvrIp, gl_SysInfo.szSvrIp);
			gl_SysInfo.uiBakSvrPort=gl_SysInfo.uiSvrPort;
		}

		if(gl_SysInfo.szESignBakSvrIp[0]==0 || gl_SysInfo.uiESignBakSvrPort==0)
		{
			strcpy(gl_SysInfo.szESignBakSvrIp, gl_SysInfo.szESignSvrIp);
			gl_SysInfo.uiESignBakSvrPort=gl_SysInfo.uiESignSvrPort;
		}

		if(gl_SysInfo.szTmsHostBak[0]==0 || gl_SysInfo.uiTmsHostPortBak==0)
		{
			strcpy(gl_SysInfo.szTmsHostBak, gl_SysInfo.szTmsHost);
			gl_SysInfo.uiTmsHostPortBak=gl_SysInfo.uiTmsHostPort;
		}
	}
	dbg("gl_SysInfo.intervaltime:%d\n",gl_SysInfo.intervaltime);
	return 0;
}

void vTmsShowHostErrMsg(uchar *pszHostErrCode)
{
    char szBuf[40];
    uchar ucErrCode;
        
    vTwoOne(pszHostErrCode, 2, &ucErrCode);

    switch(ucErrCode)
	{
		case 0x00 :         //交易成功
			strcpy(szBuf, "交易成功");
			break;
		case 0x01 :         //未知终端号
			strcpy(szBuf, "未知终端号");
			break;
		case 0x02 :         //软件为最新版本，不需要更新
			strcpy(szBuf, "软件为最新版,不需更新");
			break;
		case 0x03 :         //参数为最新版本，不需要更新
			strcpy(szBuf, "参数为最新版,不需更新");
			break;
		case 0x04 :         //服务器拒绝更新
			strcpy(szBuf, "服务器拒绝更新");
			break;
		case 0x05 :         //系统忙
			strcpy(szBuf, "系统忙");
			break;
		case 0x06 :         //找不到下载版本
			strcpy(szBuf, "找不到下载版本");
			break;
		case 0x07 :         //未知通知类型
			strcpy(szBuf, "未知通知类型");
			break;
		case 0x08 :         //Lrc校验位错误
			strcpy(szBuf, "Lrc校验位错误");
			break;
		case 0x09 :         //不需要更新
			strcpy(szBuf, "不需要更新");
			break;
		case 0x10 :         //请求版本不一致，重新下载
			strcpy(szBuf, "请求版本不一致,重下载");
			break;
		default :
			strcpy(szBuf, "服务端处理失败");
			break;
	}
	vMessage(szBuf);
    return;
}

static int iTmsConnStatus=0;
static int sg_iTmsSvrIdx=0;

void vTmsDisConn(void)
{
	iTmsConnStatus=0;
	iCommTcpDisConn();
}

int iTmsConn(void)
{
	int iRet;
    uchar szHostName[256 + 1], szSubUrl[256 + 1];
    char szSvrIP[2][16], szSvrPort[2][6];
    int iSvrIdx=sg_iTmsSvrIdx;
    uint uiPort = 0;
    
    if(iTmsConnStatus==0)
	{
        if(gl_SysInfo.ucTmsDomainOrIp == 1)	
        {         
            vDispMid(3, "域名解析...");
            iRet = iParseHttpUrl(gl_SysInfo.szTmsHost, szHostName, szSubUrl, &uiPort);
            if (iRet)
            {
                vMessage("解析域名失败");
            }
            iRet = iGetHostByName((char*)szHostName, szSvrIP[0]);		
            if(iRet)
            {
                vMessage("解析域名IP失败");
            }				
            sprintf(szSvrPort[0], "%d", gl_SysInfo.uiTmsHostPort);         
        }
        else if(gl_SysInfo.ucTmsDomainOrIp== 0)	
        {
            strcpy(szSvrIP[0], (char*)gl_SysInfo.szTmsIP1);
            sprintf(szSvrPort[0], "%d", gl_SysInfo.uiTmsHostPort);    

            dbg("gl_SysInfo.ucDomainOrIp=0 ...[%s][%d]\n", gl_SysInfo.szTmsIP1, gl_SysInfo.uiTmsHostPort);
        }
//		strcpy(szSvrIP[0], (char*)gl_SysInfo.szTmsHost);
//		strcpy(szSvrIP[1], (char*)gl_SysInfo.szTmsHostBak);
//		sprintf(szSvrPort[0], "%d", gl_SysInfo.uiTmsHostPort);
//		sprintf(szSvrPort[1], "%d", gl_SysInfo.uiTmsHostPortBak);
        
		dbg("conn to svr...[%d][%s][%s]\n", iSvrIdx,szSvrIP[iSvrIdx], szSvrPort[iSvrIdx]);
                vDispMid(3, "连接服务器...");
		iRet = iCommTcpConn(szSvrIP[iSvrIdx], szSvrPort[iSvrIdx], 10);
		if(iRet>0)
		{
            //使用备份域名
            vDispMid(3, "使用备份服务器...");  
            if(gl_SysInfo.ucTmsDomainOrIp == 1)	
            {		        	 
                iRet = iParseHttpUrl(gl_SysInfo.szTmsHostBak, szHostName, szSubUrl, &uiPort);
                if (iRet)
                {
                    vMessage("解析备用域名失败");
                    return -1*COM_PARAMETER;
                }
                iRet = iGetHostByName((char*)szHostName, szSvrIP[1]);
                if(iRet)
                {
                    vMessage("解析备用域名IP失败");
                    return -1*COM_PARAMETER;
                }
                sprintf(szSvrPort[1], "%d", gl_SysInfo.uiTmsHostPortBak);
            }else if(gl_SysInfo.ucTmsDomainOrIp== 0)	
            {   		
                strcpy(szSvrIP[1], (char*)gl_SysInfo.szTmsIP2);
                sprintf(szSvrPort[1], "%d", gl_SysInfo.uiTmsHostPortBak);
            }
				 
			iSvrIdx=(iSvrIdx+1)%2;
			dbg("conn to baksvr...[%d][%s][%s]\n", iSvrIdx,szSvrIP[iSvrIdx], szSvrPort[iSvrIdx]);
			iRet = iCommTcpConn(szSvrIP[iSvrIdx], szSvrPort[iSvrIdx], 10);
			if(iRet>0)
			{
				iSvrIdx=(iSvrIdx+1)%2;
				iRet = iCommTcpConn(szSvrIP[iSvrIdx], szSvrPort[iSvrIdx], 10);
			}
		}
		if (iRet)
			return iRet;
		sg_iTmsSvrIdx=iSvrIdx;
	}
    
    iTmsConnStatus=1;
    return 0;
}

static int iTmsSendRecv(uchar *psSnd, uint uiSndLen, uchar *psRcv, uint *puiRcvLen)
{
	uint uiDataLen;
	int iRet;
	//char szHostPort[10];
	uint uiTimeOut = 30;
		
	//strcpy(gl_SysInfo.szTmsHost, "218.17.158.202");
	//gl_SysInfo.uiTmsHostPort=27000;
    if(iTmsConnStatus==0 && (iRet=iTmsConn())!=0)
    {
        return iRet;
    }

	dbgHex("Tms Snd", psSnd, uiSndLen);

	iRet = iCommTcpSend(psSnd, uiSndLen);
	if (iRet != uiSndLen)
	{
		vMessage("发送报文失败");
		return 1;
	}

	iRet = iCommTcpRecv(psRcv, 2, uiTimeOut * 1000);
	if (iRet != 2)
	{
		dbg("tms rcv msg err:%d\n", iRet);
        //vDispVarArg(5, "rcv 2, iRet=%d", iRet);
		return 12;
	}
	uiDataLen = psRcv[0] * 256 + psRcv[1];
	if (uiDataLen < 2 || uiDataLen > RCV_MAX_LEN - 2)
	{
		dbgHex("Tms rcv1", psRcv, 2);
		dbg("tms rsp len err, datalen=%d.\n", uiDataLen);
        //vDispVarArg(5, "rcv 2,date=%02X%02X, len=%u", psRcv[0], psRcv[1], uiDataLen);
		return 13;
	}

	iRet = iCommTcpRecv(psRcv + 2, uiDataLen, 5 * 1000);
	if (iRet != uiDataLen)
	{
        if(iRet>0)
            dbgHex("Tms rcv1", psRcv, iRet+2);
		dbg("tms rcv msg err2:%d\n", iRet);
        //vDispVarArg(5, "rcv data, %d!=%d", uiDataLen, iRet);
		return 14;
	}

	*puiRcvLen = uiDataLen + 2;
	dbgHex("Tms rcv", psRcv, *puiRcvLen);
/*
	uiRspCode = psRcv[TMSHeadLen] * 256 + psRcv[TMSHeadLen + 1];
	if (uiRspCode)
	{
		dbg("TMS fail, retcode:[%02X%02X][%d].\n", psRcv[TMSHeadLen], psRcv[TMSHeadLen + 1], uiRspCode);
		return 3;
	}
*/
	return 0;
}

static int iTmsSendRecvTA(uchar *psSnd, uint uiSndLen, uchar *psRcv, int RcvExpLen, uint *puiRcvLen)
{
    uint uiDataLen;
    int iRet;
    //char szHostPort[10];
    uint uiTimeOut = 30;

    if(iTmsConnStatus==0 && (iRet=iTmsConn())!=0)
    {
        return iRet;
    }
    dbgHex("Tms Snd", psSnd, uiSndLen);

    iRet = iCommTcpSend(psSnd, uiSndLen);
    if (iRet != uiSndLen)
    {
        vMessage("发送报文失败");
        return 1;
    }
    
    iRet = iCommTcpRecv(psRcv, RcvExpLen, uiTimeOut * 1000);
    if (iRet<2+2+8+1)
    {
        dbg("tms rcv msg err:%d\n", iRet);
        return iRet;
    }
    uiDataLen = psRcv[0] * 256 + psRcv[1];
    if (uiDataLen+2 !=iRet)
    {
        dbg("tms rcv msg len err:%d, %d\n", iRet, uiDataLen);
        return 12;        
    }

    *puiRcvLen = iRet;
    //dbgHex("Tms rcv", psRcv, *puiRcvLen);

    return 0;
}

//终端注销
int iTmsTermUnReg(void)
{
	return 1;
}

//心跳上送
int iTmsHeartBeat(char cDispFlag)
{

    return 0;
}

/*
extern ulong _ulGetTimeStamp(void);
void vTestTimeStamp(void)
{
	uchar sTmSta[8];
	uchar szBuf[30];
	unsigned long long ullNum1, ullNum2;

	_vDisp(2, "测试timestamp?");
	if (_uiGetKey() == _KEY_ESC)
	{
		return;
	}

	memcpy(sTmSta, "\x75\xcf\xe4\x7d\x71\x01\x00\x00", 8);
	memcpy(&ullNum1, sTmSta, 8);

	sprintf(szBuf, "num:%llu", ullNum1);
	_vDisp(3, szBuf);

	uint rtcsec1 = _ulGetTimeStamp();
	uint rtcsec2 = _ulGetTimeStamp();
	sprintf(szBuf, "num1:%u", rtcsec1);
	_vDisp(4, szBuf);
	sprintf(szBuf, "num2:%u", rtcsec2);
	_vDisp(5, szBuf);

	_uiGetKey();

	return;
}
*/

void vUpDateAppfile(void)
{
	int ret;
	uchar sRcvTmp[1500];
	uchar szBuf[100];

	lfs_file_t newAppFile;
	lfs_file_t updateFlagFile;
	char szUpdateAppNewPath[128] = {0};
	char szUpdateFlagPath[128] = {0};

	_vDisp(1, "检查文件md5");

	sprintf(szUpdateAppNewPath, "%s/%s", DIR_UPDATE, FILE_NAME_UPDATE_NEW_APP);
	{
		mbedtls_md5_context ctx;

		int ret, len = 0;
		unsigned char md[16];
		int n=0;
		
		ret=sys_lfs_file_open(&newAppFile, szUpdateAppNewPath, LFS_O_RDONLY);
		if(ret)
		{
			_vDisp(2, szUpdateAppNewPath);
			vMessageEx("打开文件失败", 10*100);
			return;
		}
		
		mbedtls_md5_init(&ctx);
		mbedtls_md5_starts_ret( &ctx );
		while ((len = sys_lfs_file_read(&newAppFile, sRcvTmp, 1024)) > 0)
		{
			mbedtls_md5_update_ret (&ctx, sRcvTmp, len);
			n+=len;
		}
		mbedtls_md5_finish_ret(&ctx, md);
		mbedtls_md5_free( &ctx );

		sys_lfs_file_close(&newAppFile);

		vOneTwo0(md, 8, sRcvTmp);
		vOneTwo0(md+8, 8, sRcvTmp+50);
		vDispVarArg(5, "[%s]", szUpdateAppNewPath);
		_vDisp(2, sRcvTmp);
		_vDisp(3, sRcvTmp+50);
		vDispVarArg(4, "file len:%d", n);
		_uiGetKey();
	}

	//_vCls();
	sprintf(szUpdateFlagPath, "%s/%s", DIR_UPDATE, FILE_NAME_UPDATE_APP_FLAG);
	vDispVarArg(5, "[%s]", szUpdateFlagPath);
	
	//先读原值
	ret=sys_lfs_file_open(&updateFlagFile, szUpdateFlagPath, LFS_O_RDONLY);
	if(ret==0)
	{
		ret=sys_lfs_file_read(&updateFlagFile, szBuf, strlen(UPDATE_FLAG_NEED_UPDATE));
		sys_lfs_file_close(&updateFlagFile);
		vDispVarArg(6, "r UPDATE_FLAG:%02X\n", szBuf[0]);
	}
	
	ret=sys_lfs_file_open(&updateFlagFile, szUpdateFlagPath, LFS_O_CREAT | LFS_O_TRUNC | LFS_O_WRONLY);
	if(ret)
	{
		vMessage("open updatefile err");
		return;
	}
	ret=sys_lfs_file_write(&updateFlagFile, UPDATE_FLAG_NEED_UPDATE, strlen(UPDATE_FLAG_NEED_UPDATE));
	sys_lfs_file_close(&updateFlagFile);

	strcpy(szBuf, UPDATE_FLAG_NEED_UPDATE);
	vDispVarArg(7, "w UPDATE_FLAG:%02X\n", szBuf[0]);

	szBuf[0]=0xFF;
	sys_lfs_file_open(&updateFlagFile, szUpdateFlagPath, LFS_O_RDONLY);
	ret=sys_lfs_file_read(&updateFlagFile, szBuf, strlen(UPDATE_FLAG_NEED_UPDATE));
	sys_lfs_file_close(&updateFlagFile);
	dbg("read UPDATE_FLAG:%d\n", szBuf[0]);
	vDispVarArg(8, "r UPDATE_FLAG:%02X\n", szBuf[0]);
	if(_uiGetKey()==_KEY_ESC)
		return;
	vMessage("更新程序...");
	if(makeUpdateFlag()==0)
	{
		vClearLines(3);
		_vDisp(3, "正在重启POS...");
		systemReboot();
	}
    return;
}

void vDelSysUpdateFile(char force)
{
    char szAppName[50], szFlagName[50];
    char buf[10];
    lfs_file_t file;
    
    sprintf(szAppName,  "%s/%s", DIR_UPDATE, FILE_NAME_UPDATE_NEW_APP);
    sprintf(szFlagName, "%s/%s", DIR_UPDATE, FILE_NAME_UPDATE_APP_FLAG);
    
    if(force==0)
    {
        buf[0]=0;
        if(sys_lfs_file_open(&file, szFlagName, LFS_O_RDONLY)==0)
        {
            sys_lfs_file_read(&file, buf, 1);
            sys_lfs_file_close(&file);
        }
        if(buf[0]=='2' || buf[0]=='3')
            force=1;
    }
    
    if(force)
    {
        dbg("del new app file.\n");
        sys_lfs_remove(szAppName);        
        sys_lfs_remove(szFlagName);
    }
}

static uchar ucCalCrc(uchar *p, int len)
{
    int i;
    uchar crc=p[0];
    for(i=1; i<len; i++)
        crc^=p[i];
    return crc;
}
static int iRyxTmsDownloadPack(uchar type, char *pszTaskId, char *pszNewVer, ulong start, uint packSize, uchar *psData, uint *puiLen);
static int iRyxDownloadParam(uchar *pszTaskId, uchar *pszParamVer, ulong uiParamLen);
static int iRyxDownloadApp(char *pszTaskId, uchar *pszAppVer, ulong start, ulong ulAppLen);

//上送通知
int iRyxTmsUpdateEnd(char *pszParamTaskId, char *pszParamVer, char *pszAppTaskId, char *pszAppVer)
{
    uchar snd[300], rcv[300];
    uchar tmp[100];
	uint  iLen, uiRcvLen = 0;
    int ret;
    
    vClearLines(2);
    vDispCenter(2, "上送远程更新通知", 0);

    iLen=0;
	memcpy(snd+iLen, "\x00\x00", 2);	iLen+=2;		//报文总长度, 暂设0
	memcpy(snd+iLen, "22", 2);	        iLen+=2;		//命令:22通知
	if(gl_SysInfo.szPosId[0])
	{
		memcpy(snd+iLen, gl_SysInfo.szPosId, 8);	
	}else{
		memcpy(snd+iLen, "00000000", 8);
	}
	iLen+=8;
    
    tmp[0]=0;
    _uiGetSerialNo(tmp);
    dbg("POS sn=[%s]\n", tmp);
    if(tmp[0]==0)
    {
        vMessage("读取终端序列号失败");
        return 1;
    }
    memset(snd+iLen, ' ', 50);
    memcpy(snd+iLen, tmp, strlen((char*)tmp));
    iLen+=50;
    
    //软件Task
    memset(snd+iLen, ' ', 8);
    if(pszAppTaskId && pszAppTaskId[0])
        memcpy(snd+iLen, pszAppTaskId, strlen(pszAppTaskId));
    iLen+=8;
    
    //软件版本
    memset(snd+iLen, ' ', 20);
    if(pszAppVer && pszAppVer[0])
        memcpy(snd+iLen, pszAppVer, strlen(pszAppVer));
    iLen+=20;
    
    //参数Task
    memset(snd+iLen, ' ', 8);
    if(pszParamTaskId && pszParamTaskId[0])
        memcpy(snd+iLen, pszParamTaskId, strlen(pszParamTaskId));
    iLen+=8;
    
    //参数版本
    memset(snd+iLen, ' ', 20);
    if(pszParamVer && pszParamVer[0])
        memcpy(snd+iLen, pszParamVer, strlen(pszParamVer));
    iLen+=20;
    
	snd[iLen]=ucCalCrc(snd+2, iLen-2);
    iLen++;
	
    //长度赋值
    snd[0]=(iLen-2)/256;
	snd[1]=(iLen-2)%256;
    
    ret=iTmsSendRecv(snd, iLen, rcv, &uiRcvLen);
    vTmsDisConn();
    if(ret)
    {   
        vMessage("上送通知失败");
        return 1;
    }
    
    //解析
    if(memcmp(rcv+2, "00", 2))
    {
		vTmsShowHostErrMsg(rcv+2);
        return 1;
    }
    vMessage("上送通知成功");
    return 0;
}

//查询更新
int iRyxTmsUpdateQuery(void)
{
    uchar snd[300], rcv[300];
    uchar tmp[100];
	uint  iLen, uiRcvLen = 0;
    int ret;
    
    char szNewAppTask[8+1]={0};
    char szNewAppVer[20+1]={0};
    uint uiNewAppLen=0;
    
    char szNewParamTask[8+1]={0};
    char szNewParamVer[20+1]={0};
    uint uiNewParamLen=0;
    stSysInfo SysInfoBak;
    
#if 0
{
    strcpy((char*)gl_SysInfo.szTmsHost,    "119.254.93.36");
    strcpy((char*)gl_SysInfo.szTmsHostBak, "119.254.93.36");
    //strcpy((char*)gl_SysInfo.szTmsHost,    "119.254.80.46");        //测试TMS下载app
    //strcpy((char*)gl_SysInfo.szTmsHostBak, "119.254.80.46");        //测试TMS下载app
    gl_SysInfo.uiTmsHostPort=10010;
    gl_SysInfo.uiTmsHostPortBak=10010;
}
#endif

    _vCls();
    vDispCenter(1, "远程更新", 1);

#if 0
    vDispVarArg(2, "软件:%s", _pszGetAppVer(NULL));
    vDispVarArg(3, "参数:%s", gl_SysInfo.szParamVer);
    vDispMid(4, "continue?");
    if(iOK(15)!=1)
        return 1;
    vClearLines(2);
#endif

    
    if(gl_SysData.uiTransNum || gl_SysData.uiQrTransNum)
    {
	vMessage("有交易记录需结算");
        return 1;        
   }
    
    iLen=0;
	memcpy(snd+iLen, "\x00\x00", 2);	iLen+=2;		//报文总长度, 暂设0
	memcpy(snd+iLen, "20", 2);	        iLen+=2;		//命令:20查询
	if(gl_SysInfo.szPosId[0])
	{
		memcpy(snd+iLen, gl_SysInfo.szPosId, 8);	
	}else{
		memcpy(snd+iLen, "00000000", 8);
	}
	iLen+=8;
    
    tmp[0]=0;
    _uiGetSerialNo(tmp);
    dbg("POS sn=[%s]\n", tmp);
    if(tmp[0]==0)
    {
        vMessage("读取终端序列号失败");
        return 1;
    }
    memset(snd+iLen, ' ', 50);
    memcpy(snd+iLen, tmp, strlen((char*)tmp));
    iLen+=50;
    
    //软件版本
    memset(snd+iLen, ' ', 20);
   strcpy(tmp,"V20");	
    _pszGetAppVer((char*)tmp+3);
    memcpy(snd+iLen, tmp, strlen((char*)tmp));
    iLen+=20;
    
    //参数版本
    memset(snd+iLen, ' ', 20);
    if(gl_SysInfo.szParamVer[0])
    {
        memcpy(snd+iLen, gl_SysInfo.szParamVer, strlen((char*)gl_SysInfo.szParamVer));
    }else
    {
        memcpy(snd+iLen, "V0000000000", 11);
    }
    iLen+=20;
    
	snd[iLen]=ucCalCrc(snd+2, iLen-2);
    iLen++;
	
    //长度赋值
    snd[0]=(iLen-2)/256;
	snd[1]=(iLen-2)%256;
    
    ret=iTmsSendRecv(snd, iLen, rcv, &uiRcvLen);
    if(ret)
    {
        vTmsDisConn();
        return 1;
    }
    
    //解析
    if(memcmp(rcv+2, "00", 2))
    {
        vTmsDisConn();
		vTmsShowHostErrMsg(rcv+2);
        return 1;
    }
    
    if(uiRcvLen<2+2+36+36)
    {
        vTmsDisConn();
        vMessage("响应报文长度错");
        return 2;
    }

    //程序新版本
    vMemcpy0(szNewAppTask, rcv+4, 8);
    rtrim(szNewAppTask);
    vMemcpy0(szNewAppVer, rcv+4+8, 20);
    rtrim(szNewAppVer);
    vMemcpy0(tmp, rcv+4+28, 8);
    rtrim((char*)tmp);
    uiNewAppLen=ulA2L(tmp, strlen((char*)tmp));
    
    //参数新版本
    vMemcpy0(szNewParamTask, rcv+4+36, 8);
    rtrim(szNewParamTask);
    vMemcpy0(szNewParamVer, rcv+4+36+8, 20);
    rtrim(szNewParamVer);
    vMemcpy0(tmp, rcv+4+36+28, 8);
    rtrim((char*)tmp);
    uiNewParamLen=ulA2L(tmp, strlen((char*)tmp));
#if 1    
    //下载程序
    dbg("app currver:[%s], newver:[%s], task:[%.8s], len:[%u]\n", _pszGetAppVer(NULL), szNewAppVer, szNewAppTask, uiNewAppLen);
    if(szNewAppTask[0] && szNewAppVer[0] && uiNewAppLen)
    {
#ifdef BREAKPOINT_TRANS
		if(memcmp(gl_SysData.sTmsTaskId, szNewAppTask, 8) || strcmp(gl_SysData.szTmsNewVer, szNewAppVer) || uiNewAppLen!=gl_SysData.ulTmsNewLen)
		{
			//下载任务有所不同
			gl_SysData.ucTmsDownloadFlag=1;
			//gl_SysData.ulTmsTaskId=ulAppTaskId;
            memcpy(gl_SysData.sTmsTaskId, szNewAppTask, 8);
			gl_SysData.ulTmsDlStart=0;
			gl_SysData.ulTmsNewLen=uiNewAppLen;			
			strcpy(gl_SysData.szTmsNewVer, szNewAppVer);
			uiMemManaPutSysData();
		}else
		{
			//断点续传
			gl_SysData.ucTmsDownloadFlag=1;
		}
#else
		gl_SysData.ucTmsDownloadFlag=1;
		//gl_SysData.ulTmsTaskId=ulAppTaskId;
        memcpy(gl_SysData.sTmsTaskId, szNewAppTask, 8);
		gl_SysData.ulTmsDlStart=0;
		gl_SysData.ulTmsNewLen=uiNewAppLen;			
		strcpy(gl_SysData.szTmsNewVer, szNewAppVer);
		uiMemManaPutSysData();
#endif
        ulong tk=_ulGetTickTime();
       dbg("gl_SysData.ulTmsDlStart111:%d\n",gl_SysData.ulTmsDlStart);
        ret=iRyxDownloadApp(szNewAppTask, szNewAppVer, gl_SysData.ulTmsDlStart, uiNewAppLen);
        dbg("downloadapp time=[%lu]\n", _ulGetTickTime()-tk);
#if 0
        vDispVarArg(_uiGetVLines(), "dl tm=%lu", _ulGetTickTime()-tk);
        iOK(300);
#endif        
        if(ret)
        {
            szNewAppTask[0]=0;
            szNewAppVer[0]=0;
        }
    }else
    {
        szNewAppTask[0]=0;
        szNewAppVer[0]=0;
    }
#endif	
#if 1
    //下载参数
    dbg("param currver:[%s], newver:[%s], task:[%.8s], len:[%u]\n", gl_SysInfo.szParamVer, szNewParamVer, szNewParamTask, uiNewParamLen);  
    if(szNewParamTask[0] && szNewParamVer[0] && uiNewParamLen)
    {
        memcpy(&SysInfoBak, &gl_SysInfo, sizeof(gl_SysInfo));       //备份参数
        ret=iRyxDownloadParam(szNewParamTask, szNewParamVer, uiNewParamLen);
        if(ret!=0)
        {
            memcpy(&gl_SysInfo, &SysInfoBak, sizeof(gl_SysInfo));       //还原参数
            szNewParamTask[0]=0;
            szNewParamVer[0]=0;
        }
    }else
    {
        szNewParamTask[0]=0;
        szNewParamVer[0]=0;
    }
    //vTmsDisConn();
#endif    
    if(szNewParamVer[0] || szNewAppVer[0])
    {
        if(szNewParamVer[0])
        {
            //使用旧的TMS服务器参数上送通知
            //...
        }
        //上送完成通知
        #if 0
        vMessageLine(4, "完成通知(跳过)");
        #else
        iRyxTmsUpdateEnd(szNewParamTask, szNewParamVer, szNewAppTask, szNewAppVer);
        #endif        
        if(szNewParamVer[0])
        {
            //恢复TMS服务器参数为新参数
            //...
        }
    }
	vTmsDisConn();
    
    if(szNewParamVer[0])
    {
        //商户名做GBK处理
#ifdef REMOTE_GBK        
        {
            extern int iGetStringGBK(unsigned char *pszStr, int iStrLen);
            unsigned char name[50+1]; 
            
            vClearLines(2);
            strcpy((char*)name, (char*)gl_SysInfo.szMerchName);
#ifdef JTAG_DEBUG
            strcat((char*)name, "峣昇");
#endif            
            iGetStringGBK(name, -1);
            vClearLines(2);
        }
#endif
    }
    	gl_SysData.ulTmsDlStart=0;
	uiMemManaPutSysData();
#if 1	
	if(szNewAppVer[0])
	{
		//重启
		vRebootforUpdate(1);
	}
#endif	
    return 0;
}

int iRyxTmsDownloadPack(uchar type, char *pszTaskId, char *pszNewVer, ulong start, uint packSize, uchar *psData, uint *puiLen)
{
	uchar snd[300], rcv[RCV_MAX_LEN+150];
	int iRet;
	char szTermSN[20 + 1], szOldVer[20+1];
	int  iLen, iRcvLen = 0;
	uchar lrc;
	int i;

	//vDispCenter(1, "TMS下载", 1);
	//vClearLines(2);

	if(packSize>sizeof(rcv)-150)
	{
		packSize=sizeof(rcv)-150;
	}

	iLen=0;
	memcpy(snd+iLen, "\x00\x00", 2);	iLen+=2;		//报文总长度, 暂设0
	memcpy(snd+iLen, "21", 2);	        iLen+=2;		//命令:21下载
	if(gl_SysInfo.szPosId[0])
	{
		memcpy(snd+iLen, gl_SysInfo.szPosId, 8);	
	}else{
		memcpy(snd+iLen, "00000000", 8);
	}
	iLen+=8;
    
	_uiGetSerialNo((uchar*)szTermSN);
	if(type=='0')
    {
		_pszGetAppVer(szOldVer);
	}else
	{
		strcpy(szOldVer, (gl_SysInfo.szParamVer[0]!=0)?(char*)gl_SysInfo.szParamVer:"V0000000000");
	}
	sprintf(snd+iLen, "%-50.50s%-8.8s%-20.20s%-20.20s", szTermSN, pszTaskId, szOldVer, pszNewVer);
	iLen+=(50+8+20+20);
	snd[iLen++]=type;
	sprintf(snd+iLen, "%08lu%08u", start, packSize);
	iLen+=(8+8);

	lrc=0;
	for(i=2;i<iLen;i++)
		lrc^=snd[i];
	snd[iLen++]=lrc;
	snd[0]=(iLen-2)/256;
	snd[1]=(iLen-2)%256;
    
	//发送接收 
	iRet = iTmsSendRecv(snd, iLen, rcv, &iRcvLen);
	//iCommTcpDisConn();
	if (iRet || iRcvLen<4)
	{
		if (iRet == 3)
			vMessage("后台处理失败,TMS失败");
		else
        {
			//vMessage("通讯失败, TMS操作失败");
            sprintf((char*)snd, "通讯失败,TMS失败:%d", iRet);
			vMessage((char*)snd);
        }
		return iRet;
	}
	//rsp:len[2]+rsp[2]+data[n]
    if(memcmp(rcv+2, "00", 2))
    {
		vTmsShowHostErrMsg(rcv+7+2);
		return 111;		
    }

	*puiLen=ulA2L(rcv+2+2, 8);
	if(iRcvLen<2+2+8+*puiLen+1 || *puiLen>packSize)
	{
		vMessage("接收数据长度错");
		return 3;	
	}
	memcpy(psData, rcv+2+2+8, *puiLen);
	return 0;
}

#ifdef USE_HIGH_BAUDRATE
int iRyxTmsDownloadPackTA(uchar type, char *pszTaskId, char *pszNewVer, ulong start, uint packSize, uchar *psData, uint *puiLen)
{
	uchar snd[300];
	int iRet;
	char szTermSN[20 + 1], szOldVer[20+1];
	int  iLen, iRcvLen = 0;
	uchar lrc;
	int i;

	iLen=0;
	memcpy(snd+iLen, "\x00\x00", 2);	iLen+=2;		//报文总长度, 暂设0
	memcpy(snd+iLen, "21", 2);	        iLen+=2;		//命令:21下载
	if(gl_SysInfo.szPosId[0])
	{
		memcpy(snd+iLen, gl_SysInfo.szPosId, 8);	
	}else{
		memcpy(snd+iLen, "00000000", 8);
	}
	iLen+=8;
    
	_uiGetSerialNo((uchar*)szTermSN);
	if(type=='0')
    {
		_pszGetAppVer(szOldVer);
	}else
	{
		strcpy(szOldVer, (gl_SysInfo.szParamVer[0]!=0)?(char*)gl_SysInfo.szParamVer:"V0000000000");
	}
	sprintf(snd+iLen, "%-50.50s%-8.8s%-20.20s%-20.20s", szTermSN, pszTaskId, szOldVer, pszNewVer);
	iLen+=(50+8+20+20);
	snd[iLen++]=type;
	sprintf(snd+iLen, "%08lu%08u", start, packSize);
	iLen+=(8+8);

	lrc=0;
	for(i=2;i<iLen;i++)
		lrc^=snd[i];
	snd[iLen++]=lrc;
	snd[0]=(iLen-2)/256;
	snd[1]=(iLen-2)%256;
    
	//发送接收
    iRet = iTmsSendRecvTA(snd, iLen, psData, 2+2+8+packSize+1, &iRcvLen);
	if (iRet || iRcvLen<4)
	{
		if (iRet == 3)
			vMessage("后台处理失败,TMS失败");
		else
        {
			//vMessage("通讯失败, TMS操作失败");
            sprintf((char*)snd, "通讯失败,TMS失败:%d", iRet);
			vMessage((char*)snd);
        }
		return iRet;
	}
	//rsp:len[2]+rsp[2]+data[n]
    if(memcmp(psData+2, "00", 2))
    {
		vTmsShowHostErrMsg(psData+7+2);
		return 111;		
    }

	*puiLen=ulA2L(psData+2+2, 8);
	if(iRcvLen<2+2+8+*puiLen+1)
	{
		vMessage("接收数据长度错");
		return 3;	
	}
	return 0;
}
#endif

//下载参数成功改为不断开连接,便于后续上送通知
int iRyxDownloadParam(uchar *pszTaskId, uchar *pszParamVer, ulong uiParamLen)
{
	int ret;
	ulong start=0;
	uint packSize=DOWNLOAD_PACKSIZE;
	uchar sParamFile[RCV_MAX_LEN];
	uint uiRcvLen;

	_vCls();
	vDispCenter(1, "TMS下载参数", 1);
    
	dbg("down param:id=[%.8s], ver=[%.20s], len=[%u]\n", pszTaskId, pszParamVer, uiParamLen);

    if(sizeof(sParamFile)<uiParamLen)
    {
        vMessage("预分配空间不足");
        return 1;
    }
    
    vDispMid(3, "正在下载参数,请稍侯...");
	while(start<uiParamLen)
	{
		if(start+packSize>uiParamLen)
			packSize=uiParamLen-start;
		dbg("start=%lu, packsize=%d, len=%d\n", start, packSize);
		ret=iRyxTmsDownloadPack('1', (char*)pszTaskId, (char*)pszParamVer, start, packSize, sParamFile+start, &uiRcvLen);
		if(ret)
		{
            vTmsDisConn();
			vMessage("下载失败");
			return 1;
		}
		dbg("rcv len=%d\n", uiRcvLen);
		start+=uiRcvLen;
	}
    //vTmsDisConn();
	sParamFile[uiParamLen]=0;
    
	//解析参数
#ifdef 	JTAG_DEBUG
    dbgLTxt((char*)sParamFile);
    dbg("\n");
    _vDelay(50);
    dbg("ParseParam:\n");
#endif    
    
	ret=iParseParam((char*)sParamFile);
	if(ret==0)
	{
        if(gl_SysInfo.uiCommTimeout<20)         //防止超时时间太短影响交易
            gl_SysInfo.uiCommTimeout=60;
        if(gl_SysInfo.uiConnectTimeout>=60)
            gl_SysInfo.uiConnectTimeout=10;
        if(gl_SysInfo.ucCommRetryCount >=3)
	    gl_SysInfo.ucCommRetryCount=3;		
		strcpy(gl_SysInfo.szParamVer, pszParamVer);
    	uiMemManaPutSysInfo();
		vPrtParam();
        
        //gbk处理放在外部
/*
#ifdef REMOTE_GBK        
        {
            extern int iGetStringGBK(unsigned char *pszStr, int iStrLen);
            unsigned char name[50+1]; 
            
            vClearLines(2);
            strcpy((char*)name, (char*)gl_SysInfo.szMerchName);
#ifdef JTAG_DEBUG
            strcat((char*)name, "峣昇");
#endif            
            iGetStringGBK(name, -1);
            vClearLines(2);
        }
#endif      
*/
		vMessage("参数下载成功");
		return 0;
	}else
	{
		vMessage("参数解析处理失败");
		return 1;
	}
}

int iRyxDownloadApp(char *pszTaskId, uchar *pszAppVer, ulong start, ulong ulAppLen)
{	
	int ret;
	//ulong start=0;
#ifndef USE_HIGH_BAUDRATE    
	uint packSize=DOWNLOAD_PACKSIZE;         //gprs和wifi
	uchar sRcvTmp[RCV_MAX_LEN];
#else
	uint packSize=20*1000;         //gprs和wifi
	uchar sRcvTmp[packSize+100];
#endif
	ulong uiRcvLen;
	long lFileSizetmp;
	int  fileMode;
	int n=1;
    int iHighFlag=0;

	lfs_file_t newAppFile;
	char szUpdateAppNewPath[128] = {0};

	_vCls();
	vDispCenter(1, "TMS更新程序", 1);

	dbg("down app:id=[%s], ver=[%.20s], len=[%lu], start=[%lu]\n", pszTaskId, pszAppVer, ulAppLen, start);

#ifndef BREAKPOINT_TRANS
	start=0;
#endif

	fileMode=LFS_O_CREAT | LFS_O_WRONLY;
	if(start==0)
		fileMode|=LFS_O_TRUNC;
	sprintf(szUpdateAppNewPath, "%s/%s", DIR_UPDATE, FILE_NAME_UPDATE_NEW_APP);
	ret=sys_lfs_file_open(&newAppFile, szUpdateAppNewPath, fileMode);
	if(ret)
	{
		vMessage("文件打开失败,更新失败");
		return 1;
	}
dbg("start1:%ld\n",start);
	if(start)
	{
		//定位至指定位置
		lFileSizetmp = sys_lfs_file_seek(&newAppFile, start, LFS_SEEK_SET);
        if (lFileSizetmp != start)
        {
			dbg("file len err. TRUNC it.\n");
            sys_lfs_file_close(&newAppFile);
			start=0;
			fileMode|=LFS_O_TRUNC;
			sys_lfs_file_open(&newAppFile, szUpdateAppNewPath, fileMode);
        }
	}
    dbg("start2:%ld\n",start);
#ifdef USE_HIGH_BAUDRATE
		vTmsDisConn();
dbg("start3:%ld\n",start);
    if(gprs_at_setbaud(HIGH_BAUDRATE)==0)
    {dbg("start4:%ld\n",start);
			dbg("High Baud mode.\n");
			dbg("start5:%ld\n",start);
        iHighFlag=1;
		dbg("start6:%ld\n",start);
        vSetTcpTAMode(1);
		dbg("start7:%ld\n",start);
    }else
		{
			dbg("low baud mode.\n");
		}
#endif
	iTmsConn();
        dbg("start8:%ld\n",start);
	while(start<ulAppLen)
	{
		if(start+packSize>ulAppLen)
			packSize=ulAppLen-start;
        vDispVarArg(2, "当前版本:%s", _pszGetAppVer(NULL));
		vDispVarArg(3, "新 版 本:%s", pszAppVer);
		vDispVarArg(4, "下载...%lu/%lu", start, ulAppLen);
		dbg("start=%lu, packsize=%d, len=%lu\n", start, packSize, ulAppLen);
#ifdef USE_HIGH_BAUDRATE
        if(iHighFlag)
            ret=iRyxTmsDownloadPackTA('0', pszTaskId, pszAppVer, start, packSize, sRcvTmp, &uiRcvLen);
        else
#endif
            ret=iRyxTmsDownloadPack('0', pszTaskId, pszAppVer, start, packSize, sRcvTmp, &uiRcvLen);
            dbg("uiRcvLen:%ld\n",uiRcvLen);
		if(ret)
		{
			sys_lfs_file_close(&newAppFile);
#ifdef 	BREAKPOINT_TRANS
			if(ret==111)
			{
				//后台不同意,清断点续传数据
				gl_SysData.ucTmsDownloadFlag=0;
				gl_SysData.ulTmsTaskId=0;
				gl_SysData.ulTmsDlStart=0;
				uiMemManaPutSysData();
			}
#endif
            vTmsDisConn();
            
    #ifdef USE_HIGH_BAUDRATE
            if(iHighFlag)
            {
                gprs_at_setbaud(LOW_BAUDRATE);
                vSetTcpTAMode(0);  //关透传模式
                iHighFlag=0;
            }
    #endif 
            _vBuzzer();
			vMessage("下载失败");
			return 1;
		}
		dbg("rcv len=%d\n", uiRcvLen);
        
#ifdef 	BREAKPOINT_TRANS		
		//将收取的数据写到文件中
        #ifdef USE_HIGH_BAUDRATE
        if(iHighFlag)
            sys_lfs_file_write(&newAppFile, sRcvTmp+2+2+8, uiRcvLen);
        else
        #endif
            sys_lfs_file_write(&newAppFile, sRcvTmp, uiRcvLen);

		start+=uiRcvLen;
		if(n%10==0 || start>=ulAppLen)		//10包存一次
		{
			sys_lfs_file_close(&newAppFile);
			sys_lfs_file_open(&newAppFile, szUpdateAppNewPath, LFS_O_WRONLY);
			lFileSizetmp=sys_lfs_file_seek(&newAppFile, 0, LFS_SEEK_END);
			dbg("$$$$$ rcv totol len=%ld\n", lFileSizetmp);

			//更新文件信息支持断点续传
			gl_SysData.ucTmsDownloadFlag=1;			// 下载标志,是否需要更新下载
			gl_SysData.ulTmsTaskId=atol(pszTaskId);
			gl_SysData.ulTmsDlStart=start;			// app下载起始位(断点续传)
			dbg("start:%ld   gl_SysData.ulTmsDlStart:%d\n",start,gl_SysData.ulTmsDlStart);
			gl_SysData.ulTmsNewLen=ulAppLen;		// 待下载app长度	
			strcpy(gl_SysData.szTmsNewVer, pszAppVer);		// 待下载的app新版本号
			uiMemManaPutSysData();
		}
#else
		sys_lfs_file_write(&newAppFile, sRcvTmp, uiRcvLen);
		start+=uiRcvLen;		
#endif
		n++;
	}
    vTmsDisConn();

#ifdef USE_HIGH_BAUDRATE
    if(iHighFlag)
    {
        gprs_at_setbaud(LOW_BAUDRATE);
        vSetTcpTAMode(0);  //关透传模式
        iHighFlag=0;
    }
#endif    
    
	lFileSizetmp = sys_lfs_file_seek(&newAppFile, 0, LFS_SEEK_END);
	sys_lfs_file_close(&newAppFile);
	if(lFileSizetmp!=ulAppLen)
	{
#ifdef 	BREAKPOINT_TRANS
		//清断点续传数据
		gl_SysData.ucTmsDownloadFlag=0;
		gl_SysData.ulTmsTaskId=0;
		gl_SysData.ulTmsDlStart=0;
		uiMemManaPutSysData();
#endif
		dbg("recv file len err.%ld != %lu", lFileSizetmp, ulAppLen);
        _vBuzzer();
		vMessageEx("下载长度错,更新失败", 10*100);		
		return 1;
	}
	return 0;
}


