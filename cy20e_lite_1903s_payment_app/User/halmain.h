/********************************************************************************

 **********************Copyright (C), 2021, KingShiDun Inc.**********************

 ********************************************************************************
 * @file       : halmain.h
 * @brief      : halmain.c header file
 * @author     : Jay
 * @version    : 1.0
 * @date       : 2021-06-02
 * 
 * 
 * @note History:
 * @note       : Jay 2021-06-02
 * @note       : 
 *   Modification: Created file

********************************************************************************/

#ifndef __HALMAIN_H__
#define __HALMAIN_H__
#include <FreeRTOS.h>


#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */
typedef struct {
    TaskFunction_t pxTaskCode;  //main task enter
    uint16_t usStackDepth;      //main task max stack depth,unit is int(4bytes)
    Boolean enablejtag;         //enable jtag port,invalid for secure version.
} hal_str;

char *GetHALVersion(void);
Boolean checkifdebug(void);
    int HALInit(hal_str *halstr);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __VERSION_H__ */
