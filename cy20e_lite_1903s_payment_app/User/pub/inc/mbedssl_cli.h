#ifndef MBENDSSL_CLI_H_
#define MBENDSSL_CLI_H_

/**
 * \brief          		init ssl
 *
 * \param psCa     		ca
 * \param psCliCert     client cert
 * \param psCliKey      client privatekey
 *
 * \return         0 is success.
 *
 */
int iSSL_Init(char *psCa, char *psCliCert, char *psCliKey);

/**
 * \brief          		use ssl connect to server
 *
 * \param pszSvrName    server ip
 * \param iSvrPort     	server port
 * \param iTimeOut      connect timeout, current no use
 *
 * \return         0 is success.
 *
 */
int iSSL_Connect(char *pszSvrName, int iSvrPort, int iTimeOut);

/**
 * \brief          		 ssl send data to server
 *
 * \param buf    		send data
 * \param len	     	send data length
 *
 * \return         		The length of data send successfully
 *
 */
int iSSL_Send(unsigned char *buf, int len);

/**
 * \brief          		ssl recv data from server
 *
 * \param buf    		recv data buffer
 * \param len	     	The length of data expected to be received or size of the data buf
 *
 * \return         		The length of data received successfully
 *
 */
int iSSL_Recv(unsigned char *buf, int len, int iTimeOutMs);

int iSSL_DisConnect(void);
int iSSL_Free(void);

#endif
