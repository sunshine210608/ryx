#ifndef _FUNC_H
#define _FUNC_H

int iGetAmount(char *pszTitle, char *pszPrompt, char *pszPromptMsg, char *pszAmount);
int iMainOperCheck(char *pwd);
int iInputTTC(char *prompt, char *out, unsigned long *pulTTC);

//二选一
//ret: -1:取消 -2:超时  1:选1  2:选2
int iSelectOne(char *prompt, char *pszSel1, char *pszSel2, char *promptBottom, int curr, uint uiTimeOut, char bCancel);

//压缩卡号为10字节,不足位后补F
void vPackPan(uchar *pszPan, uchar *psOut);

void vUnpackPan(uchar *psIn, uchar *pszPan);

void vTakeOutCard(void);


//pszPan为null时, pszEncPin输出为明文pin，否则输出密文pin
// -1：取消 -3：超时 >=0:密码长度(0为直接按确认键)
int iInputPinAndEnc(uchar ucPass, unsigned long ulAmount, char *pszPrompt, char *pszPan, char *pszEncPin);

int iBillSign(int flag, int iRecIdx, void *pRec);

int mViewTermInfo(int flag);

void vGetBankCardTransName(uint uiTransType, uchar ucVoidFlag, char *pszTrName);

void vDispCancelAndConfirm(char cancel, char confirm);

void vIncTTC(void);
void vFillFieldLen(uchar *psField);

void vQrPayModeName(char ucSimpleFlag, char *pszPayMode, char *pszName);

void vShowCommErrMsg(int code);

int iInputIP(char *pszPrompt, char *pszIP);

int iParseUrlAndSetIP(char *pszPrompt, int iParselFlag, char *pszUrl, char *pszIP);


int iGetAutoTestFlag(void);
void vSetAutoTestFlag(int flag);

//根据机构编号取银行名称
int iGetBankName(char *pszBankCode, char *pszBankName);
#endif
