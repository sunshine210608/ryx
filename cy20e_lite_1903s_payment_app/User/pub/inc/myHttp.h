#ifndef MY_HTTP_H_
#define MY_HTTP_H_

int iParseHttpUrl(char *pszUrlAddr, char *pszHostName, char *pszSubUrl, int *piPort);

//pszHostName: 服务器地址和端口,格式: xxx.xxx.xxx.xxx:nnnn	当端口为80时可省略端口部分
int iHttpSend(char type, uchar *pszHostName, uchar *pszSubUrl, uchar *pszBody);

int iHttpRecv(char bigData, uchar *pszBody, uint uiBodySize, uint uiTimeOut, char *pszContentRange);
int iHttpSendRecv(char type, char *pszUrl, char *pszBakHost, int retry, char *pszBody, int iBodyLen, char *pszRsp, uint uiRspSize, uint uiTimeOut);

int iHttpDownloadFile(uchar *pszUrl, uchar *pszFile, uint *puiFileSize, uint *puiDownloadLen, 
            uchar ucSaveFlag, uchar *psFileHead, uint uiFleHeadLen, void (*saveFunc)(ulong, uchar *, uint));
#endif
