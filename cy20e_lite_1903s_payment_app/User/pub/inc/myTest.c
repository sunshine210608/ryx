#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "VposFace.h"
#include "Pub.h"
#include "debug.h"
#include "myBase64.h"
#include "myHttp.h"
#include "cJSON.h"

int iBase64(void)
{
    int ret;
    int len, len2;
    unsigned char buffer[2048];
    unsigned char test_src1[] = "ebS9Ut/A2FzqYL7BWQtUxSjxg8HCxRpwmzJn6afcoaJ8dHR3h4kkVad2EJxzmb2F3RA48CKPsxqEIsHcdjM966WFjK9wcDegdl6NSTq1ZTolOVZfh2sEz8hwqH08AMmSSWIEJeXhgeJvIJFt1qqHUpnl+7sZV8H/ldcKonU+6Mhr0wuY/wVBNwOaej6tiKFhvRx7nvIE7JdDWBMYPoXaUiI+VgByITUAYrR741tDDGK6xtIO7KJi57jKdm1fKi1C7cYG7OCHH/40ROhWxn/6WeH5Uy5VUAZhuHiMxto6ZzpSNuJEwIVDE0ttlUlE7ZuCMEQ5u06kP8/WxDIxM4M8HXNLvy/PxBCeQOchJKCF76DXXqpYrKFH4+B6l9Zup1l6FhnIu6RkSFy+jKW2bXAZQgTXS15/z3cE7fpKHEMWvulPgwuirk9bQzFaeLznbNjFGmWJrG/6IYitOW1uoCSAAnaczqcCc8t7QTu4CYJMl0SVeaTgSVhU3r/unbU0re6dKNy5JceQFp8ZQ2eaFRcRUpM6BGAeJQYMDUP9uBF4Ng3kNjlsbOpHtQwsWe727PIPWp0sh2G7T51C6bN3NAg3sAn1/1x0VC2KJSh17aSL7bJjS3/NwQNoApo1ET6FqO48CFLUnh0l0ZaJeWtMCWrgp30Hi7O32GLa3kcxTXTGAuc=";
    unsigned char test_src2[] = "ANJgAAAICAgwIDgAkALwAImRAAAAAFMQQQEEFQAAMDA1NDQwNTMwMDgyMjgyNzA3MDMyMDAwNdXF\r\n0rTJxsPAu+nH7Lmry74gICAgICAgICAgICAgICAgICAgICAgICAkqv2zyTlYd\/dm6m5n+iVpNqQB\r\nyMte7fHeAFExNjIyODQ4MzYzODEzODY1OTg3OSAgICAgICAgICAgxdPQy7KpICAgICAgICAgICAg\r\nICAAKTEwMDEwMDAxMDEwMDIwMDEyMDAwMDAwMTAwMDAwQzRFMjVEMTg=\r\n";
    unsigned char buffer2[1024];
    int logsize = 200;

    //dbg("  Base64 encoding test: ");

    _vDisp(2, "test base64?");
    if (_uiGetKey() != _KEY_ENTER)
        return 0;

    dbg("Base64 decoding test1: \n");
    len = 0;
    ret = iBase64_decode(buffer, sizeof(buffer), (uint *)&len, test_src1, strlen((char *)test_src1));
    if (ret == 0)
    {
        dbg("decode1 ok: inlen=[%d], outlen=[%d]\n", strlen((char *)test_src1), len);
        for (ret = 0; ret * logsize < len; ret++)
        {
            if ((ret + 1) * logsize < len)
            {
                vOneTwo0(buffer + ret * logsize, logsize, buffer2);
            }
            else
            {
                vOneTwo0(buffer + ret * logsize, len - ret * logsize, buffer2);
            }
            dbg("%s\n", (char *)buffer2);
            _vDelay(50);
        }
    }
    else
    {
        dbg("decode1 err:ret=[%d], inlen=[%d], outlen=[%d]\n", ret, strlen((char *)test_src1), len);
    }

    dbg("Base64 decoding test2: \n");
    len = 0;
    ret = iBase64_decode(buffer, sizeof(buffer), (uint *)&len, test_src2, strlen((char *)test_src2));
    dbg("iBase64_decode ret=%d\n", ret);
    if (ret == 0)
    {
        dbg("decode2 ok: inlen=[%d], outlen=[%d]\n", strlen((char *)test_src2), len);
        for (ret = 0; ret * logsize < len; ret++)
        {
            if ((ret + 1) * logsize < len)
            {
                vOneTwo0(buffer + ret * logsize, logsize, buffer2);
            }
            else
            {
                vOneTwo0(buffer + ret * logsize, len - ret * logsize, buffer2);
            }
            dbg("%s\n", (char *)buffer2);
            _vDelay(50);
        }
        memcpy(buffer2, buffer, len); //for encode
        len2 = len;
    }
    else
    {
        dbg("decode2 err:ret=[%d], inlen=[%d], outlen=[%d]\n", ret, strlen((char *)test_src2), len);
    }

    dbg("Base64 encoding test1: \n");
    len = 0;
    ret = iBase64_encode(buffer, sizeof(buffer), (uint *)&len, buffer2, len2);
    //buffer[len]=0;
    dbg("iBase64_encode ret=%d\n", ret);
    if (ret == 0)
    {
        dbg("encode1 ok: inlen=[%d], outlen=[%d]\n", len2, len);
        for (ret = 0; ret * logsize < len; ret++)
        {
            dbg("%.200s\n", (char *)buffer + ret * logsize);
            _vDelay(50);
        }
    }
    else
    {
        dbg("encode1 err:ret=[%d], inlen=[%d], outlen=[%d]\n", ret, len2, len);
    }

    dbg("\nBase64 decoding test end.\n");
    return (0);
}

int iJsonTest(void)
{
    cJSON *root;
    cJSON *arry;
    cJSON *list1, *list2;

    _vDisp(2, "test json?");
    if (_uiGetKey() != _KEY_ENTER)
        return 0;

    root = cJSON_CreateObject();                                                                                                                                                                                                                                                                                                                                                            //创建根数据对象
    cJSON_AddStringToObject(root, "endata", "im8KYG5MP1rmYUMFDMv6Iw0EX/+x007wmsaRZNNYNjiWCDZ1wVTkZz8R+yAWnBmNrCC2EDJaXRsoRKTIQpWU7qB+HH9GQNk9HityI2oqNDjJRh1emluQo24KKxuzKWm5OgbuwA/iPeiJWrGKojmMCgjNxHwGWyUoAn7UCPGGdWyQ9/55Al4z1JYQB9u1v1d8NgCC5j8svmEYXNw7dtQWrCIVi1wqRvOv+GhojT3f60TCx9mqdEXtZqmO7Htm61AcUbcBv+Lp36vcWLJpwEm1dxrZHm5aEiKNiZyz+6a4RPv/63l1PgOksv9L6FnIqHH0JuzNQikIjgExpQ0lR6Qy2g=="); //加入键值，加字符串
    cJSON_AddStringToObject(root, "signdata", "oug/Vq69SVBXbPL2WBHJG7RwFKJ7k7fxyK+AmLCZ1uZI2ovolVRzWeGI+lo+lk13UkIIsYuTPowdviKYqQYXxvW5J41YuDaQJcxSGb6j5RGB6YfRFOf+kQTQ/kK9otCqdDtT+4QtjDVME9+bZM6ifqiHg50U+N9sPhPL7vzr0Mg=");
    cJSON_AddStringToObject(root, "cname", "822827070320005-54405300");
    cJSON_AddStringToObject(root, "iccid", "12345678901234567890");

#if 1
    //数组
    arry = cJSON_CreateArray();

    list1 = cJSON_CreateObject();
    cJSON_AddStringToObject(list1, "name", "zhangshan");
    cJSON_AddNumberToObject(list1, "year", 5); //加整数
    cJSON_AddItemToArray(arry, list1);

    list2 = cJSON_CreateObject();
    cJSON_AddStringToObject(list2, "name", "lisi");
    cJSON_AddNumberToObject(list2, "year", 8); //加整数    
    cJSON_AddItemToArray(arry, list2);

    cJSON_AddItemToObject(root, "lists", arry);
#endif

    char *out = cJSON_PrintUnformatted(root); //将json形式打印成正常字符串形式
    dbg("json=[%s]\n", out);
    _vDelay(50);

    //cJSON_Print得到的指针需单独用free释放
    cJSON_free(out);
    out = NULL;

    // 释放内存
    cJSON_Delete(root);
    root = NULL;

    //解析json
    {
        char buf[] = "{\"code\":\"0000\",\"data\":\"AR9gAAAICAgwIDgAgALxAImRAAAAAFQQQQcEFQAwMDU0NDA1Mjk5ODIyODI3MDcwMzIwMDA11cXS\r\ntMnGw8C76cfsuavLviAgICAgICAgICAgICAgICAgICAgICAgICQgYRQAEs3KQUD6mTJHhux6KZeF\r\nmv2MXS0AdjR8zqLQxceusPx8V0VDSEFUfNanuLaxpseusPx8QUxJUEFZfNL4warHrrD8fFVOSU9O\r\nUEFZfNL4waq2\/s6swut8VVFSQ09ERVBBWXwAUTE2MjI4NDgzNjM4MTM4NjU5ODc5ICAgICAgICAg\r\nICDF09DLsqkgICAgICAgICAgICAgIAApMTAwMTAwMDEwMTAwMjAwMTIwMDAwMDAxMDAwMDAzRTJB\r\nRjY1OQ==\r\n\",\"message\":\"\",\"signData\":\"TlErplRwRoSyL8Iyed5ZEzkbmUdsobxkOY4+NQ8cKUeF5Evr006RhJbi+sGWkgNVWqo\/AV9wO1TK\r\nSqpyAVDR0Ixkn6BD2oVFnqxZEBWwWarRN9ZTayoFrmPscuxz10H5oJ7tXY\/KCgoqAI8T3c9EDToK\r\nPCDWFBWr3MxXXwjGgGw=\r\n\"}";
        cJSON *json;
        cJSON *json_code, *json_data, *json_msg, *json_signData;
    
        dbg("test JSON_Parse\n");

        json = cJSON_Parse(buf); //解析成json形式
        json_code = cJSON_GetObjectItem( json , "code" );  //获取键值内容
        json_data = cJSON_GetObjectItem( json , "data" );
        json_msg = cJSON_GetObjectItem( json , "message" );
        json_signData = cJSON_GetObjectItem( json , "signData" );

        dbg("code=[%s]\n", json_code->valuestring);
        dbg("message=[%s]\n", json_msg->valuestring);        
        dbg("signData=[%s]\n", json_signData->valuestring);
        _vDelay(50);
        dbg("data=[%s]\n", json_data->valuestring);
        _vDelay(50);

        // 释放内存
        cJSON_Delete(json);
        json=NULL;
    }

    return 0;
}

