/**************************************
Module name     : VPOS公用程序模块
File name       : PUB.H
Function        : 提供在POS设计中在所有应用中最常用的函数支持
Design          : 该模块只提供最常用,最基本的通用函数.
                  不常用的,有特点的函数不要添加到这个模块.
Based on        : 本模块需要VPOS2.0以上支持
**************************************/
#ifndef _PUB_H
#define _PUB_H

#define WAIT_IC 0x01
#define WAIT_NFC 0x02
#define WAIT_MAG 0x04
#define WAIT_MAN 0x08	 // 手工输卡号
#define WAIT_NOESC 0x10
#define WAIT_NOTURN 0x20

#define INPUT_NORMAL 0x0000
#define INPUT_PIN 0x0001
#define INPUT_BY_LEN 0x0002
#define INPUT_RIGHT 0x0004
#define INPUT_INITIAL 0x0008
#define INPUT_MONEY 0x0010
#define INPUT_ALPHA 0x0020
#define INPUT_ALL_CHAR 0x0040
#define INPUT_NO_CANCEL 0x0080
#define INPUT_CC 0x0200

#define INPUT_DOT_MONEY 0x0100
#define INPUT_LEFT 0x0200
#define INPUT_NULL 0x0400
#define INPUT_DOT_SPACE 0x0800
#define INPUT_CENTER_SPACE 0x1000
#define INPUT_TWO_LINE 0x2000
#define INPUT_FACTORY  0x4000
typedef struct
{
    uchar ucFirstCode; // 输入法的第一个编码
    ulong ulTimer;     // 计时器
    uchar *pszCC;      // 输入法返回的输入字符，可能是单字节西文字符或双字节汉字
} stCCInput;           // 输入法参数

// Function  : 初始化输入函数
// Design    : 用于初始化iInput()函数中用到的几个静态全局变量
// Reference : Var:sg_ucInputFill,sg_pszInputFormat,sg_piLoop,sg_ppszAlphaTable
// Attention : 如需调用iInput()函数，必须先做此初始化
void vInitInput(void);

// Function  : 设置在iInput()函数中调用的嵌入函数
// Design    : iInput()在执行过程中会不停的调用该函数，并将按键代码传给该函数
//             按键代码为0表示没有键按下，该键码可以在外部改变
//             该函数返回非0将导致iInput()函数退出并返回该值
//             使用完后要撤销外部函数 -> vSetLoop(0)
// Reference : Var : sg_piLoop
// In        : pFunc : 嵌入函数指针, 为0表示不使用嵌入函数
void vSetLoop(int (*pFunc)(uchar *));

// Function  : 设置输入法函数
// Reference : Var : sg_piCCInput
// In        : pFunc : 输入法函数指针
void vSetCCInput(int (*pFunc)(stCCInput *pCCInput));

// Function  : 设置iInput()函数输入区的填充字符
// Reference : Var : sg_ucInputFill
// In        : ucInputFill : 新的输入区的填充字符, 0恢复缺省值
void vSetInputFill(uchar ucInputFill);

// Function  : 设置在iInput()函数中格式化输入的格式串
// Design    : 当sg_pszInputFormat有效时(非空),启用该格式化字符串
//             即非格式化字符将被显示到显示器,格式化字符位置留给输入用
//             使用完后要撤销格式串 -> vSetInputFormat(0)
// Reference : Var : sg_pszInputFormat
// In        : pszFormat : 格式化串
void vSetInputFormat(uchar *pszFormat);

// Function  : 设置在iInput()函数中字母输入模式时的数字字母对应表
// Design    : ppszAlphaTable[0] -- ppszAlphaTable[9] 分别表示 '0' -- '9'
//             键对应的字母序列, 字母序列以 '\0' 结尾
// Reference : Var : sg_ppszAlphaTable
// In        : ppszAlphaTable : 数字字母对应表
void vSetAlphaTable(uchar **ppszAlphaTable);

// 键盘输入函数
// Function  : 键盘输入函数
// Design    : 光标用两元素的字符数组，交替显示其中内容的方法实现
//             字母输入：每一数字键对应一字符串(\0结尾)，如果连续敲入同一数字键
//                       则循环依次选取该字符串内各字符。也可以输入非字母符号
//             如果设置了嵌入函数，会不断地调用它。按键也会传入嵌入函数。嵌入
//             函数可改变按键值并控制iInput()的终止与否
// Reference : Var:sg_ucInputFill,sg_pszInputFormat,sg_piLoop,sg_ppszAlphaTable
///////////////////////////////////////////////////////////////////////////////
// In  : uiFlag    : 属性，可为下述各属性的组合
//                   INPUT_NORMAL    : 输入数字、需回车、左靠齐、不设初始值
//                   INPUT_PIN       : 输入密码，不要与INPUT_ALPHA属性同时使用
//                   INPUT_BY_LEN    : 达到最大长度后自动返回，不需要回车
//                   INPUT_RIGHT     : 右靠齐
//                   INPUT_INITIAL   : 使用初始值
//                   INPUT_MONEY     : 输入金额，其它属性全部失效, 需输入窗口
//                                     大小不能小于4字节，最大与最小长度无效
//                   INPUT_DOT_MONEY : 允许输入小数点的金额输入方式
//                   INPUT_ALPHA     : 字母输入属性，必须先设置字母数字对应表
//                                     vSetAlphaTable(字母数字对应表)
//                                     不能与INPUT_ALL_CHAR同时使用
//                   INPUT_ALL_CHAR  : 接受任何字符
//                   INPUT_NO_CANCEL : 不允许取消
//                   INPUT_CC        : 汉字输入属性
//       ucRow     : 输入区域行位置
//       ucCol     : 输入区域列位置
//       pszResult : INPUT_INITIAL方式时传入初始值
//       ucAreaLen : 输入窗口大小
//       ucMinLen  : 需输入的最小长度
//       ucMaxLen  : 允许输入的最大长度
//       uiTimeout : 超时时间(单位:秒)
// Out : pszResult : 输入内容, 输入金额时返回分为单位、不含小数点的串
// Ret : >=0       : 输入内容的长度
//       -1        : 用户取消
//       -2        : 参数错误
//       -3        : 超时
// Attention : 接收输入区域不能超出屏幕范围，输入最大长度不能超过
//             MAX_INPUT_LENGTH定义的长度
int iInput(uint uiFlag, uchar ucRow, uchar ucCol, uchar *pszResult,
           uchar ucAreaLen, uchar ucMinLen, uchar ucMaxLen, uint uiTimeout);

// Function  : 等待用户确认或取消
// Ret       : true  : 确认
//             false : 取消
uchar bOK(void);

// Function  : 带超时等待用户确认或取消
// Ret       : 1:确认 0:取消 -1:超时
int iOK(ushort timeout);

// Function  : 等待用户按键
// Ret       : 按键值
//
uint bPressKey(void);

// Function : 在Pos的最后一行显示消息
// In       : pszMessage : 要显示的字符串
void vShow(char *pszMessage);
void vShowVarArg(char *pszFormat, ...);

// Function : 在Pos的最后一行显示消息, 并且至少等待ulWaitTime/100秒
// In   : pszMessage : 要显示的字符串
//        lWaitTime  : 等待的时间, 0.01秒为单位, -1表示等待缺省时间
// Note : 本函数会确保上一次调用本函数显示的信息保持时间
void vShowWait(char *pszMessage, long lWaitTime);
void vShowWaitVarArg(char *pszFormat, ...);

//显示并立即退出,二次调用时等待时间为0可检查超时时间并按键键退出
//lWaitTime >0:等待时间,单位0.01秒 -1:使用缺省值500(5秒) 0:检查等待是否超时(用于二次调用)
//keyflag: lWaitTime=0时本参数有效, 0-不处理按键 0x01-取消键退出 0x02-确认键退出 0x04-任意键退出
void vShowWaitEx(char *pszMessage, long lWaitTime, int keyFlag);

// Function : 在Pos的最后一行显示消息,等待2.5秒钟或按键返回
// In       : pszMessage : 要显示的字符串
void vMessage(char *pszMessage);
void vMessageMul(char *pszMessage);
void vMessageVarArg(char *pszFormat, ...);

void vMessageLine(int line, char *pszMessage);

//指定时间等待显示
//lWaitTime: <0默认超时时间5秒 =0无超时,按键才退 >0指定时间退出(单位0.01秒)
void vMessageEx(char *pszMessage, long lWaitTime);

// Function : 等待插卡
// In       : uiSlot : 虚拟卡座号
// Ret      : 0      : 有卡片插入
//            1      : 用户取消
uint uiWaitCard(uint uiSlot);

// Function : 在某行居中显示信息
// In       : uiLine : 行号(1-n)
//            pszMsg : 内容
//            ucAttr : 属性
void vDispCenter(uint uiLine, char *pszMsg, uchar ucAttr);
void vDispMid(uint uiLine, char *pszMsg);

//带上下箭头的居中显示
void vDispCenterEx(uint uiLine, char *pszMsg, uchar ucAttr, int min, int curr, int max);

// 变长参数显示内容, 如果一行显示不下, 尾部表示为"..."
// In  : uiLine    : 显示行
//       pszFormat : 显示格式串
void vDispVarArg(uint uiLine, char *pszFormat, ...);
void vDispMidVarArg(uint uiLine, char *pszFormat, ...);

// 变长参数显示内容到辅助显示器
// In  : uiLine    : 显示行
//       pszFormat : 显示格式串
//void vDispVarArg2(uint uiLine, char *pszFormat, ...);

// 变长参数打印内容
// In  : pszFormat : 显示格式串
// Ret : 同_uiPrint()
uint uiPrintVarArg(char *pszFormat, ...);

// 清除部分显示行
// In  : uiLine : 该行以后会被清除
void vClearLines(uint uiLine);

// 14. 基础函数
// 14.1 将两个串按字节异或
// In       : psVect1  : 目标串
//            psVect2  : 源串
//            iLength  : 字节数
void vXor(uchar *psVect1, const uchar *psVect2, int iLength);

// 14.2 将两个串按字节或
// In       : psVect1  : 目标串
//            psVect2  : 源串
//            iLength  : 字节数
void vOr(uchar *psVect1, const uchar *psVect2, int iLength);

// 14.3 将两个串按字节与
// In       : psVect1  : 目标串
//            psVect2  : 源串
//            iLength  : 字节数
void vAnd(uchar *psVect1, const uchar *psVect2, int iLength);

// 14.4 将二进制源串分解成双倍长度可读的16进制串
// In       : psIn     : 源串
//            iLength  : 源串长度
// Out      : psOut    : 目标串
void vOneTwo(const uchar *psIn, int iLength, uchar *psOut);

// 14.5 将二进制源串分解成双倍长度可读的16进制串, 并在末尾添'\0'
// In       : psIn     : 源串
//            iLength  : 源串长度
// Out      : pszOut   : 目标串
void vOneTwo0(const uchar *psIn, int iLength, uchar *pszOut);

// 14.6 将可读的16进制表示串压缩成其一半长度的二进制串
// In       : psIn     : 源串
//            iLength  : 源串长度
// Out      : psOut    : 目标串
// Attention: 源串必须为合法的十六进制表示，大小写均可
//            长度如果为奇数，函数会靠近到比它大一位的偶数
void vTwoOne(const uchar *psIn, int iLength, uchar *psOut);

// 14.7 将二进制串转变成长整数
// In       : psBinString : 二进制串，高位在前
//            iLength     : 二进制串长度，可为{1,2,3,4}之一
// Ret      : 转变后的长整数
ulong ulStrToLong(const uchar *psBinString, int iLength);

// 14.8 将长整数转变成二进制串
// In       : ulLongNumber : 要转变的长整数
//            iLength      : 转变之后二进制串长度
// Out      : psBinString  : 转变后的二进制串，高位在前
void vLongToStr(ulong ulLongNumber, int iLength, uchar *psBinString);

// 14.9 将十六进制可读串转变成长整数
// In       : psHexString : 十六进制串
//            iLength     : 十六进制串长度，0-8
// Ret      : 转变后的长整数
ulong ulHexToLong(const uchar *psHexString, int iLength);
unsigned long long ullHexToLongLong(const uchar *psHexString, int iLength);

// 14.10 将长整数转变成十六进制可读串
// In       : ulLongNumber : 要转变的长整数
//            iLength      : 转变之后十六进制串长度
// Out      : psHexString  : 转变后的十六进制串
void vLongToHex(ulong ulLongNumber, int iLength, uchar *psHexString);
void vLongToHex0(ulong ulLongNumber, int iLength, uchar *psHexString);

// 14.11 将数字串转变成长整数
// In       : psString : 数字串，必须为合法的数字，不需要'\0'结尾
//            iLength  : 数字串长度
// Ret      : 转变后的长整数
ulong ulA2L(const uchar *psString, int iLength);

// 14.12 内存复制, 类似memcpy()，在目标串末尾添'\0'
// In       : psSource  : 源地址
//            iLength   : 要拷贝的串长度
// Out      : pszTarget : 目标地址
void vMemcpy0(uchar *pszTarget, const uchar *psSource, int iLength);

// 14.13 将压缩BCD码串转变成长整数
// In       : psBcdString : BCD码串，必须为合法的BCD码，不需要'\0'结尾
//            iLength     : BCD码串长度，必须小于等于12
// Ret      : 转变后的长整数
ulong ulBcd2L(const uchar *psBcdString, int iLength);

// 14.14 将长整数转变成压缩BCD码串
// In       : ulLongNumber : 要转变的长整数
//            iLength      : 转变之后BCD码串长度, 必须小于等于12
// Out      : psBcdString  : 转变后的BCD码串，高位在前
void vL2Bcd(ulong ulLongNumber, int iLength, uchar *psBcdString);

// 14.15 将二进制源串分解成双倍长度可读的3X格式串
// In       : psIn     : 源串
//            iLength  : 源串长度
// Out      : psOut    : 目标串
void vOneTwoX(const uchar *psIn, int iLength, uchar *psOut);

// 14.16 将二进制源串分解成双倍长度可读的3X格式串, 并在末尾添'\0'
// In       : psIn     : 源串
//            iLength  : 源串长度
// Out      : pszOut   : 目标串
void vOneTwoX0(const uchar *psIn, int iLength, uchar *pszOut);

// 14.17 将3X格式串压缩一半长度的二进制串
// In       : psIn     : 源串
//            iLength  : 源串长度
// Out      : psOut    : 目标串
void vTwoOneX(const uchar *psIn, int iLength, uchar *psOut);

// 14.18 检测存储内容
// In       : pMem     : 内存指针
//            ucChar   : 检测字符
//            iLength  : 检测长度
// ret      : 0        : 内存区域全部是字符ucChar
//            1        : 内存区域不全是字符ucChar
int iMemTest(void *pMem, uchar ucChar, int iLength);

// 删除字符串右侧空格和'\t'
char *rtrim(char *s);

//去除字符串尾部空格和F,字符串须'\0'结尾
char *rtrimF(char *s);

// 带超时的读取按键
// in  : iTimeout : 超时时间(秒), 0表示没有超时
// ret : >=0      : 按键
//       -3       : 超时
int iGetKeyWithTimeout(uint uiTimeout);

// 带超时的读取特定按键
// in  : iTimeout : 超时时间(秒), 0表示没有超时
// ret : =0      : 按键
//       -3       : 超时
int iGetSpecialKeyWithTimeout(uint uiTimeout, uint uiSpeKey);

/*
ucMode:   0x01-接触式IC卡   WAIT_IC
          0x02-非接IC卡     WAIT_NFC
          0x04-磁条卡       WAIT_MAG
          0x10-不允许取消   WAIT_NOESC
          0x20-不允许升降级,仅磁条和IC都允许时有效 WAIT_NOTURN
uiTimeOut:  超时时间,秒
return:  1:接触IC卡 2:非接IC卡 3:磁条卡 4:IC失败后的磁条卡(fallback)
        -1:取消    -2:超时    -3:失败
*/
int iWaitCard(uchar ucMode, uchar *pszTitle, uchar *pszPrompt, uint uiTimeOut);

//打印长日志,不超过1024*4.(原dbg无法打印超过960的日志)
void dbgLTxt(char *pszFormat, ...);
//打印长日志,非var方式,便于urlEncode后带有%号的报文打印
void dbgLTxt2(char *pszHead, char *pszMsg);
void dbgHex(uchar *head, uchar *in, uint len);

//按指定长度切分带gbk中文的字符串, 返回切分长度(len或len-1)
int iSplitGBStr(unsigned char *in, unsigned int len);

//拆分指定字符分隔的字符串
//out为out[m][n]二维数组
//返回得到的字符串个数
int iSplitSepStr(char *in, unsigned char sep, char *out, int m, int n);

//void vSetFailTransLed(void);
#endif // # ifndef _PUB_H
