/**************************************
File name     : EmvProc.h
Function      : Pboc3.0借贷记/EMV2004客户接口
Author        : Yu Jun
First edition : Mar 11th, 2014
Modified      : Mar 28th, 2014
				去除iHxEmvSetAmount(uchar *pszAmount, uchar *pszAmountOther)函数中的pszAmountOther参数
				Nov 25th, 2014
					全面支持非接及非接扩展
					增加结构stHxTermAid参数
						ucTermCtlsCapabilityFlag;			// sTermCtlsCapability存在标志, 0:无 1:存在
						sTermCtlsCapability[4];				// TDFxx, 终端非接能力, T9F66 Mark位, 最终的T9F66值要与该参数做与操作
						szTermCtlsAmountLimit[12+1];		// TDFxx, 终端非接交易限额, 空表示无
						szTermCtlsOfflineAmountLimit[12+1];	// TDFxx, 终端非接脱机交易限额, 空表示无
						szTermCtlsCvmLimit[12+1];			// TDFxx, 终端非接CVM限额, 空表示无
					增加函数:
				May 8th, 2015
					增加函数: iHxEmvSetIccSlot(int iSlotNo)
				May 11th, 2015
					修正函数 iHxEmvLoadAid() 与 iHxEmvLoadCaPublicKey()
					使函数可以一个个增加Aid/CaPublicKey以减少高层对内存的需求
**************************************/
#ifndef _EMVPROC_H
#define _EMVPROC_H

// EMV客户接口返回码
#define HXEMV_OK					0  // OK
#define HXEMV_NA					1  // 不可用
#define HXEMV_PARA					2  // 参数错误
#define HXEMV_LACK_MEMORY			3  // 存储空间不足
#define HXEMV_CORE					4  // 内部错误
#define HXEMV_NO_SLOT				5  // 不支持的卡座
#define HXEMV_NO_CARD               6  // 卡片不存在
#define HXEMV_CANCEL				7  // 用户取消
#define HXEMV_TIMEOUT				8  // 超时
#define HXEMV_NO_APP				9  // 无支持的应用
#define HXEMV_AUTO_SELECT           10 // 获取的应用可自动选择
#define HXEMV_CARD_REMOVED			11 // 卡被取走
#define HXEMV_CARD_OP				12 // 卡操作错
#define HXEMV_CARD_SW				13 // 非法卡指令状态字
#define HXEMV_NO_DATA				14 // 无数据
#define HXEMV_NO_RECORD				15 // 无记录
#define HXEMV_NO_LOG				16 // 卡片不支持交易流水记录
#define HXEMV_TERMINATE				17 // 满足拒绝条件，交易终止
#define HXEMV_USE_MAG				18 // 请使用磁条卡
#define HXEMV_RESELECT				19 // 需要重新选择应用
#define HXEMV_NOT_SUPPORTED         20 // 不支持
#define HXEMV_DENIAL				21 // 交易拒绝
#define HXEMV_DENIAL_ADVICE			22 // 交易拒绝, 有Advice
#define HXEMV_NOT_ALLOWED			23 // 服务不允许
#define HXEMV_TRANS_NOT_ALLOWED		24 // 交易不允许
#define HXEMV_FLOW_ERROR			25 // EMV流程错误
#define HXEMV_CALLBACK_METHOD       26 // 回调与非回调核心接口调用错误
#define HXEMV_NOT_ACCEPTED          27 // 不接受
#define HXEMV_TRY_OTHER_INTERFACE	28 // 尝试其它通信界面
#define HXEMV_CARD_TRY_AGAIN		29 // 要求重新提交卡片(非接)
#define HXEMV_EXPIRED_APP			30 // 过期卡(非接)
#define HXEMV_FDDA_FAIL				31 // fDDA失败,拒绝交易(非接)
#define HXEMV_FDDA_FAIL_ONLINE		32 // fDDA失败,转联机(非接)
#define HXEMV_FDDA_FAIL_TRY_OTHER_INTERFACE 33 // fDDA失败,终止交易,尝试其它通信界面(非接)
#define HXEMV_CAPP_NOT_SUPPORT		34 // 不支持CAPP
#define HXEMV_MAC_FUNC_ERROR		35 // 外部Mac计算函数报错
#define HXEMV_RMAC_NOT_SUPPORT		36 // 不支持RMac
#define HXEMV_RMAC_ERROR			37 // 读记录RMac验证失败
#define HXEMV_MULTIPLE_CARD			38 // 有多张卡
#define HXEMV_NO_PROVE				39 // GetTransProve无指定TC
#define HXEMV_CARD_ERROR			40 // 问题卡
#define HXEMV_NOT_ORIGINAL_CARD		41 // 闪卡处理, 非原卡
#define HXEMV_TRANS_NOT_COMPLETED	42 // 闪卡处理, 交易未完成

// 持卡人认证方法
#define HXCVM_PLAIN_PIN             0x01 // 脱机明文密码认证
#define HXCVM_CIPHERED_OFFLINE_PIN  0x02 // 脱机密文密码认证(现不支持)
#define HXCVM_CIPHERED_ONLINE_PIN   0x03 // 联机密文密码认证
#define HXCVM_HOLDER_ID             0x04 // 持卡人证件认证
#define HXCVM_CONFIRM_AMOUNT        0x10 // 非持卡人验证,仅用于要求确认金额
// 持卡人认证方法处理方式
#define HXCVM_PROC_OK               0x00 // 正常处理完毕
#define HXCVM_BYPASS                0x01 // 要求输密码或验证证件时选择了Bypass
#define HXCVM_FAIL                  0x02 // 证件验证没有通过
#define HXCVM_CANCEL                0x03 // 被取消
#define HXCVM_TIMEOUT               0x04 // 超时

// GAC卡片应答
#define GAC_ACTION_TC				0x00 // 批准(生成TC)
#define GAC_ACTION_AAC				0x01 // 拒绝(生成AAC)
#define GAC_ACTION_AAC_ADVICE		0x02 // 拒绝(生成AAC,有Advice)
#define GAC_ACTION_ARQC				0x03 // 要求联机(生成ARQC)

// 卡片类型
#define HXCARD_TYPE_CONTACT			1	 // 接触卡
#define HXCARD_TYPE_CONTACTLESS		2	 // 非接触卡

// 交易路径
#define GPO_ROUTE_CONTACT			0	 // 接触Pboc(标准DC或EC)
#define GPO_ROUTE_CONTACTLESS		1	 // 非接触Pboc(标准DC或EC)
#define GPO_ROUTE_QPBOC_ONLINE		2	 // qPboc联机
#define GPO_ROUTE_QPBOC_OFFLINE		3	 // qPboc脱机
#define GPO_ROUTE_QPBOC_DECLINE		4	 // qPboc拒绝

// 终端支持的应用列表结构声明
typedef struct {
    unsigned char ucAidLen;						// AID长度
    unsigned char sAid[16];						// AID
	unsigned char ucASI;						// 应用选择指示器, 0:部分名字匹配，1:全部名字匹配
    signed char   cOnlinePinSupport;            // 1:该Aid支持联机密码 0:该Aid不支持联机密码, -1表示无

	// 以上参数为AID专用, 以下参数为与终端通用参数公共部分
	unsigned char sTermAppVer[2];               // T9F09, 终端应用版本号, "\xFF\xFF"表示不存在
	unsigned long ulFloorLimit;					// T9F1B, 终端限额, 单位为分, 0xFFFFFFFF表示不存在
    int			  iMaxTargetPercentage;         // 随机选择最大百分比，-1:不存在
    int			  iTargetPercentage;            // 随机选择目标百分比，-1:不存在
	unsigned long ulThresholdValue;             // 随机选择阈值, 0xFFFFFFFF表示不存在
    unsigned char ucECashSupport;				// 1:支持电子现金 0:不支持电子现金, 0xFF表示不存在
	unsigned char szTermECashTransLimit[12+1];  // T9F7B, 终端电子现金交易限额, 空表示不存在
	unsigned char ucTacDefaultExistFlag;        // 1:TacDefault存在, 0:TacDefault不存在
    unsigned char sTacDefault[5];				// TAC-Default, 参考TVR结构
	unsigned char ucTacDenialExistFlag;         // 1:TacDenial存在, 0:TacDenial不存在
    unsigned char sTacDenial[5];				// TAC-Denial, 参考TVR结构
	unsigned char ucTacOnlineExistFlag;         // 1:TacOnline存在, 0:TacOnline不存在
    unsigned char sTacOnline[5];				// TAC-Online, 参考TVR结构
    int           iDefaultDDOLLen;              // Default DDOL长度,-1表示无
    unsigned char sDefaultDDOL[32];             // Default DDOL(TAG_DFXX_DefaultDDOL)
    int           iDefaultTDOLLen;              // Default TDOL长度,-1表示无
    unsigned char sDefaultTDOL[32];             // Default TDOL(TAG_DFXX_DefaultTDOL)
	// 以下5个参数为非接专有参数
    unsigned char ucTermCtlsCapabilityFlag;     // sTermCtlsCapability存在标志, 0:无 1:存在
	unsigned char sTermCtlsCapability[4];		// TDFxx, 终端非接能力, T9F66 Mark位, 最终的T9F66值要与该参数做与操作
	unsigned char szTermCtlsAmountLimit[12+1];	// TDFxx, 终端非接交易限额, 空表示无
	unsigned char szTermCtlsOfflineAmountLimit[12+1];	// TDFxx, 终端非接脱机交易限额, 空表示无
	unsigned char szTermCtlsCvmLimit[12+1];		// TDFxx, 终端非接CVM限额, 空表示无
} stHxTermAid;

// 终端参数结构声明
typedef struct {
    unsigned char ucTermType;                   // T9F35, 终端类型, 例:0x21
    unsigned char sTermCapability[3];           // T9F33, 终端能力
    unsigned char sAdditionalTermCapability[5]; // T9F40, 终端能力扩展
    unsigned char szMerchantId[15+1];           // T9F16, 商户号
    unsigned char szTermId[8+1];                // T9F1C, 终端号
    unsigned char szMerchantNameLocation[254+1];// T9F4E, 商户名字地址, 0-254
    unsigned int  uiTermCountryCode;            // T9F1A, 终端国家代码, 156=中国
    unsigned char szAcquirerId[11+1];           // T9F01, 收单行标识符, 6-11
    int           iMerchantCategoryCode;		// T9F15, -1:无此数据 0-9999:有效数据
	unsigned char ucPinBypassBehavior;          // PIN bypass特性 0:每次bypass只表示该次bypass 1:一次bypass,后续都认为bypass
	unsigned char ucAppConfirmSupport;          // 1:支持应用确认 0:不支持应用确认(TAG_DFXX_AppConfirmSupport)
	unsigned char ucSmSupport;					// 1:支持国密 0:不支持国密

	stHxTermAid   AidCommonPara;				// 终端通用参数与AID相关参数公共部分
} stHxTermParam;

// 终端支持的CA公钥结构声明
typedef struct {
	unsigned char ucIndex;                      // 公钥索引
	unsigned char sRid[5];                      // 该公钥所属的RID
	unsigned char sExpireDate[4];               // 有效期, YYYYMMDD
	//unsigned char ucHash;
	unsigned char ucAlg;
    unsigned char ucKeyLen;                     // 公钥模数长度，字节为单位
    unsigned char sKey[248];                    // 公钥模数
    long		  lE;                           // 公钥指数，3或65537, 如果为0, 表示这是SM2密钥
	unsigned char sCheckCode[20];					// 校验值
} stHxCaPublicKey;

// 终端与卡片都支持的应用列表结构声明
typedef struct {
    unsigned char ucAdfNameLen;					// ADF名字长度
    unsigned char sAdfName[16];                 // ADF名字
    unsigned char szLabel[16+1];                // 应用标签
    int		      iPriority;                    // 应用优先级, -1表示该应用没有提供
    unsigned char szLanguage[8+1];              // 语言指示, 应用如没提供则置为空串
    int			  iIssuerCodeTableIndex;        // 字符代码表, -1表示该应用没有提供
    unsigned char szPreferredName[16+1];        // 应用首选名称, 应用如没提供则置为空串
} stHxAdfInfo;

#ifdef __cplusplus
extern "C" {
#endif

// 获取核心信息
// out : pszCoreName    : 核心名字, 不超过40字节
//       pszCoreDesc    : 核心描述, 不超过60字节字符串
//       pszCoreVer     : 核心版本号, 不超过10字节字符串, 如"1.00"
//       pszReleaseDate : 核心发布日期, YYYY/MM/DD
//       pszCustomerDesc: 客户接口说明, 不超过100字节字符串
// ret : HXEMV_OK       : OK
int iHxEmvInfo(unsigned char *pszCoreName, unsigned char *pszCoreDesc, unsigned char *pszCoreVer, unsigned char *pszReleaseDate, unsigned char *pszCustomerDesc);

// 初始化核心
// 初始化核心需要传入IC卡操作相关指令, 如果任一IC卡操作函数为空, 则不使用传入的函数操作IC卡
// in  : pfiTestCard	: 检测卡片是否存在
//                        ret : 0 : 不存在
//                              1 : 存在
//       pfiResetCard   : 卡片复位
//                        ret : <=0 : 复位错误
//                              >0  : 复位成功, 返回值为ATR长度
//       pfiDoApdu      : 执行APDU指令
//                        in  : iApduInLen   : Apdu指令长度
//                              psApduIn     : Apdu指令, 格式: Cla Ins P1 P2 Lc DataIn Le
//                        out : piApduOutLen : Apdu应答长度
//                              psApduOut    : Apdu应答, 格式: DataOut Sw1 Sw2
//                        ret : 0            : 卡操作成功
//                              1            : 卡操作错
//       pfiCloseCard   : 关闭卡片
//                        ret : 不关心
//       iSlotNo        : 卡座号, 合法值为0-9
// ret : HXEMV_OK       : OK
//       HXEMV_PARA     : 参数错误
//       HXEMV_CORE     : 内部错误
// Note: 在调用任何其它接口前必须且先初始化核心
int iHxEmvInit(int (*pfiTestCard)(void),
			   int (*pfiResetCard)(unsigned char *psAtr),
			   int (*pfiDoApdu)(int iApduInLen, unsigned char *psApduIn, int *piApduOutLen, unsigned char *psApduOut),
			   int (*pfiCloseCard)(void),
			   int iSlotNo);

// 设置终端参数
// in  : pHxTermParam      : 终端参数
// out : pszErrTag		   : 返回HXEMV_PARA时表明出错的参数Tag值(可能会返回多个Tag值)
//                           NULL表示不需要返回值
// ret : HXEMV_OK          : OK
//       HXEMV_PARA        : 参数错误
//       HXEMV_CORE        : 内部错误
//       HXEMV_FLOW_ERROR  : EMV流程错误
// note: 允许多次设置终端参数
int iHxEmvSetParam(stHxTermParam *pHxTermParam, char *pszErrTag);

// 装载终端支持的Aid
// in  : paHxTermAid       : 终端支持的Aid数组
//       iHxTermAidNum     : 终端支持的Aid个数, 0表示增加一个Aid, -1表示清除所有Aid
// out : pszErrTag		   : 返回HXEMV_PARA时表明出错的参数Tag值(可能会返回多个Tag值)
//                           NULL表示不需要返回值
// ret : HXEMV_OK          : OK
//       HXEMV_LACK_MEMORY : 存储空间不足
//       HXEMV_PARA        : 参数错误
//       HXEMV_FLOW_ERROR  : EMV流程错误
int iHxEmvLoadAid(stHxTermAid *paHxTermAid, int iHxTermAidNum, char *pszErrTag);

// 装载CA公钥
// in  : paHxCaPublicKey   : CA公钥
//       iHxCaPublicKeyNum : CA公钥个数, 0表示增加一个CA公钥, -1表示清除所有公钥
// out : pszErrMsg		   : 出错的公钥下标, NULL表示不需要返回
// ret : HXEMV_OK          : OK
//       HXEMV_LACK_MEMORY : 存储空间不足
//       HXEMV_PARA        : 参数错误
//       HXEMV_FLOW_ERROR  : EMV流程错误
int iHxEmvLoadCaPublicKey(stHxCaPublicKey *paHxCaPublicKey, int iHxCaPublicKeyNum, char *pszErrMsg);

// qPboc交易预处理
// in  : pszAmount					: 交易金额, ascii码串, 不要小数点, 默认为缺省小数点位置, 例如:RMB1.00表示为"100"
//       uiCurrencyCode				: 货币代码
//       ucCappSupportFlag			: 支持扩展标志, 0:不支持 !0:支持
// ret : HXEMV_OK					: OK
//       HXEMV_FLOW_ERROR			: EMV流程错误
//		 HXEMV_TRY_OTHER_INTERFACE	: 满足拒绝条件，交易终止, 尝试其它通信界面
//									  应显示EMVMSG_TERMINATE信息, 如果终端支持其它通信界面, 提示用户尝试其它通信界面
//       HXEMV_CORE					: 内部错误
// Note: 每笔qPboc交易前必须先做预处理, 只有成功完成了预处理才可以进行非接交易
//       应用层要确保交易数据与预处理数据一致
//       参考: JR/T0025.12—2013, 6.2, p9
//       本函数要在iHxEmvTransInit()函数之前调用, 
//		 之后调用iHxEmvTransInit()时通知iHxEmvTransInit()函数已经做过了qPboc预处理
int iHxEmvQpbocPreProc(unsigned char *pszAmount, unsigned int uiCurrencyCode, unsigned char ucCappSupportFlag);

// 设置IC卡卡座号
// in  : iSlotNo : 卡座号，VPOS规范
// ret : HXEMV_OK       : OK
//       HXEMV_NO_SLOT  : 不支持此卡座
int iHxEmvSetIccSlot(int iSlotNo);

// 检测卡片与类型
// out : piCardType			 : HXCARD_TYPE_CONTACT     : 接触卡
//							   HXCARD_TYPE_CONTACTLESS : 非接触卡
// ret : HXEMV_OK			 : OK
//		 HXEMV_NO_CARD		 : 卡片不存在
//		 HXEMV_MULTIPLE_CARD : 有多张卡
int iHxEmvTestCard(int *piCardType);

// 关闭卡片
// ret : HXEMV_OK       : OK
int iHxEmvCloseCard(void);

// 交易初始化
// in  : iFlag             : 0 : 正常初始化
//                           b8-b7 : 保留 = 0
//							 b1    : 1:已经做过了qPboc交易预处理
// ret : HXEMV_OK          : OK
//       HXEMV_FLOW_ERROR  : EMV流程错误
//       HXEMV_CARD_REMOVED: 卡被取走
//       HXEMV_NO_CARD     : 卡片不存在
//       HXEMV_CARD_OP     : 卡片复位错误
int iHxEmvTransInit(int iFlag);

// 读取支持的应用
// in  : iIgnoreBlock	   : !0:忽略应用锁定标志, 0:锁定的应用不会被选择
//       piHxAdfNum        : 可容纳的终端与卡片同时支持的Adf个数
// out : paHxAdfInfo       : 终端与卡片同时支持的Adf列表(以优先级排序)
//       piHxAdfNum        : 终端与卡片同时支持的Adf个数
// ret : HXEMV_OK          : OK, 获取的应用必须确认
//       HXEMV_AUTO_SELECT : OK, 获取的应用可自动选择
//       HXEMV_NO_APP      : 无支持的应用
//       HXEMV_CARD_REMOVED: 卡被取走
//       HXEMV_CARD_SW     : 卡状态字非法
//       HXEMV_CARD_OP     : 卡操作错
//       HXEMV_TERMINATE   : 满足拒绝条件，交易终止
//       HXEMV_FLOW_ERROR  : EMV流程错误
//       HXEMV_LACK_MEMORY : 存储空间不足
//       HXEMV_PARA        : 参数错误
int iHxEmvGetSupportedApp(int iIgnoreBlock, stHxAdfInfo *paHxAdfInfo, int *piHxAdfNum);

// 应用选择
// in  : iIgnoreBlock	     : !0:忽略应用锁定标志, 0:锁定的应用不会被选择
//       iAidLen             : AID长度
//       psAid               : AID
// ret : HXEMV_OK            : OK
//       HXEMV_CARD_REMOVED  : 卡被取走
//       HXEMV_CARD_OP       : 卡操作错
//       HXEMV_TERMINATE     : 满足拒绝条件，交易终止
//       HXEMV_FLOW_ERROR    : EMV流程错误
//       HXEMV_CORE          : 内部错误
//       HXEMV_RESELECT      : 需要重新选择应用
//       HXEMV_NOT_SUPPORTED : 选择了不支持的应用
int iHxEmvAppSelect(int iIgnoreBlock, int iAidLen, unsigned char *psAid);

// 获取卡片非接扩展支持能力
// out : pucCappCapability : 卡片非接支持能力(DF61值)
// ret : HXEMV_OK   : OK
//       HXEMV_FLOW_ERROR		: EMV流程错误
//		 HXEMV_CAPP_NOT_SUPPORT : 卡片不支持
int iHxEmvCtlsGetCappCapability(unsigned char *pucCappCapability);

// 设置非接扩展执行标志
// in  : ucCappFlag    : 非接扩展执行标志, 定义同TDF60
//                       0 : 不支持扩展
//                       1 : 表示选择或执行分段扣费交易
//                       2 : 表示选择或执行脱机预授权交易
//                       3 : 表示选择或执行脱机预授权完成交易  
// ret : HXEMV_OK
//       HXEMV_FLOW_ERROR		: EMV流程错误
//       HXEMV_CORE				: 内部错误
//		 HXEMV_CAPP_NOT_SUPPORT : 卡片不支持
// note: 终端在选择完应用后提取TDF61的值, 判断卡片应用是否支持相应的扩展
//       终端读取扩展记录, 判断卡片应用是否支持特定的扩展, 之后设置扩展执行标志
int iHxEmvCtlsSetCappFlag(unsigned char ucCappFlag);

// 读取卡片内部数据
// in  : psTag             : 数据标签, 例:"\x9F\x79":电子现金余额
//       piOutTlvDataLen   : psOutTlvData缓冲区大小
//       piOutDataLen      : psOutData缓冲区大小
// out : piOutTlvDataLen   : 读出的数据长度, 原始格式, 包括Tag、Length、Value
//       psOutTlvData      : 读出的数据, 包括Tag、Length、Value
//       piOutDataLen      : 读出的数据长度, 解码后格式
//       psOutData         : 读出的数据(N数字型:转换成可读数字串, CN压缩数字型:去除尾部'F'后的数字串, A|AN|ANS|B|未知类型:原样返回),除B型外返回值后面会强制加一个结尾'\0', 该结束符不包括在返回的长度之内
//                           注意, 类似N3这样的数据, 返回的内容长度为N4, 注意接收缓冲区长度要给足
// ret : HXEMV_OK          : 读取成功
//       HXEMV_NO_DATA     : 无此数据
//       HXEMV_LACK_MEMORY : 缓冲区不足
//       HXEMV_CARD_REMOVED: 卡被取走
//       HXEMV_CARD_OP     : 卡操作错
//       HXEMV_CARD_SW     : 非法卡状态字
//       HXEMV_FLOW_ERROR  : EMV流程错误
//       HXEMV_CORE        : 内部错误
// note: piOutTlvDataLen或psOutTlvData任一个传入NULL，则不会返回这两个结果
//       piOutDataLen或psOutData任一个传入NULL, 将不会返回这两个结果
//       数据一旦被读出, 会放到Tlv数据库中, 再次使用可以用iHxEmvGetData()读取
int iHxEmvGetCardNativeData(unsigned char *psTag, int *piOutTlvDataLen, unsigned char *psOutTlvData, int *piOutDataLen, unsigned char *psOutData);

// 读取应用数据
// in  : psTag             : 数据标签, 例:"\x82":Aip
//       piOutTlvDataLen   : psOutTlvData缓冲区大小
//       piOutDataLen      : psOutData缓冲区大小
// out : piOutTlvDataLen   : 读出的数据长度, 原始格式, 包括Tag、Length、Value
//       psOutTlvData      : 读出的数据, 包括Tag、Length、Value
//       piOutDataLen      : 读出的数据长度, 解码后格式
//       psOutData         : 读出的数据(N数字型:转换成可读数字串, CN压缩数字型:去除尾部'F'后的数字串, A|AN|ANS|B|未知类型:原样返回),除B型外返回值后面会强制加一个结尾'\0', 该结束符不包括在返回的长度之内
//                           注意, 类似N3这样的数据, 返回的内容长度为N4, 注意接收缓冲区长度要给足
// ret : HXEMV_OK          : 读取成功
//       HXEMV_NO_DATA     : 无此数据
//       HXEMV_LACK_MEMORY : 缓冲区不足
//       HXEMV_FLOW_ERROR  : EMV流程错误
// note: piOutTlvDataLen或psOutTlvData任一个传入NULL，则不会返回这两个结果
//       piOutDataLen或psOutData任一个传入NULL, 将不会返回这两个结果
int iHxEmvGetData(unsigned char *psTag, int *piOutTlvDataLen, unsigned char *psOutTlvData, int *piOutDataLen, unsigned char *psOutData);

// 读卡片交易流水支持信息
// in  : iFlag             : 0:标准交易流水 1:圈存流水
// out : piMaxRecNum       : 最多交易流水记录个数
// ret : HXEMV_OK          : 读取成功
//       HXEMV_NO_LOG      : 卡片不支持交易流水记录
//       HXEMV_PARA        : 参数错误
//       HXEMV_FLOW_ERROR  : EMV流程错误
int iHxEmvGetLogInfo(int iFlag, int *piMaxRecNum);

// 读卡片交易流水
// in  : iFlag             : 0:标准交易流水 1:圈存流水
//       iLogNo            : 交易流水记录号, 最近的一条记录号为1
//       piLogLen          : psLog缓冲区大小
// out : piLogLen          : 交易流水记录长度
//       psLog             : 记录内容(IC卡原格式输出)
// ret : HXEMV_OK          : 读取成功
//       HXEMV_NO_RECORD   : 无此记录
//       HXEMV_LACK_MEMORY : 缓冲区不足
//       HXEMV_CARD_REMOVED: 卡被取走
//       HXEMV_CARD_OP     : 卡操作错
//       HXEMV_CARD_SW     : 非法卡指令状态字
//       HXEMV_NO_LOG      : 卡片不支持交易流水记录
//       HXEMV_PARA        : 参数错误
//       HXEMV_FLOW_ERROR  : EMV流程错误
int iHxEmvReadLog(int iFlag, int iLogNo, int *piLogLen, unsigned char *psLog);

// 修正eCash/qPboc支持能力
// in  : iECashSupport		: 电子现金支持能力
//                            0:不支持 1:支持 -1:不修正
//       psCtlsAttr			: 非接卡支持能力(9F66), NULL表示不修正
//							  8字节, 先用前4字节做And修正, 再用后4字节做Or修正
// ret : HXEMV_OK
//       HXEMV_FLOW_ERROR	: EMV流程错误
int iHxEmvAdjustEcashQpbocAttr(int iECashSupport, unsigned char *psCtlsAttr);

// GPO
// in  : pszTransTime      : 交易时间，YYYYMMDDhhmmss
//       ulTTC             : 终端交易序号, 1-999999
//       ucTransType       : 交易类型,处理码前2位表示的金融交易类型
//                           比如现金充值63表示为0x63
//       pszAmount         : 交易金额, ascii码串, 不要小数点, 默认为缺省小数点位置, 例如:RMB1.00表示为"100"
//       uiCurrencyCode    : 货币代码, 156=人民币
// ret : HXEMV_OK          : OK
//       HXEMV_CARD_REMOVED: 卡被取走
//       HXEMV_CARD_OP     : 卡操作错
//       HXEMV_CARD_SW     : 非法卡状态字
//       HXEMV_TERMINATE   : 满足拒绝条件，交易终止
//       HXEMV_RESELECT    : 需要重新选择应用
//       HXEMV_TRANS_NOT_ALLOWED : 交易不支持
//       HXEMV_PARA        : 参数错误
//       HXEMV_FLOW_ERROR  : EMV流程错误
//       HXEMV_CORE        : 内部错误
// Note: 如果返回HXEMV_RESELECT，从iHxEmvGetSupportedApp()开始重新执行流程
int iHxEmvGPO(unsigned char *pszTransTime, unsigned long ulTTC, unsigned char ucTransType, unsigned char *pszAmount, unsigned int uiCurrencyCode);

// GPO结果分析
// out : piTransRoute               : 交易走的路径
//										GPO_ROUTE_CONTACT			// 接触Pboc(标准DC或EC)
//										GPO_ROUTE_CONTACTLESS		// 非接触Pboc(标准DC或EC)
//										GPO_ROUTE_QPBOC_ONLINE		// qPboc联机
//										GPO_ROUTE_QPBOC_OFFLINE		// qPboc脱机
//										GPO_ROUTE_QPBOC_DECLINE		// qPboc拒绝
//       piSignFlag					: 需要签字标志(0:不需要签字, 1:需要签字), 仅当走的是qPboc流程时才有效
//                                    NULL表示不需要传出
//       piNeedOnlinePin            : 需要联机密码标志(0:不需要联机密码, 1:需要联机密码), 仅当走的是qPboc联机流程时才有效
//                                    NULL表示不需要传出
// ret : HXEMV_OK					: OK, 根据piTransRoute决定后续流程
//       HXEMV_FLOW_ERROR			: EMV流程错误
//       HXEMV_TERMINATE			: 满足拒绝条件，交易终止
//		 HXEMV_TRY_OTHER_INTERFACE	: 尝试其它通信界面
//       HXEMV_CORE					: 内部错误
// Note: GPO成功后才可以调用, 非接卡才需要调用
// 参考 JR/T0025.12—2013, 7.8 P40
int iHxEmvGpoAnalyse(int *piTransRoute, int *piSignFlag, int *piNeedOnlinePin);

// 传入或重新传入金额
// in  : pszAmount         : 金额
// ret : HXEMV_OK          : 设置成功
//       HXEMV_FLOW_ERROR  : EMV流程错误
//       HXEMV_PARA        : 参数错误
// note: GPO后GAC金额可能会不同, 重新设定交易金额
//       GPO后, GAC1前才可以调用
int iHxEmvSetAmount(unsigned char *pszAmount);

// 读应用记录
// ret : HXEMV_OK          : OK
//       HXEMV_CARD_REMOVED: 卡被取走
//       HXEMV_CARD_OP     : 卡操作错
//       HXEMV_CARD_SW     : 非法卡指令状态字
//       HXEMV_TERMINATE   : 满足拒绝条件，交易终止
//       HXEMV_FLOW_ERROR  : EMV流程错误
//       HXEMV_CORE        : 内部错误
//       HXEMV_EXPIRED_APP : 过期卡
int iHxEmvReadRecord(void);

// 读除了最后一条记录前的其它所有应用记录
// ret : HXEMV_OK          : OK
//       HXEMV_CARD_OP     : 卡操作错
//       HXEMV_CARD_SW     : 非法卡指令状态字
//       HXEMV_TERMINATE   : 满足拒绝条件，交易终止
//       HXEMV_FLOW_ERROR  : EMV流程错误
//       HXEMV_CORE        : 内部错误
//       HXEMV_EXPIRED_APP : 过期卡
// note: 如果为检查过期卡, 非接脱机调用iHxEmvReadRecord()时会在读到有效期后立即检查到, 不需要自行检查
//       如果还有其它检查, 才需要调用本函数(及iHxEmvReadRecordPart2())后进行所需检查
int iHxEmvReadRecordPart1(void);

// 读最后一条应用记录
// ret : HXEMV_OK          : OK
//       HXEMV_CARD_OP     : 卡操作错
//       HXEMV_CARD_SW     : 非法卡指令状态字
//       HXEMV_TERMINATE   : 满足拒绝条件，交易终止
//       HXEMV_FLOW_ERROR  : EMV流程错误
//       HXEMV_CORE        : 内部错误
int iHxEmvReadRecordPart2(void);

// 非接卡闪卡处理
// ret : HXEMV_OK          : OK, 原卡, 消费成功, 应继续进行脱机数据认证, 完成交易
//       HXEMV_CARD_OP     : 卡操作错
//       HXEMV_CARD_SW     : 非法卡指令状态字
//       HXEMV_TERMINATE   : 满足拒绝条件，交易终止
//       HXEMV_FLOW_ERROR  : EMV流程错误
//       HXEMV_CORE        : 内部错误
//		 HXEMV_NOT_ORIGINAL_CARD	: 非原卡
//		 HXEMV_TRANS_NOT_COMPLETED	: 原卡, 交易未完成
// note: 只有qPboc脱机完成交易, 且仅最后一条记录读取失败才可以调用本函数
int iHxEmvCtlsRecover(void);

// 设置卡片黑名单及分开消费标志
// in  : iBlackFlag        : 设置该卡是否为黑名单卡, 0:不为黑名单卡 1:黑名单卡
//       iSeparateFlag     : 设置该卡累计消费超限额, 0:没超限额 1:超限额
// ret : HXEMV_OK          : OK
//       HXEMV_CORE        : 内部错误
// Note: 如果读记录后发现卡片是黑名单卡或累计消费超限额, 则需要调用此函数设置账户信息
//       如果不支持或检查不许设置标记, 则不需要调用本接口
int iHxEmvSetPanFlag(int iBlackFlag, int iSeparateFlag);

// SDA或DDA
// out : piNeedCheckCrlFlag  : 是否需要判断发卡行公钥证书为黑名单标志, 0:不需要判断 1:需要判断
//       psRid               : RID[5], psRid+pucCaIndex+psCertSerNo这三项提供给应用层判断发卡行公钥证书是否在黑名单列表中. 任何一个指针传入为空都表示高层不需要
//       pucCaIndex          : CA公钥索引
//       psCertSerNo         : 发卡行公钥证书序列号[3]
// ret : HXEMV_OK            : OK
//       HXEMV_CARD_REMOVED  : 卡被取走
//       HXEMV_CARD_OP       : 卡操作错
//       HXEMV_CARD_SW       : 非法卡状态字
//       HXEMV_TERMINATE     : 满足拒绝条件，交易终止
//       HXEMV_DENIAL        : 终端行为分析结果为脱机拒绝
//       HXEMV_DENIAL_ADVICE : 终端行为分析结果为脱机拒绝, 有Advice
//       HXEMV_FLOW_ERROR    : EMV流程错误
//       HXEMV_CORE          : 内部错误
//		 HXEMV_FDDA_FAIL	 : fDDA失败,拒绝交易(非接)
//		 HXEMV_FDDA_FAIL_ONLINE : fDDA失败,转联机(非接)
//		 HXEMV_FDDA_FAIL_TRY_OTHER_INTERFACE : fDDA失败,终止交易,尝试其它通信界面(非接)
// Note: 应用层支持发卡行公钥证书黑名单检查时, piNeedCheckCrlFlag才有意义, 否则可以忽略
int iHxEmvOfflineDataAuth(int *piNeedCheckCrlFlag, unsigned char *psRid, unsigned char *pucCaIndex, unsigned char *psCertSerNo);

// 设置发卡行公钥证书为黑名单
// in  : iIssuerCertCrlFlag  : 是否在黑名单列表标志, 0:不在黑名单列表 1:在黑名单列表
// ret : HXEMV_OK            : OK
//       HXEMV_CARD_REMOVED  : 卡被取走
//       HXEMV_CARD_OP       : 卡操作错
//       HXEMV_CARD_SW       : 非法卡状态字
//       HXEMV_TERMINATE     : 满足拒绝条件，交易终止
//       HXEMV_DENIAL        : 终端行为分析结果为脱机拒绝
//       HXEMV_DENIAL_ADVICE : 终端行为分析结果为脱机拒绝, 有Advice
//       HXEMV_FLOW_ERROR    : EMV流程错误
//       HXEMV_CORE          : 内部错误
// Note: 应用层调用iHxEmvOfflineDataAuth()后得到Rid+CaIndex+CertSerNo
//       根据此数据判断发卡行公钥证书是否为黑名单,只有是黑名单,才需要调用本函数通知核心
int iHxEmvSetIssuerCertCrl(int iIssuerCertCrlFlag);

// 处理限制
// ret : HXEMV_OK            : OK
//       HXEMV_TERMINATE     : 满足拒绝条件，交易终止
//       HXEMV_FLOW_ERROR    : EMV流程错误
//       HXEMV_CORE          : 内部错误
//       HXEMV_CARD_REMOVED  : 卡被取走
//       HXEMV_DENIAL        : 终端行为分析结果为脱机拒绝
//       HXEMV_DENIAL_ADVICE : 终端行为分析结果为脱机拒绝, 有Advice
//       HXEMV_CARD_OP       : 终端行为分析结果为脱机拒绝, 申请AAC时卡片出错
int iHxEmvProcRistrictions(void);

// 终端风险管理
// ret : HXEMV_OK            : OK
//       HXEMV_TERMINATE     : 满足拒绝条件，交易终止
//       HXEMV_CORE          : 其它错误
//       HXEMV_CARD_REMOVED  : 卡被取走
//       HXEMV_CARD_OP       : 卡操作错
//       HXEMV_DENIAL        : 终端行为分析结果为脱机拒绝
//       HXEMV_DENIAL_ADVICE : 终端行为分析结果为脱机拒绝, 有Advice
//       HXEMV_FLOW_ERROR    : EMV流程错误
int iHxEmvTermRiskManage(void);

// 获取持卡人验证
// out : piCvm               : 持卡人认证方法, 支持HXCVM_PLAIN_PIN、HXCVM_CIPHERED_ONLINE_PIN、HXCVM_HOLDER_ID、HXCVM_CONFIRM_AMOUNT
//       piBypassFlag        : 允许bypass标志, 0:不允许, 1:允许
// ret : HXEMV_OK            : OK
//       HXEMV_NO_DATA       : 无需继续进行持卡人验证
//       HXEMV_CARD_REMOVED  : 卡被取走
//       HXEMV_CARD_OP       : 卡操作错
//       HXEMV_TERMINATE     : 满足拒绝条件，交易终止
//       HXEMV_DENIAL        : 终端行为分析结果为脱机拒绝
//       HXEMV_DENIAL_ADVICE : 终端行为分析结果为脱机拒绝, 有Advice
//       HXEMV_FLOW_ERROR    : EMV流程错误
//       HXEMV_CORE          : 内部错误
// Note: 与iHxEmvDoCvmMethod()搭配, 可能需要多次调用, 直到返回HXEMV_NO_DATA
int iHxEmvGetCvmMethod(int *piCvm, int *piBypassFlag);

// 执行持卡人验证
// in  : iCvmProc            : 认证方法处理方式, HXCVM_PROC_OK or HXCVM_BYPASS or HXCVM_FAIL or HXCVM_CANCEL or HXCVM_TIMEOUT
//       psCvmData           : 输入的密码, 如果为明文密码, 密码尾部要补0
// out : piPrompt            : 额外提示信息, 0:无额外提示信息 1:密码错,可重试 2:密码错,密码已锁 3:脱机密码验证成功
// ret : HXEMV_OK            : OK, 需要继续进行持卡人验证, 继续调用iHxGetCvmMethod2(), 然后再调用本函数
//       HXEMV_PARA		     : 参数错误
//       HXEMV_CANCEL        : 用户取消
//       HXEMV_TIMEOUT       : 超时
//       HXEMV_CARD_REMOVED  : 卡被取走
//       HXEMV_CARD_OP       : 卡操作错
//       HXEMV_CARD_SW       : 非法卡状态字
//       HXEMV_TERMINATE     : 满足拒绝条件，交易终止
//       HXEMV_DENIAL        : 终端行为分析结果为脱机拒绝
//       HXEMV_DENIAL_ADVICE : 终端行为分析结果为脱机拒绝, 有Advice
//       HXEMV_FLOW_ERROR    : EMV流程错误
//       HXEMV_CORE          : 内部错误
// Note: 执行的CVM必须是最近一次iHxEmvGetCvmMethod()获得的CVM
int iHxEmvDoCvmMethod(int iCvmProc, unsigned char *psCvmData, int *piPrompt);

// 获取CVM认证方法签字标志
// out : piNeedSignFlag    : 表示需要签字标志，0:不需要签字 1:需要签字
// ret : HXEMV_OK          : OK
int iHxEmvGetCvmSignFlag(int *piNeedSignFlag);

// 终端行为分析
// ret : HXEMV_OK            : OK
//       HXEMV_CORE          : 其它错误
//       HXEMV_FLOW_ERROR    : EMV流程错误
//       HXEMV_CARD_REMOVED  : 卡被取走
//       HXEMV_DENIAL        : 终端行为分析结果为脱机拒绝
//       HXEMV_DENIAL_ADVICE : 终端行为分析结果为脱机拒绝, 有Advice
int iHxEmvTermActionAnalysis(void);

// 第一次GAC
// in  : ucForcedOnline    : 1:设定强制联机标志 0:不设定强制联机标志
// out : piCardAction      : 卡片执行结果
//								GAC_ACTION_TC     : 批准(生成TC)
//								GAC_ACTION_AAC    : 拒绝(生成AAC)
//								GAC_ACTION_AAC_ADVICE : 拒绝(生成AAC,有Advice)
//								GAC_ACTION_ARQC   : 要求联机(生成ARQC)
// ret : HXEMV_OK          : OK
//       HXEMV_CORE        : 其它错误
//       HXEMV_CARD_REMOVED: 卡被取走
//       HXEMV_CARD_OP     : 卡操作错
//       HXEMV_CARD_SW     : 非法状态字
//       HXEMV_TERMINATE   : 满足拒绝条件，交易终止
//       HXEMV_NOT_ACCEPTED: 不接受
//       HXEMV_FLOW_ERROR  : EMV流程错误
int iHxEmvGac1(unsigned char ucForcedOnline , int *piCardAction);

// 第二次GAC
// in  : pszArc            : 应答码[2], NULL或""表示联机失败
//       pszAuthCode	       : 授权码[6], NULL或""表示无授权码数据
//       psOnlineData      : 联机响应数据(55域内容),一组TLV格式数据, NULL表示无联机响应数据
//       psOnlineDataLen   : 联机响应数据长度
// out : piCardAction      : 卡片执行结果
//                           GAC_ACTION_TC     : 批准(生成TC)
//							     GAC_ACTION_AAC    : 拒绝(生成AAC)
//                           GAC_ACTION_AAC_ADVICE : 拒绝(生成AAC,有Advice)
// ret : HXEMV_OK          : OK
//       HXEMV_CORE        : 其它错误
//       HXEMV_CARD_REMOVED: 卡被取走
//       HXEMV_CARD_OP     : 卡操作错
//       HXEMV_CARD_SW     : 非法卡状态字
//       HXEMV_TERMINATE   : 满足拒绝条件，交易终止
//       HXEMV_NOT_ACCEPTED: 不接受
//       HXEMV_FLOW_ERROR  : EMV流程错误
int iHxEmvGac2 (unsigned char *pszArc, unsigned char *pszAuthCode, unsigned char *psIssuerData, int iIssuerDataLen, int *piCardAction);

// 圈存闪卡处理
// ret : HXEMV_OK          : OK, 原卡, 圈存成功, 不应冲正
//       HXEMV_CARD_OP     : 卡操作错
//       HXEMV_CARD_SW     : 非法卡指令状态字
//       HXEMV_FLOW_ERROR  : EMV流程错误
//       HXEMV_CORE        : 内部错误
//		 HXEMV_NOT_ORIGINAL_CARD	: 非原卡
//		 HXEMV_TRANS_NOT_COMPLETED	: 原卡, 交易未完成
// note: 只有GAC2成功后才可以调用本函数
int iHxEmvLoadRecover(void);

// 传入非接扩展Mac计算函数
// in  : piMacCalFunc : Mac计算函数, NULL表示不计算/验证Mac
// ret : HXEMV_OK
// note: pfiMacCalFunc()原型描述
//    int pfiMacCalFunc(uchar ucSfi, long lId, uchar *psDiv, uchar *psIv, uint uiMsgLen, uchar *psMsg, uchar *psMac)
//    in  : ucSfi    : 短文件标示符
//          lId      : 记录标识, <0表示开通, 对于循环记录更新, lId=0
//          psDiv    : 分散因子(账号[7]+账号序列号[1], 如果多级分散, 高层自行获取其它级别的分散因子)
//          psIv     : 初始向量
//          uiMsgLen : 计算Mac数据长度
//          psMsg    : 计算Mac数据
//    out : psMac    : 4字节Mac
//    ret : 0        : OK
//          1        : 参数错误
//          2        : 没有支持的密钥
//          3        : Sam卡通讯失败
//          4        : 其它错误
//    note: 如果要支持多索引、多算法, 请自行读取9F10解析
int iHxEmvSetMacCalFunc(int (*pfiMacCalFunc)(unsigned char ucSfi, long lId, unsigned char *psDiv, unsigned char *psIv, unsigned int uiMsgLen, unsigned char *psMsg, unsigned char *psMac));

// 读CAPP记录
// 外部也可以在GPO后绕过核心自行组织APDU读/更新/添加记录
// in  : ucRMacFlag    : 验证RMac标志, 0:不验证RMac 1:验证RMac
//       ucSfi         : SFI
//       lId           : 记录标识, 0x0000-0xFFFF
//       ucNextFlag    : 读下一记录标记, 0:第一条记录 1:下一条记录
// out : psRecData     : 记录内容, 包括3字节记录头
//       pucRecDataLen : 记录长度
// ret : HXEMV_OK
//       HXEMV_FLOW_ERROR  : EMV流程错误
//       HXEMV_NO_RECORD
//       HXEMV_CARD_OP
//       HXEMV_CARD_SW
//       HXEMV_MAC_FUNC_ERROR
//       HXEMV_RMAC_ERROR
//       HXEMV_RMAC_NOT_SUPPORT
// note: Pboc3.0非接扩展指令
//       如果返回HXEMV_MAC_FUNC_ERROR/HXEMV_RMAC_ERROR/HXEMV_RMAC_NOT_SUPPORT错误, 返回数据依然有效
int iHxEmvExtApduReadCappData(unsigned char ucRMacFlag, unsigned char ucSfi, long lId, unsigned char ucNextFlag, unsigned char *psRecData, unsigned char *pucRecDataLen);

// 更新CAPP记录
// in  : ucRMacFlag    : 验证RMac标志, 0:不验证RMac 1:验证RMac
//       ucSfi         : SFI
//       lId           : 记录标识, <0表示为循环记录
//       ucNextFlag    : 更新下一记录标记(非循环记录有效), 0:第一条记录 1:下一条记录
//       psRecData     : 记录内容, 包括3字节记录头(循环记录不包括这3字节)
//       ucRecDataLen  : 记录长度
// ret : HXEMV_OK
//       HXEMV_FLOW_ERROR  : EMV流程错误
//       HXEMV_NO_RECORD
//       HXEMV_CARD_OP
//       HXEMV_CARD_SW
//       HXEMV_CARD_ERROR
//       HXEMV_MAC_FUNC_ERROR
//       HXEMV_RMAC_ERROR
//       HXEMV_RMAC_NOT_SUPPORT
// note: Pboc3.0非接扩展指令
//       如果返回HXEMV_RMAC_ERROR/HXEMV_RMAC_NOT_SUPPORT错误, 更新指令已经成功完成
int iHxEmvExtApduUpdCappDataCache(unsigned char ucRMacFlag, unsigned char ucSfi, long lId, unsigned char ucNextFlag, unsigned char *psRecData, unsigned char cRecDataLen);

// 添加CAPP记录
// in  : psManageKey   : 行业管理密钥密文(开通密钥加密后管理密钥密文,高层自行获取分散因子/计算密文)
//       ucSfi         : SFI
//       lId           : 记录标识, <0表示为循环记录
//       psRecData     : 记录内容, 包括3字节记录头
//       pucRecDataLen : 记录长度
// ret : HXEMV_OK
//       HXEMV_FLOW_ERROR  : EMV流程错误
//       HXEMV_CARD_OP
//       HXEMV_CARD_SW
//       HXEMV_CARD_ERROR
//       HXEMV_MAC_FUNC_ERROR
// note: Pboc3.0非接扩展指令
int iHxEmvExtApduAppendRecord(unsigned char *psManageKey, unsigned char ucSfi, unsigned long lId, unsigned char *psRecData, unsigned char ucRecDataLen);

// 读取记录
// in  : ucSfi         : SFI
//       ucRecNo       : 记录号
// out : psRecData     : 记录内容, 包括3字节记录头
//       pucRecDataLen : 记录长度
// ret : HXEMV_OK
//       HXEMV_FLOW_ERROR  : EMV流程错误
//       HXEMV_NO_RECORD
//       HXEMV_CARD_OP
//       HXEMV_CARD_SW
int iHxEmvApduReadRec(unsigned char ucSfi, unsigned char ucRecNo, unsigned char *psRecData, unsigned char *pucRecDataLen);

// 读取交易证书
// in  : uiAtc         : Atc
// out : psTc          : TC
// ret : HXEMV_OK
//       HXEMV_FLOW_ERROR  : EMV流程错误
//       HXEMV_NO_PROVE
//       HXEMV_CARD_OP
//       HXEMV_CARD_SW
// note: 非接扩展规范中增加的函数
int iHxEmvExtApduGetTransProve(unsigned int uiAtc, unsigned char *psTc);

#ifdef __cplusplus
}
#endif

#endif
