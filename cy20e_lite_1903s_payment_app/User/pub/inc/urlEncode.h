#ifndef _URL_ENCODE_H
#define _URL_ENCODE_H

/**
 * @brief URLEncode 对字符串URL编码
 *
 * @param str 原字符串
 * @param strLen 原字符串长度(不包括最后的\0)
 * @param result 结果缓冲区的地址
 * @param resultSize 结果缓冲区的大小(包括最后的\0)
 *
 * @return: >0:resultstring 里实际有效的长度
 *           0: 解码失败.
 */
int URLEncode(const char *str, const int strLen, char *result, const int resultSize);


/**
 * @brief URLDecode 对字符串URL解码,编码的逆过程
 *
 * @param str 原字符串
 * @param strLen 原字符串大小（不包括最后的\0）
 * @param result 结果字符串缓存区
 * @param resultSize 结果地址的缓冲区大小(包括最后的\0)
 *
 * @return: >0: result里实际有效的字符串长度
 *           0: 解码失败
 */
int URLDecode(const char *str, const int strLen, char *result, const int resultSize);

#endif
