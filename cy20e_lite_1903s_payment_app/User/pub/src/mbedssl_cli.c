#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif

#if defined(MBEDTLS_PLATFORM_C)
#include "mbedtls/platform.h"
#else
#include <stdio.h>
#include <stdlib.h>

//#define mbedtls_time            time
//#define mbedtls_time_t          time_t
//#define mbedtls_fprintf         fprintf
#define mbedtls_printf          dbg
//#define mbedtls_exit            exit
//#define MBEDTLS_EXIT_SUCCESS    EXIT_SUCCESS
//#define MBEDTLS_EXIT_FAILURE    EXIT_FAILURE
#endif /* MBEDTLS_PLATFORM_C */

#include "mbedtls/net_sockets.h"
#include "mbedtls/mdebug.h"
#include "mbedtls/ssl.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/error.h"
#include "mbedtls/certs.h"

#include <string.h>
#include "mbedssl_cli.h"
#include "debug.h"

#include "SEGGER_RTT.h"
#include "tcpcomm.h"

#define DEBUG_LEVEL		3

mbedtls_net_context server_fd;

mbedtls_x509_crt_profile crt_profile;
mbedtls_entropy_context entropy;
mbedtls_ctr_drbg_context ctr_drbg;
mbedtls_ssl_context ssl;
mbedtls_ssl_config conf;
mbedtls_ssl_session saved_session;

mbedtls_x509_crt cacert;
mbedtls_x509_crt clicert;
mbedtls_pk_context pkey;

/**
 * \brief          Read at most 'len' application data bytes
 *
 * \param ctx      SSL context
 * \param buf      buffer that will hold the data
 * \param len      maximum number of bytes to read
 *
 * \return         The (positive) number of bytes read if successful.
 * \return         \c 0 if the read end of the underlying transport was closed
 *                 - in this case you must stop using the context (see below).
 *
 */
int my_recv_timeout( void *ctx, unsigned char *buf, size_t len, uint32_t timeout )
{
	int ret;
	dbg("my_recv_timeout wait rcv:[%d],time=[%d]\n", len, timeout);
	if(timeout==0)
		timeout=20*1000;
	ret=iCommTcpRecv(buf, len, timeout);
	if(ret==0)
		ret=MBEDTLS_ERR_SSL_TIMEOUT;
	dbg("my_recv_timeout recved:[%d]\n", ret);
	hexdump(buf, len);
	return ret;
}

int my_recv( void *ctx, unsigned char *buf, size_t len )
{
	int ret;
	dbg("my_recv wait rcv:[%d]\n", len);
	ret=iCommTcpRecv(buf, len, 10*1000);
	dbg("my_recv recved:[%d]\n", ret);
	return ret;
}


/**
 * \brief          Try to write exactly 'len' application data bytes
 *
 * \warning        This function will do partial writes in some cases. If the
 *                 return value is non-negative but less than length, the
 *                 function must be called again with updated arguments:
 *                 buf + ret, len - ret (if ret is the return value) until
 *                 it returns a value equal to the last 'len' argument.
 *
 * \param ctx      SSL context
 * \param buf      buffer holding the data
 * \param len      how many bytes must be written
 *
 * \return         The (non-negative) number of bytes actually written if
 *                 successful (may be less than \p len).
 */
int my_send( void *ctx, const unsigned char *buf, size_t len )
{
	return iCommTcpSend((unsigned char *)buf, len);
}

static void my_debug( void *ctx, int level,
                      const char *file, int line,
                      const char *str )
{
    const char *p, *basename;

    /* Extract basename from file */
    for( p = basename = file; *p != '\0'; p++ )
        if( *p == '/' || *p == '\\' )
            basename = p + 1;

    //mbedtls_fprintf( (FILE *) ctx, "%s:%04d: |%d| %s", basename, line, level, str );
	//fflush(  (FILE *) ctx  );	
	
	dbg("%s:%04d: |%d| %s\n", basename, line, level, str );		
}


/**
 * \brief          		init ssl
 *
 * \param psCa     		ca
 * \param psCliCert     client cert
 * \param psCliKey      client privatekey
 *
 * \return         0 is success.
 *
 */
int iSSL_Init(char *psCa, char *psCliCert, char *psCliKey)
{
	const char *pers = "ssl_client";
	int ret;
		
	/*
     * 0. Initialize the RNG and the session data
     */
    //mbedtls_net_init( &server_fd );
    mbedtls_ssl_init( &ssl );
    mbedtls_ssl_config_init( &conf );
    mbedtls_x509_crt_init( &cacert );
	mbedtls_x509_crt_init( &clicert );
    mbedtls_pk_init( &pkey );
    mbedtls_ctr_drbg_init( &ctr_drbg );
	
	mbedtls_entropy_init( &entropy );
	
	mbedtls_debug_set_threshold( DEBUG_LEVEL );
	
    if( ( ret = mbedtls_ctr_drbg_seed( &ctr_drbg, mbedtls_entropy_func, &entropy,
                               (const unsigned char *) pers,
                               strlen( pers ) ) ) != 0 )
    {
        //mbedtls_printf( " failed\n  ! mbedtls_ctr_drbg_seed returned %d\n", ret );
        return -1;
    }
	
	/*
     * 1.1. Load the trusted CA
     */
    //mbedtls_printf( "  . Loading the CA root certificate ..." );
	ret=0;
	if(psCa && psCa[0])
		ret = mbedtls_x509_crt_parse( &cacert, (const unsigned char *) psCa, strlen(psCa)+1 );
    if( ret < 0 )
    {
        //mbedtls_printf( " failed\n  !  mbedtls_x509_crt_parse returned -0x%x\n\n", -ret );
        return -2;
    }
	
	/*
     * 1.2. Load own certificate and private key
     *
     * (can be skipped if client authentication is not required)
     */
    //mbedtls_printf( "  . Loading the client cert. and key...\n" );	 
	if(psCliCert && psCliCert[0])
		ret = mbedtls_x509_crt_parse( &clicert, (const unsigned char *) psCliCert, strlen(psCliCert)+1 );
    if( ret < 0 )
    {
        dbg( "clicert mbedtls_x509_crt_parse returned %d -0x%x\n\n", ret, -ret );
        return -3;
    }
	
	if(psCliKey && psCliKey[0])
		ret = mbedtls_pk_parse_key( &pkey, (const unsigned char *) psCliKey, strlen(psCliKey)+1, NULL, 0 );
    if( ret < 0 )
    {
        dbg( "pkey mbedtls_pk_parse_key returned %d -0x%x\n\n", ret, -ret );
        return -4;
    }
	
	return 0;
}

/**
 * \brief          		use ssl connect to server
 *
 * \param pszSvrName    server ip
 * \param iSvrPort     	server port
 * \param iTimeOut      connect timeout, current no use
 *
 * \return         0 is success.
 *
 */
int iSSL_Connect(char *pszSvrName, int iSvrPort, int iTimeOut)
{
	int ret;
	int flags;
	char szSvrPort[10];
	
	/*
     * 1. Start the connection
     */
	sprintf(szSvrPort, "%d", iSvrPort);
	ret=iCommTcpConn(pszSvrName, szSvrPort, 5*1000);
	if(ret)
		return -1;
	
    /*
     * 2. Setup stuff
     */
    //dbg( "  . Setting up the SSL/TLS structure...\n" );

    if( ( ret = mbedtls_ssl_config_defaults( &conf,
                    MBEDTLS_SSL_IS_CLIENT,
                    MBEDTLS_SSL_TRANSPORT_STREAM,
                    MBEDTLS_SSL_PRESET_DEFAULT ) ) != 0 )
    {
        return -2;
    }
	
	/* OPTIONAL is not optimal for security,
     * but makes interop easier in this simplified example */
	/*MBEDTLS_SSL_VERIFY_NONE MBEDTLS_SSL_VERIFY_OPTIONAL MBEDTLS_SSL_VERIFY_REQUIRED*/
    mbedtls_ssl_conf_authmode( &conf, MBEDTLS_SSL_VERIFY_OPTIONAL );
    mbedtls_ssl_conf_ca_chain( &conf, &cacert, NULL );
	ret = mbedtls_ssl_conf_own_cert( &conf, &clicert, &pkey );
    mbedtls_ssl_conf_rng( &conf, mbedtls_ctr_drbg_random, &ctr_drbg );
    mbedtls_ssl_conf_dbg( &conf, my_debug, stdout );
	
	//mbedtls_ssl_conf_ciphersuites( &conf, opt.force_ciphersuite );	//ָ���㷨

    if( ( ret = mbedtls_ssl_setup( &ssl, &conf ) ) != 0 )
    {
        return -3;
    }

    if( ( ret = mbedtls_ssl_set_hostname( &ssl, pszSvrName ) ) != 0 )
    {
        return -4;
    }

	mbedtls_ssl_set_bio( &ssl, &server_fd, my_send, my_recv, my_recv_timeout );

	/*
     * 4. Handshake
    */
    //dbg( "  . Performing the SSL/TLS handshake...\n" );
    while( ( ret = mbedtls_ssl_handshake( &ssl ) ) != 0 )
    {
        if( ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE )
        {
            return -5;
        }
    }
	
	dbg( "    [ Protocol is %s ]\n    [ Ciphersuite is %s ]\n",
                    mbedtls_ssl_get_version( &ssl ),
                    mbedtls_ssl_get_ciphersuite( &ssl ) );
	
	/*
     * 5. Verify the server certificate
    */
    //dbg( "  . Verifying peer X.509 certificate...\n" );

    /* In real life, we probably want to bail out when ret != 0 */
    if( ( flags = mbedtls_ssl_get_verify_result( &ssl ) ) != 0 )
    {
        char vrfy_buf[512];

        dbg( "verify failed\n" );

        mbedtls_x509_crt_verify_info( vrfy_buf, sizeof( vrfy_buf ), "  ! ", flags );

        dbg( "%s\n", vrfy_buf );
		//return -6;
    }

	/*
	if( mbedtls_ssl_get_peer_cert( &ssl ) != NULL )
    {
		unsigned char buf[512];
        mbedtls_printf( "  . Peer certificate information    ...\n" );
        mbedtls_x509_crt_info( (char *) buf, sizeof( buf ) - 1, "      ",
                       mbedtls_ssl_get_peer_cert( &ssl ) );
        dbg( "%s\n", buf );
    }
	*/
	
	return 0;
}

/**
 * \brief          		 ssl send data to server
 *
 * \param buf    		send data
 * \param len	     	send data length
 *
 * \return         		The length of data send successfully
 *
 */
int iSSL_Send(unsigned char *buf, int len)
{
	int ret;
	
	while( ( ret = mbedtls_ssl_write( &ssl, buf, len ) ) <= 0 )
    {
        if( ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE )
        {
            return -1;
        }
    }
	return ret;
}

/**
 * \brief          		ssl recv data from server
 *
 * \param buf    		recv data buffer
 * \param len	     	The length of data expected to be received or size of the data buf
 *
 * \return         		The length of data received successfully
 *
 */	
int iSSL_Recv(unsigned char *buf, int len, int iTimeOutMs)
{
	int ret;
	
	mbedtls_ssl_conf_read_timeout( &conf, iTimeOutMs );
	
	do
    {
		//memset( buf, 0, len );
        ret = mbedtls_ssl_read( &ssl, buf, len );

		dbg("*** mbedtls_ssl_read=%d\n", ret);
        if( ret == MBEDTLS_ERR_SSL_WANT_READ || ret == MBEDTLS_ERR_SSL_WANT_WRITE )
		{
			dbg("*** mbedtls_ssl_read continue.\n", ret);
            continue;
		}
		break;
        
		//dbg( "\n****** %d bytes read[%d]:\n", len, strlen((char *) buf));
        //dbg( "[%s]\n\n", (char *) buf );
    }while( 1 );
	
	if( ret == MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY )
	{
		dbg("*** peer close\n");
	}else if( ret < 0 )
	{
		dbg( "failed\n  ! mbedtls_ssl_read returned %d\n\n", ret );
	}
	if( ret == 0 )
	{
		dbg( "\n\nEOF\n\n" );
	}
	
	return ret;
}

int iSSL_DisConnect(void)
{
	mbedtls_ssl_close_notify( &ssl );
	iCommTcpDisConn();
	
	return 0;
}

int iSSL_Free(void)
{
    //mbedtls_net_free( &server_fd );

    mbedtls_x509_crt_free( &clicert );
    mbedtls_x509_crt_free( &cacert );
    mbedtls_pk_free( &pkey );

    mbedtls_ssl_session_free( &saved_session );
    mbedtls_ssl_free( &ssl );
    mbedtls_ssl_config_free( &conf );
    mbedtls_ctr_drbg_free( &ctr_drbg );
    mbedtls_entropy_free( &entropy );
	
	return 0;
}


