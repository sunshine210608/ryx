/**拉卡拉一键激活*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "VposFace.h"
#include "Pub.h"
#include "debug.h"
#include "myRsa.h"

//#define USE_WOLFSSL
#ifdef USE_WOLFSSL
#include <wolfssl/ssl.h>
#include <wolfssl/internal.h>
#endif

#define USE_MBEDTLS

#ifdef USE_WOLFSSL
//根据Pem/Der生成rsa公钥, 若为pem, datalen=strlen(pem)+1
//int iParseRsaPubKeyfromData(mbedtls_rsa_context *ctx_rsa, uchar *data, uint dataLen)
int iParseRsaPubKeyfromData(void *ctx_rsa, uchar *data, uint dataLen)
{
    unsigned char* der;
    unsigned char buf[500];
    int derLen;
    if(memcmp("-----BEGIN ", data, 11)==0)
    {
        //pem
        wolfSSL_PubKeyPemToDer(data, dataLen, buf, &derLen);
        der=buf;
    }else
    {
        der=data;
        derLen=dataLen;
    }
    
    //der
    WC_RNG rng;
    RsaKey *pTempRsaKey = malloc(sizeof(RsaKey));
    
    wc_InitRng(&rng);
    wc_InitRsaKey(pTempRsaKey, 0);

    unsigned int idx = 0; // Start of the data...
    int retval = wc_RsaPublicKeyDecode(der, &idx, pTempRsaKey, derLen);
    
    return 0;
}


#endif


#ifdef USE_MBEDTLS
#include "mbedtls/rsa.h"
#include "mbedtls/ssl.h"
#include "mbedtls/sha1.h"

//使用项目中mbedtls的rsa接口

static int myrand(void *rng_state, unsigned char *output, size_t len)
{
    size_t i;

    //if( rng_state != NULL )
    //    rng_state  = NULL;

    for (i = 0; i < len; ++i)
        output[i] = _ucGetRand();
    return (0);
}

//根据Pem/Der生成rsa公钥, 若为pem, datalen=strlen(pem)+1
int iParseRsaPubKeyfromData(mbedtls_rsa_context *ctx_rsa, uchar *data, uint dataLen)
{
    int ret;
    mbedtls_pk_context ctx_pk;

    mbedtls_rsa_init(ctx_rsa, MBEDTLS_RSA_PKCS_V15, 0);

    mbedtls_pk_init(&ctx_pk);
    ret = mbedtls_pk_parse_public_key(&ctx_pk, data, dataLen);
    if (ret != 0)
        return -1;
    if (mbedtls_pk_get_type(&ctx_pk) != MBEDTLS_PK_RSA)
    {
        mbedtls_pk_free(&ctx_pk);
        return -2;
    }

    ret = mbedtls_rsa_copy(ctx_rsa, mbedtls_pk_rsa(ctx_pk));

    mbedtls_pk_free(&ctx_pk);

    return ret;
}

//根据Pem/Der生成rsa密钥, 若为pem, datalen=strlen(pem)+1
int iParseRsaPriKeyfromData(mbedtls_rsa_context *ctx_rsa, uchar *data, uint dataLen, uchar *pwd, uint uiPwdLen)
{
    int ret;
    mbedtls_pk_context ctx_pk;

    mbedtls_rsa_init(ctx_rsa, MBEDTLS_RSA_PKCS_V15, 0);

    mbedtls_pk_init(&ctx_pk);
    ret = mbedtls_pk_parse_key(&ctx_pk, data, dataLen, pwd, uiPwdLen);
    if (ret != 0)
        return -1;
    if (mbedtls_pk_get_type(&ctx_pk) != MBEDTLS_PK_RSA)
    {
        mbedtls_pk_free(&ctx_pk);
        return -2;
    }

    ret = mbedtls_rsa_copy(ctx_rsa, mbedtls_pk_rsa(ctx_pk));

    mbedtls_pk_free(&ctx_pk);

    return ret;
}

//根据NDE生成rsa,公钥必须有N和E,私钥必须有NDE, P和Q可以为NULL. NDE等参数为16进制可见字符串格式
int iGenRsafromParam(mbedtls_rsa_context *ctx_rsa, uchar *RSA_N, uchar *RSA_D, uchar *RSA_E, uchar *RSA_P, uchar *RSA_Q)
{
    mbedtls_mpi K;
    int ret = 1;

    mbedtls_mpi_init(&K);
    mbedtls_rsa_init(ctx_rsa, MBEDTLS_RSA_PKCS_V15, 0);

    if (RSA_N)
    {
        MBEDTLS_MPI_CHK(mbedtls_mpi_read_string(&K, 16, (char *)RSA_N));
        MBEDTLS_MPI_CHK(mbedtls_rsa_import(ctx_rsa, &K, NULL, NULL, NULL, NULL));
    }

    if (RSA_P)
    {
        MBEDTLS_MPI_CHK(mbedtls_mpi_read_string(&K, 16, (char *)RSA_P));
        MBEDTLS_MPI_CHK(mbedtls_rsa_import(ctx_rsa, NULL, &K, NULL, NULL, NULL));
    }

    if (RSA_Q)
    {
        MBEDTLS_MPI_CHK(mbedtls_mpi_read_string(&K, 16, (char *)RSA_Q));
        MBEDTLS_MPI_CHK(mbedtls_rsa_import(ctx_rsa, NULL, NULL, &K, NULL, NULL));
    }

    if (RSA_D)
    {
        MBEDTLS_MPI_CHK(mbedtls_mpi_read_string(&K, 16, (char *)RSA_D));
        MBEDTLS_MPI_CHK(mbedtls_rsa_import(ctx_rsa, NULL, NULL, NULL, &K, NULL));
    }

    if (RSA_E)
    {
        MBEDTLS_MPI_CHK(mbedtls_mpi_read_string(&K, 16, (char *)RSA_E));
        MBEDTLS_MPI_CHK(mbedtls_rsa_import(ctx_rsa, NULL, NULL, NULL, NULL, &K));
    }
    MBEDTLS_MPI_CHK(mbedtls_rsa_complete(ctx_rsa));

    if (mbedtls_rsa_check_pubkey(ctx_rsa) != 0 && mbedtls_rsa_check_privkey(ctx_rsa) != 0)
    {
        ret = 1;
    }
    else
        ret = 0;

cleanup:
    mbedtls_mpi_free(&K);
    if (ret)
        mbedtls_rsa_free(ctx_rsa);

    return (ret);
}

int iGenRsafromParamBin(mbedtls_rsa_context *ctx_rsa, uint rsaLen, uchar *N, uchar *D, uchar *E)
{
    uchar N2[248*2+1], D2[248*2+1], E2[7];
    if(rsaLen<128 || rsaLen>248 || rsaLen%8)
        return 1;
    if(E[0]!=0x03 && memcmp(E, "\x01\x00\x01", 3))
        return 2;

    if(E[0]==0x03)
        strcpy(E2, "03");
    else
        strcpy(E2, "010001");
    vOneTwo0(N, rsaLen, N2);
    if(D && memcmp(D, "\x00\x00", 2))
    {
        vOneTwo0(D, rsaLen, D2);
        return iGenRsafromParam(ctx_rsa, N2, D2, E2, NULL, NULL);
    }else
        return iGenRsafromParam(ctx_rsa, N2, NULL, E2, NULL, NULL);
}

//取rsa的长度,例如1024位的rsa,长度为1024/8=128
int iGetRsaLen(const mbedtls_rsa_context *ctx)
{
    return (ctx->len);
}

//从pem/der中取得rsa的NDE等参数, 若为pem, datalen=strlen(pem)+1. 取出的NDE等参数为16进制可见字符串格式.
//ucMode: 0-公钥  1-私钥
//注:NDE需有足够长度空间, N/D需为密钥长度的两倍+5, 例如1024位的密钥, 长度为128，则N、N需有2*128+5的空间
int iGetRsaParamfromData(uchar ucMode, uchar *data, uint dataLen, uchar *pwd, uint uiPwdLen, uchar *N, uchar *D, uchar *E, uchar *P, uchar *Q)
{
    int ret;
    uint olen;
    mbedtls_pk_context ctx_pk;
    mbedtls_rsa_context *ctx_rsa;
    uint len;

    mbedtls_pk_init(&ctx_pk);
    if (ucMode)
        ret = mbedtls_pk_parse_key(&ctx_pk, data, dataLen, pwd, uiPwdLen);
    else
        ret = mbedtls_pk_parse_public_key(&ctx_pk, data, dataLen);
    if (ret != 0)
        return -1;
    if (mbedtls_pk_get_type(&ctx_pk) != MBEDTLS_PK_RSA)
    {
        mbedtls_pk_free(&ctx_pk);
        return -2;
    }

    ctx_rsa = mbedtls_pk_rsa(ctx_pk);
    len = mbedtls_rsa_get_len(ctx_rsa);
    if (N)
        mbedtls_mpi_write_string(&ctx_rsa->N, 16, (char *)N, len * 2 + 5, &olen);
    if (D)
        mbedtls_mpi_write_string(&ctx_rsa->D, 16, (char *)D, len * 2 + 5, &olen);
    if (E)
        mbedtls_mpi_write_string(&ctx_rsa->E, 16, (char *)E, 10, &olen);
    if (P)
        mbedtls_mpi_write_string(&ctx_rsa->P, 16, (char *)P, len + 5, &olen);
    if (Q)
        mbedtls_mpi_write_string(&ctx_rsa->Q, 16, (char *)Q, len + 5, &olen);

    mbedtls_pk_free(&ctx_pk);

    return 0;
}

int iGetRsaParamBin(mbedtls_rsa_context *ctx_rsa, uint *puiRsaLen, uchar *N, uchar *D, uchar *E)
{
    int ret;

    *puiRsaLen = mbedtls_rsa_get_len(ctx_rsa);
    if(*puiRsaLen!=128 && *puiRsaLen!=192)
        return 1;
#if 0
    uchar tmp[192*2+10];
		uint olen, len;
		len=*puiRsaLen;
    if (N)
    {
        mbedtls_mpi_write_string(&ctx_rsa->N, 16, (char *)tmp, len * 2 + 5, &olen);
        vTwoOne(tmp, olen-1, N);
    }
    if (D)
    {
        mbedtls_mpi_write_string(&ctx_rsa->D, 16, (char *)tmp, len * 2 + 5, &olen);
        if(olen>=(*puiRsaLen)*2)
            vTwoOne(tmp, olen-1, D);
        else
            memset(D, 0x00, 3);
    }
    if (E)
    {
        mbedtls_mpi_write_string(&ctx_rsa->E, 16, (char *)tmp, 10, &olen);
        vTwoOne(tmp, olen-1, E);
    }
    ret=0;
#else
    ret=mbedtls_rsa_export_raw(ctx_rsa, N, *puiRsaLen, NULL, 0, NULL, 0, D, *puiRsaLen, E, 3);
    if(ret==MBEDTLS_ERR_RSA_BAD_INPUT_DATA)
    {
        memset(D, 0x00, 3);
        ret=mbedtls_rsa_export_raw(ctx_rsa, N, *puiRsaLen, NULL, 0, NULL, 0, NULL, 0, E, 3);
    }
#endif
    return ret;  
}

//公钥加密, 源数据长度等于N的长度
int iRsaPubKeyEnc(mbedtls_rsa_context *ctx_rsa, uchar *psData, uchar *psOut)
{
    return mbedtls_rsa_public(ctx_rsa, psData, psOut);
}

//私钥解密, 源数据长度等于N的长度
int iRsaPriKeyDec(mbedtls_rsa_context *ctx_rsa, uchar *psData, uchar *psOut)
{
    return mbedtls_rsa_private(ctx_rsa, myrand, NULL, psData, psOut);
}

//公钥加密(源数据做pkcs#1填充, 原始数据编码后再加密)
int iRsaPubKeyEncPkcs1(mbedtls_rsa_context *ctx_rsa, uchar *psData, uint uiDataLen, uchar *psOut, uint *puiOutLen)
{
    int ret, i, len, pksize;

    pksize= ctx_rsa->len -11;
    //处理长度超过117的情况
    for(i=0; i*pksize<uiDataLen; i++)
    {
        len=((i+1)*pksize<uiDataLen)?pksize:uiDataLen-i*pksize;
        ret = mbedtls_rsa_pkcs1_encrypt(ctx_rsa, myrand, NULL, MBEDTLS_RSA_PUBLIC,
                                     len, psData+i*pksize, psOut+i*ctx_rsa->len);
        if(ret)
            return ret;
    }
    if(puiOutLen)
        *puiOutLen=i*ctx_rsa->len;
    return 0;

    //return mbedtls_rsa_pkcs1_encrypt(ctx_rsa, myrand, NULL, MBEDTLS_RSA_PUBLIC,
    //                                 uiDataLen, psData, psOut);
}

//私钥解密(源数据为pkcs#1填充, 解密并解码得到原始数据)
int iRsaPriKeyDecPkcs1(mbedtls_rsa_context *ctx_rsa, uchar *psData, uchar *psOut, uint uiOutSize, uint *puiOutLen)
{
    return mbedtls_rsa_pkcs1_decrypt(ctx_rsa, myrand, NULL, MBEDTLS_RSA_PRIVATE,
                                     puiOutLen, psData, psOut, uiOutSize);
}

//签名
int iRsaPriKeySignPkcs1(mbedtls_rsa_context *ctx_rsa, uchar *psData, uint uiDataLen, uchar *psOut)
{
    /* 若使用 MBEDTLS_MD_SHA1/MBEDTLS_MD_MD5 则数据长度可填0，使用固定长度20/16 */
    return mbedtls_rsa_pkcs1_sign(ctx_rsa, myrand, NULL,
                                  MBEDTLS_RSA_PRIVATE, /* MBEDTLS_MD_NONE */MBEDTLS_MD_SHA1, uiDataLen,
                                  psData, psOut);
}

//验签
int iRsaPubKeyVerifyPkcs1(mbedtls_rsa_context *ctx_rsa, uchar *psData, uint uiDataLen, uchar *psSignData)
{
    /* 若使用 MBEDTLS_MD_SHA1/MBEDTLS_MD_MD5 则数据长度可填0，使用固定长度20/16 */
    return mbedtls_rsa_pkcs1_verify(ctx_rsa, NULL, NULL,
                                    MBEDTLS_RSA_PUBLIC, /* MBEDTLS_MD_NONE */MBEDTLS_MD_SHA1, uiDataLen,
                                    psData, psSignData);
}

//根据pem生成rsa或者根据NDE参数生成rsa，使用完毕后需调用本接口做资源释放
int iRsaFree(mbedtls_rsa_context *ctx_rsa)
{
    mbedtls_rsa_free(ctx_rsa);
    return 0;
}

#if 0   //for test
#define mbedtls_printf dbg
static void dump_rsa_key(mbedtls_rsa_context *ctx)
{
    size_t olen;
    char buf[516];
    mbedtls_printf("\n  +++++++++++++++++ rsa keypair +++++++++++++++++\n\n");
    mbedtls_printf("len: %d\n", mbedtls_rsa_get_len(ctx));
    mbedtls_mpi_write_string(&ctx->N, 16, buf, sizeof(buf), &olen);
    mbedtls_printf("N: %s\n", buf);

    mbedtls_mpi_write_string(&ctx->E, 16, buf, sizeof(buf), &olen);
    mbedtls_printf("E: %s\n", buf);

    mbedtls_mpi_write_string(&ctx->D, 16, buf, sizeof(buf), &olen);
    mbedtls_printf("D: %s\n", buf);
    if (olen > 100)
        _vDelay(20); //日志较长时, 稍延时使usb口日志正常输出

    mbedtls_mpi_write_string(&ctx->P, 16, buf, sizeof(buf), &olen);
    mbedtls_printf("P: %s\n", buf);

    mbedtls_mpi_write_string(&ctx->Q, 16, buf, sizeof(buf), &olen);
    mbedtls_printf("Q: %s\n", buf);

    mbedtls_mpi_write_string(&ctx->DP, 16, buf, sizeof(buf), &olen);
    mbedtls_printf("DP: %s\n", buf);
    if (olen > 100)
        _vDelay(20);

    mbedtls_mpi_write_string(&ctx->DQ, 16, buf, sizeof(buf), &olen);
    mbedtls_printf("DQ: %s\n", buf);

    mbedtls_mpi_write_string(&ctx->QP, 16, buf, sizeof(buf), &olen);
    mbedtls_printf("QP: %s\n", buf);
    mbedtls_printf("\n  +++++++++++++++++ rsa keypair +++++++++++++++++\n\n");
}

int iRsaTest(void)
{
    uchar prikey[] = "-----BEGIN RSA PRIVATE KEY-----\r\n"
                     "MIICWwIBAAKBgQCJp3wJCTzOetUSScKDEiRk502YSWBwZwGZ93MUwyMwJXdj71PQ\r\n"
                     "afYXSviY3s6zVIUhNQD0q4L6DvsaAF8iOVCkK+xxHxtd3MvntPtoNJAifi5h8aYj\r\n"
                     "GmO3sCqyudKouPPP8fp1ttsmPa2gsyOJvmpKcDENNqSg/dIfO2kkWIKypQIDAQAB\r\n"
                     "AoGAV5UqxA0OlUlt/D7V9dM7OMA6jKRTKfFAAZiBrjoiT2XnOrxxlW911ogq+Aj7\r\n"
                     "dvpV4jT5MLNXmIajmX2XCg1xkqufLPpeXjOh3KEJ0QV4zdV4tE9TSekrbgEsETaI\r\n"
                     "cF7VOGJTmZgxsQAYx39JG9gSWjf5U+mKMxc541+uKK3jJpkCQQD1gVJKRw2JQhIk\r\n"
                     "jAMTIvPBoFzKKJtNPKYoQ2rrGFivJjvmBaAM+yRt/qzKLKljeGOqMrZ8FVHo7tp4\r\n"
                     "trVUlswrAkEAj4nmTX8ufG0jHIIxhvw44Ipf6zEZj076wykLD4/SlA1b6TUjlfzh\r\n"
                     "IGF9ED2LyL2OJ9I78YTUzQbDSJmN/o6EbwJAC2zrUX2hCaRDh+PkOL3FrlsyNk5R\r\n"
                     "e/S4nuMUVMCbT/ttijT0z+XYOi5CCF6vu2tc6AzYJUowt7B2BmwqYDdsZQJAbsDw\r\n"
                     "uwtPSyLK3Mx4erdTC665pDgWiMVgKAYbSr6mtSY5rRAbjTe6XcnbPhS/Lv1UGtXU\r\n"
                     "+t6LdEerlvEk5y4AcQJAVWwlB4KuiEsbXuV2xErlsCEHcaRdybXGct52MmerUUl8\r\n"
                     "3pUTMbzRnmBMtDrAtDybr4aqsbtVhPu47EASGPbfSg==\r\n"
                     "-----END RSA PRIVATE KEY-----\r\n";

    uchar pubkey[] = "-----BEGIN PUBLIC KEY-----\r\n"
                     "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCJp3wJCTzOetUSScKDEiRk502Y\r\n"
                     "SWBwZwGZ93MUwyMwJXdj71PQafYXSviY3s6zVIUhNQD0q4L6DvsaAF8iOVCkK+xx\r\n"
                     "Hxtd3MvntPtoNJAifi5h8aYjGmO3sCqyudKouPPP8fp1ttsmPa2gsyOJvmpKcDEN\r\n"
                     "NqSg/dIfO2kkWIKypQIDAQAB\r\n"
                     "-----END PUBLIC KEY-----\r\n";

    int ret;
    mbedtls_rsa_context ctx_pubk, ctx_prik;
    uchar sBuf[] = "12345678AABBCC";
    uchar sOut[260], sBuf2[260], szTmp[520];
    uint uiOutLen;
    uchar N[192 * 2 + 1], D[192 * 2 + 1], E[10 + 1], P[192 + 1], Q[192 + 1];

    _vDisp(2, "test rsa?");
    if (_uiGetKey() == _KEY_ESC)
        return 0;

    ret = iGetRsaParamfromData(0, pubkey, strlen((char*)pubkey)+1, NULL, 0, N, D, E, P, Q);
    if (ret == 0)
    {
        dbg("parse 0:\n");
        dbg("N:%s.\n", N);
        _vDelay(20);
        dbg("D:%s.\n", D);
        _vDelay(20);
        dbg("E:%s.\n", E);
        dbg("P:%s.\n", P);
        dbg("Q:%s.\n", Q);
    }
    else
        dbg("parse0 err. ret=%d\n", ret);

    ret = iGetRsaParamfromData(1, prikey, strlen((char*)prikey)+1,NULL, 0, N, D, E, P, Q);
    if (ret == 0)
    {
        dbg("parse 1:\n");
        dbg("N:%s.\n", N);
        _vDelay(20);
        dbg("D:%s.\n", D);
        _vDelay(20);
        dbg("E:%s.\n", E);
        dbg("P:%s.\n", P);
        dbg("Q:%s.\n", Q);
    }
    else
        dbg("parse1 err. ret=%d\n", ret);

    {
        mbedtls_rsa_context ctx_pubk2, ctx_prik2;
        ret = iGenRsafromParam(&ctx_pubk2, N, NULL, E, NULL, NULL);
        dbg("GenRsa 1 ret:%d\n", ret);
        if (ret == 0)
        {
            dump_rsa_key(&ctx_pubk2);
            iRsaFree(&ctx_pubk2);
        }

        ret = iGenRsafromParam(&ctx_prik2, N, D, E, NULL, NULL);
        dbg("GenRsa 2 ret:%d\n", ret);
        if (ret == 0)
        {
            dump_rsa_key(&ctx_prik2);
            iRsaFree(&ctx_prik2);
        }
    }

    dbg("parse pub key:\n");
    ret = iParseRsaPubKeyfromData(&ctx_pubk, pubkey, strlen((char*)pubkey)+1);
    if (ret)
        return 1;
    dump_rsa_key(&ctx_pubk);

    dbg("parse pri key:\n");
    ret = iParseRsaPriKeyfromData(&ctx_prik, prikey, strlen((char*)prikey)+1, NULL, 0);
    if (ret)
        return 2;
    dump_rsa_key(&ctx_prik);

    dbg("parse end.\n");

    ret=iGetRsaParamBin(&ctx_pubk, &uiOutLen, N, D, E);
    if(ret==0)
    {   
        dbg("iGetRsaParamBin 1:\n");
        dbg("rsa len:%d.\n", uiOutLen);
        vOneTwo0(N, uiOutLen, szTmp);
        dbg("N:%s.\n", szTmp);
        _vDelay(20);
        if(memcmp(D, "\x00\x00", 2))
            vOneTwo0(D, uiOutLen, szTmp);
        else
            szTmp[0]=0;
        dbg("D:%s.\n", szTmp);
        _vDelay(20);
        if(E[0]==0x03)
            vOneTwo0(E, 1, szTmp);
        else
            vOneTwo0(E, 3, szTmp);
        dbg("E:%s.\n", szTmp);
    }

    //公钥加密
    ret = iRsaPubKeyEncPkcs1(&ctx_pubk, sBuf, strlen((char *)sBuf), sOut, NULL);
    if (ret == 0)
    {
        vOneTwo0(sOut, 128, szTmp);
        dbg("enc1:[%s]\n", szTmp);
    }
    uiOutLen = 0;
    ret = iRsaPriKeyDecPkcs1(&ctx_prik, sOut, sBuf2, sizeof(sBuf2), &uiOutLen);
    if (ret == 0)
    {
        sBuf2[uiOutLen] = 0;
        dbg("dec out:[%s]\n", sBuf2);
    }

    //用rsa密钥中的公钥加密(私钥里面含有公钥信息)
    ret = iRsaPubKeyEncPkcs1(&ctx_prik, sBuf, strlen((char *)sBuf), sOut, NULL);
    if (ret == 0)
    {
        vOneTwo0(sOut, 128, szTmp);
        dbg("enc2:[%s]\n", szTmp);
    }

    uiOutLen = 0;
    ret = iRsaPriKeyDecPkcs1(&ctx_prik, sOut, sBuf2, sizeof(sBuf2), &uiOutLen);
    if (ret == 0)
    {
        sBuf2[uiOutLen] = 0;
        dbg("dec out:[%s]\n", sBuf2);
    }

    //此方式解密出来含有Padding信息，还需做PKCS#1解码
    ret = iRsaPriKeyDec(&ctx_prik, sOut, sBuf2);
    if (ret == 0)
    {
        vOneTwo0(sBuf2, 128, szTmp);
        dbg("dec out raw:[%s]\n", szTmp);
    }

    iRsaFree(&ctx_pubk);
    iRsaFree(&ctx_prik);

    dbg("rsatest end.\n");
    return 0;
}

#define KEY_LEN 128

#define RSA_N "9292758453063D803DD603D5E777D788" \
              "8ED1D5BF35786190FA2F23EBC0848AEA" \
              "DDA92CA6C3D80B32C4D109BE0F36D6AE" \
              "7130B9CED7ACDF54CFC7555AC14EEBAB" \
              "93A89813FBF3C4F8066D2D800F7C38A8" \
              "1AE31942917403FF4946B0A83D3D3E05" \
              "EE57C6F5F5606FB5D4BC6CD34EE0801A" \
              "5E94BB77B07507233A0BC7BAC8F90F79"

#define RSA_E "10001"

#define RSA_D "24BF6185468786FDD303083D25E64EFC" \
              "66CA472BC44D253102F8B4A9D3BFA750" \
              "91386C0077937FE33FA3252D28855837" \
              "AE1B484A8A9A45F7EE8C0C634F99E8CD" \
              "DF79C5CE07EE72C7F123142198164234" \
              "CABB724CF78B8173B9F880FC86322407" \
              "AF1FEDFDDE2BEB674CA15F3E81A1521E" \
              "071513A1E85B5DFA031F21ECAE91A34D"

#define RSA_P "C36D0EB7FCD285223CFB5AABA5BDA3D8" \
              "2C01CAD19EA484A87EA4377637E75500" \
              "FCB2005C5C7DD6EC4AC023CDA285D796" \
              "C3D9E75E1EFC42488BB4F1D13AC30A57"

#define RSA_Q "C000DF51A7C77AE8D7C7370C1FF55B69" \
              "E211C2B9E5DB1ED0BF61D0D9899620F4" \
              "910E4168387E3C30AA1E00C339A79508" \
              "8452DD96A9A5EA5D9DCA68DA636032AF"

#define PT_LEN 24
#define RSA_PT "\xAA\xBB\xCC\x03\x02\x01\x00\xFF\xFF\xFF\xFF\xFF" \
               "\x11\x22\x33\x0A\x0B\x0C\xCC\xDD\xDD\xDD\xDD\xDD"

int iRsaTest2(void)
{
    int verbose = 1;
    int ret = 0;

    size_t len;
    mbedtls_rsa_context rsa;
    unsigned char rsa_plaintext[PT_LEN];
    unsigned char rsa_decrypted[PT_LEN];
    unsigned char rsa_ciphertext[KEY_LEN];

    unsigned char sha1sum[20];

    mbedtls_mpi K;

    _vDisp(2, "test rsa2?");
    if (_uiGetKey() == _KEY_ESC)
        return 0;

    mbedtls_mpi_init(&K);
    mbedtls_rsa_init(&rsa, MBEDTLS_RSA_PKCS_V15, 0);

    MBEDTLS_MPI_CHK(mbedtls_mpi_read_string(&K, 16, RSA_N));
    MBEDTLS_MPI_CHK(mbedtls_rsa_import(&rsa, &K, NULL, NULL, NULL, NULL));
    //MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &K, 16, RSA_P  ) );
    //MBEDTLS_MPI_CHK( mbedtls_rsa_import( &rsa, NULL, &K, NULL, NULL, NULL ) );
    //MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &K, 16, RSA_Q  ) );
    //MBEDTLS_MPI_CHK( mbedtls_rsa_import( &rsa, NULL, NULL, &K, NULL, NULL ) );
    MBEDTLS_MPI_CHK(mbedtls_mpi_read_string(&K, 16, RSA_D));
    MBEDTLS_MPI_CHK(mbedtls_rsa_import(&rsa, NULL, NULL, NULL, &K, NULL));
    MBEDTLS_MPI_CHK(mbedtls_mpi_read_string(&K, 16, RSA_E));
    MBEDTLS_MPI_CHK(mbedtls_rsa_import(&rsa, NULL, NULL, NULL, NULL, &K));

    MBEDTLS_MPI_CHK(mbedtls_rsa_complete(&rsa));

    dump_rsa_key(&rsa);

    if (verbose != 0)
        mbedtls_printf("  RSA key validation: ");

    if (mbedtls_rsa_check_pubkey(&rsa) != 0 ||
        mbedtls_rsa_check_privkey(&rsa) != 0)
    {
        if (verbose != 0)
            mbedtls_printf("failed\n");

        ret = 1;
        goto cleanup;
    }

    if (verbose != 0)
        mbedtls_printf("passed\n  PKCS#1 encryption : ");

    memcpy(rsa_plaintext, RSA_PT, PT_LEN);

    if (mbedtls_rsa_pkcs1_encrypt(&rsa, myrand, NULL, MBEDTLS_RSA_PUBLIC,
                                  PT_LEN, rsa_plaintext,
                                  rsa_ciphertext) != 0)
    {
        if (verbose != 0)
            mbedtls_printf("failed\n");

        ret = 1;
        goto cleanup;
    }

    if (verbose != 0)
        mbedtls_printf("passed\n  PKCS#1 decryption : ");

    if (mbedtls_rsa_pkcs1_decrypt(&rsa, myrand, NULL, MBEDTLS_RSA_PRIVATE,
                                  &len, rsa_ciphertext, rsa_decrypted,
                                  sizeof(rsa_decrypted)) != 0)
    {
        if (verbose != 0)
            mbedtls_printf("failed\n");

        ret = 1;
        goto cleanup;
    }

    if (memcmp(rsa_decrypted, rsa_plaintext, len) != 0)
    {
        if (verbose != 0)
            mbedtls_printf("failed\n");

        ret = 1;
        goto cleanup;
    }

    if (verbose != 0)
        mbedtls_printf("passed\n");

    if (verbose != 0)
        mbedtls_printf("  PKCS#1 data sign  : ");

    if (mbedtls_sha1_ret(rsa_plaintext, PT_LEN, sha1sum) != 0)
    {
        if (verbose != 0)
            mbedtls_printf("failed\n");

        return (1);
    }

    if (mbedtls_rsa_pkcs1_sign(&rsa, myrand, NULL,
                               MBEDTLS_RSA_PRIVATE, MBEDTLS_MD_SHA1, 0,
                               sha1sum, rsa_ciphertext) != 0)
    {
        if (verbose != 0)
            mbedtls_printf("failed\n");

        ret = 1;
        goto cleanup;
    }

    if (verbose != 0)
        mbedtls_printf("passed\n  PKCS#1 sig. verify: ");

    if (mbedtls_rsa_pkcs1_verify(&rsa, NULL, NULL,
                                 MBEDTLS_RSA_PUBLIC, MBEDTLS_MD_SHA1, 0,
                                 sha1sum, rsa_ciphertext) != 0)
    {
        if (verbose != 0)
            mbedtls_printf("failed\n");

        ret = 1;
        goto cleanup;
    }

    if (verbose != 0)
        mbedtls_printf("passed\n");

    if (verbose != 0)
        mbedtls_printf("\n");

cleanup:
    mbedtls_mpi_free(&K);
    mbedtls_rsa_free(&rsa);
    return (ret);
}
#endif
#endif      //USE_MBEDTLS

