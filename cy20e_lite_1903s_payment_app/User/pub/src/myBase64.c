#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif
#include "mbedtls/md5.h"

#include <stdio.h>
#include "myBase64.h"
#include "sys_littlefs.h"

//使用项目中mbedtls的base64接口
extern int mbedtls_base64_encode(unsigned char *dst, size_t dlen, size_t *olen,
                                 const unsigned char *src, size_t slen);
extern int mbedtls_base64_decode(unsigned char *dst, size_t dlen, size_t *olen,
                                 const unsigned char *src, size_t slen);

//使用项目中mbedtls的md5接口
extern int mbedtls_md5_ret(const unsigned char *input,
                           size_t ilen,
                           unsigned char output[16]);

//使用项目中mbedtls的sha1接口
extern int mbedtls_sha1_ret( const unsigned char *input,
                      size_t ilen,
                      unsigned char output[20] );

/**
 * \brief          Encode a buffer into base64 format
 *
 * \param dst      destination buffer
 * \param dlen     size of the destination buffer
 * \param olen     number of bytes written
 * \param src      source buffer
 * \param slen     amount of data to be encoded
 *
 * \return         0 if successful, or MBEDTLS_ERR_BASE64_BUFFER_TOO_SMALL.
 *                 *olen is always updated to reflect the amount
 *                 of data that has (or would have) been written.
 *                 If that length cannot be represented, then no data is
 *                 written to the buffer and *olen is set to the maximum
 *                 length representable as a size_t.
 */
int iBase64_encode(unsigned char *dst, size_t dlen, size_t *olen,
                   const unsigned char *src, size_t slen)
{
    return mbedtls_base64_encode(dst, dlen, olen, src, slen);
}

/**
 * \brief          Decode a base64-formatted buffer
 *
 * \param dst      destination buffer (can be NULL for checking size)
 * \param dlen     size of the destination buffer
 * \param olen     number of bytes written
 * \param src      source buffer
 * \param slen     amount of data to be decoded
 *
 * \return         0 if successful, MBEDTLS_ERR_BASE64_BUFFER_TOO_SMALL, or
 *                 MBEDTLS_ERR_BASE64_INVALID_CHARACTER if the input data is
 *                 not correct. *olen is always updated to reflect the amount
 *                 of data that has (or would have) been written.
 */
int iBase64_decode(unsigned char *dst, size_t dlen, size_t *olen,
                   const unsigned char *src, size_t slen)
{
    return mbedtls_base64_decode(dst, dlen, olen, src, slen);
}

/**
 * \brief          Output = MD5( input buffer )
 *
 * \param input    buffer holding the data
 * \param ilen     length of the input data
 * \param output   MD5 checksum result
 *
 * \return         0 if successful
 */
int iMd5(const unsigned char *input, size_t ilen, unsigned char output[16])
{
    return mbedtls_md5_ret(input, ilen, output);
}

extern void vOneTwo0(const unsigned char *psIn, int iLength, unsigned char *pszOut);
//计算MD5, 并转为可见字符
int iMd5Str(const unsigned char *input, size_t ilen, unsigned char output[32+1])
{
    unsigned char sTmp[16];
    mbedtls_md5_ret(input, ilen, sTmp);
    vOneTwo0(sTmp, 16, output);
    return 0;
}

int iSha1( const unsigned char *input,
                      size_t ilen,
                      unsigned char output[20] )
{
    return mbedtls_sha1_ret(input, ilen, output);
}

/*
mbedtls_md5_context ctxMd5;
int iMd5_Init(void)
{
    mbedtls_md5_init(&ctxMd5);
    return mbedtls_md5_starts_ret( &ctxMd5 );
}
int iMd5_Update(unsigned char *input, unsigned int ilen)
{
    return mbedtls_md5_update_ret( &ctxMd5, input, ilen );
}
int iMd5_Finish(unsigned char *output)
{
    return mbedtls_md5_finish_ret( &ctxMd5, output );
}
int iMd5_Free(void)
{
    mbedtls_md5_free( &ctxMd5 );
    return 0;
}
*/

//计算文件md5值
//输入参数： pszFileName     文件名
//          psHead          文件内容之外需要起始计算的数据部分
//          uiHeadLen       起始数据长度,可为0
//输出参数： psMd5           16字节的md5
//返回值：0-成功 其它-失败
int iMd5File(char *pszFileName, unsigned char *psHead, unsigned int uiHeadLen, unsigned char *psMd5)
{
    lfs_file_t file;
    mbedtls_md5_context ctxMd5;
    int ret;
    unsigned char buf[1024];
    int len;

    ret = sys_lfs_file_open(&file, pszFileName, LFS_O_RDONLY);
    if(ret)
        return ret;

    mbedtls_md5_init(&ctxMd5);
    ret=mbedtls_md5_starts_ret(&ctxMd5);
    if(ret==0 && uiHeadLen && psHead)
    {
        ret=mbedtls_md5_update_ret(&ctxMd5, psHead, uiHeadLen);
    }
    while(ret==0 && (len=sys_lfs_file_read(&file, buf, sizeof(buf)))>0)
    {
        ret=mbedtls_md5_update_ret(&ctxMd5, buf, len);
    }
    if(ret==0)
        ret=mbedtls_md5_finish_ret(&ctxMd5, psMd5);

    mbedtls_md5_free( &ctxMd5 );
    sys_lfs_file_close(&file);
    return ret;
}
