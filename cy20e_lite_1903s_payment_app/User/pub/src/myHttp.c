#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "VposFace.h"
#include "Pub.h"
//#include "AppGlobal.h"
#include "tcpcomm.h"
#include "debug.h"
#include "sys_littlefs.h"
#include "myBase64.h"
#include "MemMana_cjt.h"
#ifdef APP_LKL
#include "TMS_lkl.h"
#endif

#define PRT_HTTP_LOG

int iParseHttpUrl(char *pszUrlAddr, char *pszHostName, char *pszSubUrl, int *piPort);

extern void serialHex(unsigned char *head, unsigned char *in, unsigned int len);

uchar sg_cNoDisp=0;
//static uchar sg_cBKSvrFlag=0;          //银行卡服务器:0-使用正常服务器 1-使用备用服务器
void vSetCommNoDisp(uchar disp)
{
    sg_cNoDisp=disp;
}

#ifndef APP_LKL
void vPrtLklDebugLog(char cPrtHeadTail, char cTxtType, char *title, char *psLog, int len)
{}
#endif

void vReplaceStr(char *pszString, const char *pszSrc, const char *pszDest)
{
	char *p0, *p1;
	int iLen, iRemainLen;

	if (NULL == pszString || NULL == pszSrc || NULL == pszDest)
	{
		return;
	}
	p0 = pszString;
	while (1)
	{
		p1 = strstr(p0, pszSrc);
		if (NULL != p1)
		{
			if (strlen(pszSrc) == strlen(pszDest))
			{
				memcpy(p1, pszDest, strlen(pszDest));
			}
			else
			{
				iLen = strlen(p0) - strlen(pszSrc) + strlen(pszDest);
				iRemainLen = strlen(p1 + strlen(pszSrc));
				memmove(p1 + strlen(pszDest), p1 + strlen(pszSrc), iRemainLen);
				memcpy(p1, pszDest, strlen(pszDest));
				p0[iLen] = 0;
			}
			p0 = p1 + strlen(pszDest);
			continue;
		}
		else
		{
			break;
		}
	}
}

uchar sg_ucLinkStaus=0;	//连接状态: 0-短链接 1-长链接 2-长链接且链接已通
int iSetConnStatus(char cLinkFlag, char *pszUrl)
{
	uchar szHostName[256 + 1], szSubUrl[256 + 1];
	uint uiPort=0;
	char szHostIp[20], szHostPort[10];
	int iRet;

	if(sg_ucLinkStaus==2 && cLinkFlag==0)	//长链接改短链接,若已链接则先挂断
		iCommTcpDisConn();

	if(cLinkFlag==0 || cLinkFlag==1)
		sg_ucLinkStaus=cLinkFlag;
	
	if(cLinkFlag && pszUrl)
	{
		iRet = iParseHttpUrl(pszUrl, szHostName, szSubUrl, &uiPort);
		if (iRet)
			return -1*COM_PARAMETER;
        if(!sg_cNoDisp)
            vDispMid(3, "解析域名...");
		if( iGetHostByName(szHostName, szHostIp) )
		{
			vMessage("解析域名失败");
			return -1*COM_PARAMETER;
		}
		sprintf(szHostPort, "%d", uiPort);
        if(!sg_cNoDisp)
            vDispMid(3, "连接服务器...");
		iRet=iCommTcpConn(szHostIp, szHostPort, 10);
		if(iRet)
		{
            if(!sg_cNoDisp)
                _vDisp(3, "");
			return -1*COM_ERR_NO_CONNECT;
		}
		sg_ucLinkStaus=2;
	}
	return 0;
}

int iParseHttpUrl(char *pszUrlAddr, char *pszHostName, char *pszSubUrl, int *piPort)
{
	char *p0, *p1;

	*piPort = 80;
	vReplaceStr(pszUrlAddr, "\\", "/");

	p0 = strstr(pszUrlAddr, "://"); //搜索"http://"
	if (NULL == p0)
		p0 = pszUrlAddr;
	else
		p0 += 3;

	p1 = strstr(p0, "/");
	if (NULL == p1)
	{
        if(pszSubUrl)
            pszSubUrl[0] = 0;
		strcpy(pszHostName, p0);
	}
	else
	{
		vMemcpy0((uchar *)pszHostName, (uchar *)p0, p1 - p0);
        if(pszSubUrl)
            strcpy(pszSubUrl, p1);
	}
	p1 = strstr(pszHostName, ":");
	if (p1)
	{
		*p1 = 0;
		*piPort = atoi(p1 + 1);
	}
	dbg("HttpAddr:%s|%s|%s|%d\n", pszUrlAddr, pszHostName, pszSubUrl, *piPort);
	return 0;
}

//type:0-银行卡http  1-扫码http 3-查询费用
//Host: 服务器地址和端口,格式: xxx.xxx.xxx.xxx:nnnn	当端口为80时可省略端口部分
int iHttpSend(char cReqHeadType, uchar *pszHostName, uint uiPort, uchar *pszSubUrl, uchar *pszBody, int iLen)
{
	char szHttpHead[300];
	int iRet, iBodyLen;
	char szUrlPort[10];
	char szTmp[50];

	if (uiPort == 80)
		szUrlPort[0] = 0;
	else
		sprintf(szUrlPort, ":%u", uiPort);
    
    //dbg("iHttpSend iLen:%d\n", iLen);
    
    iBodyLen = (iLen<0)?strlen((char *)pszBody):iLen;
	if (uiPort == 80 || uiPort == 0)
		szUrlPort[0] = 0;
	else
		sprintf(szUrlPort, ":%u", uiPort);
	sprintf(szHttpHead, "POST %s HTTP/1.1\r\n"
						"Host: %s%s\r\n"
						"Content-Length: %d\r\n",
			pszSubUrl, pszHostName, szUrlPort, iBodyLen);
    if(cReqHeadType==1)     //扫码应用
    {
        //application/octet-stream
        strcat(szHttpHead, "X-V: 2.0\r\n");
    }else if(cReqHeadType==2)     //查询费用
    {
        //application/octet-stream
        strcat(szHttpHead, "Content-Type: application/json;charset=utf-8\r\n");
    }else if(cReqHeadType==0)
    {
        strcat(szHttpHead, "Content-Type: application/x-www-form-urlencoded\r\n");
    }
    strcat(szHttpHead, "\r\n");     //head与body分隔行
    
#ifdef PRT_HTTP_LOG
	dbgLTxt("\n*** Http Req Head(%d):[%s]\n", strlen(szHttpHead), szHttpHead);
	sprintf(szTmp, "*** Http Req Body(%d):", iBodyLen);
    if(cReqHeadType==0 || cReqHeadType==2)
        dbgLTxt2(szTmp, pszBody);
    else
    {
        dbgHex("body sign", pszBody, 16);
        dbgLTxt2(szTmp, pszBody+16);
    }
#endif

	iRet = iCommTcpSend((uchar *)szHttpHead, strlen(szHttpHead));
	{
		ulong tick=0;
		tick = get_tick();
		dbg("tick_start:%ld\n", tick);
	}
	if (iRet == strlen(szHttpHead))
	{
		iRet = iCommTcpSend(pszBody, iBodyLen);
		if (iRet == iBodyLen)
			return 0;
	}

	return -1;
}

int HTTP_GetContentLength(char *revbuf)
{
    char *p1 = NULL, *p2 = NULL;
    int HTTP_Body = 0;//内容体长度

    p1 = strstr(revbuf,"Content-Length");
    if(p1 == NULL)
        return -1;
    else
    {
        p2 = p1+strlen("Content-Length")+ 2;
        HTTP_Body = atoi(p2);
        return HTTP_Body;
    }
}

#define RECV_HTTPHEAD_LEN       100
#define RECV_HTTPHEAD_BIGLEN    500
int iHttpRecv(char bigData, uchar *pszBody, uint uiBodySize, uint *puiRcvLen, uint uiTimeOut, char *pszContentRange)
{
	char szBuff[RECV_HTTPHEAD_BIGLEN*2+1], szTmp[50];
	char *pszBuf, *pBody, *pTmp;
	uint uiRecvLen, uiBodyLen = 0;
	int iRet, iExpRcvLen;
    uchar ucHttpRspNotOK=0;

	if (uiBodySize < 30)
	{
		dbg("uiBodySize=[%d], too small !!!\n", uiBodySize);
		return 1;
	}

	//收取HttpHead
	memset(szBuff, 0, sizeof(szBuff));
	uiRecvLen = 0;
	//iExpRcvLen = sizeof(szBuff) - 1;
	if(bigData)
        iExpRcvLen=RECV_HTTPHEAD_BIGLEN;
    else
    	iExpRcvLen=RECV_HTTPHEAD_LEN;
		
	pszBuf = szBuff;
	pBody = NULL;
	while (iExpRcvLen > 0)
	{
		iRet = iCommTcpRecv((uchar *)pszBuf, iExpRcvLen, uiTimeOut * 1000);
		if (iRet <= 0)
		{
			dbg("RecvData iRet=[%d], uiRecvLen=%d\n", iRet, uiRecvLen);
			if (iRet == 0)
				iRet = -9;
			return iRet;
		}
		pszBuf[iRet] = 0;
		uiTimeOut = 5;

		uiRecvLen += iRet;

		//搜索http head与body空行(分隔符"\r\n\r\n")
		if ((pBody = strstr(szBuff, "\r\n\r\n")) == NULL)
		{
			pszBuf = szBuff + uiRecvLen;
			iExpRcvLen = sizeof(szBuff) - 1 - uiRecvLen;
			if(bigData==0 && iExpRcvLen>RECV_HTTPHEAD_LEN)
				iExpRcvLen=RECV_HTTPHEAD_LEN;
			continue;
		}

#ifdef PRT_HTTP_LOG
		sprintf(szTmp, "\n*** Http Rsp Head(%d)=[%%.%ds]\n", pBody - szBuff, pBody - szBuff);
		dbgLTxt(szTmp, szBuff);
#endif
#if 0      
		if (strstr(szBuff, " 200 OK") == NULL && strstr(szBuff, " 206 Partial Content") == NULL)
		{
			//http返回不成功
			dbg("http head NOT OK!!!\n");
            //vMessage("http响应报文失败");
			//return -2;
            ucHttpRspNotOK=1;
		}
#endif        
		pBody[2] = 0;

		pBody += 4;
		break;
	}

	if (pBody == NULL)
	{
		dbg("RecvData Head=[%s], uiRecvLen=%d, no body err!\n", szBuff, uiRecvLen);
		return 1;
	}

	//搜索Content-Length
	if ((pszBuf = strstr(szBuff, "Content-Length:")) == NULL)
	{
		dbg("Content-Length err, uiRecvLen=%d, rcv=[%s]\n", uiRecvLen, szBuff);
        if(ucHttpRspNotOK)
            return -2;
		return 2;
	}

	pszBuf = pszBuf + strlen("Content-Length:");

	if ((pTmp = strstr(pszBuf, "\r\n")) == NULL)
	{
		dbg("Content-Length err2, uiRecvLen=%d, rcv=[%s]\n", uiRecvLen, szBuff);
		return 3;
	}
	if (pTmp - pszBuf > 10)
	{
		dbg("Content-Length err3, uiRecvLen=%d, rcv=[%s]\n", uiRecvLen, szBuff);
		return 3;
	}
	vMemcpy0((uchar *)szTmp, (uchar *)pszBuf, pTmp - pszBuf);

	//跳过空格,找到数字起始位置
	pszBuf = szTmp;
	//atoi会自动跳过前部空格
	/*
	while (*pszBuf)
	{
		if (*pszBuf >= '0' && *pszBuf <= '9')
			break;
		pszBuf++;
	}
*/
	uiBodyLen = atoi(pszBuf);
	if (uiBodyLen + 1 > uiBodySize)
	{
		dbg("Content-Length err4, uiBodySize=[%d], BodyLen=[%d]\n", uiBodySize, uiBodyLen);
		return 1;
	}
	
	if (pszContentRange!=NULL && (pszBuf = strstr(szBuff, "Content-Range: ")) != NULL)
	{
		pszContentRange[0]=0;
		pszBuf+=strlen("Content-Range: ");
        if(memcmp(pszBuf, "bytes ", 6)==0)
            pszBuf+=6;
		if ((pTmp = strstr(pszBuf, "\r\n")) != NULL)
		{
			vMemcpy0(pszContentRange, pszBuf, pTmp-pszBuf);
		}
	}

	uiRecvLen = uiRecvLen - (pBody - szBuff);
	memcpy(pszBody, pBody, uiRecvLen);
	
	pszBuf = (char *)pszBody + uiRecvLen;
	while (uiBodyLen > uiRecvLen)
	{
		iRet = iCommTcpRecv((uchar *)pszBuf, uiBodyLen - uiRecvLen, uiTimeOut * 1000);
		if (iRet <= 0)
		{
			dbg("body RecvData uiRet=[%d], uiRecvLen=%d\n", iRet, uiRecvLen);
			return 1;
		}

		pszBuf += iRet;
		uiRecvLen += iRet;
	}
	pszBody[uiBodyLen] = 0;
	*puiRcvLen=uiBodyLen;
	    
#ifdef PRT_HTTP_LOG
	//dbgLTxt("\n*** Http Rsp Body(%d):[%s]\n\n", uiBodyLen, pszBody);
	sprintf(szTmp, "*** Http Rsp Body(%d):", uiBodyLen);
	if(bigData==0)
		dbgLTxt2(szTmp, pszBody);
	else
		dbg("%s\n", szTmp);
#endif

#if 0    
    if(ucHttpRspNotOK)
        return -2;
#endif    
	return 0;
}

// ==0:成功 <0:参数失败或者发送失败 >0:接收失败
int iHttpSendRecv(char cType, char *pszUrl, char *pszBakHost, int retry, char *pszBody, int iBodyLen, char *pszRsp, uint uiRspSize, uint uiTimeOut)
{
	uchar szHostName[256 + 1], szSubUrl[256 + 1];
	uint uiPort=0, uiLen;
	char szHostIp[20], szHostPort[10];
	int iRet;
	char buf[50];
    int connNum;
//    char *pszBakDomain;
    int iUseBakFlag=0;
    
    if(retry<=0)
        retry=1;

    vPrtLklDebugLog(0, 2, "url", pszUrl, 0);
	iRet = iParseHttpUrl(pszUrl, szHostName, szSubUrl, &uiPort);
	if (iRet)
	{
		vPrtLklDebugLog(0, 1, "", "get domain url err.\n", 0);
        vMessage("解析域名失败");
		return -1*COM_PARAMETER;
	}
	if(sg_ucLinkStaus!=2)
	{
        if(!sg_cNoDisp)
            vDispMid(3, "解析域名...");

        iRet=iGetHostByName((char*)szHostName, szHostIp);
        
RE_CONN:        
        if(iRet && pszBakHost && pszBakHost[0])
        {
            //使用备份域名
            vDispMid(3, "使用备份服务器...");
            iRet=iParseHttpUrl(pszBakHost, szHostName, NULL, &uiPort);
            if (iRet)
            {
                vPrtLklDebugLog(0, 1, "", "get domain url err.\n", 0);
                vMessage("解析域名失败");
                return -1*COM_PARAMETER;
            }
            iUseBakFlag=1;
            iRet=iGetHostByName(szHostName, szHostIp);
        }
		if(iRet)
		{
			vPrtLklDebugLog(0, 2, "get domain iP err", (char*)szHostName, 0);
			vMessage("解析域名失败");
			return -1*COM_PARAMETER;
		}
		vPrtLklDebugLog(0, 2, "get domain iP ok", szHostName, 0);

		sprintf(szHostPort, "%d", uiPort);

		connNum=0;
		sprintf(buf, "%s:%s", szHostIp, szHostPort);
        while(connNum<retry)
        {
            if(!sg_cNoDisp)
            {
                if(connNum)
                    vDispMidVarArg(3, "连接服务器第%d次...", connNum+1);
                else
                    vDispMid(3, "连接服务器...");
            }
            iRet=iCommTcpConn(szHostIp, szHostPort, 10);
            if(iRet==0)
                break;
            _vDelay(100);
            connNum++;
        }
        if(iRet && pszBakHost && pszBakHost[0] && iUseBakFlag==0)
        {
            goto RE_CONN;
        }
		if(iRet)
		{
            if(!sg_cNoDisp)
                _vDisp(3, "");
			vPrtLklDebugLog(0, 2, "conn svr err", buf, 0);
			return -1*COM_ERR_NO_CONNECT;
		}
		vPrtLklDebugLog(0, 2, "conn svr OK", buf, 0);
		if(sg_ucLinkStaus==1)	
			sg_ucLinkStaus=2;	//修改状态为已链接
        if(iUseBakFlag)
            _vDisp(3,"");
	}
    if(!sg_cNoDisp)
        vDispMid(3, "发送请求...");
	vPrtLklDebugLog(0, 1, "", "send http req msg...\n", 0);
	iRet=iHttpSend(cType, szHostName, uiPort, szSubUrl, pszBody, iBodyLen);
	dbg("iHttpSend ret:%d\n", iRet);		
	if(iRet==0)
	{
        if(!sg_cNoDisp)
            vDispMid(3, "接收响应...");
		vPrtLklDebugLog(0, 1, "", "recv http rsp msg...\n", 0);
		iRet=iHttpRecv(0, pszRsp, uiRspSize, &uiLen, uiTimeOut, NULL);
		dbg("iHttpRecv ret:%d\n", iRet);
	{
		ulong tick2=0;
		tick2 = get_tick();
		dbg("tick_end:%ld\n", tick2);
	}		
        if(iRet)
        {
            if(iRet<0)
            {
                if(iRet==-2)
                    iRet=-1*COM_ERR_RECV_HTTPHEAD;
                else
                    iRet=-1*COM_ERR_RECV;
            }else
                iRet=COM_ERR_RECV;
        }
	}else
		iRet=-1*COM_ERR_SEND;

	if(sg_ucLinkStaus==0)
		iCommTcpDisConn();
    if(!sg_cNoDisp)
        _vDisp(3, "");
	return iRet;
}

#if 0
int iHttpRecvFile(uchar *pszBody, uint uiRecvSize, uint *puiRecvLen, uint *puiFileSize, uint uiTimeOut)
{
	char szBuff[1024], szTmp[30];
	char *pszBuf, *pBody, *pTmp;
	uint uiRecvLen, uiBodyLen = 0;
	int iRet, iExpRcvLen;

	if (uiRecvSize < 30)
	{
		dbg("uiBodySize=[%d], too small !!!\n", uiBodySize);
		return 1;
	}

	//收取HttpHead
	memset(szBuff, 0, sizeof(szBuff));
	uiRecvLen = 0;
	iExpRcvLen = sizeof(szBuff) - 1;
	pszBuf = szBuff;
	pBody = NULL;
	while (iExpRcvLen > 0)
	{
		iRet = iCommTcpRecv((uchar *)pszBuf, iExpRcvLen, uiTimeOut * 1000);
		if (iRet <= 0)
		{
			dbg("RecvData iRet=[%d], uiRecvLen=%d\n", iRet, uiRecvLen);
			if (iRet == 0)
				iRet = -9;
			return iRet;
		}
		pszBuf[iRet] = 0;
		uiTimeOut = 5;

		uiRecvLen += iRet;

		//搜索http head与body空行(分隔符"\r\n\r\n")
		if ((pBody = strstr(szBuff, "\r\n\r\n")) == NULL)
		{
			pszBuf = szBuff + uiRecvLen;
			iExpRcvLen = sizeof(szBuff) - 1 - uiRecvLen;
			continue;
		}

		sprintf(szTmp, "header=[%%.%ds]\n", pBody - szBuff);
		//dbg(szTmp, szBuff);
		dbgLTxt(szTmp, szBuff);

		if (strstr(szBuff, " 200 OK") == NULL && strstr(szBuff, " 206 Partial Content") == NULL)
		{
			//http返回不成功
			dbg("http head NOT OK!!!\n");
			//return -2;
		}
		pBody[2] = 0;

		pBody += 4;
		break;
	}

	if (pBody == NULL)
	{
		dbg("RecvData Head=[%s], uiRecvLen=%d, no body err!\n", szBuff, uiRecvLen);
		return 1;
	}

	//搜索Content-Range
	if(*puiFileSize==0)
	{
		if ((pszBuf = strstr(szBuff, "Content-Range:")) != NULL)
		{
			pTmp=strchr(pszBuf, '/');
			if(pTmp)
			{
				*puiFileSize=atoi(pTmp+1);
			}
		}
	}

	//搜索Content-Length
	if ((pszBuf = strstr(szBuff, "Content-Length:")) == NULL)
	{
		dbg("Content-Length err, uiRecvLen=%d, rcv=[%s]\n", uiRecvLen, szBuff);
		return 2;
	}

	pszBuf = pszBuf + strlen("Content-Length:");

	if ((pTmp = strstr(pszBuf, "\r\n")) == NULL)
	{
		dbg("Content-Length err2, uiRecvLen=%d, rcv=[%s]\n", uiRecvLen, szBuff);
		return 3;
	}
	if (pTmp - pszBuf > 10)
	{
		dbg("Content-Length err3, uiRecvLen=%d, rcv=[%s]\n", uiRecvLen, szBuff);
		return 3;
	}
	vMemcpy0((uchar *)szTmp, (uchar *)pszBuf, pTmp - pszBuf);

	//跳过空格,找到数字起始位置
	pszBuf = szTmp;
	/*	
	//atoi会自动跳过前部空格
	while (*pszBuf)
	{
		if (*pszBuf >= '0' && *pszBuf <= '9')
			break;
		pszBuf++;
	}
*/
	uiBodyLen = atoi(pszBuf);
	if (uiBodyLen + 1 > uiRecvSize)
	{
		dbg("Content-Length err4, uiBodySize=[%d], BodyLen=[%d]\n", uiRecvSize, uiBodyLen);
		return 1;
	}
	memcpy(pszBody, pBody, uiRecvLen - (pBody - szBuff));
	uiRecvLen = uiRecvLen - (pBody - szBuff);

	pszBuf = (char *)pszBody + uiRecvLen;
	while (uiBodyLen > uiRecvLen)
	{
		iRet = iCommTcpRecv((uchar *)pszBuf, uiBodyLen - uiRecvLen, uiTimeOut * 1000);
		if (iRet <= 0)
		{
			dbg("body RecvData uiRet=[%d], uiRecvLen=%d\n", iRet, uiRecvLen);
			return 1;
		}

		pszBuf += iRet;
		uiRecvLen += iRet;
	}
	pszBody[uiBodyLen] = 0;

	dbgLTxt("Recv Rsp(%d):[%s]\n", uiBodyLen, pszBody);

	return 0;
}
#endif

//http下载文件
/*
	pszUrl:	下载地址
	pszFile:	文件名或数据空间，根据ucSaveFlag做处理
	puiFileSize:	文件长度
	puiDownloadLen:	实际已下载的文件长度(不含文件头长度), 同时做输入和输出参数。输入时作为断点续传起始位置。
	ucSaveFlag:	保存标志.若为1,则pszFile为文件名,需保存到文件中。为0则下载内容直接缓存在pszFile变量中。
    psFileHead: 文件头信息,拉卡拉下载的应用文件头部有多余数据
    uiFleHeadLen: 拉卡拉下载的应用文件头部信息长度，参数文件和其它非拉卡拉应用文件此处填0
    saveFunc: 保存断点续传长度变量的函数指针, 可为NULL
ret:	0-下载成功  <0下载失败  1-部分下载
*/
int iHttpDownloadFile(uchar *pszUrl, uchar *pszFile, uint *puiFileSize, uint *puiDownloadLen, 
            uchar ucSaveFlag, uchar *psFileHead, uint uiFleHeadLen, void (*saveFunc)(ulong, uchar *, uint))
//int iHttpDownloadFile(char *pszUrl, char *file, uint uiFileSize)
{
	uchar szHostName[256 + 1], szSubUrl[256 + 1];
	uint uiPort = 0;
	int iRet;
	uchar szUrlPort[10];
	uint uiHaveLen = 0;         //已下载字节数(含文件头长度)
	char szHttpHead[400];
	uint uiPackSize = 8000; //每包大小
	uchar rcv[8000 + 100];
	uint uiLen, uiFileSize;
	uchar szHostIp[20], szHostPort[10];
	lfs_file_t file;
	long lFileSizetmp=0;
    uchar first=1;
    char szContentRange[20+1];
    char fileStat=0;
    
	//检查文件
	if(ucSaveFlag)
	{
		if(*puiDownloadLen==0)
			iRet = sys_lfs_file_open(&file, pszFile, LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC);
		else
			iRet = sys_lfs_file_open(&file, pszFile, LFS_O_WRONLY);
		if (iRet != 0)
		{
			dbg("iHttpDownloadFile, open file fail:[%s] %d\n", pszFile, iRet);
			vMessage("打开文件失败");
			return -101;
		}
        fileStat=1;
        
		if(*puiDownloadLen)
		{
			/*
			//检查文件大小
			lFileSizetmp = sys_lfs_file_seek(&file, 0, LFS_SEEK_END);
			if(lFileSizetmp<*puiDownloadLen)
			{
				*puiDownloadLen=0;
				sys_lfs_file_close(&file);
				sys_lfs_file_open(&file, pszFile, LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC);
			}
			*/
			//定位到指定位置
			if(sys_lfs_file_seek(&file, *puiDownloadLen, LFS_SEEK_SET)!=*puiDownloadLen)
			{
				sys_lfs_file_close(&file);
				*puiDownloadLen=0;
				sys_lfs_file_open(&file, pszFile, LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC);
			}
			if(*puiFileSize>0 && *puiDownloadLen==*puiFileSize)
			{
				sys_lfs_file_close(&file);
				return 0;
			}
		}
	}

	iRet = iParseHttpUrl(pszUrl, szHostName, szSubUrl, &uiPort);
	if (iRet)
	{
        if(fileStat)
            sys_lfs_file_close(&file);
		vMessage("解析下载地址失败");
		return -1;
	}

	if (uiPort == 80 || uiPort == 0)
		szUrlPort[0] = 0;
	else
		sprintf(szUrlPort, ":%u", uiPort);

	if( iGetHostByName(szHostName, szHostIp) )
	{
        if(fileStat)
            sys_lfs_file_close(&file);
		vMessage("解析域名失败");
		return -1;
	}
    
	//连接服务器
	_vDisp(3, "连接服务器");
	sprintf(szHostPort, "%d", uiPort);
    uiFileSize=*puiFileSize;
	iRet = iCommTcpConn(szHostIp, szHostPort, 10);
	if(iRet)
	{
        if(fileStat)
            sys_lfs_file_close(&file);
		vMessage("连接服务器失败");
		return -2;
	}

	szContentRange[0]=0;
	uiHaveLen=*puiDownloadLen;
    if(uiHaveLen)
        uiHaveLen+=uiFleHeadLen;
	while (uiHaveLen < uiFileSize)
	{
		//vDispVarArg(4, "下载文件: %d/%d", uiHaveLen, uiFileSize);        
        _vDisp(3, "下载文件:");
        vDispVarArg(4, "  %d/%d", uiHaveLen, uiFileSize);
        
		uiLen=(uiHaveLen+uiPackSize>=uiFileSize)?uiFileSize:uiHaveLen+uiPackSize;
		sprintf(szHttpHead, "GET %s HTTP/1.1\r\n"
							"Host: %s%s\r\n"
							"Range: bytes=%u-%u\r\n"
							"\r\n",
				szSubUrl, szHostName, szUrlPort, uiHaveLen, uiLen - 1);

		vDispMid(3, "发送请求...");
		iRet = iCommTcpSend((uchar *)szHttpHead, strlen(szHttpHead));
		if (iRet != strlen(szHttpHead))
		{
			if(first)
				iRet=-4;
			else
				iRet=4;
			break;
		}
        
		vDispMid(3, "接收响应...");
		uiLen = 0;
		if(first)
			iRet = iHttpRecv(1, rcv, sizeof(rcv), &uiLen, 10, szContentRange);
		else
			iRet = iHttpRecv(1, rcv, sizeof(rcv), &uiLen, 10, NULL);
		if (iRet)
		{
			if(first)
				iRet=-5;
			else
				iRet=5;	
			break;
		}

		if(first && szContentRange[0])
		{
			dbg("Content-Range: bytes %s\n", szContentRange);
			//得到服务器返回的文件大小
			char *p;
			p=strchr(szContentRange, '/');
			if(p)
			{
				lFileSizetmp=atol(p+1);
				if(lFileSizetmp!=*puiFileSize)
				{
                    if(fileStat)
                        sys_lfs_file_close(&file);
					iCommTcpDisConn();
					//vDispVarArg(_uiGetVLines(), "filelen %ld!=%ld", lFileSizetmp, *puiFileSize);
					vMessage("文件长度信息不正确");
					return -102;
				}
			}
		}

		//写文件
		if(ucSaveFlag)
		{
            if(uiHaveLen==0)        //从0开始下,有文件头
            {
                if(uiLen<uiFleHeadLen || (uiFleHeadLen && rcv[0]!=0x01))      //0x01:单个文件
                {
                    iRet=-103;
                    break;
                }
                if(psFileHead && uiFleHeadLen)
                {
                    dbg("uiFleHeadLen=%d\n", uiFleHeadLen);
                    memcpy(psFileHead, rcv, uiFleHeadLen);
                }

                sys_lfs_file_write(&file, (char *)rcv+uiFleHeadLen, uiLen-uiFleHeadLen);                
                *puiDownloadLen+=(uiLen-uiFleHeadLen);
            }
            else
            {
                sys_lfs_file_write(&file, (char *)rcv, uiLen);
                *puiDownloadLen+=uiLen;
            }
            #if 1
            //文件关闭以实际写入
            {
                //_vDisp(3, "");
                sys_lfs_file_close(&file);
                sys_lfs_file_open(&file, pszFile, LFS_O_WRONLY | LFS_O_APPEND);
            }
            #endif
            if(saveFunc)
            {
                if(uiHaveLen==0)
                    saveFunc(*puiDownloadLen, rcv, uiFleHeadLen);
                else
                    saveFunc(*puiDownloadLen, NULL, 0);
            }
		}else
		{
            dbg("uiLen=%d\n", uiLen);
			memcpy(pszFile, rcv, uiLen);            
		}
        first=0;
		uiHaveLen += uiLen;
	}
	if(fileStat)
	{
		sys_lfs_file_close(&file);
	}
	iCommTcpDisConn();
	/*
	if(iRet)
		vMessage("下载失败");
	else
		vMessage("下载成功");
	*/
	return iRet;
}

int iTcpConnSvr(char *pszSvrIp, char *pszSvrPort)
{
	int iRet;

	if(sg_ucLinkStaus!=2)
	{
        if(!sg_cNoDisp)
            vDispMid(3, "连接服务器...");
		iRet=iCommTcpConn(pszSvrIp, pszSvrPort, 10);
		if(iRet)
		{
            if(!sg_cNoDisp)
                _vDisp(3, "");
			return -1*COM_ERR_NO_CONNECT;
		}
		if(sg_ucLinkStaus==1)	
			sg_ucLinkStaus=2;	//修改状态为已链接
		return 0;
	}else
		return 0;
}

static char sg_szCurSvrIp[15+1], sg_szCurSvrPort[5+1];
void vSetCurSvrIP(char *ip, char *port)
{
    strcpy(sg_szCurSvrIp, ip);
    strcpy(sg_szCurSvrPort, port);
}
void vGetCurSvrIP(char *ip, char *port)
{
    strcpy(ip, sg_szCurSvrIp);
    strcpy(port, sg_szCurSvrPort);
}

// ==0:成功 <0:参数失败或者发送失败 >0:接收失败
int iTcpSendRecv(char *pszSvrIp, char *pszSvrPort, char *pszBakSvrIp, char *pszBakSvrPort,
        uchar *psSnd, uint uiSndLen, uchar *psRsp, uint uiRspSize, uint *puiRcvLen, uint iTimeOutMs,uchar ucRevFlag)
{
	//uchar szHostName[256 + 1], szSubUrl[256 + 1];
	//uint uiPort=0;
    uint uiLen;
	char szHostIp[20], szHostPort[10];
	int iRet;
/*
	iRet = iParseHttpUrl(pszUrl, szHostName, szSubUrl, &uiPort);
	if (iRet)
		return -1*COM_PARAMETER;
*/
	if(sg_ucLinkStaus!=2)
	{
		#if 0
        vDispMid(3, "解析域名...");
		if( iGetHostByName(szHostName, szHostIp) )
		{
			vMessage("解析域名失败");
			return -1*COM_PARAMETER;
		}
		sprintf(szHostPort, "%d", uiPort);
        #else
        strcpy(szHostIp, pszSvrIp);
        strcpy(szHostPort, pszSvrPort);
        #endif
        
        vSetCurSvrIP("", "");
        if(!sg_cNoDisp)
            vDispMid(3, "连接服务器...");
		iRet=iCommTcpConn(szHostIp, szHostPort, gl_SysInfo.uiConnectTimeout);
        if(iRet>0)
        {
            if(!sg_cNoDisp)
                vDispMid(3, "连接服务器2...");
            strcpy(szHostIp, pszBakSvrIp);
            strcpy(szHostPort, pszBakSvrPort);
            iRet=iCommTcpConn(szHostIp, szHostPort, gl_SysInfo.uiConnectTimeout);
            if(iRet>0)
            {
                if(!sg_cNoDisp)
                    vDispMid(3, "连接服务器1...");
                strcpy(szHostIp, pszSvrIp);
                strcpy(szHostPort, pszSvrPort);
                iRet=iCommTcpConn(szHostIp, szHostPort, gl_SysInfo.uiConnectTimeout);
            }
        }
		if(iRet)
		{
            if(!sg_cNoDisp)
                _vDisp(3, "");
			return -1*COM_ERR_NO_CONNECT;
		}
        vSetCurSvrIP(szHostIp, szHostPort);
        
		if(sg_ucLinkStaus==1)	
			sg_ucLinkStaus=2;	//修改状态为已链接
	}
    if(!sg_cNoDisp)
        vDispMid(3, "发送请求...");
    iRet = iCommTcpSend(psSnd, uiSndLen);
	dbg("iCommTcpSend ret:%d\n", iRet);
    if(iRet!=uiSndLen)
        iRet=-1*COM_ERR_SEND;
    else
    {  
        #if 1
    if(ucRevFlag)
    {	
       uchar szDateTime[14+1] = {0};
	   
        //冲正报文		 
        memcpy(gl_Send8583.Msg01, "0400", 4);
		
	_vGetTime(szDateTime);
        vMemcpy0(gl_Send8583.Field13, szDateTime+4, 4);
        vMemcpy0(gl_Send8583.Field12, szDateTime+8,6);
		
	gl_Send8583.Field26[0] = 0;
	gl_Send8583.Field35[0] = 0;
	gl_Send8583.Field36[0] = 0;
        memcpy(gl_Send8583.Field52, "\x00\x00", 2);
	gl_Send8583.Field53[0] = 0;	
        memcpy(gl_Send8583.Field39, "98", 2);
	memcpy(gl_Send8583.Field58, "\x00\x00", 2);
	memcpy(gl_Send8583.Field59, "\x00\x00", 2);

       
   //     sprintf((char *)gl_Send8583.Field61, "%06lu", gl_SysData.ulBatchNo); //gl_TransRec.ulVoidTTC);
   //     strcpy(gl_Send8583.Field61+6, gl_Send8583.Field11);
	
/*
	else if(gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMPVOID)
	{
	vOneTwo0(gl_TransRec.sDateTime+1, 2, szBuf);
	sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", gl_SysData.ulBatchNo, gl_TransRec.ulVoidTTC, szBuf);
	}else if(gl_TransRec.uiTransType==TRANS_TYPE_PREAUTHVOID )
	{
	//sprintf((char *)gl_Send8583.Field61, "%06lu%06lu0000", gl_SysData.ulBatchNo, gl_TransRec.ulVoidTTC);
	vOneTwo0(gl_TransRec.sDateTime+1, 2, szBuf);
	sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", 0L, 0L, szBuf);
	}	
	*/
        uiMemManaPut8583(MSG8583_TYPE_REV, &gl_Send8583);
    }
#endif   

    	if(!sg_cNoDisp)
            vDispMid(3, "接收响应...");
        dbg("rcv timeout:%d\n", iTimeOutMs);
        iRet=iCommTcpRecv(psRsp, 2, iTimeOutMs);
        dbg("iCommTcpRecv1 ret:%d\n", iRet);
        if(iRet==2)
        {
            uiLen=psRsp[0]*256+psRsp[1];
            dbg("rcv len1:%u,[%02X][%02x]\n", uiLen, psRsp[0], psRsp[1]);
            if(2+uiLen<=uiRspSize)
            {
                iRet=iCommTcpRecv(psRsp+2, uiLen, 500+uiLen);
                dbg("iCommTcpRecv2 ret:%d\n", iRet);
                if(iRet==uiLen)
				{
					dbgHex("rsp data:", psRsp, 2+uiLen);
					serialHex("rsp 8583 data:", psRsp ,2+uiLen);
					*puiRcvLen=2+uiLen;
                    iRet=0;
				}else
                    iRet=COM_ERR_RECV;
            }else
                iRet=COM_ERR_RECV;                
        }else
            iRet=COM_ERR_RECV;
    }
	if(sg_ucLinkStaus==0)
		iCommTcpDisConn();
	if(!sg_cNoDisp)
        _vDisp(3, "");
	return iRet;
}

void vTestDownFile(void)
{
	int ret=0;
	char url[260], szFileName[100];
	uint uiFilelen, uiDownlen;
	ulong ultick;
    
    vDispCenter(1, "下载文件测试", 1);
    vClearLines(2);
    
    _vDisp(2, "按确认键开始下载");
    if(_uiGetKey()!=_KEY_ENTER)
        return;
    _vDisp(2, "");
    
	strcpy(szFileName, "/SysMem/testfile1.jpg");
    uiDownlen=0;
    
    ultick=_ulGetTickTime();
#if 0
	strcpy(url, "http://car3.autoimg.cn/cardfs/product/g24/M05/4A/CF/1024x0_1_q95_autohomecar__ChsEeV9d_Q6AAFnPACUoYkPwToQ526.jpg");
    uiFilelen=385229;
    
    ret = iHttpDownloadFile(url, szFileName, &uiFilelen, &uiDownlen, 1, NULL, 0, NULL);       //非拉卡拉应用文件    
#else
    strcpy(url, "http://lkl-bu-mer-basic-public.oss-cn-shanghai.aliyuncs.com/tms/task/merged_file/825aaba8-8cb9-41de-bb4d-1191069dcef8?Expires=4739844196&OSSAccessKeyId=LTAIOiB5t5vi8ivT&Signature=QyLoq50MwAiUZ%2BMOWskJxS9JHpU%3D");
	uiFilelen=648028;   //0x09e35c
    
	ret = iHttpDownloadFile(url, szFileName, &uiFilelen, &uiDownlen, 1, NULL, 1+3+8, NULL);
#endif    
	dbg("download %d time=%lu\n", uiFilelen, _ulGetTickTime()-ultick);    
	{
		lfs_file_t file;
		ret = sys_lfs_file_open(&file, szFileName, LFS_O_RDONLY);
		if(ret==0)
		{
			ret=sys_lfs_file_seek(&file, 0, LFS_SEEK_END);
			sys_lfs_file_close(&file);
			dbg("downloadfile testfile1.jpg ok. len=%d.\n", ret);
		}else
		{
			dbg("open downloadfile: testfile1.jpg fail.\n");
		}
	}

	ultick=_ulGetTickTime();
	ret=iMd5File(szFileName, NULL, 0, url);
	dbg("iMd5File %d time=%lu\n", uiFilelen, _ulGetTickTime()-ultick);
	dbg("iMd5File ret=%d\n", ret);
	dbgHex("file md5", url, 16);

	sys_lfs_remove(szFileName);		//删除文件
    
    if(uiDownlen==uiFilelen)
    {
        vMessage("文件下载成功");
    }else
		vMessage("文件下载失败");

	return;
}
