/********************************************************************************

 *************************Copyright (C), 2019, KingShiDun Inc.**************************

 ********************************************************************************
 * @file     	 : test_pmu.h
 * @brief   		  : test_pmu.c header file
 * @author       : Jay
 * @version        : 1.0
 * @date          : 2019-11-13
 * 
 * 
 * @note History:
 * @note        : Jay 2019-11-13
 * @note        : 
 *   Modification: Created file

********************************************************************************/

#ifndef __TEST_PMU_H__
#define __TEST_PMU_H__


#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern int TestIdleMode(void*m);
extern int TestMCUReset(void*m);
extern int TestSleepMode(void*m);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __TEST_PMU_H__ */
