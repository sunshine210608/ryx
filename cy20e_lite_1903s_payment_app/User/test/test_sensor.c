#include <stdio.h>
#include <emv_core.h>
#include <iso7816_3.h>

#include <mhscpu.h>
#include <debug.h>
#include "sensor.h"
#include "hardwaretest.h"




int TestSensor(void*m){
    uint32_t state;
    uint16_t keyval;
    int ret = NG,cnt=0,retry=10;
    dbg("enable sensor\n");
    SENSOR_Configuration(ENABLE);
    SENSOR_NVIC_Configuration();
    LcdClear();
    if(((SMenuItem*)m)->name)
        LcdPutsc(((SMenuItem*)m)->name, 0);
    do{
        mdelay(800);
        state = getSensorState();
        if(state){
            cnt = 0;
            showSensorMessage();
        }else{
            dbg("All sensor done\n");
            LcdClean(C_WHITE);
            LcdDisplayMessageCenterC("All Sensor OK");
            if(++cnt > 4){
                ret=OK;
                break;
            }
        }
        KeyBoardRead(&keyval);
        if(keyval==KEY_CANCEL)
            break;
    }while(retry--);

    dbg("disable sensor\n");
    SENSOR_Configuration(DISABLE);
    return ret;
}


void waitforcleansensor(void){
    uint16_t key;
    uint32_t state;

    LcdClear();
    do{
        state = SENSOR->SEN_STATE; 
        if(state){
            showSensorMessage();
        }else break;
    }while(1);
}


/*
show sensor info when it is been actived or do nothing
*/
void showSensor(uint32_t state){
    char buf[32],ln=3;
    struct tm gettm;
  
    LcdClear();
    if(state){
        
        LcdDisplayMessageCenter("Detected Attack", 2);

        rtc_getattatime(&gettm);
        sprintf(buf,"ATTA:%d %d %d %d:%d:%d\n",gettm.tm_year+1900,gettm.tm_mon+1,gettm.tm_mday,gettm.tm_hour,gettm.tm_min,gettm.tm_sec);
    
        dbg("Sensor state:0x%x,time=%s %x\n",state,buf,RTC_GetAttrackTime());
        LcdPutsl(buf, ln++);
        
        if(state & EXTS1_ACTIVE){
            LcdDisplayMessageCenter("EXT1 Actived",ln++);
        }
        if(state & EXTS2_ACTIVE){
            LcdDisplayMessageCenter("EXT2 Actived",ln++);
        }
        if(state & EXTS3_ACTIVE){
            LcdDisplayMessageCenter("EXT3 Actived",ln++);
        }
        if(state & EXTS4_ACTIVE){
            LcdDisplayMessageCenter("EXT4 Actived",ln++);
        }

        if(state & SENSOR_IT_VOL_HIGH){
            LcdDisplayMessageCenter("HighVol Actived",ln++);
        }
        if(state & SENSOR_IT_VOL_LOW){
            LcdDisplayMessageCenter("LowVol Actived",ln++);
        }
        if(state & SENSOR_IT_TEMPER_HIGH){
            LcdDisplayMessageCenter("HighTemper Actived",ln++);
        }
        if(state & SENSOR_IT_TEMPER_LOW){
            LcdDisplayMessageCenter("LowTemper Actived",ln++);
        }
        if(state & SENSOR_IT_GLITCH){
            LcdDisplayMessageCenter("Glitch Actived",ln++);
        }
        if(state & SENSOR_IT_XTAL32K){
            LcdDisplayMessageCenter("XTAL32K Actived",ln++);
        }
        if(state & SENSOR_IT_MESH){
            LcdDisplayMessageCenter("Mesh Actived",ln++);
        }
        if(state & SENSOR_IT_SSC){
            LcdDisplayMessageCenter("SSC Actived",ln++);
        }
        
        sprintf(buf,"reg:%X\n",state);
        LcdDisplayMessageBottom(buf);

        //gSensorState = 0;
    }
}

void testsensor(void){
    uint32_t state,laststate=0;
    uint16_t keyval;
    int ret = NG,cnt=0;
    struct tm gettm;
    char buf[32];

    dbg("set rtc first\n");
    //set rtc first
    TestRTC(NULL);
    LcdClear();
    
    dbg("enable sensor 0x%x\n",SENSOR->SEN_EXTS_START);
    
    state = SENSOR->SEN_STATE; dbg("SEN_STATE=0x%x\n",state);
    if(state){
        dbg("sensor from power off 0x%x\n",state);
    }
    
    if(SENSOR->SEN_EXTS_START & BIT(31)){
        dbg("Tamper already Working\n");
    }else{
        dbg("Start tamper enable\n");
        KeyBoardClean();
        do{
            LcdDisplayMessageCenterC("Press OK to Enable Sensor\n");
            KeyBoardRead(&keyval);
            if(keyval==KEY_OK){
                break;
            }
        }while(state);
        
        SENSOR_Configuration(ENABLE);
    }
    SENSOR_NVIC_Configuration();

    
    do{
        mdelay(800);
        {
            rtc_gettime(&gettm);
            sprintf(buf,"%d %d %d %d:%d:%d\n",gettm.tm_year+1900,gettm.tm_mon+1,gettm.tm_mday,gettm.tm_hour,gettm.tm_min,gettm.tm_sec);
            
        	LcdPutsc(buf, 0);
        }
        state = getSensorState();
        if(laststate!=state){
            laststate = state;
            LcdClear();
        }
        if(state){
            cnt = 0;
            dbg("wait F1 clean sensor\n");
            while(1){
                {
                    rtc_gettime(&gettm);
                    sprintf(buf,"%d %d %d %d:%d:%d\n",gettm.tm_year+1900,gettm.tm_mon+1,gettm.tm_mday,gettm.tm_hour,gettm.tm_min,gettm.tm_sec);
                    
                	LcdPutsc(buf, 0);
                }
                showSensorMessage();
                KeyBoardRead(&keyval);
                if(keyval==KEY_F1){
                    keyval = 0;
                    SENSOR_ClearITPendingBit();
                    dbg("clean sensor\n");
                    break;
                }
            }
        }else{
            dbg("All sensor ok %x %d\n",state,cnt);
            if(cnt++>=5)
            LcdDisplayMessageCenterC("All Sensor OK");
            else
                LcdDisplayMessageCenterC("Detecting...");
        } 
    }while(1);
}
