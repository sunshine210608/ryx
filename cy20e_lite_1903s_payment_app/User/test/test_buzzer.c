#include <string.h>
#include <stdio.h>
#include "mhscpu.h"
#include "msr.h"
#include "hardwaretest.h"
#include "buzzer.h"

int test_buzzer(void){
    char i,strtmp[32]={0};
    
    dbg("Buzzer Test\n");
    enterTestItem("Buzzer Test");
    for(i=1;i<=5;i++){
        memset(strtmp,0,sizeof(strtmp));
        sprintf(strtmp,"Freq:%d Hz",i*1000);
        LcdDisplayMessageCenterC(strtmp);
        BuzzerSetFreq(i*1000);
        BuzzerOn(1000);
        mdelay(1000);
    }

    BuzzerSetFreq(BUZZER_DEFAULT_FREQ);
    return 0;
}


int TestBuzzer(void*m){
    SMenuItem *psi = (SMenuItem*)m;

    LcdClear();
    LcdPutsc(psi->name, 0);
    
    test_buzzer();
    return MJ;
}

