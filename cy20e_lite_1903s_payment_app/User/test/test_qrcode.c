/********************************************************************************

 *************************Copyright (C), 2019, KingShiDun Inc.**************************

 ********************************************************************************
 * @file     	 : test_qrcode.c
 * @brief   		  : .C file function description
 * @author       : Jay
 * @version        : 1.0
 * @date          : 2019-10-31
 * 
 * 
 * @note History:
 * @note        : Jay 2019-10-31
 * @note        : 
 *   Modification: Created file

********************************************************************************/
#include <stdio.h>
#include <string.h>
#include "mhscpu.h"
#include "mhscpu_dcmi.h"
#include "mhscpu_timer.h"
#include "mhscpu_i2c.h"

#include "DecodeLib.h"
#include "debug.h"
//#include "beep.h"
#include "ProjectConfig.h"
#include "lcd.h"
static unsigned char *pool;
extern void DCMI_NVICConfig(void);
extern void Cameraclk_Configuration(void);
extern void CameraI2CGPIOConfig(void);

void PLEDInit(void){
	GPIO_InitTypeDef GPIO_InitStruct;

    //PRN_PAPER=of_get_named_gpio(node, "prn_paper_gpio", 0);
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Remap = GPIO_Remap_1;
	GPIO_Init(GPIOH, &GPIO_InitStruct);
	
	//GPIO_SetBits(GPIOH, GPIO_Pin_6);
	
	GPIO_ResetBits(GPIOH, GPIO_Pin_6);
}


void QRLedOn(void){
    GPIO_SetBits(GPIOH, GPIO_Pin_6);
}

void QRLedOff(void){
    GPIO_ResetBits(GPIOH, GPIO_Pin_6);
}


/**
 *单buff解码Demo
 *
 */
 void SingleBuffDecodeTest(void)
 {
     int i;
    //定义保存解码结果的数组
    uint8_t result[2100] = {0};
    int32_t resnum;    //返回解码结果数量，为0，表示解码失败，为-1表示解码数组偏小
    DecodeConfigTypeDef DecodeCfg = {0};  //设置解那种码
    
    //配置解码信息
    /**
     * 解码默认配置
     *
     * cfg->cfgCODE128 = (1u | (0u << DE_MIN_LEN) | (0xffu << DE_MAX_LEN));
     * cfg->cfgCODE39 = (1u | (0u << DE_MIN_LEN) | (0xffu << DE_MAX_LEN));
     * cfg->cfgCODE93 = (1u | (0u << DE_MIN_LEN) | (0xffu << DE_MAX_LEN));
     * cfg->cfgEAN13 = (1u | (1u << DE_EMIT_CHECK));
     * cfg->cfgUPC_A = (1u | (1u << DE_EMIT_CHECK));
     * cfg->cfgEAN8 = (1u | (1u << DE_EMIT_CHECK));
     * cfg->cfgUPC_E0 = (1u | (1u << DE_EMIT_CHECK));
     * cfg->cfgUPC_E1 = (1u | (1u << DE_EMIT_CHECK));
     * cfg->cfgISBN13 = 0u;
     * cfg->cfgInterleaved2of5 = (0u | (6u << DE_MIN_LEN) | (0xffu << DE_MAX_LEN));
     * 
     * cfg->cfgQRCode = 1u | 1u << (DEQR_MISSINGCORNER) | (1u << DEQR_CURVE);
     * cfg->cfgPDF417 = 0u;
     * cfg->cfgDataMatrix = 0u;
     * 
     * cfg->cfgGlobal = cfg->cfgGlobal = (1u << DEBAR_PRECISION);
     * 
     */
    DecodeConfigInit(&DecodeCfg);
    FUNCIN;
	while(1) 
	{
        
        //使能采图
        DecodeDcmiStart();
        //检测采图是否完成，需要中断调用DCMI_CallBackFrame()函数
        
        while( !DecodeDcmiFinish());
        
        //开始解码
        resnum = DecodeStart(&DecodeCfg, 2100, result);
        
        if(resnum > 0)
        {
            for (i = 0; i < resnum; i++)
            {
                dbg("%c", result[i]);printf("%c", result[i]);
            }
            dbg("\r\n");printf("\r\n");

            //解码成功将两个buff状态置为空闲，防止重复解码         
            CleanDecodeBuffFlag();

            {
                char buf[32];
                sprintf(buf,"QRCode Decode Success:%d",resnum);
                LcdDisplayMessageCenterC(buf);
                beepone(200);break;    
            }  
        }
        else if(resnum < 0)
        {
            if(resnum == DecodeResOverflowError)
            {
                dbg("解码结果溢出\n");
                LcdDisplayMessageCenterC("QRDecode Failed1");
            }
            else if(resnum == DecodeImageNoDoneError)
            {
                dbg("图片未采集完成\n");
                LcdDisplayMessageCenterC("QRDecode Failed2");
            }
            else if(resnum == DecodeLibNoInitError)
            {
                dbg("库未初始化\n");
                LcdDisplayMessageCenterC("QRDecode Failed3");
            }
        }
        else
        {
            dbg("Decoding failed\n");
            LcdDisplayMessageCenterC("QRDecode Failed4");
        }	
    }
 }


void test_qrcode(void)
{
    DecodeInitTypeDef DecodeInitStruct;
    DecodeFlagTypeDef ret;
    
   	/*SYSCTRL_SYSCLKSourceSelect(SELECT_EXT12M);
	SYSCTRL_PLLConfig(SYSCTRL_PLL_192MHz);
	SYSCTRL_PLLDivConfig(SYSCTRL_PLL_Div_None);
	SYSCTRL_HCLKConfig(SYSCTRL_HCLK_Div2);
	SYSCTRL_PCLKConfig(SYSCTRL_PCLK_Div2);	/* PCLK >= 48M */

    SYSCTRL_AHBPeriphClockCmd(SYSCTRL_AHBPeriph_OTP, ENABLE);
    SYSCTRL_AHBPeriphResetCmd(SYSCTRL_AHBPeriph_OTP, ENABLE);

    PLEDInit();
	FUNCIN;
    //BEEP_Init();
    
    /* I2C Pin Config */
    CameraI2CGPIOConfig();
    
    /* Camera CLK enable, should before DecodeInit */
    Cameraclk_Configuration();

    pool = malloc(DECODE_BUFF_SIZE);
    if(pool==NULL){
        dbg("malloc poll error\n");
        return ;
    }
    
    /* DecodeLib & MEM Pool Init */
    DecodeInitStruct.pool = pool;
    DecodeInitStruct.size = DECODE_BUFF_SIZE;
    DecodeInitStruct.CAM_PWDN_GPIOx = CAM_PWDN_GPIO;
    DecodeInitStruct.CAM_PWDN_GPIO_Pin = CAM_PWDN_GOIO_PIN;
    DecodeInitStruct.CAM_RST_GPIOx = CAM_RST_GPIO;
    DecodeInitStruct.CAM_RST_GPIO_Pin = CAM_RST_GOIO_PIN;
    DecodeInitStruct.CAM_I2Cx = I2C0;
    DecodeInitStruct.SensorConfig = NULL;
    DecodeInitStruct.SensorCfgSize = 0;
    
    ret = DecodeInit(&DecodeInitStruct);
    if (ret != DecodeInitSuccess)
    {
        switch (ret)
        {
            case DecodeInitCheckError:
                dbg("Decoding Library Check Failure!\r\n");
            break;
            
            case DecodeInitMemoryError:
                dbg("Insufficient memory in decoding library!\r\n");
            break;
            
            case DecodeInitSensorError:
                dbg("Camera initialization failed!\r\n");
            break;
			
            default:
                break;
        }
        while(1);
    }
    
    /* Should invoked after DecodeInit() */
    DCMI_NVICConfig();
    
 	dbg("Decode Demo V%d.%d.%d\n", (GetDecodeLibVerison() >> 16) & 0xff, (GetDecodeLibVerison() >> 8) & 0xff, GetDecodeLibVerison() & 0xff);
    
    if (DECODE_BUFF_SIZE >= SINGLE_BUFF_MIN_SIZE && DECODE_BUFF_SIZE < DOUBLE_BUFF_MIN_SIZE)
    {
        /* Single Buffer Example */
        QRLedOn();
        SingleBuffDecodeTest();
        QRLedOff();
    }
    dbg("CloseDecode\n");
    /* Release hardware resources */
    CloseDecode();
    SYSCTRL_APBPeriphClockCmd(SYSCTRL_APBPeriph_I2C0, DISABLE);
    SYSCTRL_AHBPeriphClockCmd(SYSCTRL_AHBPeriph_DMA, DISABLE);

    free(pool);
}

