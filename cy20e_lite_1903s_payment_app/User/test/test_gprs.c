#include <string.h>
#include <stdio.h>
#include "mhscpu.h"
#include "test_gprs.h"
#include "gprs.h"
#include "gprs_at.h"
#include "debug.h"
#include "hardwaretest.h"

int gprs_at_test()
{	

	int ret;

    
	gprs_UART_Configuration();
	
	gprs_gpio_init();

	if (gprs_at_MoudleIsActive() == 0)
	{
		dbg("Need gprs_hardware_reset()\n");
		gprs_hardware_reset();
		if (gprs_at_MoudleIsActive() == 0)
		{
			dbg("Need gprs_hardware_reset()\n");
			gprs_hardware_reset();
			if (gprs_at_MoudleIsActive() == 0)
			{
				dbg("gprs_hardware_reset() can't work\n");
				return -9;
			}
		}
		
	}

	ret = gprs_at_DisableEcho();
	if (ret < 0)
	{
		dbg("gprs_at_DisableEcho error\n");
		return -1;
	}

	ret = gprs_at_CheckSIM();
	if (ret < 0)
	{
		dbg("gprs_at_CheckSIM error\n");
		return -1;
	}
	
	ret = gprs_at_GetSignalQuality();
	if (ret < 0)
	{
		dbg("gprs_at_GetSignalQuality error\n");
		return -2;
	}

	ret = gprs_at_CheckRegister();
	if (ret < 0)
	{
		dbg("gprs_at_CheckRegister error\n");
		return -3;
	}

	ret = gprs_at_CheckGprsAttachment();
	if (ret < 0)
	{
		dbg("gprs_at_CheckGprsAttachment error\n");
		return -4;
	}

	ret = gprs_at_IPShut();
	if (ret < 0)
	{
		dbg("gprs_at_IPShut error\n");
		return -5;
	}

	ret = gprs_at_SetAPN();
	if (ret < 0)
	{
		dbg("gprs_at_SetAPN error\n");
		return -6;
	}

	ret = gprs_at_BuildGprsLink();
	if (ret < 0)
	{
		dbg("gprs_at_BuildGprsLink error\n");
		return -7;
	}

	ret = gprs_at_GetLocalIp();
	if (ret < 0)
	{
		dbg("gprs_at_GetLocalIp error\n");
		return -8;
	}

	
	//ret = gprs_at_TcpConnectHost("120.55.60.177", "6666");

	//ret = gprs_at_TcpSnd("1234567890", 10);
	return 0;
}


int gprs_at_test_ex()
{	
	int ret;    

	dbg("gprs_at_test_ex\n");
	
	gprs_UART_Configuration();
	
	gprs_initwithPG4();

	gprs_hardware_reset();

	dbg("before gprs_at_at\n");

	gprs_at_at();

	return 0;
}


int TestGPRS(void*m){
    int ret;
    char buf[32];
    enterTestItem("GPRS Test");LcdDisplayMessageCenterC("GPRS Auto Test...");
    gprs_pwr_on();
    ret=gprs_at_test();
    if(ret){
        sprintf(buf,"GPRS Test FAIL,code=%d",ret);
        LcdDisplayMessageCenterC(buf);
    }else{
        LcdDisplayMessageCenterC("GPRS Test PASS");
    }
    gprs_pwr_off();
    mdelay(1500);
    return ret?NG:OK;
}

int gprs_setAutoAnswer()
{ 
 int ret;

 gprs_UART_Configuration();

 gprs_gpio_init();

 if (gprs_at_MoudleIsActive() == 0)
 {
  dbg("Need gprs_hardware_reset()\n");
  gprs_hardware_reset();
  if (gprs_at_MoudleIsActive() == 0)
  {
   dbg("Need gprs_hardware_reset()\n");
   gprs_hardware_reset();
   if (gprs_at_MoudleIsActive() == 0)
   {
    dbg("gprs_hardware_reset() can't work\n");
    return -1;
   }
  }
 }

 mdelay(10*1000);

 ret = gprs_at_SetAutoAnswer();
 if (ret != 0)
 {
  return -2;
 }

 ret = gprs_at_save();
 if (ret != 0)
 {
  return -3;
 }

 ret = gprs_at_GetAutoAnswer();
 if (ret != 0)
 {
  return -4;
 }

 return 0;
}

