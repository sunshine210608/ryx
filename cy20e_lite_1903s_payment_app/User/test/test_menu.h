

#define MAX_MENUITEM_NUMBER		30
#define MAX_ITEM_NAME_LEN       16

typedef struct itemresult_t{
	char name[MAX_ITEM_NAME_LEN];
	int pass;
	int failed;
	int total;
    char lastresult;
	char autotest;
} ItemResult;

typedef struct menu_t SMenu;

typedef int (*VFUNC)(void *);
typedef struct menuitem_t {
	char *name;		// menu item's name.
	VFUNC func;		// handle function for this menu item.
	SMenu *submenu;		// submenu for this menu item.
	ItemResult *result;
} SMenuItem;

typedef struct menu_t {
	char *name;		// menu's name.
	int   num;		// menu items' number in this menu.

	// Following two members is used to record menu status, they are used
	// only at runtime.
	int   cur;			/* current selected item.
						 * Range: 1 -- MAX_MENUITEM_NUMBER.
						 *just for display
						 */
	int   startline;	/* Start line to display, indecates the menu item
						 * displayed on the top line of the LCD.
						 * Range: 0 -- MAX_MENUITEM_NUMBER.
						 * just for running
						 */
	// Menu items
	SMenuItem items[MAX_MENUITEM_NUMBER];
	SMenu *topmenu;
} SMenu;

enum{
    OK,
    NG,
    MJ,  //Manual judgement
    EXIT  // exit menu.user select NG or exit test
};

#define AUTOTEST_MODE       (((SMenuItem*)m)->result->autotest == 1)

#define MAX_NUM_ONEPAGE     9
int TestManual(void *m);
SMenuItem* MenuDisplay(SMenu *m);
void HardwareTestManual(void);
int EnterAutoTestItems(SMenu * Mainmenu);


