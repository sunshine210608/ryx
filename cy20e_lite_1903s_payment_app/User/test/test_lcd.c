/********************************************************************************

 *************************Copyright (C), 2019, KingShiDun Inc.**************************

 ********************************************************************************
 * @file     	 : test_lcd.c
 * @brief   		  : .C file function description
 * @author       : Jay
 * @version        : 1.0
 * @date          : 2019-10-08
 * 
 * 
 * @note History:
 * @note        : Jay 2019-10-08
 * @note        : 
 *   Modification: Created file

********************************************************************************/



#include <string.h>
#include <stdio.h>
#include "mhscpu.h"
#include "lcd.h"
#include "hardwaretest.h"
//#include "test_lcd.h"
int TestLCDColor(void *m);
int TestLCDBacklight(void *m);


void test_lcd(void){
    UG_COLOR colors[]={C_WHITE,C_BLACK,C_RED,C_GREEN,C_BLUE};
    char i;    
    FUNCIN;
    
    lcd_SetBackLight(100);
    while(1){
        for(i=0;i<(sizeof(colors)/sizeof(colors[0]));i++){
            dbg("color=%x\n",colors[i]);
            LcdClean(colors[i]);
            /*key=keyboard_waitkeyevent(2000);
            if(key>0){
                dbg("press key exit\n");
                return ;
            }*/
            mdelay(2000);
        }
        return ;
    }
}


SMenu menu_testLCD =
{"Test LCD", 2, 1, 0,
	{
		{"LCD Color", TestLCDColor, NULL},
		{"LCD BackLight",TestLCDBacklight, NULL},
        NULL
	}
};


int TestLCDColor(void *m)
{
    UG_COLOR colors[]={C_WHITE,C_BLACK,C_RED,C_GREEN,C_BLUE};
    char i;
    
    FUNCIN;
    lcd_SetBackLight(100);
    for(i=0;i<(sizeof(colors)/sizeof(colors[0]));i++){
        dbg("color=%x\n",colors[i]);
        LcdClean(colors[i]);
        mdelay(2000);
    }
    return MJ;
}


int TestLCDBacklight(void *m)
{
    char i ;
    char tmp[32];
    for(i=0;i<=100;i+=25){
        sprintf(tmp,"LCD BackLight %d%",i);
        LcdDisplayMessageCenterC(tmp);
        lcd_SetBackLight(i);
        mdelay(2000);
    }
    return MJ;
}

int TestLCD(void *m)
{
    FUNCIN;
	SMenuItem *item = NULL;
	while(1)
	{
		item = (SMenuItem *)MenuDisplay(&menu_testLCD);
		if(item == NULL)
			break;

		item->func((void*)item);
	}
    return MJ;
}

