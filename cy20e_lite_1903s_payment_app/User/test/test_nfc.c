#include <string.h>
#include <stdio.h>
#include <mhscpu.h>
#include <mhscpu_kcu.h>
#include "rfid.h"
#include "mh523.h"
#include "iso14443_4.h"
#include "iso14443a.h"
#include "iso14443b.h"
#include "define.h"

#include "debug.h"
#include "hardwaretest.h"


#define COM_BUF_SIZE        50
#define HEAD                0x68

typedef struct r_w_reg_s
{
	u8 addr;          //读写的寄存器地址
	u8 rw_dat;        //读写的寄存器值
}r_w_reg;

typedef struct pc_tx_pkt_s
{
	u8 head;          //包头
	u8 len;           //包长：从包头到校验和
	u8 cmd;	          //包命令

	union datu
	{
		r_w_reg reg;
		u8 dat[1];    //发送或接收的数据
	}datu;
}pc_tx_pkt;

typedef struct lpcd_config_s
{
	u8 delta;
	u32 t_inactivity_ms;
	u8 skip_times;
	u8 t_detect_us;
}lpcd_config_t;

#if (POWERON_POLLING)
	u8 g_query_mode = 1; //手动操作芯片模式
	u8 g_typa_poll = TRUE;
	u8 g_typb_poll = FALSE;
	u8 g_need_reconfig = TRUE;//需要重新初始化PCD的协议类型
#else
	u8 g_query_mode = 0; //手动操作芯片模式
	u8 g_typa_poll = FALSE;
	u8 g_typb_poll = FALSE;
	u8 g_need_reconfig = FALSE;//需要重新初始化PCD的协议类型
#endif

u32 XDATA g_polling_cnt = 0;
u8 g_lpcd_started = FALSE; //LPCD周期自动探测卡功能是否开启
u8 g_lpcd_config_test_start = FALSE;//LPCD周期自动探测卡功能是否开启
lpcd_config_t XDATA g_lpcd_config;



#if 0
pc_tx_pkt XDATA *recv_packet(void)
{
	pc_tx_pkt *XDATA ppkt;
	
	while(uart_getbytes() && tx_buf_index < COM_BUF_SIZE )
	{
		com_tx_buf[tx_buf_index] = (int8_t)uart_RecvChar();
		tx_buf_index++;
	}
	if ( tx_buf_index >= 4 )
	{
		u8 i;
		u8 rest = 4;
		
		for (i=0; i<tx_buf_index-3; )//
		{
			ppkt = (pc_tx_pkt *)(com_tx_buf + i);
			if (ppkt->head == 0x68 && ppkt->len > 0 
				&& ppkt->len + i < COM_BUF_SIZE)//判断是不是能接受完包
			{
				
				if (ppkt->len + i <= tx_buf_index)//判断该包有没有接受完包？
				{
					u8 sum = check_sum((u8 XDATA *)ppkt, ppkt->len - 1);
					
					if (sum == ppkt->datu.dat[ppkt->len - 4] )
					{	
						return ppkt;
					}
					else
					{
						i++;
					}
				}
				else
				{
					 rest = tx_buf_index - i;
					 break;
				}
			}
			else
			{
				i++;
			}
		}
		memmove(com_tx_buf, com_tx_buf + tx_buf_index - rest, rest);
		tx_buf_index = rest;
	}			

	return NULL;
}


char prase_packet(pc_tx_pkt XDATA *ppkt)
{
	switch(ppkt->cmd)
	{
		case COM_PKT_CMD_READ_REG:	//要求把地址和值都返回给pc
		{
			r_w_reg XDATA reg;			 
			reg.addr = ppkt->datu.reg.addr;
			reg.rw_dat = read_reg(ppkt->datu.reg.addr);		
				
			make_packet(COM_PKT_CMD_READ_REG, (u8 *)&reg, sizeof(reg));
			break;
		}
		
		case COM_PKT_CMD_WRITE_REG:
		{		
			write_reg(ppkt->datu.reg.addr, ppkt->datu.reg.rw_dat);
			break;
		}

		case COM_PKT_CMD_QUERY_MODE:
		{
			if (ppkt->datu.dat[0] == 1)
			{
				g_query_mode = TRUE;
				if (ppkt->datu.dat[1] == 1)
				{//typeA poll
					pcd_config('A');
					g_typa_poll = TRUE;
				}
				else
				{
					g_typa_poll = FALSE;
				}
				if (ppkt->datu.dat[2] == 1)
				{//typeB poll
					pcd_config('B');
					g_typb_poll = TRUE;
				}
				else
				{
					g_typb_poll = FALSE;
				}
				if (g_typa_poll && g_typb_poll)
				{
					g_need_reconfig = TRUE;
				}
				else
				{
					g_need_reconfig = FALSE;
				}
				g_polling_cnt = *((u32*)&ppkt->datu.dat[3]);
				printf("g_polling_cnt=%lu\n", g_polling_cnt);
			}
			else
			{
				g_query_mode = FALSE;
			}

			break;
		}
		case COM_PKT_CMD_CHIP_RESET:
		{
			pcd_reset();
			mdelay(7);//carrier off 7ms
			pcd_antenna_on();
		break;
		}
		case COM_PKT_CMD_HALT:
		{
			pcd_hlta();
			printf("HltA\n");
		 	break;
		}
		case COM_PKT_CMD_LPCD:
		{
			g_lpcd_started = TRUE;
			pcd_lpcd_start();
			break;
		}
		case COM_PKT_CMD_LPCD_CONFIG_TEST:
		{
			int XDATA z;
			memcpy(&g_lpcd_config, &ppkt->datu.dat[0], sizeof(g_lpcd_config));
			g_lpcd_config.t_inactivity_ms = ntohl(g_lpcd_config.t_inactivity_ms);
			for (z = 0; z < sizeof(g_lpcd_config);z++)
			{
				printf(" %02x", ppkt->datu.dat[z]);
			}
			pcd_lpcd_config_start(g_lpcd_config.delta, g_lpcd_config.t_inactivity_ms, g_lpcd_config.skip_times, g_lpcd_config.t_detect_us);	
			printf("config=%d,%lu,%d,%d\n",g_lpcd_config.delta, g_lpcd_config.t_inactivity_ms, g_lpcd_config.skip_times, g_lpcd_config.t_detect_us);
			g_lpcd_config_test_start = TRUE;
			break;
		}
		case COM_PKT_CMD_LPCD_CONFIG_TEST_STOP:
		{
			pcd_lpcd_end();
			g_lpcd_config_test_start = FALSE;
			break;
		}
	
		default:
			return FALSE;
			//break;
	}
	return TRUE;
}


void discard_pc_pkt(pc_tx_pkt XDATA *ppkt)
{
	int bytes;
	u8 XDATA *p = ((u8 XDATA *)ppkt) + ppkt->len;

	bytes = (p - com_tx_buf);
	if (bytes <= tx_buf_index)
	{
		memmove(com_tx_buf, com_tx_buf + bytes, tx_buf_index - bytes);
		tx_buf_index -= bytes;
	}
	else
	{
		tx_buf_index = 0;
	}
}
#endif


void make_packet(u8 cmd, u8 *buf, u8 len)
{
	if(COM_PKT_CMD_EXCHANGE==cmd){
        dbg("COM_PKT_CMD_EXCHANGE ok %d\n",len);
        hexdump(buf,len);
    }
    //dbg("make_packet cmd:%x\n",cmd);
}


void nfctest(void){
    tick tks=get_tick();
    u8 XDATA status;
    tick XDATA g_statistic_last_t = 0;
    LcdClean(C_WHITE);
    enterTestItem("NFC Test");
    LcdDisplayMessageCenterC("Please swip NFC Card");
    dbg("start detect card\n");
    while(!is_timeout(tks, 30*1000))
	{	
        dbg("timeout=%d %d %d\n",tks,get_tick(),is_timeout(tks, 30*1000));
        mdelay(1000);

		//自动轮询寻卡模式
		if (g_query_mode && (g_typa_poll || g_typb_poll))
		{
			u8 cmd[2];
			u8 status_a = 1, status_b = 1;

			pcd_antenna_off();
			mdelay(10);
			pcd_antenna_on();
			mdelay(10);
			
			if (g_typa_poll == TRUE)
			{
				if (g_typa_poll & g_typb_poll)
				{
					pcd_config('A');
				}
				cmd[1] = 0x52;

				//pcd_default_info();
					
				status_a = com_reqa((u8 *)&cmd);//询所有A卡				
			}
		
			if (g_typb_poll == TRUE)
			{	
				if (g_typa_poll & g_typb_poll)
				{
					pcd_config('B');
				}			
				cmd[1] = 0x08;
				status_b = com_reqb((u8 *)&cmd);//询所有B卡
			}
		
			//点亮对应灯
			if (status_a == MI_OK || status_b == MI_OK)
			{
                dbg("Detect Card Type%s\n",status_a == MI_OK?"A":"B");
				//led_success_on();//成功
				{
                    unsigned char PPSE_SELECT_APDU[] = { 0x00, 0xA4, 0x04, 0x00, 0x0E, 0x32, 0x50, 0x41, 0x59,
        0x2E, 0x53, 0x59, 0x53, 0x2E, 0x44, 0x44, 0x46, 0x30, 0x31, 0x00 };
                    //unsigned char PPSE_RANDOM_APDU[] = {0,0x84,0,0,4};
                    unsigned char loop_buf[256];
                    
                  

	                {
                        dbg("do RATS\n");
                        loop_buf[0]=0;
                        com_typea_rats(loop_buf);
                    }
    
                    //apdu test.
                    dbg("do apdu %s\n",__TIME__);
                  #if 1
                    loop_buf[1] = sizeof(PPSE_SELECT_APDU);
                    memcpy(loop_buf+2, PPSE_SELECT_APDU, sizeof(PPSE_SELECT_APDU));
                    hexdump(loop_buf+2,loop_buf[1]);
                    com_exchange(loop_buf);
                    LcdDisplayMessageCenterC("nfc apdu test ok");
                  #else
                    memcpy(loop_buf, PPSE_RANDOM_APDU, sizeof(PPSE_RANDOM_APDU));
                    tx_len = sizeof(PPSE_RANDOM_APDU);
                    status = ISO14443_4_HalfDuplexExchange(&g_pcd_module_info, loop_buf, tx_len, loop_buf, &rx_len);
                    dbg("apdu status=%d\n",status);
                    if (status == MI_OK){
                        hexdump(loop_buf,rx_len);
                    }
                  #endif
                }
			}
			else
			{
//				led_fail_on();//失败
                continue;
			}
		
		}

		//cos指令轮询操作
		if (g_cos_loop == TRUE)
		{
			if (g_cos_loop_times > 0)
			{
				com_exchange(g_loop_buf);
				g_cos_loop_times--;
				mdelay(20);
			}
			else
			{
				mdelay(200);
				g_cos_loop = FALSE;
				g_statistic_refreshed = TRUE;
				statistic_print();
				make_packet(COM_PKT_CMD_TEST_STOP, NULL, NULL);
			}
		}
		
		//自动卡检测
		if (g_lpcd_started == TRUE)
		{
			if (TRUE == pcd_lpcd_check())
			{
				g_lpcd_started = FALSE;
				make_packet(COM_PKT_CMD_LPCD, NULL, NULL);
			}
		}
		//自动卡检测参数测试
		if (g_lpcd_config_test_start == TRUE)
		{
			if (TRUE == pcd_lpcd_check())
			{
				u8 XDATA tag_type[2];
					
				g_statistics.lpcd_cnt++;
				g_statistic_refreshed = TRUE;
				statistic_print();
				
				//验证是否有卡入场
				pcd_config('A');
				status = pcd_request(0x52, tag_type);
				//刚检测到卡时可能处于距离边缘，有可能第一次寻卡失败，所以增加第二次寻卡验证
				if (status != MI_OK)
				{					
					status = pcd_request(0x52, tag_type);
				}
				///刚检测到卡时可能处于距离边缘，如果前两次寻卡失败，则第三次寻卡验证
				if (status != MI_OK)
				{					
					status = pcd_request(0x52, tag_type);
				}
				
				if (status == MI_OK)
				{//有卡片入场
					//可添加具体的应用功能代码
					/*

					*/
					//等待卡离场,应用代码中可不必等待离场
					while(1)
					{
						pcd_antenna_off();
						mdelay(10);
						pcd_antenna_on();
						mdelay(10);	
						status = pcd_request(0x52, tag_type);
						if(status != MI_OK)
						{//一次验证卡离场
							mdelay(100);
							status = pcd_request(0x52, tag_type);
							if(status != MI_OK)
							{//二次验证卡离场
								mdelay(100);
								status = pcd_request(0x52, tag_type);
								if(status != MI_OK)
								{//三次验证卡离场
									mdelay(100);
									pcd_lpcd_config_start(g_lpcd_config.delta, g_lpcd_config.t_inactivity_ms, g_lpcd_config.skip_times, g_lpcd_config.t_detect_us);	
									break;	
								}
							}
	
						}
					}
				}
				else
				{
					g_statistics.lpcd_fail++;
					g_statistic_refreshed = TRUE;
					pcd_lpcd_config_start(g_lpcd_config.delta, g_lpcd_config.t_inactivity_ms, g_lpcd_config.skip_times, g_lpcd_config.t_detect_us);	
				}
			}
		}
		
		//输出统计信息
		if (is_timeout(g_statistic_last_t, 100))
		{
			g_statistic_last_t = get_tick();
			//statistic_print();
		}

        return;
	}

    LcdDisplayMessageCenterC("NFC Detect Timeout"); mdelay(1500);
}
void test_nfc(void){
    
    dbg("%s\n",__FUNCTION__);
    #if (POWERON_POLLING)
        dbg("its NFC Polling mode\n");
    #endif
    nfctest();
    mdelay(2000);
}

int TestNFC(void*m){
    uint8_t status_a = 1, status_b = 1 ,status,tag_type[2];
    char strtmp[32],cmd[2];
    uint16_t key;
    
    LcdClear();
    LcdDisplayMessageCenterC("Detect NFC Card...\n");
    //1.detect
    do{
        pcd_antenna_off();
    	mdelay(10);
    	pcd_antenna_on();
    	mdelay(10);

        //1.1 poll typea
        pcd_config('A');
    	cmd[1] = 0x52;
    	status_a = com_reqa((u8 *)&cmd);//询所有A卡			

        //1.2 poll typeb
        pcd_config('B');		
		cmd[1] = 0x08;
		status_b = com_reqb((u8 *)&cmd);//询所有B卡

        KeyBoardRead(&key);
        if(key==KEY_CANCEL){
            pcd_antenna_off();
            return NG;
        }
    }while(status_a != MI_OK && status_b != MI_OK);

    if(status_a == MI_OK)
        sprintf(strtmp,"Detect Card TypeA\n");
    if(status_b == MI_OK)
        sprintf(strtmp,"Detect Card TypeB\n");
    else{
        sprintf(strtmp,"Detect Card Error\n");
        mdelay(1000);
        pcd_antenna_off();
        return NG;
    }
    
    dbg(strtmp);
    LcdDisplayMessageCenterC(strtmp);mdelay(1000);
    
    //2.do apdu
    if (status_a == MI_OK || status_b == MI_OK)
	{
        unsigned char PPSE_SELECT_APDU[] = { 0x00, 0xA4, 0x04, 0x00, 0x0E, 0x32, 0x50, 0x41, 0x59,
0x2E, 0x53, 0x59, 0x53, 0x2E, 0x44, 0x44, 0x46, 0x30, 0x31, 0x00 };
        //unsigned char PPSE_RANDOM_APDU[] = {0,0x84,0,0,4};
        unsigned char loop_buf[256];
        
        dbg("do RATS for typea\n");
        loop_buf[0]=0;
        if(status_a == MI_OK)
            com_typea_rats(loop_buf);            
        
        //apdu test.
        loop_buf[1] = sizeof(PPSE_SELECT_APDU);
        memcpy(loop_buf+2, PPSE_SELECT_APDU, sizeof(PPSE_SELECT_APDU));
        hexdump(loop_buf+2,loop_buf[1]);
        com_exchange(loop_buf);
        LcdDisplayMessageCenterC("nfc apdu test ok");
        
    }
    
    //3.remove
    //等待卡离场,应用代码中可不必等待离场
    dbg("wait remove nfc card\n");
	while(1)
	{
		pcd_antenna_off();
		mdelay(10);
		pcd_antenna_on();
		mdelay(10);	
		status = pcd_request(0x52, tag_type);
		if(status != MI_OK)
		{//一次验证卡离场
			mdelay(100);
			status = pcd_request(0x52, tag_type);
			if(status != MI_OK)
			{//二次验证卡离场
				mdelay(100);
				status = pcd_request(0x52, tag_type);
				if(status != MI_OK)
				{//三次验证卡离场
					mdelay(100);
					pcd_lpcd_config_start(g_lpcd_config.delta, g_lpcd_config.t_inactivity_ms, g_lpcd_config.skip_times, g_lpcd_config.t_detect_us);	
					break;	
				}
			}

		}
        LcdDisplayMessageCenterC("wait remove nfc card\n");
        
	}
    pcd_antenna_off();
    LcdDisplayMessageCenterC("remove nfc card success!\n");mdelay(1000);
    return OK;
}
