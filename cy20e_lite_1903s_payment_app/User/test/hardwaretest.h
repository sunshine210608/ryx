
#include "ugui.h"
#include "lcd.h"
#include "keyboard.h"
#include "systick.h"
#include "common.h"
#include "lcdgui.h"
#include "rtc.h"
#include "touchpanel.h"

#include "test_menu.h"
#include "test_printer.h"
#include "test_lcd.h"
#include "test_sci.h"
#include "test_keyboard.h"
#include "test_msr.h"
#include "test_nfc.h"
#include "test_buzzer.h"
#include "test_tp.h"
#include "test_rtc.h"
#include "test_charge.h"
#include "test_wifi.h"
#include "test_gprs.h"
#include "test_sensor.h"
#include "test_result.h"
#include "test_flash.h"
#include "test_about.h"
#include "test_pmu.h"

#include "common.h"
#include "debug.h"
enum{
    FAIL,
    PASS
};

 
//#define CFG_SENSOR_ENABLE
//#define CFG_QRCODE_ENABLE
//#define UNIT_TEST

#define DEFAULT_TIMEOUTMS           30000
void setAutoTest(char autoflag);
char getAutoTest(void);
void AutoHardwareTest(void);
void enterTestItem(char*itemname);

