
#ifndef TEST_LOOPBACK_H
    #define TEST_LOOPBACK_H

    #ifdef __cplusplus
    extern "C"
    {
    #endif

    #include <stdint.h>
    
    
    void test_sci(uint8_t u8Slot);


    #ifdef __cplusplus
    }
    #endif

#endif

int TestICC(void *m);
int TestSAM(void *m);

