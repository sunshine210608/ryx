#include <stdio.h>
#include <emv_core.h>
#include <iso7816_3.h>

#include <mhscpu.h>
#include <debug.h>

#include "test_emv.h"
#include "test_sci.h"
#include "hardwaretest.h"
#include "sci.h"


#if VERSION_410 == EMV_VERSION

uint8_t cal_lrc(uint8_t *p_buf, uint16_t len)
{
    uint8_t lrc = 0;
    int i = 0;

    for (i = 0; i < len; i++)
    {
        lrc ^= p_buf[i];
    }
    return lrc;
} 

void loop_back(uint8_t u8Slot)
{
    uint8_t i, lrc = 0;
    int32_t s32Stat = 0;
    uint8_t  atr[65];
    ST_APDU_RSP     rsp;
    ST_APDU_REQ     apdu_req;

#if 1
    ST_APDU_REQ  sel_visa_req = {{0x00, 0xA4, 0x04, 0x00},  //cmd
                                  0x00000007,               //lc
                                {0xA0, 0x00, 0x00, 0x00, 0x03, 0x10, 0x10, 0x00},
                                  0x00000100                //le
                                };
#endif

    while (1)
    {
        wait_Ns(3);
        //Init emv_devs param.
        iso7816_device_init();
        while (0 != iso7816_detect(u8Slot))
        {
            dbg("Wait insert ICC!\n");
            delay_Ms(500);
        }
        dbg("Insert IC\n");
        if (0 == (s32Stat = iso7816_init(u8Slot, VCC_5000mV | SPD_1X, atr)))
        {
            //dbg("iso7816_init() finished.\n");
        }
        else
        {
            dbg("iso7816_init() failed %d.\n", s32Stat);
        }

        lrc = 0;
        rsp.len_out = 0;
        while (0 == iso7816_detect(u8Slot) && 0 == s32Stat)
        {
            if ('b' == uart_RecvChar())
            {
                dbg("Break loop!\n");
                break;
            }
            if (rsp.len_out < 6)
            {
                memcpy(&apdu_req, &sel_visa_req, 16);//lc is four bytes len
                apdu_req.cmd[3] = lrc;
                apdu_req.le = 256;
            }
            else
            {
                memcpy(apdu_req.cmd, rsp.data_out, 3);
                apdu_req.cmd[3] = lrc;
                apdu_req.lc = 0;
                apdu_req.le = 0;

                if (rsp.data_out[3] > 2 && rsp.data_out[4] > 0)
                {
                    apdu_req.lc = rsp.data_out[4];
                    for (i = 0; i < apdu_req.lc; i++)
                    {
                        apdu_req.data_in[i] = i;
                    }
                }
                if (4 == rsp.data_out[3] || 2 == rsp.data_out[3])
                {
                    if (0 == rsp.data_out[5])
                    {
                        apdu_req.le = 256;
                    }
                    else
                    {
                        apdu_req.le = rsp.data_out[5];
                    }
                }
            }
            //delay_Ms(300);
            if (0 != (s32Stat = iso7816_exchange(u8Slot, AUTO_GET_RSP, &apdu_req, &rsp)))
            {
                dbg("Exchange failed %d!\n", s32Stat);
                rsp.len_out = 0;
                break;
            }
            /*
            else
            {
                print_hex("recv:", rsp.data_out, rsp.len_out);
                dbg("SWA: %x SWB: %x!\n", rsp.swa, rsp.swb);
            }
            */
            
            if (0x6A == rsp.swa && 0x82 == rsp.swb)
            {
                dbg("This test Case finished!\n");
                break;
            }
            lrc = cal_lrc(rsp.data_out, rsp.len_out);
            lrc ^= rsp.swa;
            lrc ^= rsp.swb;
        }
        iso7816_close(u8Slot);
        //dbg("Shut down ICC!\n");
        /*
        while('c' != s32Stat)
        {
            dbg("Press 'c' to continue\n");
            while (-1UL == (s32Stat = uart_RecvChar()));
            dbg("%c\n", s32Stat);
        }
        */
    }
}

#elif VERSION_43A == EMV_VERSION

#define ICBS_TEST       (1)

#endif

int test_sci0(uint8_t u8Slot,uint8_t s32Vol)
{
    int32_t s32Stat = 0;
    uint8_t  atr[65];
    char buf[32],*vol;
    int ret;
    ST_APDU_RSP     rsp;
    ST_APDU_REQ     apdu_req;
    tick tks=get_tick();
    uint16_t key;
    
    if(s32Vol==VCC_1800mV)
        vol = "1.8v";
    else if(s32Vol==VCC_3000mV)
        vol = "3.0v";
    else if(s32Vol==VCC_5000mV)
        vol = "5.0v";  
        
    
    if(u8Slot == SMARTCARD_SLOT)
        sprintf(buf,"IC Card %s Test",vol);
    else
        sprintf(buf,"SAM Card Test");

    enterTestItem(buf);dbg(buf);
    //1.detect card
    do{
        ret=iso7816_detect(u8Slot);
        if(ret){
            LcdDisplayMessageCenterC("Please insert IC card");
            
        }else
            break;
        KeyBoardRead(&key);
        if(key==KEY_CANCEL)
            return NG;
    }while(!is_timeout(tks, DEFAULT_TIMEOUTMS));
    if(ret){
        LcdDisplayMessageCenterC("detect insert IC card timeout");
        return NG;
    }
    
    if(u8Slot==SMARTCARD_SLOT){
        LcdDisplayMessageCenterC("detected IC Card");
        SCI_SetMV(s32Vol);
    }
    
    //2.init device
    iso7816_device_init();
    if ((s32Stat = iso7816_init(u8Slot, s32Vol | SPD_1X, atr)) != 0)
    {
        dbg("iso7816_init failed %d!\n", s32Stat);
        if(u8Slot==SMARTCARD_SLOT){
            LcdDisplayMessageCenterC("ic card power up failed");
        }
        else{
            LcdDisplayMessageCenterC("sam card power up failed");
        }
        return NG;
    }
    
    //3.do apdu
    dbg("Prepare to send select PSE!\n");
    memcpy(apdu_req.cmd, "\x00\xa4\x04\x00", 4);
    apdu_req.lc = 14;
    memcpy( apdu_req.data_in, "1PAY.SYS.DDF01", apdu_req.lc);
    apdu_req.le = 256;
    
    if (0 != (s32Stat = iso7816_exchange(u8Slot, AUTO_GET_RSP, &apdu_req, &rsp)))
    {
        dbg("Exchange failed %d!\n", s32Stat);
        //power off and return ;
        iso7816_close(u8Slot);
        return NG;
    }

    hexdump(rsp.data_out,rsp.len_out);
    if(u8Slot==SMARTCARD_SLOT)
        LcdDisplayMessageCenterC("ICC apdu test ok");
    else
        LcdDisplayMessageCenterC("SAM apdu test ok");
    
    //4.remove card
    iso7816_close(u8Slot);

    if(u8Slot==SMARTCARD_SLOT){
        LcdDisplayMessageCenterC("please remove ic card");
        tks=get_tick();
        //Waiting for card drawn.
        while ((ret= iso7816_detect(u8Slot))==0){
            if(is_timeout(tks, DEFAULT_TIMEOUTMS)){
                dbg("detect ic tmout3\n");
                break;
            }
            
            KeyBoardRead(&key);
            if(key==KEY_CANCEL)
                return NG;
        }
        if(!s32Stat && ret)
            ret = OK;
        else
            ret = NG;
    }else{
        //sam just do test apdu
        if(!s32Stat)
            ret = OK;
        else
            ret = NG;
    }
    mdelay(1000);
    return ret;
    
}


int TestICC(void *m)
{
    int ret;
    FUNCIN; 
	ret=test_sci0(0,VCC_1800mV); mdelay(1000);
    if(ret) return ret;
    ret=test_sci0(0,VCC_3000mV); mdelay(1000);
    if(ret) return ret;
    ret=test_sci0(0,VCC_5000mV); mdelay(1000);
    if(ret) return ret;
    return OK;
}
int TestSAM(void *m)
{
    FUNCIN; 
	return test_sci0(2,VCC_3000mV);
}


