#include <string.h>
#include <stdio.h>
#include <time.h>
#include "mhscpu.h"
#include "define.h"
#include "hardwaretest.h"
#include "mhscpu.h"
#include "debug.h"
#include "rtc.h"



void test_rtc0(void){
    struct tm systm,pgettm;

    dbg("%s\n",__FUNCTION__);
    //1.set time
    
    memset(&systm,0,sizeof(systm));
    systm.tm_year = 119; //1900 年开始计数
    systm.tm_mon  = 8;   //0~11表示1~12个月
    systm.tm_mday = 24;
    systm.tm_hour = 11;
    systm.tm_min  = 30;
    systm.tm_sec  = 0;

    //1.1 set sec to tim
    rtc_settime(&systm);
    
    //2.get time
    rtc_gettime(&pgettm);
    
    dbg("RTC time:%d %d %d %d:%d:%d\n",pgettm.tm_year+1900,pgettm.tm_mon+1,pgettm.tm_mday,pgettm.tm_hour,pgettm.tm_min,pgettm.tm_sec);
}

// put time on screen in a certain format.
// date format in argument "d": hhmmssYYYYMMDD
// display format on screen (ocuppy row 4 and 5):
//		time:hh:mm:ss
//		date:YYYY-MM-DD
int PutTime(char *d)
{
	char str[32];
	sprintf(str, "Time:%c%c:%c%c:%c%c", d[0], d[1], d[2], d[3], d[4], d[5]);
	LcdPutsc(str, 6);
	sprintf(str, "Date:%c%c%c%c-%c%c-%c%c",
				   	d[6], d[7], d[8], d[9], d[10], d[11], d[12], d[13]);
	LcdPutsc(str, 7);
	return 0;
}

char KeypadScancodeToAscii(char sc)
{
	dbg("sc=0x%x\n",sc);
	if( sc == KEY_0 ) return '0';
	if( sc == KEY_1 ) return '1';
	if( sc == KEY_2 ) return '2';
	if( sc == KEY_3 ) return '3';
	if( sc == KEY_4 ) return '4';
	if( sc == KEY_5 ) return '5';
	if( sc == KEY_6 ) return '6';
	if( sc == KEY_7 ) return '7';
	if( sc == KEY_8 ) return '8';
	if( sc == KEY_9 ) return '9';
	///if( sc == KEY_00) return '*';
	//if( sc == KEY_SP) return '.';
	return 'x';
}


int RtcSetTime(void)
{
	char str[32];
	char date[15];	// store new date. format: hhmmssYYYYMMDD
	int idx = 0,ret;
	int blink=0;

	struct tm systm,pgettm;
    unsigned short keyval;
    unsigned char key;

	LcdClear();
	LcdPutsc("Set new time", 0);
	rtc_gettime(&pgettm);
    sprintf(str,"%02d:%02d:%02d\n",pgettm.tm_hour,pgettm.tm_min,pgettm.tm_sec);
    dbg(str);
	LcdPutsc(str, 3);
	sprintf(str, "%04d-%02d-%02d\n",pgettm.tm_year+1900,pgettm.tm_mon+1,pgettm.tm_mday);
	LcdPutsc(str, 4);
    
	LcdPutsb(" Cancel          OK ");

    memset(date, BLANK, sizeof(date));
	date[0] = CURSOR;
	PutTime(date);
	while(1)
	{
		rtc_gettime(&pgettm);
        sprintf(str,"%02d:%02d:%02d\n",pgettm.tm_hour,pgettm.tm_min,pgettm.tm_sec);
        //dbg(str);
    	LcdPutsc(str, 3);
    	sprintf(str, "%04d-%02d-%02d\n",pgettm.tm_year+1900,pgettm.tm_mon+1,pgettm.tm_mday);
    	LcdPutsc(str, 4);

		PutTime(date);

		//blink
		if(blink++%2)
			date[idx] = ' ';
		else
			date[idx] = CURSOR;
		
		if(KeyBoardRead(&keyval))
			continue;

        key = KEYVALUE(keyval);
		if((key == KEY_0) || (key == KEY_5) ||
			(key == KEY_1) || (key == KEY_6) ||
			(key == KEY_2) || (key == KEY_7) ||
			(key == KEY_3) || (key == KEY_8) ||
			(key == KEY_4) || (key == KEY_9))
		{			
            if(KEYTYPE(keyval)==KEY_RELEASE)
                continue;
            
			if(idx >= sizeof(date)-1)
				continue;
			char ch = KeypadScancodeToAscii(key);
			date[idx] = ch;
			date[++idx] = CURSOR;
			PutTime(date);

		}
		if(keyval == KEY_CLR)
		{
			if(idx <= 0)
				continue;
			date[idx] = BLANK;
			date[--idx] = CURSOR;
			PutTime(date);
		}
		if(keyval == KEY_OK)
		{
			int i;
			if(idx != sizeof(date)-1)
				continue;
			for(i=0; i<14; i++)
				date[i] -= 0x30;

			systm.tm_hour = date[0] * 10 + date[1];
			systm.tm_min  = date[2] * 10 + date[3];
			systm.tm_sec  = date[4] * 10 + date[5];
			
			systm.tm_year = date[6] * 1000 + date[7] * 100;
			systm.tm_year += date[8] * 10 + date[9] - 1900;
			
			systm.tm_mon  = date[10] * 10 + date[11] - 1;
			systm.tm_mday = date[12] * 10 + date[13];

			if(systm.tm_hour > 23) systm.tm_hour = 23;
			if(systm.tm_min  > 59) systm.tm_min  = 59;
			if(systm.tm_sec  > 59) systm.tm_sec  = 59;
			if(systm.tm_year < 100) systm.tm_year = 100;
			if(systm.tm_year > 137) systm.tm_year = 137;
			if(systm.tm_mon  > 11) systm.tm_mon  = 11;
			if(systm.tm_mon  < 0 ) systm.tm_mon  = 0;
			if(systm.tm_mday > 31) systm.tm_mday = 31;
			if(systm.tm_mday < 1) systm.tm_mday = 1;

			ret=rtc_settime(&systm);
            if(ret) dbg("rtc_settime error\n");
			break;
		} 
		if(keyval == KEY_CANCEL)
		{
			break;
		}
	}

    KeyBoardClean();
    LcdClear();
	return 0;
}


int TestRTC(void*m)
{
    struct tm pgettm;
    char buf[48];
    unsigned short keyval;
    int ret;
        
    LcdClear();
    if(((SMenuItem*)m)->name)
        LcdPutsc(((SMenuItem*)m)->name, 0);

    rtc_gettime(&pgettm);
    sprintf(buf,"%04d %02d %02d %02d:%02d:%02d\n",pgettm.tm_year+1900,pgettm.tm_mon+1,pgettm.tm_mday,pgettm.tm_hour,pgettm.tm_min,pgettm.tm_sec);
    dbg(buf);
    KeyBoardClean();
    do{
        if(BPU->SEN_ANA0 & BIT(10)){
            LcdPutsc("USE EXTERN 32K OSC", 2);
        }else
            LcdPutsc("USE INTERN 32K OSC", 2);
            
        rtc_gettime(&pgettm);
        sprintf(buf,"%04d %02d %02d %02d:%02d:%02d\n",pgettm.tm_year+1900,pgettm.tm_mon+1,pgettm.tm_mday,pgettm.tm_hour,pgettm.tm_min,pgettm.tm_sec);
        LcdDisplayMessageCenterC(buf);
        LcdDisplayMessageBottom("Setting");
        ret=KeyBoardRead(&keyval);
        if(ret) continue;

        if((keyval)==KEY_F1 || (keyval)==KEY_F2){
            RtcSetTime();
        }else if((keyval)==KEY_CANCEL)
            break;
    }while(1);
    return MJ;
}


void showrtc(void){
    struct tm pgettm;
    char buf[48];
    rtc_gettime(&pgettm);
    sprintf(buf,"RTC:%04d %02d %02d %02d:%02d:%02d\n",pgettm.tm_year+1900,pgettm.tm_mon+1,pgettm.tm_mday,pgettm.tm_hour,pgettm.tm_min,pgettm.tm_sec);
    dbg(buf);
}
