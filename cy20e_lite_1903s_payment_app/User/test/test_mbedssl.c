/*
 *  SSL client demonstration program
 *
 *  Copyright (C) 2006-2015, ARM Limited, All Rights Reserved
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may
 *  not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 *  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  This file is part of mbed TLS (https://tls.mbed.org)
 */

#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif

#if defined(MBEDTLS_PLATFORM_C)
#include "mbedtls/platform.h"
#else
#include <stdio.h>
#include <stdlib.h>

#define mbedtls_time            time
#define mbedtls_time_t          time_t
#define mbedtls_fprintf         fprintf
#define mbedtls_printf          dbg
#define mbedtls_exit            exit
#define MBEDTLS_EXIT_SUCCESS    EXIT_SUCCESS
#define MBEDTLS_EXIT_FAILURE    EXIT_FAILURE
#endif /* MBEDTLS_PLATFORM_C */

#include "mbedtls/net_sockets.h"
#include "mbedtls/debug.h"
#include "mbedtls/ssl.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/error.h"
#include "mbedtls/certs.h"

#include <string.h>
//#include "ssl_test.h"
#include "debug.h"
#include "mbedssl_cli.h"

#define SERVER_PORT "4433"
//#define SERVER_PORT "4543"
//#define SERVER_NAME "localhost"
#define SERVER_NAME "192.168.2.113"
#define GET_REQUEST "GET / HTTP/1.0\r\n\r\n"

#define DEBUG_LEVEL 3

/*
extern int rtc_gettime(struct tm* pstrtm);
time_t time(time_t* timer)
{
	struct tm pgettm;
	time_t tmsec;
	int ret;
	ret=rtc_gettime(&pgettm);
	if(ret){
		//dbg("get rtc error\n");
		return 0;
	}
	tmsec = mktime(&pgettm);
		
	if(timer!=NULL)
		*timer=tmsec;
	return tmsec;
}
*/

extern int wifi_at_tcp_connect(char *szIpHost, int port);
extern int wifi_at_tcp_send(unsigned char *sndData, int sndLen);
extern int wifi_at_tcp_recv(unsigned char *rcvData, int expRcvLen, int timeoutMs);

#if 0
/**
 * \brief          Read at most 'len' application data bytes
 *
 * \param ctx      SSL context
 * \param buf      buffer that will hold the data
 * \param len      maximum number of bytes to read
 *
 * \return         The (positive) number of bytes read if successful.
 * \return         \c 0 if the read end of the underlying transport was closed
 *                 - in this case you must stop using the context (see below).
 *
 */
int my_recv_timeout( void *ctx, unsigned char *buf, size_t len, uint32_t timeout )
{
	int ret;
	dbg("my_recv_timeout wait rcv:[%d],time=[%d]\n", len, timeout);
	if(timeout==0)
		timeout=20*1000;
	ret=wifi_at_tcp_recv(buf, len, timeout);
	if(ret==0)
		ret=MBEDTLS_ERR_SSL_TIMEOUT;
	dbg("my_recv_timeout recved:[%d]\n", ret);
	hexdump(buf, len);
	return ret;
}

int my_recv( void *ctx, unsigned char *buf, size_t len )
{
	int ret;
	dbg("my_recv wait rcv:[%d]\n", len);
	ret=wifi_at_tcp_recv(buf, len, 10*1000);
	dbg("my_recv recved:[%d]\n", ret);
	return ret;
}

/**
 * \brief          Try to write exactly 'len' application data bytes
 *
 * \warning        This function will do partial writes in some cases. If the
 *                 return value is non-negative but less than length, the
 *                 function must be called again with updated arguments:
 *                 buf + ret, len - ret (if ret is the return value) until
 *                 it returns a value equal to the last 'len' argument.
 *
 * \param ctx      SSL context
 * \param buf      buffer holding the data
 * \param len      how many bytes must be written
 *
 * \return         The (non-negative) number of bytes actually written if
 *                 successful (may be less than \p len).
 */
int my_send( void *ctx, const unsigned char *buf, size_t len )
{
	return wifi_at_tcp_send((unsigned char *)buf, len);
}
#else
extern int my_recv_timeout( void *ctx, unsigned char *buf, size_t len, uint32_t timeout );
extern int my_recv( void *ctx, unsigned char *buf, size_t len );
extern int my_send( void *ctx, const unsigned char *buf, size_t len );
#endif

static void my_debug( void *ctx, int level,
                      const char *file, int line,
                      const char *str )
{
    const char *p, *basename;

    /* Extract basename from file */
    for( p = basename = file; *p != '\0'; p++ )
        if( *p == '/' || *p == '\\' )
            basename = p + 1;

    //mbedtls_fprintf( (FILE *) ctx, "%s:%04d: |%d| %s", basename, line, level, str );
	//fflush(  (FILE *) ctx  );	
	
	dbg("%s:%04d: |%d| %s\n", basename, line, level, str );		
}

int ssl_test(void)
{
    int ret = 1, len;
    int exit_code = MBEDTLS_EXIT_FAILURE;
    mbedtls_net_context server_fd;
    uint32_t flags;
    unsigned char buf[1024];
    const char *pers = "ssl_client1";

    mbedtls_entropy_context entropy;
    mbedtls_ctr_drbg_context ctr_drbg;
    mbedtls_ssl_context ssl;
    mbedtls_ssl_config conf;
    mbedtls_x509_crt cacert;

#if defined(MBEDTLS_DEBUG_C)
    mbedtls_debug_set_threshold( DEBUG_LEVEL );
#endif

    /*
     * 0. Initialize the RNG and the session data
     */
    //mbedtls_net_init( &server_fd );
		
    mbedtls_ssl_init( &ssl );
    mbedtls_ssl_config_init( &conf );
    mbedtls_x509_crt_init( &cacert );
    mbedtls_ctr_drbg_init( &ctr_drbg );

    dbg( "\n  . Seeding the random number generator...\n" );
    fflush( stdout );

    mbedtls_entropy_init( &entropy );
    if( ( ret = mbedtls_ctr_drbg_seed( &ctr_drbg, mbedtls_entropy_func, &entropy,
                               (const unsigned char *) pers,
                               strlen( pers ) ) ) != 0 )
    {
        dbg( " failed\n  ! mbedtls_ctr_drbg_seed returned %d\n", ret );
        goto exit;
    }

    dbg( " ok\n" );

    /*
     * 0. Initialize certificates
     */
    dbg( "  . Loading the CA root certificate ...\n" );
    fflush( stdout );

    ret = mbedtls_x509_crt_parse( &cacert, (const unsigned char *) mbedtls_test_cas_pem,
                          mbedtls_test_cas_pem_len );
    if( ret < 0 )
    {
        dbg( " failed\n  !  mbedtls_x509_crt_parse returned -0x%x\n\n", -ret );
        goto exit;
    }

    dbg( " ok (%d skipped)\n", ret );

    /*
     * 1. Start the connection
     */
    dbg( "  . Connecting to tcp/%s/%s...\n", SERVER_NAME, SERVER_PORT );
    fflush( stdout );

	/*
    if( ( ret = mbedtls_net_connect( &server_fd, SERVER_NAME,
                                         SERVER_PORT, MBEDTLS_NET_PROTO_TCP ) ) != 0 )
    {
        dbg( " failed\n  ! mbedtls_net_connect returned %d\n\n", ret );
        goto exit;
    }
	*/
	ret=wifi_at_tcp_connect(SERVER_NAME, atoi(SERVER_PORT));
	if(ret)
	{
		dbg( " failed\n  ! mbedtls_net_connect returned %d\n\n", ret );
        goto exit;
	}
    dbg( "conn ok\n" );
	
	
    /*
     * 2. Setup stuff
     */
    dbg( "  . Setting up the SSL/TLS structure...\n" );
    fflush( stdout );

    if( ( ret = mbedtls_ssl_config_defaults( &conf,
                    MBEDTLS_SSL_IS_CLIENT,
                    MBEDTLS_SSL_TRANSPORT_STREAM,
                    MBEDTLS_SSL_PRESET_DEFAULT ) ) != 0 )
    {
        dbg( " failed\n  ! mbedtls_ssl_config_defaults returned %d\n\n", ret );
        goto exit;
    }

    dbg( " ok\n" );

    /* OPTIONAL is not optimal for security,
     * but makes interop easier in this simplified example */
    mbedtls_ssl_conf_authmode( &conf, MBEDTLS_SSL_VERIFY_OPTIONAL );
    mbedtls_ssl_conf_ca_chain( &conf, &cacert, NULL );
    mbedtls_ssl_conf_rng( &conf, mbedtls_ctr_drbg_random, &ctr_drbg );
    mbedtls_ssl_conf_dbg( &conf, my_debug, stdout );

    if( ( ret = mbedtls_ssl_setup( &ssl, &conf ) ) != 0 )
    {
        dbg( " failed\n  ! mbedtls_ssl_setup returned %d\n\n", ret );
        goto exit;
    }

    if( ( ret = mbedtls_ssl_set_hostname( &ssl, SERVER_NAME ) ) != 0 )
    {
        dbg( " failed\n  ! mbedtls_ssl_set_hostname returned %d\n\n", ret );
        goto exit;
    }

    //mbedtls_ssl_set_bio( &ssl, &server_fd, mbedtls_net_send, mbedtls_net_recv, NULL );
	mbedtls_ssl_set_bio( &ssl, &server_fd, my_send, my_recv, my_recv_timeout );

    /*
     * 4. Handshake
     */
    dbg( "  . Performing the SSL/TLS handshake...\n" );
    fflush( stdout );

    while( ( ret = mbedtls_ssl_handshake( &ssl ) ) != 0 )
    {
        if( ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE )
        {
            dbg( " failed\n  ! mbedtls_ssl_handshake returned -0x%x\n\n", -ret );
            goto exit;
        }
    }

    dbg( " ok\n" );

    /*
     * 5. Verify the server certificate
     */
    dbg( "  . Verifying peer X.509 certificate...\n" );

    /* In real life, we probably want to bail out when ret != 0 */
    if( ( flags = mbedtls_ssl_get_verify_result( &ssl ) ) != 0 )
    {
        char vrfy_buf[512];

        dbg( " failed\n" );

        mbedtls_x509_crt_verify_info( vrfy_buf, sizeof( vrfy_buf ), "  ! ", flags );

        dbg( "%s\n", vrfy_buf );
    }
    else
        dbg( " ok\n" );

    /*
     * 3. Write the GET request
     */
    dbg( "  > Write to server:\n" );
    fflush( stdout );

    len = sprintf( (char *) buf, GET_REQUEST );

    while( ( ret = mbedtls_ssl_write( &ssl, buf, len ) ) <= 0 )
    {
        if( ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE )
        {
            dbg( " failed\n  ! mbedtls_ssl_write returned %d\n\n", ret );
            goto exit;
        }
    }

    len = ret;
    dbg( " %d bytes written\n\n%s\n", len, (char *) buf );

    /*
     * 7. Read the HTTP response
     */
    //dbg( "  < Read from server:" );
    //fflush( stdout );
	dbg( "  < Read from server:\n" );
	
    do
    {
        len = sizeof( buf ) - 1;
        memset( buf, 0, sizeof( buf ) );
        ret = mbedtls_ssl_read( &ssl, buf, len );

		dbg("*** mbedtls_ssl_read=%d\n", ret);
        if( ret == MBEDTLS_ERR_SSL_WANT_READ || ret == MBEDTLS_ERR_SSL_WANT_WRITE )
		{
			dbg("*** mbedtls_ssl_read continue.\n", ret);
            continue;
		}
		
        if( ret == MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY )
		{
			dbg("*** peer close\n");
            break;
		}
		
        if( ret < 0 )
        {
            dbg( "failed\n  ! mbedtls_ssl_read returned %d\n\n", ret );
            break;
        }

        if( ret == 0 )
        {
            dbg( "\n\nEOF\n\n" );
            break;
        }

        len = ret;
		dbg( "\n****** %d bytes read[%d]:\n", len, strlen((char *) buf));
        dbg( "[%s]\n\n", (char *) buf );
    }
    while( 1 );

	dbg("*** recv end.\n");
	
    mbedtls_ssl_close_notify( &ssl );

    exit_code = MBEDTLS_EXIT_SUCCESS;

exit:

#ifdef MBEDTLS_ERROR_C
    if( exit_code != MBEDTLS_EXIT_SUCCESS )
    {
        char error_buf[100];
        mbedtls_strerror( ret, error_buf, 100 );
        dbg("Last error was: %d - %s\n\n", ret, error_buf );
    }
#endif

    //mbedtls_net_free( &server_fd );

    mbedtls_x509_crt_free( &cacert );
    mbedtls_ssl_free( &ssl );
    mbedtls_ssl_config_free( &conf );
    mbedtls_ctr_drbg_free( &ctr_drbg );
    mbedtls_entropy_free( &entropy );

    dbg( "  + Press Enter to exit this program.\n" );
    
    return( exit_code );
}

int iTestSSL2(void)
{
	int ret;
	unsigned char send[1024];
	unsigned char recv[2048];
	
	char cli_cert[]="-----BEGIN CERTIFICATE-----\r\n"
"MIIDWTCCAkGgAwIBAgIEElgeuDANBgkqhkiG9w0BAQsFADBdMRgwFgYDVQQDEw8x\r\n"
"OTIuMTY4LjE2OS4yNTExDDAKBgNVBAsTA2RldjEMMAoGA1UEChMDanNkMQswCQYD\r\n"
"VQQHEwJTWjELMAkGA1UECBMCR0QxCzAJBgNVBAYTAkNOMB4XDTE5MTIyNzA3MTIx\r\n"
"MFoXDTI5MTIyNDA3MTIxMFowXTEYMBYGA1UEAxMPMTkyLjE2OC4xNjkuMjUxMQww\r\n"
"CgYDVQQLEwNkZXYxDDAKBgNVBAoTA2pzZDELMAkGA1UEBxMCU1oxCzAJBgNVBAgT\r\n"
"AkdEMQswCQYDVQQGEwJDTjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEB\r\n"
"AI3PU7ZWCWxBcKf+2W9aY3UJnlc/2YGhiwOpEVp/lGp+MyB2FH5X7GJ7+K38i1MB\r\n"
"hrlSr42tPbNxOA2E8OD2YAD8QX8A4Y7rimJwI+HIi9HH+KuYO/FMlrd644gl6O4Q\r\n"
"Jh+YqpFIvYhh3k1EmtGhbgNfFXIag/IDFoIPaxsSqFmdIOIrB7qJxfDpKLxmarM6\r\n"
"FUGO8mYK0cjBZ1+UPvk/bY2xqzmDL+07PpeXqvMZQRwilh6rcqxTiI69oQZiQ13D\r\n"
"N3ZGI1P7dRUaE94zQ29Rp6GVDv086MmDkHU3q3vaIs2fneeIsoLSDM46weDip6gT\r\n"
"lY0v230eilmB4n0isML2ev0CAwEAAaMhMB8wHQYDVR0OBBYEFK/DYNUdVAOWEIY3\r\n"
"V6hCc3QqKiaXMA0GCSqGSIb3DQEBCwUAA4IBAQAtW0ZJkiYzBO+Ro8hGuQEFvqpn\r\n"
"YXlYVUfLSLMXEyUa6o1XK83bOJnqO/9DPqwjUkrjw6xwbh8TVU9zvYHh1+J+dcZS\r\n"
"wN0MmGUpBRT1Vt/NpHIPkEAopDXBpyxHNZejuhnmemwPlBOwofUa9l7xcinMbgRt\r\n"
"wl8JIOfOnpDdd4rgtYzOc74U+ROECD1Ha0CI9Rjs5WKSMBG3ZpiF4QYvU9+qOO2r\r\n"
"c6sW9NAgF6HNA0uQYDSJoZGsMnEakwMX1wT1f8rnzK3OOLNfeO5S2TL6Cvk6w7Mq\r\n"
"X+6j80yqDbd0yIgTC+johgndFUZ6BuEKHNQTfpaaCs9fQ5EiI3uBxDSgeqe9\r\n"
"-----END CERTIFICATE-----\r\n";
	
	char cli_key[]="-----BEGIN PRIVATE KEY-----\r\n"
"MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCNz1O2VglsQXCn\r\n"
"/tlvWmN1CZ5XP9mBoYsDqRFaf5RqfjMgdhR+V+xie/it/ItTAYa5Uq+NrT2zcTgN\r\n"
"hPDg9mAA/EF/AOGO64picCPhyIvRx/irmDvxTJa3euOIJejuECYfmKqRSL2IYd5N\r\n"
"RJrRoW4DXxVyGoPyAxaCD2sbEqhZnSDiKwe6icXw6Si8ZmqzOhVBjvJmCtHIwWdf\r\n"
"lD75P22Nsas5gy/tOz6Xl6rzGUEcIpYeq3KsU4iOvaEGYkNdwzd2RiNT+3UVGhPe\r\n"
"M0NvUaehlQ79POjJg5B1N6t72iLNn53niLKC0gzOOsHg4qeoE5WNL9t9HopZgeJ9\r\n"
"IrDC9nr9AgMBAAECggEAbNQ6o0j97EWAZnzPc4SDpVMfYEgQ7UoDPGjod3JHfT6i\r\n"
"j/vumFNzhtUrMKMKbUZTlS/eqDTWkDqIUN/AWHTbkoYE4anUcHFU/1yePO1CWFlF\r\n"
"/rJS9kjXtbGqolatnt1n1IG+DMctJVguWVAYHF7t5cp309yDfBxgjqm0gc9cwYAG\r\n"
"icGZFrGcO8i/sljseV32z09BA22nyBEZ1Exkc6MF2dSbfjrsbNrYwJFVL4Lflf+w\r\n"
"/+L8wmMt9ORiQ5k7k65LaGoTQ+kE5Ykltcpu9HKDVlz+oMAKPfWEdT8MfPm3Md9W\r\n"
"wglFHrpFqI1Wy+vtLnphA8aw2qmA2rW5QN2f19Bp5QKBgQDzIFQ5uZkr6zp7kk/L\r\n"
"mRh6Q1JKXA28PFgvgr/1jPThyvQnLHxBxuN0SZREgcnSMIToJ8CN52dFRFtZBHsn\r\n"
"V7kTY/Esm+04vSgJAg2zpnU5ABqFO8Xx+Ty5vJ49+7uMNyRVZwIAXgD7gPLFIYZn\r\n"
"ihoZ1LgcTJdA3aO+iLYEfMniLwKBgQCVUZ1gGC/xDGlO0PW4iQD7oT96zQ2dhfkJ\r\n"
"K5hrfCTfYnIoPSVAwtfDpmkMULlYSTLd1nv9dr8ShfCdyzqYIK5g5KRRaDKYJ1DQ\r\n"
"/1vAMiel0R0u2vJgI+vg/m0Qnlui7t8LHCrGe7xvnBbWNxFSDAeQkXw1IKAmH20F\r\n"
"asRUK2uGkwKBgQCwMtXXn/KqKagQtlCuNR3QPcn9qgkqSnF+vTtxMd6nZPbdDRhg\r\n"
"c0uUk16o54bkldU8itK6BOKLCKdLNDwsnx66NswqkDaz0CKbpKlZcWGzFagittcW\r\n"
"LpMb6N6l/TJGxA/I9QY2TepYW3OV+l3129hesBNeLwPPNtHc3CdNyUJmcQKBgQCF\r\n"
"XihatD/d5WAJ7coL0RL3rcatQIlwsUEGV9ID7xZgD+Y22qZzeZSORAx/23owyPCO\r\n"
"BA0rDu0K9mc8CVGEn2whTxcVPyQxkqw0gGDLAgE3sdeHCjiCdpMwmw9/UHI4zXKa\r\n"
"0cXDErH0Xk5ndzgZOHVpQwjVyxY/9sHBnONO6plX0QKBgQDHkzyyzQkiUXwP/CNJ\r\n"
"uiSXyhOZvnazxzH8NT9qddkmgHbzhI4R5bOf3Z2w7C96XQpqiOm64JX+JzMjQC0n\r\n"
"UqVYNohgQFI24JgdEWpRWz3oZWi+0RfDj/hEAq3mKhJYtek64xgaQs+tj0tFXoOC\r\n"
"eZl4n6P8fjY9K41htH162OuRqA==\r\n"
"-----END PRIVATE KEY-----\r\n";
	
	dbg("cli cert len=[%d]\n", strlen(cli_cert));
	dbg("cli prikey len=[%d]\n", strlen(cli_key));
	ret=iSSL_Init(NULL, cli_cert, cli_key);
	dbg("iSSL_Init ret=%d\n", ret);
	if(ret)
	{
		iSSL_Free();
		return ret;
	}
	
	ret=iSSL_Connect("192.168.2.113", 4455, 10);
	dbg("iSSL_Connect ret=%d\n", ret);
	if(ret)
	{
		iSSL_Free();
		return ret;
	}
	
	strcpy((char*)send, "GET / HTTP/1.0\r\n\r\n");
	ret=iSSL_Send(send, strlen((char*)send));
	dbg("iSSL_Send ret=%d\n", ret);
	if(ret<=0)
	{
		iSSL_DisConnect();
		iSSL_Free();
		return ret;
	}
	
	ret=iSSL_Recv(recv, sizeof(recv)-1, 20*1000);
	dbg("iSSL_Recv ret=%d\n", ret);
	if(ret<=0)
	{
		iSSL_DisConnect();
		iSSL_Free();
		return ret;
	}
	recv[ret]=0;
	dbg("recv data=[%.100s]\n", recv);
	
	iSSL_DisConnect();
	iSSL_Free();
	
	return 0;
}

