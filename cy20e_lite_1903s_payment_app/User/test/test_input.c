#include <stdio.h>
#include <string.h>
#include "VposFace.h"
#include "pub.h"


void vTestInput(void)
{
	int ret;
	int line=1;
	char szTmp[20];
	
	_vPosInit();		//初始化
	vInitInput();		//必须先调用1次
	
	_vCls();
	
	_vDisp(line, "input num1:[      ]");
	ret=iInput(INPUT_NORMAL, line, 12, (unsigned char *)szTmp, 6, 3, 6, 30);
	if(ret>0)
	{
		vDispVarArg(line+1, "You input is [%s]", szTmp);
	}else
	{
		vDispVarArg(line+1, "input fail: [%d]", ret);
	}
	_uiGetKey();
	
	line++;
	
	//使用初值,达到最大长度后直接返回
	_vDisp(line, "input num2:[      ]");
	strcpy(szTmp, "123");
	ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_BY_LEN, line, 12, szTmp, 6, 3, 6, 30);
	if(ret>0)
	{
		vDispVarArg(line+1, "You input is [%s]", szTmp);
	}else
	{
		vDispVarArg(line+1, "input fail: [%d]", ret);
	}
	_uiGetKey();	
	
	line++;
	
	//输入金额
	_vDisp(line, "input amt1:[        ]");
	strcpy(szTmp, "321");
	ret=iInput(INPUT_MONEY|INPUT_INITIAL, line, 12, szTmp, 8, 1, 8, 30);
	if(ret>0)
	{
		vDispVarArg(line+1, "You input is [%s]", szTmp);
	}else
	{
		vDispVarArg(line+1, "input fail: [%d]", ret);
	}
	_uiGetKey();
	
	line++;
	
	//带小数点输入金额
	_uiMapKey(_KEY_FN, _KEY_DOT);			//功能键映射为小数点键
	_vDisp(line, "input amt2:[        ]");
	ret=iInput(INPUT_DOT_MONEY, line, 12, szTmp, 8, 1, 8, 30);
	if(ret>0)
	{
		vDispVarArg(line+1, "You input is [%s]", szTmp);
	}else
	{
		vDispVarArg(line+1, "input fail: [%d]", ret);
	}
	_uiMapKey(_KEY_FN, _KEY_FN);			//恢复原值
	_uiGetKey();
	
	line++;	

	//输入密码
	_vDisp(line, "input pin :[      ]");
	ret=iInput(INPUT_PIN, line, 12, szTmp, 6, 6, 6, 30);
	if(ret>0)
	{
		vDispVarArg(line+1, "You input is [%s]", szTmp);
	}else
	{
		vDispVarArg(line+1, "input fail: [%d]", ret);
	}
	_uiGetKey();
	
	line++;	
	
	//输入字母
	{
		uchar *apszAlphaTable[10] = {
			"0.!@#$%^&*()-=_+;':\",/<>?`~", 
			"1qzQZ", 
			"2abcABC", 
			"3defDEF", 
			"4ghiGHI", 
			"5jklJKL", 
			"6mnoMNO", 
			"7pqrsPQRS", 
			"8tuvTUV", 
			"9wxyzWXYZ"
		};
		
		vSetAlphaTable(apszAlphaTable);
		
		_vDisp(line, "input char:[          ]");
		strcpy(szTmp, "test");
		ret=iInput(INPUT_ALPHA|INPUT_INITIAL, line, 12, szTmp, 10, 4, 10, 30);
		if(ret>0)
		{
			vDispVarArg(line+1, "You input is [%s]", szTmp);
		}else
		{
			vDispVarArg(line+1, "input fail: [%d]", ret);
		}
		_uiGetKey();
		
		line++;
	}	
}
