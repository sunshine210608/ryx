#include <string.h>
#include <stdio.h>
#include <time.h>
#include "mhscpu.h"
#include "define.h"
#include "hardwaretest.h"
#include "mhscpu.h"
#include "debug.h"
#include "battery.h"



int TestCharge(void*m){
    uint16_t keyval;
    char str[32],charge;
    int ret=NG,mv,cnt=0,lastchr=0xff;
    
    LcdClear();
    LcdPutsc(((SMenuItem*)m)->name, 0);
    while(1){
        mv=battery_get_voltage();
        sprintf(str,"Battery Voltage:%dmv",mv);
        LcdPutsc(str, 4); //dbg(str);
        charge = battery_charger_getstate();
        sprintf(str,"State:%s\n",charge==BATTERY_IS_CHARGING?"Charging":"No Charging");
        LcdPutsc(str, 6);//dbg(str);

        if(lastchr==0xff) lastchr = charge;
        if(charge != lastchr){
            lastchr = charge;
            if(++cnt >= 6)
                return OK;
        }
        if(KeyBoardRead(&keyval)) continue;
        if(keyval == KEY_CANCEL)
            break;
    }
    return ret;
}
