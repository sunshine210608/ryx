/********************************************************************************

 *************************Copyright (C), 2019, KingShiDun Inc.**************************

 ********************************************************************************
 * @file     	 : test_about.c
 * @brief   		  : .C file function description
 * @author       : Jay
 * @version        : 1.0
 * @date          : 2019-11-08
 * 
 * 
 * @note History:
 * @note        : Jay 2019-11-08
 * @note        : 
 *   Modification: Created file

********************************************************************************/
#include <stdio.h>
#include "hardwaretest.h"
#include "_compileinfo.h"
int TestAbout(void*m){
    char ln = 3;
    char buf[32];
    uint16_t key;
    LcdClear();
    LcdPutsc("Test About", 0);

    

    sprintf(buf,"Firmware:%s",FWVERSION);
    LcdPutsl(buf, ln++);

    sprintf(buf,"VersionCode:%d",VERSIONCODE);
    LcdPutsl(buf, ln++);

    sprintf(buf,"Author:%s",AUTHOR);
    LcdPutsl(buf, ln++);

    sprintf(buf,"Time:%s",COMPILE_TIME);
    LcdPutsl(buf, ln++);

    KeyBoardClean();
    while(1){
        KeyBoardRead(&key);
        if(key==KEY_CANCEL)
            break;
    }
    return 0;
}
