
#include "resetmcu.h"
//
// Created by jay on 2016/12/9.
//
//#include <fcntl.h>
#include "emvDebug.h"
#include "err.h"
#include "resetmcu.h"
#include "define.h"

int ResetMCU(void)
{
#ifndef CY20_EMVL2_KERNEL	
    int fd;
    if((fd=open(RESET_PIN_DEV,O_RDWR))<0){
        LOGE("Open %s failed.",RESET_PIN_DEV);
        return ERR_OPENUART;
    }

    if( write(fd,RESET_PIN_LOW, sizeof(RESET_PIN_LOW)) != sizeof(RESET_PIN_LOW))
    {
        LOGE("%s Error 2",__FUNCTION__);
        return ERR_WRITEUART;
    }
    //TODO maybe we can reduce this value again.
    usleep(100);
    if( write(fd,RESET_PIN_HIGH, sizeof(RESET_PIN_HIGH)) != sizeof(RESET_PIN_HIGH))
    {
        LOGE("%s Error 3",__FUNCTION__);
        return ERR_WRITEUART;
    }
    close(fd);
    LOGD("%s Done",__FUNCTION__);
    return ERR_NONE;
		
#else
		return ERR_NONE;
#endif
}
