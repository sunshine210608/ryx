/********************************************************************************

 *************************Copyright (C), 2017, ScienTech Inc.**************************

 ********************************************************************************
 * File Name     : comm.c
 * Author        : Jay
 * Date          : 2017-02-08
 * Description   : .C file function description
 * Version       : 1.0
 * Function List :
 * 
 * Record        :
 * 1.Date        : 2017-02-08
 *   Author      : Jay
 *   Modification: Created file
 * This file is used to POS send command to Server and then get response from Server (Server eg: TMS).
********************************************************************************/


#include <stdint.h>
#include <string.h>
#include "systick.h"
#include "protocol.h"
#include "serial.h"
#include "emvDebug.h"
#include "err.h"
#include "wakeupmcu.h"
#include "resetmcu.h"
#include "gprs.h"
#include "define.h"
#include "msr.h"
#include "tcpcomm.h"
#include "customize_emvl2.h"
#include "sec_common.h"
#include "nfc.h"

#define STX 0x02
#define ETX 0x03
#define ACK 0x06


int sendcmd(unsigned char cmd,unsigned char*data,unsigned short datalen)
{
    int len=0;
    uint8_t buf[MAX_BUFFER_SIZE];
    buf[len++]=STX;
    buf[len++]=cmd;
    buf[len++]=(uint8_t)(datalen>>8);
    buf[len++]=(uint8_t)(datalen&0xff);
    if(datalen>0){
        memcpy(buf+len,data,datalen);
        len+=datalen;
    }
    buf[len++]=ETX;
    buf[len++]=CalculateLRC(buf,len);
    //hexdump(buf,len);
#ifndef CY20_EMVL2_KERNEL
    if(serialwrite(GPRS_UART,buf,(size_t)len) != len){
        LOGE("send cmd failed");
        return -1;
    }
#else
	if (iCommTcpSend(buf, len) != len)
	{
		dbg("send cmd failed\n");
		return -1;
	}
	
#endif
    return 0;
}

/**
 * 5 bytes  STX CMD len1 len0 data ETX LRC
 */
int getresponse(char cmdid,int timeout,unsigned char*res,int*reslen)
{
    uint8_t buf[MAX_BUFFER_SIZE];
    uint16_t datalen;
    int len;
#ifndef CY20_EMVL2_KERNEL
    len= (uint16_t)serialread(fd,buf,4,timeout);
#else
	len = iCommTcpRecv(buf, 4, timeout);
#endif
    if(len==4 && buf[0] == STX){
        datalen = ((buf[2]<<8)+buf[3]);
        //LOGD("GET CTL len = %d",datalen);
        if(datalen+6 > MAX_BUFFER_SIZE){
            LOGE("more that max buffer\n");
            return ERR_SIZE;
        }
#ifndef CY20_EMVL2_KERNEL
        len = (uint16_t)serialread(fd,buf+4,datalen+2,500);
#else
        len = iCommTcpRecv(buf+4,datalen+2,1000);
#endif
        if(len!=(datalen+2)){
            //vMessageVarArg("TcpRecv len=%d, ex=%d", len, datalen+2);
            
            LOGE("read err %d %d\n",len,datalen+2);
            return ERR_READ;
        }
        if(CalculateLRC(buf,datalen+6)){
            LOGE("Lrc err %x %x",*(buf+datalen+6-1),CalculateLRC(buf,datalen+6));
            //hexdump(buf,datalen+6);
            return ERR_LRC;
        }
        //hexdump(buf,datalen+6);
        //LOGD("%s\n",buf+6);
        if(cmdid!=buf[1]){
            LOGE("cmdid not match %x %x\n",cmdid,buf[1]);
            return ERR_CMDID;
        }
        memcpy(res,buf+4,datalen);
        *reslen = datalen;
        return ERR_NONE;
    }
	//vMessageVarArg("TcpRecv len=%d, buf[0]=%02X", len, buf[0]);
    LOGE("Not get response %d",len);
    return ERR_NORES;
}

int executecmd(char cmd,int timeout,unsigned char *res,int *rlen)
{
    int ret;
    int len;

    //LOGD("sendcmd");
    ret=sendcmd(cmd,NULL,0);
    if(ret){
        LOGE("sendcmd error");
        return ret;
    }
  
    //LOGD("wait response");
    //if((ret=getresponse(cmd,timeout,res,&len))){
    ret = getresponse(cmd,timeout,res,&len);
	if (ret != 0)
	{
        LOGE("get response error %d",ret);
        return ret;
    }
 
    *rlen = len;
    //LOGD("%s Done",__FUNCTION__);
    return ERR_NONE;
}

int executecmd_ex(char cmd,int timeout,unsigned char *cmddata, int cmdlen,unsigned char *res,int *rlen)
{
    int ret;
    int len;
    
    //LOGD("sendcmd");
    //LOGD("cmdlen = %d\n", cmdlen);
    ret=sendcmd(cmd,cmddata,cmdlen);
    if(ret){
        //vMessageVarArg("sendcmd ret:%d", ret);
        LOGE("sendcmd error");
        return ret;
    }
 
    //LOGD("wait response");
    //if((ret=getresponse(cmd,timeout,res,&len))){
    ret = getresponse(cmd,timeout,res,&len);
	if (ret != 0)
	{
        //vMessageVarArg("getresponse ret:%d", ret);
        LOGE("get response error %d",ret);
        return ret;
    }
 
    *rlen = len;
    //LOGD("%s Done",__FUNCTION__);
    return ERR_NONE;
}


 #if 0
 /*status :
           '0', Detect Type A or Type B card.
           '1', Card col
           'F', no card detect

 return value: 0 is ok, other is failed.
 */
int nfc_detect(unsigned char *status)
{
    int ret;
    int tmplen;
    unsigned char tmp[1024];

    memset(tmp, 0, sizeof(tmp));
    ret = executecmd(0x18, 5000, tmp, &tmplen);
    if (ret == 0)
    {
        *status = tmp[0];
        //LOGD("Status:%x,Datalen:%d\n",*status);
        //hexdump(data,*datalen);
    }

    return ret;
}

/*status :
           '0', Detect Type A or Type B card.
           'F', no card detect

 return value: 0 is ok, other is failed.
 */
int nfc_apdu(unsigned char *status, unsigned char * capdu, int capdulen, unsigned char * rapdu, int *rapdulen)
{
    int ret;
    int tmplen;
    unsigned char tmp[1024];

    if (capdu == NULL)
    {
        LOGE("Parameters error");
        return -2;
    }

    memset(tmp, 0, sizeof(tmp));
    ret = executecmd_ex(0x19, 1000*10, capdu, capdulen, tmp, &tmplen);
    if (ret == 0)
    {
        *status = tmp[0];
        memcpy(rapdu, tmp + 1, tmplen - 1);
        *rapdulen = tmplen - 1;
        //LOGD("Status:%x,Datalen:%d\n",*status);
        //hexdump(data,*datalen);
    }

    return ret;
}

/*status :
           '0', Deselect OK
           'F', Deselect error

 return value: 0 is ok, other is failed.
 */
int nfc_deselect(unsigned char *status)
{
    int ret;
    int tmplen;
    unsigned char tmp[1024];

    memset(tmp, 0, sizeof(tmp));
    ret = executecmd(0x1a, 1000, tmp, &tmplen);
    if (ret == 0)
    {
        *status = tmp[0];
        //LOGD("Status:%x,Datalen:%d\n",*status);
        //hexdump(data,*datalen);
    }

    return ret;
}
#else
/*status :
           '0', Detect Type A or Type B card.
           '1', Card col
           'F', no card detect

 return value: 0 is ok, other is failed.
 */
int nfc_open(void)
{
	NFCOpen();

	return 0;
}

int nfc_close(void)
{
	NFCClose();

	return 0;
}

int nfc_detect(byte *status)
{
	int ret;
/*
#ifdef 	NFC_IC_MH1XXX
    ret = picc_detect();

	if (ret == 0)
	{
		*status = '0';
	}else if (ret == -1)
	{
		*status = 'F';
	}else if (ret == -2)
	{
		*status = '1';
	}else
	{
		*status = 'F';
	}

	return 0;
#else
	ret = NFCDetect();

	if (ret == 0)
	{
		*status = '0';
	}else 
	{
		
	}
#endif
*/
	ret = NFCDetect();

	if (ret == NFC_ERR_NONE)
	{
		*status = '0';
	}else if (ret == NFC_ERR_NO_CARD)
	{
		*status = 'F';
	}else if (ret == NFC_ERR_COLLISION)
	{
		*status = '1';
	}else
	{
		*status = '2';
	}

	return 0;
}

#ifdef FUDAN_CARD_DEMO
int nfc_fudan_detect(byte *status, byte *uid, int *uidLen)
{
	int ret;
	
    ret = picc_fudan_detect(uid, uidLen);
	if (ret == 0)
	{
		*status = '0';
	}else if (ret == -1)
	{
		*status = 'F';
	}else if (ret == -2)
	{
		*status = '1';
	}else
	{
		*status = 'F';
	}

	return 0;
	
}
#endif

/*
 return value: 0 is ok, other is failed.
 */
int nfc_apdu(byte *status, unsigned char * capdu, int capdulen, unsigned char * rapdu, int *rapdulen)
{
	int ret;
	
	//dbg("capdulen = %d\n", capdulen);
    //ret = picc_apdu(capdu, capdulen, rapdu, rapdulen);
	ret = NFCSendAPDU(capdu, capdulen, rapdu, (unsigned short *)rapdulen);
	if (ret != 0)
	{
		*status = '1';
	}else
	{
		*status = '0';
	}

	return 0;
}

/*status :
         
 return value: 0 is ok, other is failed.
 */
int nfc_deselect(byte *status)
{
	NFCClose();
	
	*status = '0';
	
	return 0;
}

#endif

void msr_open(void)
{
	msr_powerup();    
	mdelay(150);    
	reset_adc();
}

void msr_close(void)
{
	msr_powerdown();
}
#if 0
/*status : '0', swipe card OK
           'F', No swipe card

 *data : if status = '0',the data will be filled with MSR data
        MSR format:{1}XXXXXXX{2}XXXXXXX{3}XXXXXXXXX
        if status is other value, no data

 *datalen : if  status = '0', the datalen will be filled with MSR data length
            if status is other value, *datalen = 0;

 return value: 0 is ok, other is failed.
 */
int wait_msr_event(unsigned char *status, unsigned char *data, int *datalen)
{
    int ret;
    int tmplen;
    unsigned char tmp[1024];

    memset(tmp, 0, sizeof(tmp));
    ret = executecmd(0x1b, 30*1000,tmp,&tmplen);
    if (ret == 0)
    {
        *status = tmp[0];
        memcpy(data, tmp + 1, tmplen - 1);
        *datalen = tmplen - 1;
        if (*status == '0')
        {
            LOGD("Status:%x,Datalen:%d\n", *status, *datalen);
        }
            //hexdump(data,*datalen);
    }

    return ret;
}
#else
/*status : '0', swipe card OK
           'F', No swipe card

 *data : if status = '0',the data will be filled with MSR data
        MSR format:{1}XXXXXXX{2}XXXXXXX{3}XXXXXXXXX
        if status is other value, no data

 *datalen : if  status = '0', the datalen will be filled with MSR data length
            if status is other value, *datalen = 0;

 return value: 0 is ok, other is failed.
 */
int wait_msr_event(unsigned char *status, unsigned char *data, int *datalen)
{
  	int ret;
	//int ifd;
	track_data msr[MSR_MAX_TRACK];
	int loop = 0;

	msr_open();

	while(loop ++ < 10)
	{
		memset(msr, 0x00, sizeof(msr));
		ret = readMSRData(&msr);
		if (ret == 0)
		{
			//Swipe card
			*status = '0';
			 dbg("%d %d %d\n",msr[0].len,msr[1].len,msr[2].len);
			 if (msr[1].len != 0)
			 {
			 	dbg("msr[1].buf = %s\n", msr[1].buf);
				sprintf((char *)data, "{2}%s", msr[1].buf);
				*datalen = strlen((char *)data);
			 }
			 msr_close();
			 return 0;
		}

		mdelay(100);
	}
	*status = 'F';
	 msr_close();
	return 0;
	
}

#endif

/*status : '0', swipe card OK
		  '1', ICC Power on OK
		  '2', ICC Present but power on failed.
           'F', Timeout
           
 *data : if status = '0',the data will be filled with MSR data
        MSR format:{1}XXXXXXX{2}XXXXXXX{3}XXXXXXXXX
        if status is other value, no data

 *datalen : if  status = '0', the datalen will be filled with MSR data length
            if status is other value, *datalen = 0;

 return value: 0 is ok, other is failed.
 */
 #if 0
int wait_msr_icc_present_event(unsigned char *status, unsigned char *data, int *datalen)
{
    int ret;
    int tmplen;
    unsigned char tmp[1024];

    memset(tmp, 0, sizeof(tmp));
    ret = executecmd(ICC_MSR_PRESENT_CHECK, 30*1000,tmp,&tmplen);
    if (ret == 0)
    {
        *status = tmp[0];
        memcpy(data, tmp + 1, tmplen - 1);
        *datalen = tmplen - 1;
        if (*status == '0')
        {
            LOGD("Status:%x,Datalen:%d\n", *status, *datalen);
        }
            //hexdump(data,*datalen);
    }

    return ret;
}
 
#else
/*status : '0', swipe card OK
		  '1', ICC Power on OK
		  '2', ICC Present but power on failed.
           'F', Timeout
           
 *data : if status = '0',the data will be filled with MSR data
        MSR format:{1}XXXXXXX{2}XXXXXXX{3}XXXXXXXXX
        if status is other value, no data

 *datalen : if  status = '0', the datalen will be filled with MSR data length
            if status is other value, *datalen = 0;

 return value: 0 is ok, other is failed.
 */

int wait_msr_icc_present_event(unsigned char *status, unsigned char *data, int *datalen)
{
  	int ret;
	int ifd;
	int iloop = 0;
	track_data msr[MSR_MAX_TRACK];

	msr_open();

	while(iloop++ < 10)
	{
		memset(msr, 0x00, sizeof(msr));
		ret = readMSRData(&msr);
		if (ret == 0)
		{
			//Swipe card
			*status = '0';
			dbg("Swipe card OK\n");
			dbg("%d %d %d\n",msr[0].len,msr[1].len,msr[2].len);
			if (msr[1].len != 0)
			{
				dbg("msr[1].buf = %s\n", msr[1].buf);
			sprintf((char *)data, "{2}%s", msr[1].buf);
			*datalen = strlen((char *)data);
			}

			msr_close();
			return 0;
		}else
		{
			//dbg("No Swipe card \n");
		}

		if (SCR_Detect(ifd) == TRUE)
		{
			if (SCR_PowerOn(ifd, NULL, NULL) == TRUE)
			{
				*status = '1';
				SCR_PowerOff(ifd);
			}else
			{
				*status = '2';
			}
			*datalen = 0;

			msr_close();
			return 0;
		}
		mdelay(100);
	}

	msr_close();
	*status = 'F';
    return 0;
}
#endif

/*status :
		  '1', ICC Power on OK
		  '2', ICC Present but power on failed.
           	  'F', Timeout
           
 return value: 0 is ok, other is failed.
 */

int wait_icc_present_event(unsigned char *status)
{
  //int ret;
	int ifd;
	int iloop = 0;
	//track_data msr[MSR_MAX_TRACK];

	while(iloop++ < 10)
	{
		
		if (SCR_Detect(ifd) == TRUE)
		{
			if (SCR_PowerOn(ifd, NULL, NULL) == TRUE)
			{
				*status = '1';
				SCR_PowerOff(ifd);
			}else
			{
				*status = '2';
			}
			
			return 0;
		}
		mdelay(100);
	}

	*status = 'F';
    return 0;
}


/*status :
		  '3' - Nfc detect ok
           	  'F', Timeout
           
 return value: 0 is ok, other is failed.
 */

int wait_nfc_event(unsigned char *status)
{
  //int ret;
	//int ifd;
	int iloop = 0;
	unsigned char tmpStatus;

	nfc_open();
	while(iloop++ < 10)
	{
		nfc_detect(&tmpStatus);

		if (tmpStatus == '0')
		{
			nfc_close();
			*status = '3';
			return 0;
		}
	
		mdelay(100);
	}
	
	nfc_close();

	*status = 'F';
    return 0;
}


