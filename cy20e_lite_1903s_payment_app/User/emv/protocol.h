//
// Created by jay on 2016/12/11.
//

#ifndef SITCFIRMWARE_PROTOCOL_H
#define SITCFIRMWARE_PROTOCOL_H

#define MAX_BUFFER_SIZE   4096

int executecmd(char cmd,int timeout,unsigned char *res,int *len);
int executecmd_ex(char cmd,int timeout,unsigned char *cmddata, int cmdlen,unsigned char *res,int *rlen);

int nfc_detect(unsigned char *status);
int nfc_apdu(unsigned char *status, unsigned char * capdu, int capdulen, unsigned char * rapdu, int *rapdulen);
int nfc_deselect(unsigned char *status);
int wait_msr_event(unsigned char *status, unsigned char *data, int *datalen);
int wait_msr_icc_present_event(unsigned char *status, unsigned char *data, int *datalen);


typedef enum{
	GETVERSION=0x11,  
	NFC_DETECT,
	NFC_APDU,
	NFC_DESELECT,
	WAIT_MSR,
	ICC_MSR_PRESENT_CHECK,
	CMDID_TMS_START_DOWNLOAD = 0X41,
	CMDID_TMS_RECEIVE_DOWNLOAD = 0x42,
	CMDID_TMS_END_DOWNLOAD = 0x43,
	CMDID_TMS_CHECK_UPDATE = 0x45,
	CMDID_PED_ASK_SERVER_ACTIVATE_ITSELF = 0x50,
	CMDID_PED_UPDATE_LOCATION_TO_SERVER = 0x60,
	CMDID_PED_AUTH_SERVER=0x70,
	CMDID_PED_ASK_SERVER_RAND=0x71,
	CMDID_PED_ASK_SERVER_AUTH_ITSELF=0x72,
	
} CommandID;

#endif //SITCFIRMWARE_PROTOCOL_H
