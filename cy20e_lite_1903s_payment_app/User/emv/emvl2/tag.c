#include <string.h>
#include <stdlib.h>
#include "OsTypes.h"
#include "tag.h"
#include "defines.h"
#include "misce.h"
#include "utils.h"
#include "emvDebug.h"

TAG_TYPE tagTypeArray[]=

{

	{0x9F01,TAG_TYPE_N},{0x9F02,TAG_TYPE_N},{0x9F03,TAG_TYPE_N},

	{0x9F42,TAG_TYPE_N},{0x9F44,TAG_TYPE_N},{0x5F25,TAG_TYPE_N},

	{0x5F24,TAG_TYPE_N},{0x5F34,TAG_TYPE_N},{0x9F3B,TAG_TYPE_N},

	{0x9F43,TAG_TYPE_N},{0x9F11,TAG_TYPE_N},{0x5F28,TAG_TYPE_N},

	{0x42,TAG_TYPE_N},	{0x9F15,TAG_TYPE_N},{0x9F39,TAG_TYPE_N},

	{0x5F30,TAG_TYPE_N},{0x9F1A,TAG_TYPE_N},{0x9F35,TAG_TYPE_N},

	{0x5F2A,TAG_TYPE_N},{0x5F36,TAG_TYPE_N},{0x9A,TAG_TYPE_N},

	{0x9F3C,TAG_TYPE_N},{0x9F3D,TAG_TYPE_N},{0x9F41,TAG_TYPE_N},

	{0x9F21,TAG_TYPE_N},{0x9C,TAG_TYPE_N},

	{0x5A,TAG_TYPE_CN}, {0x9F20,TAG_TYPE_CN}

};



//------------------------------------------------------------------------------------------

void InitTagList(TagList *pTagList)

{

	memset(pTagList, 0, sizeof(TagList));

}

//------------------------------------------------------------------------------------------
void InitTag3ByteList(Tag3ByteList *pTag3ByteList)
{
	memset(pTag3ByteList, 0, sizeof(Tag3ByteList));
}


//------------------------------------------------------------------------------------------

void FreeTagList(TagList *pTagList)

{

	tti_int32 index;



	for (index=0; index<pTagList->ItemCount; index++)

	{

		free(pTagList->tagItem[index].Value);
		//free_Counter();

	}



	memset(pTagList, 0, sizeof(TagList));

}



//------------------------------------------------------------------------------------------

void appendTagList(TagList *sourceTagList, TagList *descTagList)

{

	tti_int32 index;



	for (index=0; index<(sourceTagList->ItemCount); index++)

	{

		SetTagValue((sourceTagList->tagItem[index]).Tag, 

			(sourceTagList->tagItem[index]).Value, 

			(sourceTagList->tagItem[index]).Length, descTagList);

	}

}


//------------------------------------------------------------------------------------------

tti_int32 GetTagCount(TagList *pTagList)

{

	return pTagList->ItemCount;

}



//------------------------------------------------------------------------------------------

tti_int32 GetTagIndex(TagList *pTagList, tti_uint16 tag)

{

	tti_int32 i;

	

	for (i=0; i<pTagList->ItemCount; i++)

	{

		if (pTagList->tagItem[i].Tag == tag)

		{

			return i;

		}

	}

	

	return (-1);

}



//------------------------------------------------------------------------------------------

tti_bool TagIsExisted(TagList *pTagList, tti_uint16 tag)

{

	if (GetTagIndex(pTagList, tag) < 0)

	{

		return FALSE;

	}

	else

	{

		return TRUE;

	}

}




//------------------------------------------------------------------------------------------

tti_bool TagDataIsMissing(TagList *pTagList, tti_uint16 tag)

{

	if (!TagIsExisted(pTagList, tag))

	{

		return TRUE;

	}



	if (GetTagValueSize(pTagList, tag)==0)

	{

		return TRUE;

	}



	return FALSE;

}



//------------------------------------------------------------------------------------------

tti_bool TagIsDuplicate(TagList *pTagList, tti_uint16 tag)

{

	tti_int32 index;



	for (index=0; index<pTagList->duplicateItemCount; index++)

	{

		if (tag==pTagList->duplicateItem[index])

		{

			return TRUE;

		}

	}



	return FALSE;

}



//------------------------------------------------------------------------------------------

tti_bool TagIsTemplate(tti_uint16 tag)

{



	if ((tag & 0xFF00))

	{

		if((tag & 0x2000))

		{

			return TRUE;

		}

	}

	else

	{

		if(tag & 0x0020)

		{

			return TRUE;

		}

	}



	return FALSE;

}



//------------------------------------------------------------------------------------------

tti_int32 SetTagItem(tti_uint16 tag, tti_byte*pValue, tti_int32 length, TagItem *tagItem)

{
	//Jason move it on 2017/03/24
	tagItem->Value = (tti_byte *)malloc(length);
	if (NULL == tagItem->Value)
	{
		//dbg("SetTagItem: malloc error\n");
		return STATUS_FAIL;
	}
	//malloc_Counter();

	//tagItem->Value = (tti_byte *)malloc(length);
	//Jason move end

	tagItem->Tag = tag;

	tagItem->Length = length;

	memcpy(tagItem->Value, pValue, length);

	

	return STATUS_OK;

}



//------------------------------------------------------------------------------------------

tti_int32 AppendTagValue(tti_uint16 tag, tti_byte* pValue, tti_int32 length, TagList *pTagList)

{

	if (pTagList->ItemCount>=MAX_TAG_LIST_SIZE)

	{

		return STATUS_FAIL;

	}

/*

	if (length==0)

	{

		return STATUS_OK;

	}

*/

	if(STATUS_OK != SetTagItem(tag, pValue, length, &pTagList->tagItem[pTagList->ItemCount]))

	{

		return STATUS_FAIL;

	}



	(pTagList->ItemCount)++;

	

	return STATUS_OK;

}



//------------------------------------------------------------------------------------------

tti_int32 SetTagValue(tti_uint16 tag, tti_byte* pValue, tti_int32 length, TagList *pTagList)

{

	//DPRINTF("pTagList->ItemCount = %d\n", pTagList->ItemCount);
	if (pTagList->ItemCount>=MAX_TAG_LIST_SIZE)

	{
		//DPRINTF("pTagList->ItemCount = %d\n", pTagList->ItemCount);
		return STATUS_FAIL;

	}

/*

	if (length==0)

	{

		return STATUS_OK;

	}

*/

	if (TagIsExisted(pTagList, tag))
	{
		//DPRINTF("TagIsExisted == TRUE\n");
		//Jason added on 2017/03/24, it must be free before add the exist tag to void malloc the same tag size again. 
		free((pTagList->tagItem[GetTagIndex(pTagList, tag)]).Value);
		//free_Counter();
		//Jason added end
		return SetTagItem(tag, pValue, length, &pTagList->tagItem[GetTagIndex(pTagList, tag)]);
	}

	else
	{
		return AppendTagValue(tag, pValue, length, pTagList);
	}

}



//------------------------------------------------------------------------------------------

tti_byte* GetTagValue(TagList *pTagList, tti_uint16 tag)

{

	if (TagIsExisted(pTagList, tag))

	{

		return pTagList->tagItem[GetTagIndex(pTagList, tag)].Value;

	}

	else

	{

		return NULL;

	}

}

tti_int32 GetTag3ByteValueAndSize(Tag3ByteList *pTag3ByteList, tti_int32 tag, tti_byte *value, 
	tti_int32 *size)
{
	tti_int32 i;

	for (i=0; i<pTag3ByteList->ItemCount; i++)
	{
		if (pTag3ByteList->tag3ByteItem[i].Tag == tag)
		{
			memcpy(value, pTag3ByteList->tag3ByteItem[i].Value, pTag3ByteList->tag3ByteItem[i].Length);
			*size = pTag3ByteList->tag3ByteItem[i].Length;
			return 0;
		}
	}

	return -1;
}



//------------------------------------------------------------------------------------------

tti_int32 GetTagValueSize(TagList *pTagList, tti_uint16 tag)

{

	if (TagIsExisted(pTagList, tag))

	{

		return pTagList->tagItem[GetTagIndex(pTagList, tag)].Length;

	}

	else

	{

		return 0;

	}

}



//------------------------------------------------------------------------------------------

tti_int32  ParseTlvTagInfo(tti_byte *pTagStartPos, tti_uint16 size, tti_uint16 *pTag, tti_uint16 *pTagSize)

{

	if (0 == size)

	{

		return STATUS_FAIL;

	}

	

	if ((*pTagStartPos & 0x1F) == 0x1F)

	{	

		if (*(pTagStartPos+1) & 0x80)

		{

			return STATUS_FAIL;

		}

		

		*pTag =(tti_uint16) (((*pTagStartPos)<<8) + *(pTagStartPos+1));

		*pTagSize = 2;

	}

	else

	{

		*pTag = *pTagStartPos;

		*pTagSize = 1;

	}



	if(*pTagSize >= size)

	{

		return STATUS_FAIL;

	}

	

	return STATUS_OK;

}



//------------------------------------------------------------------------------------------

tti_int32 ParseTlvLengthInfo(tti_byte *pLenStartPos, tti_uint16 size, 

	tti_uint16 *pLength, tti_uint16 *pLengthSize)

{

	tti_uint8 i;



	if(0 == size) 

	{

		return STATUS_FAIL;

	}

	

	if (*pLenStartPos & 0x80)

	{

		if ((*pLenStartPos & 0x7F) > 2)

		{

			return STATUS_FAIL;

		}

		

		*pLengthSize = (tti_uint16) (1 + (*pLenStartPos & 0x7F));

		

		*pLength = 0;

		pLenStartPos += 1;

		for (i=0; i < (*pLengthSize) - 1; i++)

		{

			*pLength <<= 8;

			*pLength =(tti_uint16) (*pLength + *pLenStartPos);

			pLenStartPos++;

		}	

	}

	else

	{

		*pLength = *pLenStartPos;

		*pLengthSize = 1;

	}



	if(*pLengthSize > size)

	{

		return STATUS_FAIL;

	}

	

	return STATUS_OK;

}



//------------------------------------------------------------------------------------------

tti_int32 ParseTlvInfo(tti_byte *pTagStartPos,  tti_uint16 size, tti_uint16 *pTag, tti_uint16 *pTagSize, 

	tti_uint16 *pLength, tti_uint16 *pLengthSize)

{

	tti_byte *CurPos =(tti_byte *) pTagStartPos;

	tti_byte *EndPos =(tti_byte *)(pTagStartPos + size);



	if(STATUS_OK != ParseTlvTagInfo(CurPos, (tti_uint16)(EndPos-CurPos), pTag, pTagSize))

	{

		return STATUS_FAIL;

	}



	CurPos += *pTagSize;



	if(STATUS_OK != ParseTlvLengthInfo(CurPos, (tti_uint16)(EndPos-CurPos), pLength, pLengthSize))

	{
		//for 4.2c
		//DPRINTF("ParseTlvLengthInfo error\n");
		return STATUS_FAIL;

	}



	return STATUS_OK;

}



//------------------------------------------------------------------------------------------

tti_int32 BuildTagList(tti_byte *buf, tti_int32 size, TagList *pTagList)

{

	return BuildTagListInMode(buf, size, 0, pTagList);

}



//------------------------------------------------------------------------------------------

tti_int32 BuildTagListInMode(tti_byte *buf, tti_int32 size, tti_int32 mode, TagList *pTagList)

{

	tti_uint16 Tag;

	tti_uint16	TagSize;

	tti_uint16 lengthSize;

	tti_uint16	Length;

	tti_byte 	*CurPos, *EndPos;



	if ((buf==NULL)||(size==0))

	{

		return STATUS_OK;

	}

	//DPRINTF("TLV vaule:\n");
	//printByteArray(buf, size);

	CurPos=buf;

	EndPos=(buf+size);



	while(CurPos < EndPos)

	{

		if ((*CurPos==0x00)||(*CurPos==0xFF))

		{
			DPRINTF("(*CurPos==0x00)||(*CurPos==0xFF)\n");
			CurPos++;

			continue;

		}

		if(STATUS_OK != ParseTlvInfo(CurPos, (tti_uint16)(EndPos-CurPos), &Tag, &TagSize, &Length, &lengthSize))

		{
			DPRINTF("ParseTlvInfo error\n");
			return STATUS_FAIL;

		}

		CurPos += TagSize + lengthSize;


		if (TagIsExisted(pTagList, Tag))

		{
			pTagList->duplicateItem[pTagList->duplicateItemCount]=Tag;

			(pTagList->duplicateItemCount)++;
			

		}



		if (TagIsTemplate(Tag))

		{

			if (mode&TAGMODE_RESERVE_ALL_ITEM)

			{

				if (mode&TAGMODE_DUPLICATE_ITEM_ALLOWED)

				{

					if (STATUS_OK!=AppendTagValue(Tag, CurPos, Length, pTagList))

					{
						DPRINTF("AppendTagValue error\n");
						return STATUS_FAIL;

					}

				}

				else

				{

					if( STATUS_OK != SetTagValue(Tag, CurPos, Length, pTagList))

					{
						DPRINTF("SetTagValue error\n");

						return STATUS_FAIL;

					}

				}

			}

			if(STATUS_OK != BuildTagListInMode(CurPos, Length, mode, pTagList))

			{
				
				DPRINTF("BuildTagListInMode error\n");
				return STATUS_FAIL;

			}

		}

		else

		{

			if (mode&TAGMODE_DUPLICATE_ITEM_ALLOWED)

			{

				if (STATUS_OK!=AppendTagValue(Tag, CurPos, Length, pTagList))

				{
					DPRINTF("Not TEMP: BuildTagListInMode error\n");
					return STATUS_FAIL;

				}

			}

			else

			{

				//printf("Tag = [%04x], Length = %d\n", Tag, Length);
				//printByteArray(CurPos, Length);
				if( STATUS_OK != SetTagValue(Tag, CurPos, Length, pTagList))

				{
					DPRINTF("Not TEMP: SetTagValue error\n");
					
					return STATUS_FAIL;

				}

			}

		}

		

		CurPos += Length;

	}

	

	if(CurPos != EndPos)

	{
		DPRINTF("CurPos != EndPos\n");
		if (CurPos > EndPos)
		{
			DPRINTF("CurPos > EndPos\n");
		}
		return STATUS_FAIL;

	}

	else

	{

		return STATUS_OK;

	}

} 



//------------------------------------------------------------------------------------------

tti_int32 GetTagType(tti_uint16 tag)

{

	tti_int32 index, tagCount;

	

	tagCount = sizeof(tagTypeArray)/sizeof(TAG_TYPE);



	for	(index = 0; index < tagCount; index++)

	{

		if (tag == tagTypeArray[index].Tag)

		{

			return tagTypeArray[index].Type;

		}

	}



	return TAG_TYPE_OTHER;

}



//------------------------------------------------------------------------------------------

void FillDataField(TagList *tagList, tti_uint16 tag, tti_uint16 ValueLength, tti_byte *resultStream )

{

	tti_uint16 Length;

	tti_byte *pValue;

	tti_byte zeroBuffer[BUFFER_SIZE_256BYTE]={0};



	memset(resultStream, 0, ValueLength);

	if (TagIsTemplate(tag))

	{

		return;

	}

	pValue=GetTagValue(tagList, tag);

	if (pValue!=NULL)

	{

		Length=GetTagValueSize(tagList, tag);

	}

	else

	{

		pValue=zeroBuffer;

		Length=BUFFER_SIZE_256BYTE;

	}

		

	if (ValueLength < Length)

	{

		if (GetTagType(tag) == TAG_TYPE_N)

		{

			memcpy(resultStream,pValue + Length - ValueLength, ValueLength);

		}

		else

		{

			memcpy(resultStream, pValue,ValueLength);

		}

	}

	else if (ValueLength == Length)

	{

		memcpy(resultStream, pValue, ValueLength);

	}

	else

	{

		if (GetTagType(tag) == TAG_TYPE_N)

		{

			memset(resultStream,0,ValueLength);

			memcpy(resultStream + ValueLength -Length,

				pValue, Length);

		}

		else if (GetTagType(tag) == TAG_TYPE_CN)

		{

			memset(resultStream, 0xFF, ValueLength);

			memcpy(resultStream, pValue, Length);

		}

		else

		{

			memset(resultStream,0,ValueLength);

			memcpy(resultStream, pValue, Length);

		}

	}

}



//------------------------------------------------------------------------------------------

tti_bool tagIsInDOL(TagList *tagList, tti_uint16 dolType, tti_uint16 targetTag)

{

	tti_uint16 	tag;

	tti_uint16 	tagSize;



	tti_uint16 	length, lengthSize;

	tti_byte 		*curPos, *endPos;



	curPos=GetTagValue(tagList, dolType);

	if (curPos==NULL)

	{

		return FALSE;

	}



	endPos =curPos + GetTagValueSize(tagList, dolType);

	

	while (curPos < endPos)

	{

		if (STATUS_OK != ParseTlvInfo(curPos, (tti_uint16)(endPos - curPos), &tag, &tagSize, 

										&length, &lengthSize))

		{

			return FALSE;

		}



		if (targetTag==tag)

		{

			return TRUE;

		}



		curPos += tagSize + lengthSize;	

	}



	return FALSE;

}



//------------------------------------------------------------------------------------------

tti_int32 BuildDOLToStream(TagList *tagList, tti_uint16 dolType, 

	tti_byte *resultStream, tti_byte *streamLength)

{

	tti_uint16 	tag;

	tti_uint16 	tagSize;



	tti_uint16 	length, lengthSize;

	tti_byte 		*curPos, *endPos;



	curPos=GetTagValue(tagList, dolType);

	if (curPos==NULL)

	{

		return STATUS_FAIL;

	}



	endPos =curPos + GetTagValueSize(tagList, dolType);

	

	*streamLength=0;

	while (curPos < endPos)

	{

		if (STATUS_OK != ParseTlvInfo(curPos, (tti_uint16)(endPos - curPos), &tag, &tagSize, 

										&length, &lengthSize))

		{

			return STATUS_FAIL;

		}



		curPos += tagSize + lengthSize;	

		FillDataField(tagList, tag, length, resultStream + *streamLength);

		

		(*streamLength) +=length;

	}



	if (curPos!= endPos)

	{

		return STATUS_FAIL;

	}

	else

	{

		return STATUS_OK;

	}

}

//Jason added on 2012.04.17, start
tti_int32 ParseDOLStreamToTagList(TagList *tagList, tti_uint16 dolType, 
	tti_byte *Stream, tti_byte streamLength)
{
	tti_uint16 	tag;
	tti_uint16 	tagSize;
	tti_uint16 	length, lengthSize;
	tti_byte 		*curPos, *endPos;
	tti_byte streamOffset = 0;

	curPos = GetTagValue(tagList, dolType);
	if (curPos==NULL)
	{
		DPRINTF("1111\n");
		return STATUS_FAIL;
	}
	
	endPos =curPos + GetTagValueSize(tagList, dolType);	
	while (curPos < endPos)	
	{
		if (STATUS_OK != ParseTlvInfo(curPos, (tti_uint16)(endPos - curPos), &tag, &tagSize, 
										&length, &lengthSize))
		{
			DPRINTF("2222\n");
			return STATUS_FAIL;
		}

		curPos += tagSize + lengthSize; 
		//FillDataField(tagList, tag, length, resultStream + *streamLength);
		SetTagValue(tag, Stream + streamOffset, length, tagList);
		
		streamOffset +=length;
	}

	if (curPos!= endPos)
	{
		DPRINTF("3333\n");
		return STATUS_FAIL;
	}
	else if (streamOffset != streamLength)
	{
		DPRINTF("4444\n");
		return STATUS_FAIL;
	}

	return STATUS_OK;
	
}
//End

//------------------------------------------------------------------------------------------
void PrintOutTagList(TagList *pTagList, tti_tchar* promoptMsg)
{
#if 0
	tti_uint16 i;

	if (promoptMsg!=NULL)

	{

		DPRINTF("%s\n", promoptMsg);

	}

	DPRINTF("count=%d\n", pTagList->ItemCount);

	for (i=0; i<pTagList->ItemCount; i++)

	{

		DPRINTF("tag=%X length=%d\n", pTagList->tagItem[i].Tag, pTagList->tagItem[i].Length);

		printByteArray(pTagList->tagItem[i].Value,  pTagList->tagItem[i].Length);

	}
#endif
}


//------------------------------------------------------------------------------------------

void PrintOutTag3ByteList(Tag3ByteList *pTagList, tti_tchar* promoptMsg)
{
	tti_uint16 i;

	if (promoptMsg!=NULL)
	{
		DPRINTF("%s\n", promoptMsg);
	}

	DPRINTF("count=%d\n", pTagList->ItemCount);

	for (i=0; i<pTagList->ItemCount; i++)
	{
		DPRINTF("tag=%X length=%d\n", pTagList->tag3ByteItem[i].Tag, pTagList->tag3ByteItem[i].Length);
		printByteArray(pTagList->tag3ByteItem[i].Value,  pTagList->tag3ByteItem[i].Length);
	}
}

tti_int32 GetTag3ByteIndex(Tag3ByteList *pTagList, tti_int32 tag)
{
	tti_int32 i;

	for (i=0; i<pTagList->ItemCount; i++)
	{
		if (pTagList->tag3ByteItem[i].Tag == tag)
		{
			return i;
		}
	}

	return (-1);
}

//------------------------------------------------------------------------------------------

tti_int32 BuildTagListOneLevel(tti_byte *buf, tti_int32 size, TagList *pTagList)

{

	tti_uint16 Tag;

	tti_uint16	TagSize;

	tti_uint16 lengthSize;

	tti_uint16	Length;

	tti_byte 	*CurPos, *EndPos;



	if ((buf==NULL)||(size==0))

	{

		return STATUS_OK;

	}



	CurPos=buf;

	EndPos=(buf+size);



	while(CurPos < EndPos)

	{

		if ((*CurPos==0x00)||(*CurPos==0xFF))

		{

			CurPos++;

			continue;

		}

		if(STATUS_OK != ParseTlvInfo(CurPos, (tti_uint16)(EndPos-CurPos), &Tag, &TagSize, &Length, &lengthSize))

		{

			return STATUS_FAIL;

		}

		CurPos += TagSize + lengthSize;



		if (TagIsExisted(pTagList, Tag))

		{

			pTagList->duplicateItem[pTagList->duplicateItemCount]=Tag;

			(pTagList->duplicateItemCount)++;

		}



		if( STATUS_OK != AppendTagValue(Tag, CurPos, Length, pTagList))

		{

			return STATUS_FAIL;

		}

		

		CurPos += Length;

	}

	

	if(CurPos != EndPos)

	{

		return STATUS_FAIL;

	}

	else

	{

		return STATUS_OK;

	}

} 

tti_int32 BuildTagListOneLevelBctc(tti_byte *buf, tti_int32 size, TagList *pTagList, Tag3ByteList *pTag3ByteList)

{
	tti_uint16 Tag;

	tti_uint16	TagSize;

	tti_uint16 lengthSize;

	tti_uint16	Length;

	tti_byte 	*CurPos, *EndPos;

	
	if ((buf==NULL)||(size==0))
	{
		return STATUS_OK;
	}


	CurPos=buf;

	EndPos=(buf+size);

	while(CurPos < EndPos)

	{
		//Check BCTC tag 0xDF8101 t0 0xDF8107, and tag 0xDFC10X
		if (memcmp(CurPos, "\xDF\x81", 2) == 0 ||
			memcmp(CurPos, "\xDF\xc1", 2) == 0)
		{
			pTag3ByteList->tag3ByteItem[pTag3ByteList->ItemCount].Tag = CurPos[0]*0x10000 + CurPos[1]*0x100 + CurPos[2];
			pTag3ByteList->tag3ByteItem[pTag3ByteList->ItemCount].Length = CurPos[3];
			memcpy(pTag3ByteList->tag3ByteItem[pTag3ByteList->ItemCount].Value, CurPos + 4, CurPos[3]);
			(pTag3ByteList->ItemCount)++;
			CurPos += 4 +  CurPos[3];
			continue;
		}
	

		if ((*CurPos==0x00)||(*CurPos==0xFF))

		{

			CurPos++;

			continue;

		}

		if(STATUS_OK != ParseTlvInfo(CurPos, (tti_uint16)(EndPos-CurPos), &Tag, &TagSize, &Length, &lengthSize))

		{

			return STATUS_FAIL;

		}

		CurPos += TagSize + lengthSize;



		if (TagIsExisted(pTagList, Tag))

		{

			pTagList->duplicateItem[pTagList->duplicateItemCount]=Tag;

			(pTagList->duplicateItemCount)++;

		}



		if( STATUS_OK != AppendTagValue(Tag, CurPos, Length, pTagList))

		{

			return STATUS_FAIL;

		}

		

		CurPos += Length;

	}

	

	if(CurPos != EndPos)

	{

		return STATUS_FAIL;

	}

	else

	{

		return STATUS_OK;

	}

} 



//------------------------------------------------------------------------------------------

void ResetDuplicateStatus(TagList *pTagList)

{

	pTagList->duplicateItemCount=0;

}



//------------------------------------------------------------------------------------------

void removeTag(TagList *pTagList, tti_uint16 tag)

{

	tti_int32 index, tagIndex;



	tagIndex=GetTagIndex(pTagList, tag);



	if (tagIndex>0)

	{

		free(pTagList->tagItem[tagIndex].Value);
		//free_Counter();

		for (index=tagIndex; index<pTagList->ItemCount-1; index++)

		{

			memcpy(&pTagList->tagItem[index], &pTagList->tagItem[index+1], sizeof(TagItem));

		}

		(pTagList->ItemCount)--;

	}

}


tti_int32 TagBuildTLV(TagList *pTagList, tti_uint16 tag, tti_byte *pTLVData, tti_uint16 *pTLVDataLen)
{
	tti_uint16 index = 0;
	tti_int32 tagValueSize;


	if (TagIsExisted(pTagList, tag) == FALSE)
	{
		//DPRINTF("TagIsExisted = FALSE");
		return STATUS_FAIL;
	}

	if (tag > 0xff)
	{
		//Two byte size tag
		pTLVData[0] = tag/256;
		pTLVData[1] = tag%256;
		index += 2;
	}else
	{
		//One byte size tag
		pTLVData[0] = tag%256;
		index += 1;
	}

	tagValueSize = GetTagValueSize(pTagList, tag);
	if (tagValueSize == 0)
	{
		DPRINTF("tagValueSize == 0");
		return STATUS_FAIL;
	}else if (tagValueSize < 128)
	{
		pTLVData[index] = tagValueSize;
		index += 1;
	}else if (tagValueSize < 256)
	{
		pTLVData[index] = 0x81;
		pTLVData[index + 1] = tagValueSize;
		index += 2;
	}else if (tagValueSize < 65536)
	{
		pTLVData[index] = 0x82;
		pTLVData[index + 1] = tagValueSize/256;
		pTLVData[index + 2] = tagValueSize%256;
		index += 3;
	}else
	{
		return STATUS_FAIL;
	}

	memcpy(pTLVData + index, GetTagValue(pTagList, tag), tagValueSize);
	index += tagValueSize;

	*pTLVDataLen = index;

	return STATUS_OK;
}

tti_int32 TagArrsBuildTLV(TagList *pTagList, tti_uint16 tagArr[], tti_uint16 tagArrCount, 
	tti_byte *pTLVData, tti_uint16 *pTLVDataLen)
{
	tti_int32 ret;
	tti_uint16 loop;
	tti_uint16 totalLen = 0;
	tti_uint16 tmpLen;

	for (loop = 0; loop < tagArrCount; loop ++)
	{
		ret = TagBuildTLV(pTagList, tagArr[loop], pTLVData + totalLen, &tmpLen);
		if (ret == STATUS_OK)
		{
			totalLen += tmpLen;
		}
	}

	*pTLVDataLen = totalLen;
	return STATUS_OK;
}

	
tti_int32 TagListBuildTLV(TagList *pTagList,	tti_byte *pTLVData, tti_uint16 *pTLVDataLen)
{
	tti_int32 ret;
	tti_uint16 loop;
	tti_uint16 totalLen = 0;
	tti_uint16 tmpLen;

	for (loop = 0; loop < pTagList->ItemCount; loop ++)
	{
		ret = TagBuildTLV(pTagList, pTagList->tagItem[loop].Tag, pTLVData + totalLen, &tmpLen);
		if (ret == STATUS_OK)
		{
			totalLen += tmpLen;
		}
	}

	*pTLVDataLen = totalLen;
	return STATUS_OK;
}



