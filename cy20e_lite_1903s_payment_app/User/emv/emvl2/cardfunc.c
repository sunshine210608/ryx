#include <string.h>
#include <stdio.h>
#include "sysTick.h"
#include "defines.h"
#include "cardfunc.h"
#include "misce.h"
#include "EMVL2.h"
#include "emvDebug.h"
#include "protocol.h"
#include "../Util.h"
#include "debug.h"
#include "customize_emvl2.h"


tti_int32				__SCR_Handle = 0;
Fn_SCR_PowerOn 			__SCR_PowerOn= NULL;
Fn_SCR_PowerOff 		__SCR_PowerOff= NULL;
Fn_SCR_ExchangeApdu 	__SCR_ExchangeApdu = NULL;


//------------------------------------------------------------------------------------------
tti_uint16 getResponseCode(const tti_byte *response, tti_uint16 responseLength)
{
	if (responseLength<2)
	{
		return 0;
	}
	else
	{
		return (tti_uint16) ((response[responseLength-2] <<8) + response[responseLength-1]);
	}
}

//------------------------------------------------------------------------------------------
tti_int32 EmvIOInit()
{
	//SCardConnect(3, 9600);
//	SCardTimeOut(2000);

    /*
	if(smartcardInit(SMARTCARD_SLOT,VCC_5V, 4000000))
	{
		dbg("smartcardInit error\n");
		
		return STATUS_ERROR;
	}
	*/
	return STATUS_OK;
}

//------------------------------------------------------------------------------------------
tti_int32 EmvIOTransmit(tti_byte *SendBuf, tti_uint16 SendLength, tti_byte *RecvBuf, 
	tti_uint16 *RecvLength, tti_uint16 *StatusWord)
{
	tti_byte TempRecvBuf[MAX_CARD_MESSAGE_SIZE];
	tti_uint16 TempRecvLen = sizeof(TempRecvBuf);
	tti_uint16 tempStatusWord;

	//Line;
	/*
	if (SCARD_S_SUCCESS != SCardTransmit(SendBuf, SendLength, TempRecvBuf, &TempRecvLen))
	{
		return STATUS_ERROR;
	}
	*/
	//Line;
    int i;
    DPRINTF("** apdu in : ");
    for(i=0; i<SendLength; i++)
        DPRINTF("%02X ", SendBuf[i]);
    DPRINTF("\n");
	if (!__SCR_ExchangeApdu(__SCR_Handle, SendBuf, (tti_int16)SendLength, 
			TempRecvBuf, (tti_int16 *)&TempRecvLen))
	{
		Line;
		return STATUS_ERROR;
	}
    DPRINTF("** apdu out: ");
    for(i=0; i<TempRecvLen; i++)
        DPRINTF("%02X ", TempRecvBuf[i]);
    DPRINTF("\n");
    
	//Line;
	*StatusWord = getResponseCode(TempRecvBuf, TempRecvLen);
	//Line;

	if(((*StatusWord)&0xFF00)==0x6100)
	{
		DPRINTF("Jason added for 6100\n");
		memcpy(SendBuf, "\x00\xC0\x00\x00", 4);
		SendBuf[4]=TempRecvBuf[1];
		SendLength= 5;
		
		return EmvIOTransmit(SendBuf, SendLength, RecvBuf, RecvLength, StatusWord);
	}
	//Line;
	if(((*StatusWord) &0xFF00) == 0x6c00)
	{
		DPRINTF("Jason added for 6c00\n");
		
		DPRINTF("Case 6c00:SendLength[%d]\n", SendLength);
		SendBuf[4]=TempRecvBuf[1];
		//Jason 20101102 delete start
		/*
		SendLength +=1;
		*/
		//Jason 20101102 delete end

		//Jason 20101102 add start
		SendLength = 5;
		//Jason 20101102 add end
		return EmvIOTransmit(SendBuf, SendLength, RecvBuf, RecvLength, StatusWord);
	}
	//Line;
	if (SendBuf[1]==0xa4 || SendBuf[1]==0xa8 || SendBuf[1]==0xae || SendBuf[1]==0x88)
	{
		if ((((*StatusWord)&0xFF00)==0x6200)||
			(((*StatusWord)&0xFF00)==0x6300)||
			(*StatusWord==0x9001))
		{
			if (TempRecvLen==2)
			{
				DPRINTF("Jason added for 6200 and 6300\n");
		
				memcpy(SendBuf, "\x00\xC0\x00\x00\x00", 5);
				SendLength=5;

				return EmvIOTransmit(SendBuf, SendLength, RecvBuf, RecvLength, &tempStatusWord);
			}
		}
	}

	//Line;
	*RecvLength=TempRecvLen-2;
	memcpy(RecvBuf, TempRecvBuf, *RecvLength);
	 
	return  STATUS_OK;
}

	

//------------------------------------------------------------------------------------------
tti_int32  EmvIOIsCardPresent()
{
	//tti_byte atr[255];
	//tti_int8 iatr = sizeof(atr);
    
#ifdef VPOS_APP	
    if( SCR_Detect(0)==TRUE)
    {
        return 0;
    }
#else    
	if (__SCR_PowerOn(__SCR_Handle, atr, &iatr) == TRUE)
	{
		return 0;
	}
#endif
	return -1;
}

/*
tti_int32  EmvIOIsCardPresent()
{
	tti_byte atr[255];
	tti_byte iatr=sizeof(atr);
	return SmartcardPoweron(0, &atr);
}
*/

//------------------------------------------------------------------------------------------
tti_int32 EmvIOSessionEnd()
{
//	SCardDisconnect();
	//return SCardPowerDown();
#ifndef VPOS_APP    
	__SCR_PowerOff(__SCR_Handle);
#endif
	return 0;
}

//------------------------------------------------------------------------------------------
tti_int32 SmartCardSelect(tti_byte *aidName, tti_byte aidNameLen, 
	tti_byte *response, tti_uint16 *respLen)
{
	tti_byte command[MAX_CARD_MESSAGE_SIZE];
	tti_uint16 commandLen;
	tti_uint16 status;

	DPRINTF("new SmartCardSelect\n");

	command[0]=0x00;
	command[1]=0xA4;
	command[2]=0x04;
	command[3]=0x00;
	command[4]=aidNameLen;
	memcpy(&command[5], aidName, aidNameLen);
	command[5 + aidNameLen] = 0x00;
	
	commandLen=5+aidNameLen + 1;
	
	if (EmvIOTransmit(command, commandLen, response, respLen, &status)==STATUS_ERROR)
	{
		return STATUS_ERROR;
	}

	if (status==0x9000)
	{
		return STATUS_OK;
	}
	else if (status==0x6A81)
	{
		return STATUS_CARD_BLOCK;
	}
	else if (status==0x6283)
	{
/*		if (*respLen==0)
		{
			command[0]=0x00;
			command[1]=0xc0;
			command[2]=0x00;
			command[3]=0x00;
			command[4]=0x00;
			commandLen=5;
			
			EmvIOTransmit(command, commandLen, response, respLen, &status);
		}
*/		
		return STATUS_APP_BLOCKED;
	}
	else
	{
		return STATUS_FAIL;
	}
}

//------------------------------------------------------------------------------------------
tti_int32 SmartCardSelectNext(tti_byte *aidName, tti_byte aidNameLen, 
	tti_byte *response, tti_uint16 *respLen)
{
	tti_byte command[MAX_CARD_MESSAGE_SIZE];
	tti_uint16 commandLen;
	tti_uint16 status;

	command[0]=0x00;
	command[1]=0xA4;
	command[2]=0x04;
	command[3]=0x02;
	command[4]=aidNameLen;
	memcpy(&command[5], aidName, aidNameLen);
	command[5 + aidNameLen] = 0x00;
	commandLen=5+aidNameLen+1;
	
	EmvIOTransmit(command, commandLen, response, respLen, &status);

	if (status==0x9000)
	{
		return STATUS_OK;
	}
	else if (status==0x6A81)
	{
		return STATUS_CARD_BLOCK;
	}
	else if (status==0x6283)
	{
/*		if (*respLen==0)
		{
			command[0]=0x00;
			command[1]=0xc0;
			command[2]=0x00;
			command[3]=0x00;
			command[4]=0x00;
			commandLen=5;
			
			EmvIOTransmit(command, commandLen, response, respLen, &status);
		}
*/		
		return STATUS_APP_BLOCKED;
	}
	else
	{
		return STATUS_FAIL;
	}
}

//------------------------------------------------------------------------------------------
tti_int32 SmartCardGetprocessOption(tti_byte *gpoData, tti_byte gpoDatalen, 
	tti_byte *response, tti_uint16 *respLen)
{
	tti_byte command[MAX_CARD_MESSAGE_SIZE];
	tti_uint16 commandLen;
	tti_uint16 status;

	command[0]=0x80;
	command[1]=0xA8;
	command[2]=0x00;
	command[3]=0x00;
	command[4]=gpoDatalen;
	memcpy(&command[5], gpoData, gpoDatalen);
	command[5+gpoDatalen] = 0x00;
	commandLen=5+gpoDatalen+1;

	EmvIOTransmit(command, commandLen, response, respLen, &status);

	if (status==0x9000)
	{
		return STATUS_OK;
	}
	else if (status==0x6985)
	{
		return STATUS_GPO_NO_ACCEPTED;
	}
	else
	{
		return STATUS_FAIL;
	}
}

//------------------------------------------------------------------------------------------
tti_int32	SmartCardReadRecord(tti_byte SFI, tti_byte index, tti_byte *response, tti_uint16 *respLen)
{
	tti_byte command[MAX_CARD_MESSAGE_SIZE];
	tti_uint16 commandLen;
	tti_uint16 status;

	DPRINTF("SFI = %02x\n", SFI);

	command[0]=0x00;
	command[1]=0xB2;
	command[2]=index;
	//Jason modified for 4.3d
	command[3]=(SFI&0xF8)|0x04;
	//end
	command[4] = 0x00;
	commandLen=4 + 1;
	
	EmvIOTransmit(command, commandLen, response, respLen, &status);

	if (status==0x9000)
	{
		return STATUS_OK;
	}
	else
	{
		if (status==0x6A83)
		{
			ReportLine();
			return STATUS_RECORD_NOT_FOUND;
		}
		else
		{
			DPRINTF("status = %04x\n", status);
			ReportLine();
			return STATUS_FAIL;
		}
	}
}

//------------------------------------------------------------------------------------------
tti_int32 ParseGPOResponse(tti_byte *response, tti_uint16 respLen, TagList *tagList)
{
	tti_byte *point;
	tti_uint16 aflLength, aflLenSize;

	if ((*response)==0x77)
	{
		if (BuildTagList(response, respLen, tagList)!=STATUS_OK)
		{
			return STATUS_FAIL;
		}
		else
		{
			return STATUS_OK;
		}
	}
	if ((*response)==0x80)
	{
		point=response+1;
		if (ParseTlvLengthInfo(point, respLen-1, &aflLength, &aflLenSize)==STATUS_OK)
		{
			point+=aflLenSize;
			SetTagValue(TAG_AIP, point, 2, tagList);
			SetTagValue(TAG_AFL, point+2, aflLength-2, tagList);

			return STATUS_OK;
		}
	}

	return STATUS_FAIL;
}

//------------------------------------------------------------------------------------------
//Jason modified on 2014.03.14
tti_int32 SmartCardGenerateAC(tti_byte cid, tti_byte cdaSign, tti_byte *data, tti_byte dataLen, 
	tti_byte *response, tti_uint16 *respLen)
{
	tti_byte command[MAX_CARD_MESSAGE_SIZE];
	tti_uint16 commandLen;
	tti_uint16 status;

	//Line;

	command[0]=0x80;
	command[1]=0xAE;
	command[2]=(cid | cdaSign);
	command[3]=0x00;
	command[4]=dataLen;
	memcpy(&command[5], data, dataLen);
	commandLen=5+dataLen;

	//Line;
	EmvIOTransmit(command, commandLen, response, respLen, &status);
	//Line;

	if (status==0x9000)
	{
		DPRINTF("GenerateAC response\n");
		hexdump(response, *respLen);
		return STATUS_OK;
	}
	else
	{
		return STATUS_FAIL;
	}
}
//End
//------------------------------------------------------------------------------------------
tti_int32 SmartCardInternalAuth(tti_byte *data, tti_byte dataLen, tti_byte *response, tti_uint16 *respLen)
{
	tti_byte command[MAX_CARD_MESSAGE_SIZE];
	tti_uint16 commandLen;
	tti_uint16 status;

	command[0]=0x00;
	command[1]=0x88;
	command[2]=0x00;
	command[3]=0x00;
	command[4]=dataLen;
	memcpy(&command[5], data, dataLen);
	commandLen=5+dataLen;
	
	EmvIOTransmit(command, commandLen, response, respLen, &status);

	if (status==0x9000)
	{
		return STATUS_OK;
	}
	else
	{
		return STATUS_ERROR;
	}
}

//------------------------------------------------------------------------------------------
tti_int32 SmartCardGetData(tti_uint16 tag, tti_byte *response, tti_uint16 *respLen)
{
	tti_byte command[MAX_CARD_MESSAGE_SIZE];
	tti_uint16 commandLen;
	tti_uint16 status;

	command[0]=0x80;
	command[1]=0xCA;
	command[2]=(tti_byte)(tag>>8);
	command[3]=(tti_byte)(tag&0xFF);
	commandLen=4;
	
	EmvIOTransmit(command, commandLen, response, respLen, &status);

	if (status==0x9000)
	{
		return STATUS_OK;
	}
	else
	{
		return STATUS_FAIL;
	}
}

//------------------------------------------------------------------------------------------
tti_int32 SmartCardExternalAuth(tti_byte *data, tti_byte dataLen)
{
	tti_byte command[MAX_CARD_MESSAGE_SIZE];
	tti_uint16 commandLen;
	tti_byte tempBuffer[MAX_CARD_MESSAGE_SIZE];
	tti_uint16 tempBufferLen;	
	tti_uint16 status;

	command[0]=0x00;
	command[1]=0x82;
	command[2]=0x00;
	command[3]=0x00;
	command[4]=dataLen;
	memcpy(&command[5], data, dataLen);
	commandLen=5+dataLen;
	
	EmvIOTransmit(command, commandLen, tempBuffer, &tempBufferLen, &status);

	if (status==0x9000)
	{
		return STATUS_OK;
	}
	else
	{
		return STATUS_FAIL;
	}
}

//------------------------------------------------------------------------------------------
tti_int32 SmartCardSendScript(tti_byte *data, tti_byte dataLen)
{
	tti_byte tempBuffer[MAX_CARD_MESSAGE_SIZE];
	tti_uint16 tempBufferLen;	
	tti_uint16 status;

	EmvIOTransmit(data, dataLen, tempBuffer, &tempBufferLen, &status);
	status&=0xFF00;
	
	if ((status==0x9000)||(status==0x6200)||(status==0x6300))
	{
		return STATUS_OK;
	}
	else
	{
		return STATUS_FAIL;
	}
}

//------------------------------------------------------------------------------------------
 tti_int32 SmartCardGetChallenge(tti_byte *response, tti_uint16 *responseLen)
{
	tti_byte command[MAX_CARD_MESSAGE_SIZE];
	tti_uint16 commandLen;
	tti_uint16 status;
	
 	command[0]=0x00;
	command[1]=0x84;
	command[2]=0x00;
	command[3]=0x00;
	
	commandLen = 4;

	EmvIOTransmit(command, commandLen, response, responseLen, &status);
	
	if (status==0x9000)
	{
		return STATUS_OK;
	}
	else
	{
		return STATUS_FAIL;
	}
}

//------------------------------------------------------------------------------------------
tti_int32 SmartCardVerifyPin(tti_byte *data, tti_byte dataLen, tti_byte pinType, tti_byte *retryTime)
{
	tti_byte command[MAX_CARD_MESSAGE_SIZE];
	tti_uint16 commandLen;
	tti_byte tempBuffer[MAX_CARD_MESSAGE_SIZE];
	tti_uint16 tempBufferLen;	
	tti_uint16 status;

	command[0]=0x00;
	command[1]=0x20;
	command[2]=0x00;
	command[3]=pinType;
	command[4]=dataLen;
	memcpy(&command[5], data, dataLen);
	commandLen=5+dataLen;
	
	EmvIOTransmit(command, commandLen, tempBuffer, &tempBufferLen, &status);
	
	if (status==0x9000)
	{
		return STATUS_OK;
	}
	else if ((status&0xFFF0)==0x63c0)
	{
		*retryTime=(tti_byte)(status&0x0F);
		if (status==0x63c0)
		{
			return STATUS_PIN_RETRY_LIMIT;
		}
		else
		{
			return STATUS_FAIL;
		}
	}
	else if ((status==0x6983)||(status==0x6984))
	{
		return STATUS_PIN_RETRY_LIMIT;
	}
	else
	{
		return STATUS_ERROR;
	}
}

extern uint32 GetNfcStartTick(void);

//------------------------------------------------------------------------------------------
tti_int32 EmvContactlessIOTransmit(tti_byte *SendBuf, tti_uint16 SendLength, tti_byte *RecvBuf, 
	tti_uint16 *RecvLength, tti_uint16 *StatusWord)
{
	tti_byte TempRecvBuf[MAX_CARD_MESSAGE_SIZE];
	int TempRecvLen = sizeof(TempRecvBuf);
	//tti_uint16 tempStatusWord;
	tti_byte status;
	int ret;

	
	dbg("Before nfc_apdu : %d\n", get_diff_tick(get_tick(), GetNfcStartTick()));
	ret = nfc_apdu(&status, SendBuf, SendLength, TempRecvBuf, &TempRecvLen);
	dbg("After nfc_apdu  : %d\n", get_diff_tick(get_tick(), GetNfcStartTick()));
	//LOGD("nfc_apdu return = %d", ret);
	if (ret != 0)
	{
		LOGE("nfc_apdu ERR");
		return STATUS_ERROR;
	}
	if (status != '0')
	{
		LOGE("nfc_apdu ERR, status = %c", status);
		return STATUS_ERROR;
	}
	//if (TempRecvBuf < 2)
	if (TempRecvLen < 2)
	{
		LOGE("nfc_apdu ERR, TempRecvLen < 2", TempRecvBuf);
		return STATUS_ERROR;
	}
	hexdumpEx("List CAPDU", SendBuf, SendLength);
	hexdumpEx("List RAPDU", TempRecvBuf, TempRecvLen);

	*StatusWord = getResponseCode(TempRecvBuf, TempRecvLen);

#if 0
	if(((*StatusWord)&0xFF00)==0x6100)
	{
		DPRINTF("Jason added for 6100\n");
		memcpy(SendBuf, "\x00\xC0\x00\x00", 4);
		SendBuf[4]=TempRecvBuf[1];
		SendLength= 5;
		
		return EmvContactlessIOTransmit(SendBuf, SendLength, RecvBuf, RecvLength, StatusWord);
	}
	
	if(((*StatusWord) &0xFF00) == 0x6c00)
	{
		DPRINTF("Jason added for 6c00\n");
		
		DPRINTF("Case 6c00:SendLength[%d]\n", SendLength);
		SendBuf[4]=TempRecvBuf[1];
		//Jason 20101102 delete start
		/*
		SendLength +=1;
		*/
		//Jason 20101102 delete end

		//Jason 20101102 add start
		SendLength = 5;
		//Jason 20101102 add end
		return EmvContactlessIOTransmit(SendBuf, SendLength, RecvBuf, RecvLength, StatusWord);
	}

	if (SendBuf[1]==0xa4)
	{
		if ((((*StatusWord)&0xFF00)==0x6200)||
			(((*StatusWord)&0xFF00)==0x6300)||
			(*StatusWord==0x9001))
		{
			if (TempRecvLen==2)
			{
				DPRINTF("Jason added for 6200 and 6300\n");
		
				memcpy(SendBuf, "\x00\xC0\x00\x00\x00", 5);
				SendLength=5;

				return EmvContactlessIOTransmit(SendBuf, SendLength, RecvBuf, RecvLength, &tempStatusWord);
			}
		}
	}
#endif

	*RecvLength=TempRecvLen-2;
	memcpy(RecvBuf, TempRecvBuf, *RecvLength);
	 
	return  STATUS_OK;
}

tti_int32 EmvContactlessIOSessionEnd()
{
	tti_byte status;
	tti_int32 ret;

	ret = nfc_deselect(&status);
	if (ret != 0)
	{
		LOGE("nfc_deselect ERR");
		return STATUS_ERROR;
	}
	if (status != '0')
	{
		LOGE("nfc_deselect ERR, status = %c", status);
		return STATUS_ERROR;
	}
	return STATUS_OK;
}


tti_int32 NfcSmartCardSelect(tti_byte *aidName, tti_byte aidNameLen, 
	tti_byte *response, tti_uint16 *respLen)
{
		tti_byte command[MAX_CARD_MESSAGE_SIZE];
		tti_uint16 commandLen;
		tti_uint16 status;
	
		//DPRINTF("NfcSmartCardSelect SmartCardSelect\n");
	
		command[0]=0x00;
		command[1]=0xA4;
		command[2]=0x04;
		command[3]=0x00;
		command[4]=aidNameLen;
		memcpy(&command[5], aidName, aidNameLen);
		command[5 + aidNameLen] = 0x00;
		
		commandLen=5+aidNameLen + 1;

		//Line;
		if (EmvContactlessIOTransmit(command, commandLen, response, respLen, &status)==STATUS_ERROR)
		{
			return STATUS_ERROR;
		}
	
		if (status==0x9000)
		{
			return STATUS_OK;
		}
		else if (status==0x6A81)
		{
			return STATUS_CARD_BLOCK;
		}
		else if (status==0x6283)
		{
	/*		if (*respLen==0)
			{
				command[0]=0x00;
				command[1]=0xc0;
				command[2]=0x00;
				command[3]=0x00;
				command[4]=0x00;
				commandLen=5;
				
				EmvIOTransmit(command, commandLen, response, respLen, &status);
			}
	*/		
			return STATUS_APP_BLOCKED;
		}
		else
		{
			return STATUS_FAIL;
		}

}


tti_int32 NfcGetprocessOption(tti_byte *gpoData, tti_byte gpoDatalen, 
	tti_byte *response, tti_uint16 *respLen)
{
		tti_byte command[MAX_CARD_MESSAGE_SIZE];
		tti_uint16 commandLen;
		tti_uint16 status;
		
		command[0]=0x80;
		command[1]=0xA8;
		command[2]=0x00;
		command[3]=0x00;
		command[4]=gpoDatalen;
		memcpy(&command[5], gpoData, gpoDatalen);
		command[5+gpoDatalen] = 0x00;
		commandLen=5+gpoDatalen+1;
		
		if (EmvContactlessIOTransmit(command, commandLen, response, respLen, &status)==STATUS_ERROR)
		{
			return STATUS_ERROR;
		}
		
		if (status==0x9000)
		{
			return STATUS_OK;
		}
		else if (status==0x6985)
		{
			return STATUS_GPO_NO_ACCEPTED;
		}else if (status==0x6986)
		{
			return STATUS_GPO_POWER_RESET_ERR;
		}
		else
		{
			return STATUS_FAIL;
		}

}

	
	//------------------------------------------------------------------------------------------
	tti_int32 NfcReadRecord(tti_byte SFI, tti_byte index, tti_byte *response, tti_uint16 *respLen)
	{
		tti_byte command[MAX_CARD_MESSAGE_SIZE];
		tti_uint16 commandLen;
		tti_uint16 status;
	
		//DPRINTF("SFI = %02x\n", SFI);
	
		command[0]=0x00;
		command[1]=0xB2;
		command[2]=index;
		//Jason modified for 4.3d
		command[3]=(SFI&0xF8)|0x04;
		//end
		command[4] = 0x00;
		commandLen=4 + 1;
		
		EmvContactlessIOTransmit(command, commandLen, response, respLen, &status);
	
		if (status==0x9000)
		{
			return STATUS_OK;
		}
		else
		{
			if (status==0x6A83)
			{
				ReportLine();
				return STATUS_RECORD_NOT_FOUND;
			}
			else
			{
				DPRINTF("status = %04x\n", status);
				ReportLine();
				return STATUS_FAIL;
			}
		}
	}

tti_int32 NfcCardGetData(tti_uint16 tag, tti_byte *response, tti_uint16 *respLen)
{
	tti_byte command[MAX_CARD_MESSAGE_SIZE];
	tti_uint16 commandLen;
	tti_uint16 status;

	command[0]=0x80;
	command[1]=0xCA;
	command[2]=(tti_byte)(tag>>8);
	command[3]=(tti_byte)(tag&0xFF);
	commandLen=4;
	
	EmvContactlessIOTransmit(command, commandLen, response, respLen, &status);

	if (status==0x9000)
	{
		return STATUS_OK;
	}
	else
	{
		return STATUS_FAIL;
	}
}