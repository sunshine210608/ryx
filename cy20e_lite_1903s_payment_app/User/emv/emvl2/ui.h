#ifndef _UI_H_
#define _UI_H_

#include "OsTypes.h"
#include "EvntMask.h"
#include "tag.h"

/*
#define	DISPLAY_LEFT_ALIGN		101
#define	DISPLAY_RIGHT_ALIGN		102
#define	DISPLAY_CENTER_ALIGN		103
#define	DISPLAY_IN_NORMAL			0
#define	DISPLAY_IN_REVERSE		1
*/
//void DisplayStringInFormat(tti_tchar *pLine, tti_int32 uiDisplayLine, 
//	tti_int32 uiDispStartCol, tti_int32 uiDispEndCol, tti_int32 uiFormat, tti_int32 uiDispMode);
void ClearScreen(void);
//void DisplayInfoRight( tti_tchar *pInfo, tti_int32 line);
void DisplayInfoLeft( tti_tchar *pInfo, tti_int32 line);
//void TtiOsClearLines(tti_int32 uiStartLine, tti_int32 uiEndLine);
//TEvntMask WaitAKeyDuringTime(TEvntMask WaitEvnt, tti_uint16 uiTimeOut);
tti_int32 EditorLine(tti_int32 line, tti_int32 colume, tti_tchar *resultBuffer, tti_int32 bufferSize);
void RequestPinDataEntry(tti_byte *InBuff, int InLen, tti_byte *OutBuff, int *OutLen);

tti_int32 EditorLineForOnlinePIN(tti_int32 line, tti_int32 colume, tti_tchar *resultBuffer, tti_int32 bufferSize);
int BuildPinBlock( tti_byte *InputPin, int PinLen, tti_byte *PinBlock);

//tti_int32 DisplayLog(tti_int32 logIndex, TagList *tagList, tti_bool bIsLast);

tti_int32 DisplayCorfim(tti_tchar *szDisplay, int iLine);

tti_bool IsCredentialMatch(tti_byte *credentialType, tti_byte *credentialNumber);

tti_int32 DisplayLog(tti_int32 logIndex, TagList *tagList, tti_bool bIsLast);


#endif
