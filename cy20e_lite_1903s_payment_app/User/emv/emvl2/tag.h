#ifndef _TAG_H_
#define _TAG_H_

#include "OsTypes.h"

//#define	MAX_TAG_LIST_SIZE	150
#define	MAX_TAG_LIST_SIZE	200
//#define	MAX_TAG_LIST_SIZE	100

#define TAG_TYPE_N 			0

#define TAG_TYPE_CN 		1

#define	TAG_TYPE_OTHER	2

typedef struct _TAG_TYPE
{
	tti_uint16 Tag;
	tti_byte   Type;
} TAG_TYPE;



typedef struct _TAG_INFO

{

	tti_uint16 Tag;

	tti_int32 Length;

	tti_byte *Value;

} TagItem;


typedef struct _TAG_LIST

{

	tti_int32 ItemCount;

	TagItem tagItem[MAX_TAG_LIST_SIZE]; //FIXME:this is not satisfied, should be fix later.

	tti_int32 duplicateItemCount;

	tti_uint16 duplicateItem[MAX_TAG_LIST_SIZE];

} TagList;

typedef struct _TAG_3BYTE_INFO

{

	tti_int32 Tag;

	tti_int32 Length;
	//tti_byte Value[256];
	tti_byte Value[60];
} Tag3ByteItem;

typedef struct _TAG_3BYTE_LIST

{

	tti_int32 ItemCount;

	Tag3ByteItem tag3ByteItem[20]; //FIXME:this is not satisfied, should be fix later.

} Tag3ByteList;



#define	TAGMODE_DUPLICATE_ITEM_ALLOWED	 	0x01

#define	TAGMODE_RESERVE_ALL_ITEM			0x02



#define 	TAG_ADF_NAME						0x4F

#define 	TAG_APP_LABEL 						0x50

#define	TAG_TRACK2							0x57

#define	TAG_PAN							0x5A

#define	TAG_ENTRY_TEMPLATE				0x61

#define	TAG_FCI_TEMPLATE					0x6F

#define	TAG_TEMPERATE_70					0x70

#define	TAG_TEMPERATE_71					0x71

#define	TAG_TEMPERATE_72					0x72

#define	TAG_DIRECTORY_TEMPLATE			0x73

#define	TAG_AMOUNT_BINARY				0x81

#define 	TAG_AIP 							0x82

#define 	TAG_DF_NAME 						0x84

#define	TAG_SCRIPT							0x86

#define	TAG_APP_PRIOR						0x87

#define 	TAG_SFI							0x88

#define 	TAG_AUTH_CODE					0x89

#define	TAG_RESPONSE_CODE				0x8A

#define	TAG_CDOL1							0x8C

#define	TAG_CDOL2							0x8D

#define	TAG_CVM							0x8E

#define	TAG_CA_PUBLIC_KEY_INDEX			0x8F

#define	TAG_ISSUER_PK_CERTIFICATE		0x90

#define	TAG_ISSUER_AUTH_DATA				0x91

#define	TAG_ISSUER_PK_REMAINDER			0x92

#define	TAG_SDA_SSAD						0x93

#define 	TAG_AFL 							0x94

#define	TAG_TVR							0x95

#define	TAG_TDOL							0x97

#define	TAG_TDOL_HASH_VALUE				0x98

#define	TAG_TRANSACTION_DATE				0x9A

#define	TAG_TSI							0x9B

#define	TAG_TRANSACTION_TYPE				0x9C

#define	TAG_DDF_NAME						0x9D

#define	TAG_FCI_PROPRIETARY_TEMPLATE	0xA5

#define	TAG_CARDHOLDER_NAME				0x5F20

#define	TAG_APP_EXPIRATION_DATE			0x5F24

#define	TAG_APP_EFFECTIVE_DATE			0x5F25

#define	TAG_ISSUER_COUNTRY_CODE			0x5F28

#define	TAG_TRANS_CURRENCY_CODE			0x5F2A

#define	TAG_PREFERRED_LANGUAGE			0x5F2D

#define	TAG_SERVICE_CODE					0x5F30

#define TAG_PAN_SEQU_NUMBER				0x5F34

#define	TAG_TRANS_CURRENCY_EXP			0x5F36

#define	TAG_ACCOUNT_TYPE					0x5F57

#define	TAG_ACQUIRER_ID					0x9F01

#define TAG_AMOUNT						0x9F02

#define	TAG_AMOUNT_OTHER					0x9F03

#define	TAG_AMOUNT_OTHER_BINARY			0x9F04

#define	TAG_TERMINAL_AID					0x9F06

#define	TAG_AUC							0x9F07

#define	TAG_ICC_APP_VERSION_NUMBER		0x9F08

#define	TAG_APP_VERSION_NUMBER			0x9F09

#define TAG_IAC_DEFAULT					0x9F0D

#define TAG_IAC_DENIAL					0x9F0E

#define TAG_IAC_ONLINE					0x9F0F

#define	TAG_ISSUER_APP_DATA				0x9F10

#define	TAG_ISSUER_CODE_INDEX			0x9F11

#define	TAG_PREFERRED_NAME				0x9F12

#define	TAG_LAST_ATC						0x9F13

#define	TAG_LOWER_OFFLINE_LIMIT			0x9F14

#define	TAG_MERCHANT_CATEGORY_CODE		0x9F15

#define	TAG_MERCHANT_ID					0x9F16

#define TAG_PIN_TRY_COUNTER 				0x9F17

#define	TAG_SCRIPT_ID						0x9F18

#define	TAG_TOKEN_REQUESTOR_ID				0x9F19

#define	TAG_TERM_COUNTRY_CODE			0x9F1A

#define	TAG_FLOOR_LIMIT					0x9F1B

#define	TAG_TERMINAL_ID					0x9F1C

#define	TAG_TERMINAL_RISK_DATA			0x9F1D

#define	TAG_IFD_SERIAL_NUMBER			0x9F1E

#define	TAG_TRANSACTION_TIME				0x9F21

#define TAG_CA_PUBLIC_KEY_INDEX_TERM	0x9F22

#define	TAG_UPPER_OFFLINE_LIMIT			0x9F23

#define TAG_PAYMENT_ACCOUNT_REF			0x9F24

#define	TAG_CRYPTOGRAM					0x9F26

#define	TAG_CRYPTOGRAM_INFO_DATA		0x9F27

#define TAG_PIN_CERTIFICATE				0x9F2D

#define TAG_PIN_EXPONENT					0x9F2E

#define TAG_PIN_PK_REMAINDER				0x9F2F

#define	TAG_ISSUER_PK_EXPONENT			0x9F32

#define	TAG_TERMINAL_CAPABILITIES		0x9F33

#define	TAG_CVM_RESULTS					0x9F34

#define	TAG_TERMINAL_TYPE					0x9F35

#define TAG_ATC							0x9F36

#define	TAG_UNPREDICTABLE_NUMBER		0x9F37

#define	TAG_PDOL							0x9F38

#define	TAG_POS_ENTRY_MODE				0x9F39

#define	TAG_TRANS_REF_CURRENCY_CODE	0x9F3C

#define	TAG_TRANS_REF_CURRENCY_EXP		0x9F3D

#define	TAG_ADDITIONAL_TERM_CAPABILITY	0x9F40

#define	TAG_TRANS_SEQ_COUNTER			0x9F41

#define	TAG_APP_CURRENCY_CODE			0x9F42

#define	TAG_DATA_AUTH_CODE				0x9F45

#define	TAG_ICC_PK_CERTIFICATE			0x9F46

#define	TAG_ICC_PK_EXPONENT				0x9F47

#define	TAG_ICC_PK_REMAINDER				0x9F48

#define	TAG_DDOL							0x9F49

#define	TAG_SDA_TAGLIST					0x9F4A

#define	TAG_DDA_SDAD						0x9F4B

#define	TAG_DYNAMIC_DATA					0x9F4C

#define	TAG_LOG_ENTRY						0x9F4D

#define	TAG_MERCHANT_LOCATION			0x9F4E

#define TAG_LOG_FORMAT 					0x9F4F

#define	TAG_VLP							0x9F7A

#define	TAG_FCI_ISSUER_DATA				0xBF0C

#define TAG_CREDENTIAL_NUMBER				0x9F61

#define TAG_CREDENTIAL_TYPE					0x9F62



//BCTC Server defined TAG
#define TAG_ONLINE_PIN_BLOCK_BCTC		0x99

#define TAG_TRANSACTION_RESULT_BCTC		0x03

#define TAG_SCRIPT_RESULT_BCTC		0xDF31

#define TAG_CA_PUBLIC_KEY_SN_BCTC		0xDF08

#define TAG_CA_PUBLIC_KEY_EFFECTIVE_DATE_BCTC		0xDF05

#define TAG_CA_PUBLIC_KEY_HASH_ALG_BCTC		0xDF06

#define TAG_CA_PUBLIC_KEY_SIGN_ALG_BCTC		0xDF07

#define TAG_CA_PUBLIC_KEY_MOD_BCTC		0xDF02

#define TAG_CA_PUBLIC_KEY_EXPONENT_BCTC		0xDF04

#define TAG_CA_PUBLIC_KEY_CHECKSUM_BCTC		0xDF03

#define TAG_AID_PART_MATCH_BCTC				0xDF01

#define TAG_TAC_DEFAULT_BCTC				0xDF11

#define TAG_TAC_ONLINE_BCTC				0xDF12

#define TAG_TAC_DENIAL_BCTC				0xDF13

#define TAG_DDOL_DEFAULT_BCTC			0xDF14

#define TAG_THRESHOLD_AMOUNT_BCTC		0xDF15

#define TAG_TARGET_MAX_PERCENTAGE_BCTC	0xDF16

#define TAG_TARGET_PERCENTAGE_BCTC	0xDF17

#define TAG_QPBOC_OFFLINE_AMOUNT_LIMIT_BCTC		0xDF19

#define TAG_QPBOC_TRANS_AMOUNT_LIMIT_BCTC		0xDF20

#define TAG_CVM_FLOOR_LIMIT_AMOUNT_BCTC		0xDF21

#define TAG_TRANS_REF_CURRENCY_CONVERSION_BCTC		0xDF8101

#define TAG_DEFAULT_TDOL_BCTC		0xDF8102

#define TAG_DOWNLOAD_COMMAND_FLAG_BCTC	0xDF8103

#define TAG_TRANSACTION_COMMAND_FLAG_BCTC	0xDF8106

#define TAG_TRACK2_DATA_BCTC		0xDF8107

#define TAG_QPBOC_STATUS_CHECK		0xDFC108

//BCTC Server defined end

//EC define, start
#define TAG_EC_BALANCE				0x9F79
#define TAG_EC_BALANCE_LIMIT		0x9F77
#define TAG_EC_ISSUER_AUTH_CODER	0x9F74
#define TAG_EC_SING_TRANS_LIMIT		0x9F78
#define TAG_EC_RESET_THRESHOLD 		0x9F6D
#define TAG_EC_BALANCE_FROM_GEN_AC	0x9F79

#define TAG_EC_TERMINAL_SUPPORT_IND	0x9F7A
#define TAG_EC_TERMINAL_TRANS_LIMIT	0x9F7B

//EC define, end

//QPBOC define, start
#define TAG_QPBOC_PRODUCT_INFO				0x9F63
#define TAG_TERMINAL_TRANSACTION_ATTRIBUTES	0x9F66
#define TAG_CARD_TRANSACTION_ATTRIBUTES		0x9F6C
#define TAG_QPBOC_OFFLINE_BALANCE			0x9F5D
#define TAG_QPBOC_CARD_AUTH_DATA			0x9F69
//#define TAG_QPBOC_9F7C					0x9F7C
#define TAG_QPBOC_USER_SPEC_DATA			0x9F7C
#define TAG_QPBOC_9F25						0x9F25

#define TAG_QPBOC_9F51						0x9F51
#define TAG_QPBOC_DF71						0xDF71
//QPBOC define, end

tti_int32 GetTagCount(TagList *pTagList);



void InitTagList(TagList *pTagList);



void FreeTagList(TagList *pTagList);



void appendTagList(TagList *sourceTagList, TagList *descTagList);



tti_int32 BuildTagList(tti_byte *buf, tti_int32 size, TagList *pTagList);



tti_int32 BuildTagListInMode(tti_byte *buf, tti_int32 size, tti_int32 mode, TagList *pTagList);



void PrintOutTagList(TagList *pTagList, tti_tchar* promoptMsg);



tti_bool TagIsExisted(TagList *pTagList, tti_uint16 tag);



tti_bool TagDataIsMissing(TagList *pTagList, tti_uint16 tag);



tti_bool TagIsDuplicate(TagList *pTagList, tti_uint16 tag);



tti_int32 AppendTagValue(tti_uint16 tag, tti_byte* pValue, tti_int32 length, TagList *pTagList);



tti_int32 SetTagValue(tti_uint16 tag, tti_byte* pValue, tti_int32 length, TagList *pTagList);



tti_byte* GetTagValue(TagList *pTagList, tti_uint16 tag);



tti_int32 GetTagValueSize(TagList *pTagList, tti_uint16 tag);



tti_bool tagIsInDOL(TagList *tagList, tti_uint16 dolType, tti_uint16 targetTag);



tti_int32 BuildDOLToStream(TagList *tagList, tti_uint16 dolType, 

	tti_byte *resultStream, tti_byte *streamLength);



tti_int32 ParseTlvLengthInfo(tti_byte *pLenStartPos, tti_uint16 size, 

	tti_uint16 *pLength, tti_uint16 *pLengthSize);



tti_int32 BuildTagListOneLevel(tti_byte *buf, tti_int32 size, TagList *pTagList);



void ResetDuplicateStatus(TagList *pTagList);



void removeTag(TagList *pTagList, tti_uint16 tag);


tti_int32 ParseTlvInfo(tti_byte *pTagStartPos,  tti_uint16 size, tti_uint16 *pTag, tti_uint16 *pTagSize, 

	tti_uint16 *pLength, tti_uint16 *pLengthSize);


void FillDataField(TagList *tagList, tti_uint16 tag, tti_uint16 ValueLength, tti_byte *resultStream );


tti_int32 GetTagIndex(TagList *pTagList, tti_uint16 tag);


tti_int32 ParseDOLStreamToTagList(TagList *tagList, tti_uint16 dolType, 
	tti_byte *Stream, tti_byte streamLength);


tti_bool TagIsTemplate(tti_uint16 tag);

tti_int32 TagBuildTLV(TagList *pTagList, tti_uint16 tag, tti_byte *pTLVData, tti_uint16 *pTLVDataLen);

tti_int32 TagArrsBuildTLV(TagList *pTagList, tti_uint16 tagArr[], tti_uint16 tagArrCount, 
	tti_byte *pTLVData, tti_uint16 *pTLVDataLen);

tti_int32 TagListBuildTLV(TagList *pTagList,	tti_byte *pTLVData, tti_uint16 *pTLVDataLen);


void PrintOutTag3ByteList(Tag3ByteList *pTagList, tti_tchar* promoptMsg);

tti_int32 BuildTagListOneLevelBctc(tti_byte *buf, tti_int32 size, TagList *pTagList, Tag3ByteList *pTag3ByteList);

void InitTag3ByteList(Tag3ByteList *pTag3ByteList);

tti_int32 GetTag3ByteValueAndSize(Tag3ByteList *pTag3ByteList, tti_int32 tag, tti_byte *value, 
	tti_int32 *size);



#endif

