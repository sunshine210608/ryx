#ifndef _DEFINES_H_
#define _DEFINES_H_

#include "emvDebug.h"
#include "tag.h"

#ifndef VPOS_APP
#define VPOS_APP
#endif

#define	VERSION_NUMBER	"008C"

#define Bit1	0x00000001
#define Bit2	0x00000002
#define Bit3	0x00000004
#define Bit4	0x00000008
#define Bit5	0x00000010
#define Bit6	0x00000020
#define Bit7	0x00000040
#define Bit8	0x00000080

#define	PIN_BYPASS_NO_ALLOW		0
#define	PIN_BYPASS_CURR_ONLY		1
#define	PIN_BYPASS_NEXT_ALLOW	2

#define	TVR_SIZE			5
#define	TSI_SIZE			2
#define	AUC_SIZE			2

#define TRANS_TYPE_GOODS_AND_SERVICE 	0x00
#define TRANS_TYPE_CASH 					0x01
#define TRANS_TYPE_CASHBACK 				0x09

#define	AIP_STATIC_DATA_AUTH			0x40
#define	AIP_DYNAMIC_DATA_AUTH		0x20
#define	AIP_CARDHOLDER_VERIFY		0x10
#define	AIP_TERM_RISK_MANAGEMENT 	0x08
#define	AIP_ISSUER_AUTH				0x04
//Jason added on 2014.03.15
#define AIP_COMBINED_DYNAMIC_DATA_AUTH	0x01
//End

#define	MAX_DISPLAY_CHAR	20

#define	LONG_DATE_SIZE		8
#define	DATE_SIZE				6
#define	TIME_SIZE				6
#define	INT32_SIZE				16
#define	INT16_SIZE				5
#define	INT8_SIZE				3
#define	BOOL_SIZE				1
#define	AMOUNT_SIZE			12
#define	AMOUNT_SIZE_IN_TAG	6

#define	MULTI_LANG_SIZE		8

#define	OFFLINE_AUTH_DATA_SIZE	2048

#define	MAX_FCI_ISSUER_DATA	128

//#define	OFFLINE_AUTH_DATA_SIZE	1024

#define	SESSION_KEY_SIZE		32
#define	PIN_NUMBER_SIZE		12

#define	MONTHS_IN_YEAR	12

#define	SEPERATOR		'|'

#define	CVM_RESULT_SIZE			3

#define 	CVM_RESULT_UNKNOWN  		0x00
#define 	CVM_RESULT_FAIL			0x01
#define 	CVM_RESULT_SUCCESS		0x02
#define	CVM_RESULT_UNRECOGNISED	0x04
#define	CVM_RESULT_ERROR			0x08
#define	CVM_RESULT_NOT_SUPPORT	0x10
#define	CVM_RESULT_CANCEL			0x20


#define	CVM_FAIL_PROCESS						0x00
#define	CVM_OFFLINE_PLAIN_PIN					0x01
#define	CVM_ONLINE_ENCIPHER_PIN				0x02
#define	CVM_OFFLINE_PLAIN_PIN_AND_SIGN		0x03
#define	CVM_OFFLINE_ENCIPHER_PIN				0x04
#define	CVM_OFFLINE_ENCIPHER_PIN_AND_SIGN	0x05
#define	CVM_SIGN								0x1E
#define	CVM_NO_REQUIRE						0x1F
#define	CVM_NO_USED							0x3F
#define CVM_CREDENTIAL						0x20

#define	CVM_AMOUNT_X_SIZE	4
#define	CVM_AMOUNT_Y_SIZE	4

#define	CARD_SESSION_IDLE				0
#define	CARD_SESSION_SELECT_APP		1
#define	CARD_SESSION_TRANSACTION	2
#define	CARD_SESSION_TRANSCOMPLETE	3

#define	INC_STATUS_OK				0
#define	INC_STATUS_FAULTURE		1
#define	INC_STATUS_TIMEOUT		2
#define	INC_STATUS_CARD_BLOCKED	3
#define	INC_STATUS_INVALID_COMM	4
#define	INC_STATUS_FORMAT_ERROR	5
#define	INC_STATUS_APP_BLOCKED	6
#define	INC_STATUS_CANCELED		7
#define	INC_STATUS_NO_APP		8
#define	INC_STATUS_GPO_NO_ACCDEPTED	9
#define	INC_STATUS_READ_RECORD_ERR	'a'

#define	AUC_DOMESTIC_CASH			0x0180
#define	AUC_INTERNATIONAL_CASH		0x0140
#define	AUC_DOMESTIC_GOODS			0x0120
#define	AUC_INTERNATIONAL_GOODS		0x0110
#define	AUC_DOMESTIC_SERVICE			0x0108
#define	AUC_INTERNATIONAL_SERVICE	0x0104
#define	AUC_ATM						0x0102
#define	AUC_TERMINAL					0x0101
#define	AUC_DOMESTIC_CASHBACK		0x0280
#define	AUC_INTERNATIONAL_CASHBACK	0x0240

#define	TERM_CAP_MANUAL_KEY_ENTRY	0x0180
#define	TERM_CAP_MAGNETIC_STRIPE	0x0140
#define	TERM_CAP_IC_WITH_CONTACTS	0x0120
#define	TERM_CAP_PLAIN_PIN			0x0280
#define	TERM_CAP_ONLINE_PIN			0x0240
#define	TERM_CAP_SIGNATURE			0x0220
#define	TERM_CAP_ENCIPHER_PIN		0x0210
#define	TERM_CAP_NO_CVM_REQUIRED	0x0208
#define	TERM_CAP_CREDENTIAL			0x0201
#define	TERM_CAP_SDA					0x0380
#define	TERM_CAP_DDA					0x0340
#define	TERM_CAP_CARD_CAPTURE		0x0320
#define	TERM_CAP_CDA					0x0308

#define	TERM_CAP_ADDON_CASH				0x0180
#define	TERM_CAP_ADDON_GOODS			0x0140
#define	TERM_CAP_ADDON_SERVICES			0x0120
#define	TERM_CAP_ADDON_CASHBACK			0x0110
#define	TERM_CAP_ADDON_INQUIRY			0x0108
#define	TERM_CAP_ADDON_TRANSFER			0x0104
#define	TERM_CAP_ADDON_PAYMENT			0x0102
#define	TERM_CAP_ADDON_ADMIN			0x0101
#define	TERM_CAP_ADDON_CASH_DEPOSIT	0x0280

#define	MAX_EXP_LEN				3
#define MAX_CA_PUBLIC_KEY_SIZE	248

#define	HASH_RESULT_SIZE			20
#define	BUFFER_SIZE_32BYTE		32
#define	BUFFER_SIZE_256BYTE		256
#define	BUFFER_SIZE_512BYTE		512
#define	BUFFER_SIZE_1K				1024
#define	BUFFER_SIZE_2K				2048
#define	BUFFER_SIZE_4K				4096
#define	BUFFER_SIZE_10K				6144
#define 	MAX_CARD_MESSAGE_SIZE	260
#define	CANDIDATE_AID_NUMBER		20
#define	AID_NUMBER                  20
#define	AID_NAME_SIZE				16
#define	AID_LABEL_SIZE				30
#define	RESPONSE_CODE_SIZE		2
#define PREFERRED_LANG_SIZE		8

#define	ITEM_IN_EACH_PAGE			3

#define STATUS_OK					0
#define STATUS_FAIL 				1
#define	STATUS_PIN_RETRY_LIMIT		2
#define	STATUS_CANCEL				3
#define	STATUS_CARD_BLOCK			4
#define	STATUS_RECORD_NOT_FOUND		5
#define	STATUS_ERROR				6
#define	STATUS_APP_BLOCKED			7
#define	STATUS_TIMEOUT				8
#define	STATUS_SERVICE_NOT_ALLOWED	9
#define	STATUS_NOT_MATCH			10
#define	STATUS_NOT_EXACT_MATCH		11
#define	STATUS_EXACT_MATCH			12
#define	STATUS_NOT_NEED_AID			13
#define	STATUS_GPO_NO_ACCEPTED		14
#define	STATUS_READ_RECORD_ERR		15
#define	STATUS_CARD_EXP_ERR			16
//Jason added on 2020/05/12
#define STATUS_GPO_POWER_RESET_ERR	17
#define STATUS_ADF_NO_PDOL			18
#define STATUS_GPO_NO_RESPONSE_ERR  19
#define STATUS_QPBOC_CVM_DECLINE	20
//End

#define QPBOC_CID_STATUS_ARQC		0
#define QPBOC_CID_STATUS_AAC		1
#define QPBOC_CID_STATUS_TC			2
#define QPBOC_CID_STATUS_ERR		99




#define MESSAGE_NONE_JUSTIFY		0
#define MESSAGE_LEFT_JUSTIFY		1
#define MESSAGE_RIGHT_JUSTIFY		2

#define	VERIFY_PIN_TYPE_PLAINTEXT	0x80
#define	VERIFY_PIN_TYPE_ENCIPHER		0x88


#define	CID_AAR	0xC0
#define	CID_ARQC	0x80
#define 	CID_TC		0x40
#define	CID_AAC	0x00

#define	HANDLE_BEFORE_GC	1
#define	HANDLE_AFTER_GC	2

#define 	EMV_RESULT_GOOD_OFFLINE			0
#define	EMV_RESULT_ONLINE					1
#define	EMV_RESULT_REJECT					2
#define	EMV_RESULT_REVERSAL				3
#define	EMV_RESULT_COMM_ERROR			4
#define	EMV_RESULT_SERVICE_NOT_ALLOWED	5
//Jason added 20110905
#define	EMV_RESULT_INVALID_ARC			6

//Siken added 2011.09.23
#define	EMV_RESULT_SERVICE_NOT_ALLOWED_WITH_TC	7


#define	RESP_MODE_FAILED				0
#define	RESP_MODE_HOST_APPROVED		1
#define	RESP_MODE_VOICE_APPROVED	2
#define	RESP_MODE_OTHER_APPROVED	3
#define	RESP_MODE_HOST_DECLINED		4
#define	RESP_MODE_VOICE_DECLINED	5
//Jason added 20110905
#define RESP_MODE_INVALID_ARC		6
//End

//Jason added on 2014.01.13
#define CLEAR_TVR_OFFLINE_DATA_AUTH_NOT_PERFORM	0x017f
//End

#define TVR_OFFLINE_DATA_AUTH_NOT_PERFORM	0x0180
#define	TVR_SDA_FAILED						0x0140
#define	TVR_ICC_DATA_MISSING				0x0120
#define	TVR_CARD_APPEARS_ON_EXECPTION_FILE	0x0110
#define	TVR_DDA_FAILED						0x0108
#define	TVR_CDA_FAILED						0x0104
//Jason added on 2014.01.13
#define TVR_SDA_SELECTED					0x0102
//End

#define	TVR_APP_VERSION_IS_NOT_SAME			0x0280
#define	TVR_EXPIRED_APPLICATION				0x0240
#define	TVR_APP_NOT_EFFECTIVE				0x0220
#define	TVR_SERVICE_NOT_ALLOWED				0x0210
#define	TVR_NEW_CARD							0x0208

#define	TVR_CARDHOLDER_VERIFY_NOT_SUCCESS	0x0380
#define	TVR_UNRECOGNISED_CVM					0x0340
#define	TVR_PIN_TRY_LIMIT_EXCEEDDED			0x0320
#define	TVR_NEEDED_PINPAD_NOT_WORKING		0x0310
#define	TVR_PIN_WAS_NOT_ENTERED				0x0308
#define	TVR_ONLINE_PIN_ENTERED				0x0304

#define	TVR_EXCEEDS_FLOOR_LIMIT				0x0480
#define	TVR_LOWER_OFFLINE_LIMIT_EXCEEDED	0x0440
#define	TVR_UPPER_OFFLINE_LIMIT_EXCEEDED	0x0420
#define	TVR_RANDOM_SELECTED_FOR_ONLINE		0x0410
#define	TVR_FORCE_ONLINE						0x0408

#define	TVR_DEFAULT_TDOL_USED				0x0580
#define	TVR_ISSUER_AUTH_FAILED				0x0540
#define	TVR_SCRIPT_FAILED_BEFORE_FINAL_GC	0x0520
#define	TVR_SCRIPT_FAILED_AFTER_FINAL_GC	0x0510

#define	TSI_OFFLINE_DATA_AUTH_PERFORMED	0x0180
#define	TSI_CARDHOLDER_VERFIFY_PERFORMED	0x0140
#define	TSI_CARD_RISK_MAMANGE_PERFORMED	0x0120
#define	TSI_ISSUER_AUTH_PERFORMED			0x0110
#define	TSI_TERMINAL_RISK_PERFORMED			0x0108
#define	TSI_SCRIPT_PROCESS_PERFORMED		0x0104

#define	CVM_ALWAYS								0
#define	CVM_IF_UNATTENDED_CASH					1
#define	CVM_IF_NO_MANUAL_CASH_AND_CASHBACK	2
#define	CVM_IF_SUPPORTED_CVM						3
#define	CVM_IF_MANUAL_CASH						4
#define	CVM_IF_PURCHASE_CASHBACK				5
#define	CVM_IF_UNDER_AMOUNT_X					6
#define	CVM_IF_OVER_AMOUNT_X						7
#define	CVM_IF_UNDER_AMOUNT_Y					8
#define	CVM_IF_OVER_AMOUNT_Y						9

typedef enum _ITEM_TYPE
{
	ItemType_HexString,
	ItemType_String,
	ItemType_Date,
	itemType_Time,
	ItemType_LongDate,
	ItemType_ByteArray,
	ItemType_Bool,
	ItemType_Int8,
	ItemType_Int16,
	ItemType_Int32,
	ItemType_Amount,
	ItemType_Loop,
	ItemType_AidList
} ItemType;

typedef struct _REQUEST_API_DESCRIBLE_ITEM
{
	void* storeAddress;
	tti_byte itemType;
	tti_byte itemSize;
	
} RequestApiDescribleitem;

typedef struct _REQUEST_API_DESCRIBLE_TABLE
{
	tti_uint16 itemNum;
	RequestApiDescribleitem *item;
} RequestApiDescribleTable;

#define 	ITEM_TYPE_HEXSTRING		1
#define	ITEM_TYPE_STRING			2


#define	ITEM_FILL_TYPE_CERTAIN	10
#define	ITEM_FILL_TYPE_UNCERTAIN	20

#define	IFD_SERIAL_NUMBER_SIZE			8
#define	MERCHANT_CATEGORY_CODE_SIZE	4
#define	MERCHANT_ID_SIZE					15
#define 	TERMINAL_ID_SIZE	 				8
#define 	COUNTRY_CODE_SIZE					3
#define	CURRENCY_CODE_SIZE				3
#define	TERMINAL_CAPABILITIES_SIZE		3
#define	ADDTIONAL_TERMINAL_CAPABILITIES_SIZE	5
#define	ISSUER_AUTH_DATA_SIZE			128
#define	MAX_SCRIPTS_NUMBER		20
#define	MAX_SCRIPTS_SIZE			256
#define	MAX_SCRIPT_RESULT_SIZE	32
#define	ACCOUNT_TYPE_SIZE			2
#define TERMINAL_TYPE_SIZE	 	1


#define	TRANS_DATE_SIZE	6
#define	TRANS_TIME_SIZE	6

#define	TAC_SIZE	5
#define	DOL_SIZE	128
#define	APP_VERSION_NUMBER_SIZE	2

#define	PIN_BLOCK_SIZE		32

#define	PSE_NAME	"1PAY.SYS.DDF01"
#define PPSE_NAME   "2PAY.SYS.DDF01"


typedef struct _CANDIDATE_INFO
{
	tti_int32 aidLength;
	tti_byte aidName[AID_NAME_SIZE];   
	tti_byte verionNumber[APP_VERSION_NUMBER_SIZE];
	tti_bool exactMatch;
} CANDIDATE_INFO;

typedef struct _CANDIDATE_LIST
{
	tti_byte count;
	CANDIDATE_INFO candidateAidInfo[CANDIDATE_AID_NUMBER];
	
} CANDIDATE_LIST;

typedef struct _AID_INFO
{
	tti_int32 aidLength;
	tti_byte aidName[AID_NAME_SIZE];   
	tti_tchar aidLabel[AID_LABEL_SIZE];
	tti_tchar aidPreferLabel[AID_LABEL_SIZE];
	tti_byte verionNumber[APP_VERSION_NUMBER_SIZE];
	tti_byte priority;
	tti_bool mustConform;
	tti_tchar issuerCodeTableIndex[2+1];
	tti_tchar fciIssuerData[MAX_FCI_ISSUER_DATA+1];
} AID_INFO;

typedef struct _AID_LIST
{
	tti_int32 count;
	AID_INFO aidInfo[AID_NUMBER];
	tti_byte PSEMultiLang[MULTI_LANG_SIZE];
	tti_int32 PSEMultiLangLen;
} AID_LIST;

typedef struct _DOLType
{
	tti_int32 len;
	tti_byte value[DOL_SIZE];
} DOLType;


typedef struct _RESPONSE_API_DEFINES_ITEM
{
	tti_uint16 tagIndex;
	tti_byte 	itemType;
	tti_byte 	fillType;
	tti_byte	fillSize;
	tti_byte 	alignType;
} ResponseApiDefinesItem;

typedef struct _API_DEFINES
{
	tti_uint16 itemNum;
	ResponseApiDefinesItem *apiDefinesItem;
} ApiDefines;

typedef struct _TerminalParam 
{
	char IFDSerialNumber[IFD_SERIAL_NUMBER_SIZE+1];
	char terminalCountryCode[COUNTRY_CODE_SIZE+1];
	tti_byte terminalType;
	tti_byte terminalCapabilities[TERMINAL_CAPABILITIES_SIZE];
	tti_byte addtionalTerminalCapabilities[ADDTIONAL_TERMINAL_CAPABILITIES_SIZE];
	CANDIDATE_LIST CandidateAidList;
	tti_bool supportPSE;
} TerminalParam;

typedef struct _CommonRunningData
{
	tti_int32 pinBypassMode;
	tti_bool bypassPinEntry;
	tti_bool defaultTDOLUsed;
	tti_byte conditions;
	tti_bool cardHot;
	tti_bool forceOnline;
	tti_int32 targetPercentage;
	tti_int32 maxTargetPercentage;
	tti_int32 thresholdAmount;
	tti_int32 transLogAmount;

	tti_int32 randomValueForTransSelect;
	
	tti_byte defaultTerminalActionCode[TAC_SIZE];
	tti_byte denialTerminalActionCode[TAC_SIZE];
	tti_byte onlineTerminalActionCode[TAC_SIZE];

	tti_byte IACCodeType;

	tti_byte hashAlgorithm;
	tti_byte publicKeyAlgorithm;
	tti_byte checkSum[HASH_RESULT_SIZE];
	tti_int32 checkSumLen;

	tti_byte emvResultCode;
	tti_byte signatureRequired;
	tti_tchar pinBlock[PIN_BLOCK_SIZE+1];
	DOLType defaultTDOL;
	DOLType defaultDDOL;
} CommonRunningData;

typedef struct _HostResponseData
{
	tti_byte responseMode;
} HostResponseData;

typedef struct _Certificate
{
	tti_int32 expLen;
	tti_byte exp[MAX_EXP_LEN];
	tti_int32 modLen;
	tti_byte	modData[MAX_CA_PUBLIC_KEY_SIZE];
	tti_bool existed;
} Certificate;

typedef struct _SCRIPT
{
	tti_byte scriptValue[MAX_SCRIPTS_SIZE];
	tti_int32 scriptLen;
} Script;

typedef struct _SCRIPT_RESULT
{
	tti_byte scriptResult[MAX_SCRIPT_RESULT_SIZE];
	tti_int32 scriptResultLen;
} ScriptResult;

typedef struct _SCRIPT_LIST
{
	Script scripts[MAX_SCRIPTS_NUMBER];
	ScriptResult scriptResults[MAX_SCRIPTS_NUMBER];
	tti_int32 scriptsNumber;
} ScriptsList;

typedef struct _RunningTimeEnv
{
	tti_bool panIsOk;
	AID_LIST aidList;
	tti_byte status;
	tti_byte offlineAuthData[OFFLINE_AUTH_DATA_SIZE];
	tti_int32 offlineAuthDataLen;
	CommonRunningData commonRunningData;
	HostResponseData hostResponseData;
	ScriptsList scriptsList;
	Certificate caPublicKey;
	Certificate issuerPublicKey;
	Certificate iccPublicKey;
	Certificate pinPublicKey;
	TagList tagList;
	tti_bool offlineAuthWasDone;
	tti_tchar enterOfflinePlainPinString[MAX_DISPLAY_CHAR+1];
	tti_tchar enterOfflineEncryptPinString[MAX_DISPLAY_CHAR+1];
	tti_tchar enterOnlinePinString[MAX_DISPLAY_CHAR+1];
	tti_tchar lastPintryString[MAX_DISPLAY_CHAR+1];
	tti_tchar sessionKey[SESSION_KEY_SIZE+1];
	//Jason added on 2014.03.15
	tti_bool cdaIsNeedPerform;
	tti_bool cdaAlreadyPerform;
	tti_bool cdaIsSecondGacAskAAC;
	tti_byte pdolStream[BUFFER_SIZE_256BYTE];
	tti_int32 pdolStreamLen;
	tti_byte cdol1Stream[BUFFER_SIZE_256BYTE];
	tti_int32 cdol1StreamLen;
	tti_byte cdol2Stream[BUFFER_SIZE_256BYTE];
	tti_int32 cdol2StreamLen;
	tti_bool cdaIsGacRspTmp77;
	tti_bool cdaIsNeedParsedCID;
	tti_byte cdaParsedCID[BUFFER_SIZE_32BYTE];
	//End

	tti_bool qpbocCvmSign;
} RunningTimeEnv;

//Jason added Start: 2019/11/18
#define CY20_EMVL2_KERNEL
//Jason added End: 2019/11/18

//Jason added Start: 2020/02/12
//#define EMVL2_TEST_ENTER_PIN		
//Jason added End: 2020/02/12



#endif
