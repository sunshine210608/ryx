#ifndef _GLOBAL_H_
#define _GLOBAL_H_
#include "defines.h"
extern TerminalParam terminalParam;
extern RunningTimeEnv runningTimeEnv;

extern AID_LIST aidList;



extern tti_tchar PSEPreferredLang[PREFERRED_LANG_SIZE+1];



extern tti_byte cid[];

#ifdef __cplusplus
extern "C" {
#endif

void initGlobalData(void);
void	initRunningTimeEnv(RunningTimeEnv *runningTimeEnv);
tti_bool IsSupportIssuerAuth(RunningTimeEnv *runningTimeEnv);
tti_bool  IsPerformTermRiskManage(RunningTimeEnv *runningTimeEnv);
tti_bool terminalSupport(tti_uint16 termCapabilityType);
tti_bool terminalAddOnSupport(tti_uint16 termAddOnCapabilityType);
void SetTVR(tti_uint16 tvr);
void ClearTVR(tti_uint16 tvr);
void SetTSI(tti_uint16 tsi);
tti_bool needToDo(tti_byte condition);
tti_bool getAppUsageControl(tti_uint16 AppUsageControl);
void setCVMResult(tti_byte perform, tti_byte condition, tti_byte result);
#ifdef __cplusplus
}
#endif

#endif

