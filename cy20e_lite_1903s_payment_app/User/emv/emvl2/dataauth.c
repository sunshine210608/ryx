#include <string.h>
#include "defines.h"
#include "global.h"
#include "misce.h"
#include "utils.h"
#include "sha1.h"
#include "cardfunc.h"
#include "tag.h"
#include "BigNum.h"
#include "emvDebug.h"
//#include "util.h"


//------------------------------------------------------------------------------------------

tti_int32 RSA(Certificate *certificate, tti_byte *src, tti_int32 srcLen,
		 tti_byte *result, int *resultLen)

{
#if 1
	mpi	 mpiSrc, mpiMod, mpiExp, mpiDest;

	

	mpi_init(&mpiSrc, &mpiMod, &mpiExp, &mpiDest, NULL);

	

	if ( 0 != mpi_import(&mpiSrc,src, srcLen))

	{

		mpi_free(&mpiSrc, &mpiExp, &mpiMod, &mpiDest, NULL);

		return STATUS_FAIL;

	}

	

	if ( 0 != mpi_import(&mpiMod,certificate->modData, certificate->modLen))

	{

		mpi_free(&mpiSrc, &mpiExp, &mpiMod, &mpiDest, NULL);

		return STATUS_FAIL;

	}



	if ( 0 != mpi_import(&mpiExp, certificate->exp, certificate->expLen))

	{

		mpi_free(&mpiSrc, &mpiExp, &mpiMod, &mpiDest, NULL);

		return STATUS_FAIL;

	}



	if (0 != mpi_exp_mod( &mpiDest, &mpiSrc, &mpiExp, &mpiMod, NULL))

	{

		mpi_free(&mpiSrc, &mpiExp, &mpiMod, &mpiDest, NULL);

		return STATUS_FAIL;

	}



	if (0 != mpi_export(&mpiDest, result, resultLen))

	{

		mpi_free(&mpiSrc, &mpiExp, &mpiMod, &mpiDest, NULL);

		return STATUS_FAIL;

	}

	

//	mpi_show("\n", &mpiDest, 16);



	mpi_free(&mpiSrc, &mpiExp, &mpiMod, &mpiDest, NULL);



	return STATUS_OK;
#endif

	//return 0;
}



//------------------------------------------------------------------------------------------
tti_int32 checkRawIssuerCertificate(tti_byte *checkedRawCertificate, tti_int32 len, 
	RunningTimeEnv *runningTimeEnv)
{
	tti_byte hashSource[BUFFER_SIZE_1K];
	tti_int32 hashSourceLen;
	tti_byte hashResult[HASH_RESULT_SIZE];
	//tti_byte tempPanInCert[BUFFER_SIZE_32BYTE], tempPan[BUFFER_SIZE_32BYTE];
	tti_tchar tempPanInCert[BUFFER_SIZE_32BYTE], tempPan[BUFFER_SIZE_32BYTE];

	DPRINTF("check raw issuer certificate\n");

	if ((checkedRawCertificate[0]!=0x6a)||(checkedRawCertificate[1]!=0x02)||
		(checkedRawCertificate[len-1]!=0xbc))
	{
		DPRINTF("11\n");
		return STATUS_FAIL;
	}

	if (TagIsExisted(&runningTimeEnv->tagList, TAG_TRANSACTION_DATE))
	{
		if (isExpiredDate(&checkedRawCertificate[6], 
			GetTagValue(&runningTimeEnv->tagList, TAG_TRANSACTION_DATE)))
		{
			DPRINTF("22\n");
			return STATUS_FAIL;
		}
	}
	
	if ((checkedRawCertificate[11]!=0x01)||(checkedRawCertificate[12]!=0x01))
	{
		DPRINTF("333\n");
		return STATUS_FAIL;
	}

	translateBcdArrayToString(&checkedRawCertificate[2], 4, tempPanInCert);
	translateBcdArrayToString(GetTagValue(&runningTimeEnv->tagList, TAG_PAN),
		GetTagValueSize(&runningTimeEnv->tagList, TAG_PAN), tempPan);

	if ((strlen(tempPanInCert)>strlen(tempPan))||
		(strncmp(tempPanInCert, tempPan, strlen(tempPanInCert))!=0))
	{
		DPRINTF("444\n");
		return STATUS_FAIL;
	}

	memcpy(hashSource, checkedRawCertificate+1, len-HASH_RESULT_SIZE-1-1);
	hashSourceLen=len-HASH_RESULT_SIZE-1-1;

	memcpy(hashSource+hashSourceLen, GetTagValue(&runningTimeEnv->tagList, TAG_ISSUER_PK_REMAINDER),
		GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_PK_REMAINDER));
	hashSourceLen+=GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_PK_REMAINDER);

	memcpy(hashSource+hashSourceLen, GetTagValue(&runningTimeEnv->tagList, TAG_ISSUER_PK_EXPONENT),
		GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_PK_EXPONENT));
	hashSourceLen+=GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_PK_EXPONENT);

	DPRINTF("hashSource\n");
	printByteArray(hashSource, hashSourceLen);
	sha1_csum(hashSource, hashSourceLen, hashResult);

	if (memcmp(checkedRawCertificate+len-HASH_RESULT_SIZE-1, hashResult, HASH_RESULT_SIZE)!=0)
	{
		DPRINTF("55\n");
		return STATUS_FAIL;
	}

	DPRINTF("hashResult\n");
	printByteArray(hashResult, HASH_RESULT_SIZE);
	DPRINTF("checkedRawSDACertificate\n");
	printByteArray(checkedRawCertificate+len-HASH_RESULT_SIZE-1, HASH_RESULT_SIZE);
		
	return STATUS_OK;
}
//------------------------------------------------------------------------------------------

tti_int32 getIssuerCertificate(RunningTimeEnv *runningTimeEnv)
{
	tti_byte result[BUFFER_SIZE_1K];
	tti_byte tmpallzeroandff[BUFFER_SIZE_256BYTE];
	int len;
	//tti_uint16 keyRemainderLen;

	DPRINTF("get issuer certificate\n");

	if (runningTimeEnv->issuerPublicKey.existed)
	{
		return STATUS_OK;
	}

	memset(result, 0, BUFFER_SIZE_1K);
	
	if (!TagIsExisted(&runningTimeEnv->tagList, TAG_ISSUER_PK_CERTIFICATE)||
		!TagIsExisted(&runningTimeEnv->tagList, TAG_ISSUER_PK_EXPONENT)||
		!TagIsExisted(&runningTimeEnv->tagList, TAG_CA_PUBLIC_KEY_INDEX))
	{
		ReportLine();
		SetTVR(TVR_ICC_DATA_MISSING);
		return STATUS_FAIL;
	}

	//Jason added on 2017/04/25
	if (GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_PK_CERTIFICATE) == 0 ||
		GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_PK_EXPONENT) == 0)
	{
		ReportLine();
		SetTVR(TVR_ICC_DATA_MISSING);
		return STATUS_FAIL;
	}

	//Jason Disabled it on 2020/05/19
/*
	if (TagIsExisted(&runningTimeEnv->tagList, TAG_ISSUER_PK_REMAINDER) && 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_PK_REMAINDER) == 0)
	{
		ReportLine();
		SetTVR(TVR_ICC_DATA_MISSING);
		return STATUS_FAIL;
	}
*/
	//End

	memset(tmpallzeroandff, 0, sizeof(tmpallzeroandff));
	if (memcmp(tmpallzeroandff, GetTagValue(&runningTimeEnv->tagList, TAG_ISSUER_PK_CERTIFICATE), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_PK_CERTIFICATE)) == 0 ||
		memcmp(tmpallzeroandff, GetTagValue(&runningTimeEnv->tagList, TAG_ISSUER_PK_CERTIFICATE), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_PK_CERTIFICATE)) == 0)
	{
		ReportLine();
		return STATUS_FAIL;
	}

	memset(tmpallzeroandff, 0xff, sizeof(tmpallzeroandff));
	if (memcmp(tmpallzeroandff, GetTagValue(&runningTimeEnv->tagList, TAG_ISSUER_PK_CERTIFICATE), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_PK_CERTIFICATE)) == 0 ||
		memcmp(tmpallzeroandff, GetTagValue(&runningTimeEnv->tagList, TAG_ISSUER_PK_CERTIFICATE), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_PK_CERTIFICATE)) == 0)
	{
		ReportLine();
		return STATUS_FAIL;
	}
	//End

	//intCertificate(&runningTimeEnv->caPublicKey);

	ReportLine();
	len=BUFFER_SIZE_1K;
	if (RSA(&runningTimeEnv->caPublicKey, 
		GetTagValue(&runningTimeEnv->tagList, TAG_ISSUER_PK_CERTIFICATE),
		GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_PK_CERTIFICATE), 
		result, &len)!=STATUS_OK)
	{
		ReportLine();
		return STATUS_FAIL;
	}
	ReportLine();

	runningTimeEnv->issuerPublicKey.modLen=*(result+13);

	if (runningTimeEnv->issuerPublicKey.modLen>len-36)
	{
		ReportLine();
		memcpy(runningTimeEnv->issuerPublicKey.modData, result+15, len-36);
		if (TagIsExisted(&runningTimeEnv->tagList, TAG_ISSUER_PK_REMAINDER)
			&&(GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_PK_REMAINDER)==
			runningTimeEnv->issuerPublicKey.modLen-(len-36)))
		{
			ReportLine();
			memcpy(runningTimeEnv->issuerPublicKey.modData+len-36, 
				GetTagValue(&runningTimeEnv->tagList, TAG_ISSUER_PK_REMAINDER), 
				runningTimeEnv->issuerPublicKey.modLen-(len-36));
			ReportLine();
		}
		else
		{
			ReportLine();
			SetTVR(TVR_ICC_DATA_MISSING);
			return STATUS_FAIL;
		}
	}
	else
	{
		memcpy(runningTimeEnv->issuerPublicKey.modData, result+15, 
			runningTimeEnv->issuerPublicKey.modLen);
	}

	DPRINTF("Recover Issuer Cerfificate:\n");
	printByteArray(result, len);
	if (checkRawIssuerCertificate(result, len, runningTimeEnv)!=STATUS_OK)
	{
		ReportLine();
		return STATUS_FAIL;
	}

	runningTimeEnv->issuerPublicKey.expLen=*(result+14);
	if (runningTimeEnv->issuerPublicKey.expLen!=
		GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_PK_EXPONENT))
	{
		ReportLine();
		return STATUS_FAIL;
	}
	memcpy(runningTimeEnv->issuerPublicKey.exp, 
		GetTagValue(&runningTimeEnv->tagList, TAG_ISSUER_PK_EXPONENT),
		runningTimeEnv->issuerPublicKey.expLen);

	runningTimeEnv->issuerPublicKey.existed=TRUE;

	return STATUS_OK;
}

//------------------------------------------------------------------------------------------

tti_int32 setCertificate(tti_tchar *modulus, tti_tchar *exp, Certificate *certificate)

{

	tti_int32 modulusLen;

	tti_int32 expLen;



	modulusLen=strlen(modulus);



	if (((modulusLen/2)*2!=modulusLen)||(modulusLen>MAX_CA_PUBLIC_KEY_SIZE*2))

	{

		return STATUS_FAIL;

	}



	expLen=strlen(exp);

	if (((expLen/2)*2!=expLen)||(expLen>MAX_EXP_LEN*2))

	{

		return STATUS_FAIL;

	}



	TranslateStringToByteArray(modulus, certificate->modData, &certificate->modLen);

	TranslateStringToByteArray(exp, certificate->exp, &certificate->expLen);

	

	return STATUS_OK;

}



//------------------------------------------------------------------------------------------
tti_int32 checkRawSDACertificate(tti_byte *checkedRawSDACertificate, tti_int32 len, 
	RunningTimeEnv *runningTimeEnv)
{
	tti_byte hashSource[BUFFER_SIZE_4K];
	tti_int32 hashSourceLen;
	tti_byte hashResult[HASH_RESULT_SIZE];
	tti_byte *pTagValue;

	DPRINTF("check raw sda certificate\n");

	if ((checkedRawSDACertificate[0]!=0x6a)||(checkedRawSDACertificate[1]!=0x03)||
		(checkedRawSDACertificate[len-1]!=0xbc))
	{		
		DPRINTF("11\n");
		return STATUS_FAIL;
	}
	
	if (checkedRawSDACertificate[2]!=0x01)
	{
		DPRINTF("22\n");
		return STATUS_FAIL;
	}

	memcpy(hashSource, checkedRawSDACertificate+1, len-HASH_RESULT_SIZE-1-1);
	hashSourceLen=len-HASH_RESULT_SIZE-1-1;

	if (runningTimeEnv->offlineAuthDataLen>0)
	{
		memcpy(hashSource+hashSourceLen, runningTimeEnv->offlineAuthData, runningTimeEnv->offlineAuthDataLen);
		hashSourceLen+=runningTimeEnv->offlineAuthDataLen;
	}

	if (TagIsExisted(&runningTimeEnv->tagList, TAG_SDA_TAGLIST))
	{
		DPRINTF("TAG_SDA_TAGLIST  exist\n");
		pTagValue=GetTagValue(&runningTimeEnv->tagList, TAG_SDA_TAGLIST);
		if (*pTagValue!=TAG_AIP)
		{
			DPRINTF("33\n");
			return STATUS_FAIL;
		}
		memcpy(hashSource+hashSourceLen, GetTagValue(&runningTimeEnv->tagList, TAG_AIP),
			GetTagValueSize(&runningTimeEnv->tagList, TAG_AIP));

		hashSourceLen+=GetTagValueSize(&runningTimeEnv->tagList, TAG_AIP);
	}

	DPRINTF("hashSource\n");
	printByteArray(hashSource, hashSourceLen);
	sha1_csum(hashSource, hashSourceLen, hashResult);

	if (memcmp(checkedRawSDACertificate+len-HASH_RESULT_SIZE-1, hashResult, HASH_RESULT_SIZE)!=0)
	{
		DPRINTF("44\n");
		DPRINTF("hashResult\n");
		printByteArray(hashResult, HASH_RESULT_SIZE);
		DPRINTF("checkedRawSDACertificate\n");
		printByteArray(checkedRawSDACertificate+len-HASH_RESULT_SIZE-1, HASH_RESULT_SIZE);
		return STATUS_FAIL;
	}

	return STATUS_OK;
}
//------------------------------------------------------------------------------------------
tti_int32 performSDA(RunningTimeEnv *runningTimeEnv)
{
	tti_byte SDAdata[BUFFER_SIZE_512BYTE];
	int SDAdataLen;

	DPRINTF("perform sda\n");
	
	SetTSI(TSI_OFFLINE_DATA_AUTH_PERFORMED);

	//Jason added on 2014.01.13
	SetTVR(TVR_SDA_SELECTED);
	//End
	
	if (getIssuerCertificate(runningTimeEnv)!=STATUS_OK)
	{
		DPRINTF("getIssuerCertificate return error\n");
		return STATUS_FAIL;
	}

	if (!TagIsExisted(&runningTimeEnv->tagList, TAG_SDA_SSAD))
	{
		SetTVR(TVR_ICC_DATA_MISSING);
		return STATUS_FAIL;
	}
	
	SDAdataLen=BUFFER_SIZE_512BYTE;
	if (RSA(&runningTimeEnv->issuerPublicKey, 
		GetTagValue(&runningTimeEnv->tagList, TAG_SDA_SSAD),
		GetTagValueSize(&runningTimeEnv->tagList, TAG_SDA_SSAD), 
		SDAdata, &SDAdataLen)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	DPRINTF("Recover SDA data :\n");
	printByteArray(SDAdata, SDAdataLen);
	if (checkRawSDACertificate(SDAdata, SDAdataLen, runningTimeEnv)!=STATUS_OK)
	{
		DPRINTF("checkRawSDACertificate return error\n");
		return STATUS_FAIL;
	}

	SetTagValue(TAG_DATA_AUTH_CODE, &SDAdata[3], 2, &runningTimeEnv->tagList);
	return STATUS_OK;
}
//------------------------------------------------------------------------------------------

tti_int32 checkRawIccCertificateFormat(tti_byte *checkedRawIccCertificate, tti_int32 len, 
	RunningTimeEnv *runningTimeEnv)
{
	//tti_byte tempPanInCert[BUFFER_SIZE_32BYTE], tempPan[BUFFER_SIZE_32BYTE];
	tti_tchar tempPanInCert[BUFFER_SIZE_32BYTE], tempPan[BUFFER_SIZE_32BYTE];

	DPRINTF("check icc certificate format\n");

	if ((checkedRawIccCertificate[0]!=0x6a)||(checkedRawIccCertificate[1]!=0x04)||

		(checkedRawIccCertificate[len-1]!=0xbc))

	{

		return STATUS_FAIL;

	}



	if ((checkedRawIccCertificate[17]!=0x01)||(checkedRawIccCertificate[18]!=0x01))

	{

		return STATUS_FAIL;

	}



	if (TagIsExisted(&runningTimeEnv->tagList, TAG_TRANSACTION_DATE))

	{

		if (isExpiredDate(&checkedRawIccCertificate[12], 

			GetTagValue(&runningTimeEnv->tagList, TAG_TRANSACTION_DATE)))

		{

			return STATUS_FAIL;

		}

	}



	translateBcdArrayToString(&checkedRawIccCertificate[2], 10, tempPanInCert);

	translateBcdArrayToString(GetTagValue(&runningTimeEnv->tagList, TAG_PAN),

		GetTagValueSize(&runningTimeEnv->tagList, TAG_PAN), tempPan);



	DPRINTF("len=%d   %d\n", strlen(tempPanInCert), strlen(tempPan));



	DPRINTF("str=%s   %s\n", tempPanInCert, tempPan);

	

	if ((strlen(tempPanInCert)>strlen(tempPan))||

		(strncmp(tempPanInCert, tempPan, strlen(tempPanInCert))!=0))

	{

		return STATUS_FAIL;

	}


	DPRINTF("checkRawIccCertificateFormat ok\n");
	return STATUS_OK;

}



//------------------------------------------------------------------------------------------

tti_int32 checkRawIccCertificate(tti_byte *checkedRawIccCertificate, tti_int32 len, 

	RunningTimeEnv *runningTimeEnv)

{

	tti_byte hashSource[BUFFER_SIZE_4K];

	tti_int32 hashSourceLen;

	tti_byte hashResult[HASH_RESULT_SIZE];

	tti_byte *pTagValue;



	DPRINTF("check raW ICC certificate\n");

	

	printByteArray(checkedRawIccCertificate, len);



	if (checkRawIccCertificateFormat(checkedRawIccCertificate, len, runningTimeEnv)!=STATUS_OK)

	{
		
		DPRINTF("checkRawIccCertificateFormat error\n");
		return STATUS_FAIL;

	}



	memcpy(hashSource, checkedRawIccCertificate+1, len-HASH_RESULT_SIZE-1-1);

	hashSourceLen=len-HASH_RESULT_SIZE-1-1;



	if (TagIsExisted(&runningTimeEnv->tagList, TAG_ICC_PK_REMAINDER))

	{

		memcpy(hashSource+hashSourceLen, GetTagValue(&runningTimeEnv->tagList, TAG_ICC_PK_REMAINDER),

			GetTagValueSize(&runningTimeEnv->tagList, TAG_ICC_PK_REMAINDER));

		hashSourceLen+=GetTagValueSize(&runningTimeEnv->tagList, TAG_ICC_PK_REMAINDER);

	}



	memcpy(hashSource+hashSourceLen, GetTagValue(&runningTimeEnv->tagList, TAG_ICC_PK_EXPONENT),

		GetTagValueSize(&runningTimeEnv->tagList, TAG_ICC_PK_EXPONENT));

	hashSourceLen+=GetTagValueSize(&runningTimeEnv->tagList, TAG_ICC_PK_EXPONENT);



	if (runningTimeEnv->offlineAuthDataLen>0)

	{

		memcpy(hashSource+hashSourceLen, runningTimeEnv->offlineAuthData, runningTimeEnv->offlineAuthDataLen);

		hashSourceLen+=runningTimeEnv->offlineAuthDataLen;

	}



	if (TagIsExisted(&runningTimeEnv->tagList, TAG_SDA_TAGLIST))

	{

		pTagValue=GetTagValue(&runningTimeEnv->tagList, TAG_SDA_TAGLIST);

		if (*pTagValue!=TAG_AIP)

		{
			
			DPRINTF("TAG_AIP error\n");
			return STATUS_FAIL;

		}

		memcpy(hashSource+hashSourceLen, GetTagValue(&runningTimeEnv->tagList, TAG_AIP),

			GetTagValueSize(&runningTimeEnv->tagList, TAG_AIP));

		hashSourceLen+=GetTagValueSize(&runningTimeEnv->tagList, TAG_AIP);

	}

	

	sha1_csum(hashSource, hashSourceLen, hashResult);



	if (memcmp(checkedRawIccCertificate+len-HASH_RESULT_SIZE-1, hashResult, HASH_RESULT_SIZE)!=0)

	{
		DPRINTF("memcmp hashResult error\n");

		return STATUS_FAIL;

	}



	return STATUS_OK;

}



//------------------------------------------------------------------------------------------

tti_int32 getIccCertificate(RunningTimeEnv *runningTimeEnv)

{

	tti_byte result[BUFFER_SIZE_1K];

	int len;



	DPRINTF("get icc certificate\n");



	if (runningTimeEnv->iccPublicKey.existed)

	{

		return STATUS_OK;

	}

	

	if (getIssuerCertificate(runningTimeEnv)!=STATUS_OK)

	{
		DPRINTF("getIssuerCertificate error");
		return STATUS_FAIL;

	}



	memset(result, 0, BUFFER_SIZE_1K);



	if (!TagIsExisted(&runningTimeEnv->tagList, TAG_ICC_PK_CERTIFICATE)||

		!TagIsExisted(&runningTimeEnv->tagList, TAG_ICC_PK_EXPONENT))

	{
        ReportLine();
		SetTVR(TVR_ICC_DATA_MISSING);

		return STATUS_FAIL;

	}



	len=BUFFER_SIZE_1K;

	

	if (RSA(&runningTimeEnv->issuerPublicKey, 

		GetTagValue(&runningTimeEnv->tagList, TAG_ICC_PK_CERTIFICATE),

		GetTagValueSize(&runningTimeEnv->tagList, TAG_ICC_PK_CERTIFICATE), 

		result, &len)!=STATUS_OK)

	{
        ReportLine();
		return STATUS_FAIL;

	}





	runningTimeEnv->iccPublicKey.modLen=*(result+19);

	if (runningTimeEnv->iccPublicKey.modLen>len-42)

	{

		memcpy(runningTimeEnv->iccPublicKey.modData, result+21, len-42);

		if (TagIsExisted(&runningTimeEnv->tagList, TAG_ICC_PK_REMAINDER))

		{

			memcpy(runningTimeEnv->iccPublicKey.modData+len-42, 

				GetTagValue(&runningTimeEnv->tagList, TAG_ICC_PK_REMAINDER), 

				GetTagValueSize(&runningTimeEnv->tagList, TAG_ICC_PK_REMAINDER));

		}

		else

		{
            ReportLine();
			SetTVR(TVR_ICC_DATA_MISSING);

			return STATUS_FAIL;

		}

	}

	else

	{

		memcpy(runningTimeEnv->iccPublicKey.modData, result+21,

			runningTimeEnv->iccPublicKey.modLen);

	}



	if (checkRawIccCertificate(result, len, runningTimeEnv)!=STATUS_OK)

	{
        ReportLine();
		return STATUS_FAIL;

	}



	runningTimeEnv->iccPublicKey.expLen=*(result+20);

	memcpy(runningTimeEnv->iccPublicKey.exp, 

		GetTagValue(&runningTimeEnv->tagList, TAG_ICC_PK_EXPONENT),

		runningTimeEnv->iccPublicKey.expLen);



	runningTimeEnv->iccPublicKey.existed=TRUE;


    ReportLine();
	return STATUS_OK;

}



//------------------------------------------------------------------------------------------

tti_int32 checkRawPinCertificateFormat(tti_byte *rawPinCertificate, tti_int32 len, 

		RunningTimeEnv *runningTimeEnv)

{

	return checkRawIccCertificateFormat(rawPinCertificate, len, runningTimeEnv);

}



//------------------------------------------------------------------------------------------

tti_int32 checkRawPinCertificate(tti_byte *rawPinCertificate, tti_int32 len, 

		RunningTimeEnv *runningTimeEnv)

{

	tti_byte hashSource[BUFFER_SIZE_1K];

	tti_int32 hashSourceLen;

	tti_byte hashResult[HASH_RESULT_SIZE];

	tti_byte *pTagValue;



	DPRINTF("check raw pin certificate\n");



	if (checkRawPinCertificateFormat(rawPinCertificate, len, runningTimeEnv)!=STATUS_OK)

	{

		return STATUS_FAIL;

	}



	memcpy(hashSource, rawPinCertificate+1, len-HASH_RESULT_SIZE-1-1);

	hashSourceLen=len-HASH_RESULT_SIZE-1-1;



	if (TagIsExisted(&runningTimeEnv->tagList, TAG_PIN_PK_REMAINDER))

	{

		memcpy(hashSource+hashSourceLen, GetTagValue(&runningTimeEnv->tagList, TAG_PIN_PK_REMAINDER),

			GetTagValueSize(&runningTimeEnv->tagList, TAG_PIN_PK_REMAINDER));

		hashSourceLen+=GetTagValueSize(&runningTimeEnv->tagList, TAG_PIN_PK_REMAINDER);

	}



	memcpy(hashSource+hashSourceLen, GetTagValue(&runningTimeEnv->tagList, TAG_PIN_EXPONENT),

		GetTagValueSize(&runningTimeEnv->tagList, TAG_PIN_EXPONENT));

	hashSourceLen+=GetTagValueSize(&runningTimeEnv->tagList, TAG_PIN_EXPONENT);



	if (TagIsExisted(&runningTimeEnv->tagList, TAG_SDA_TAGLIST))

	{

		pTagValue=GetTagValue(&runningTimeEnv->tagList, TAG_SDA_TAGLIST);

		if ((*(pTagValue)!=TAG_AIP)||

			(GetTagValueSize(&runningTimeEnv->tagList, TAG_SDA_TAGLIST)!=1))

		{
			DPRINTF("1111 error\n");
			return STATUS_FAIL;

		}

		memcpy(hashSource+hashSourceLen, GetTagValue(&runningTimeEnv->tagList, TAG_AIP),

			GetTagValueSize(&runningTimeEnv->tagList, TAG_AIP));

		hashSourceLen+=GetTagValueSize(&runningTimeEnv->tagList, TAG_AIP);

	}





	sha1_csum(hashSource, hashSourceLen, hashResult);



	if (memcmp(rawPinCertificate+len-HASH_RESULT_SIZE-1, hashResult, HASH_RESULT_SIZE)!=0)

	{

		DPRINTF("rawPinCertificate hash error\n");
		return STATUS_FAIL;

	}



	return STATUS_OK;

}



//------------------------------------------------------------------------------------------

tti_int32 getPinCertificate(RunningTimeEnv *runningTimeEnv)

{

	tti_byte result[BUFFER_SIZE_1K];

	int len=BUFFER_SIZE_1K;



	DPRINTF("get pin certificate\n");



	if (runningTimeEnv->pinPublicKey.existed)

	{

		return STATUS_OK;

	}

	

	if (getIssuerCertificate(runningTimeEnv)!=STATUS_OK)

	{

		return STATUS_FAIL;

	}



	memset(result, 0, BUFFER_SIZE_1K);



	if (!TagIsExisted(&runningTimeEnv->tagList, TAG_PIN_CERTIFICATE)||

		!TagIsExisted(&runningTimeEnv->tagList, TAG_PIN_EXPONENT))

	{

		return STATUS_FAIL;

	}



	if (RSA(&runningTimeEnv->issuerPublicKey, 

		GetTagValue(&runningTimeEnv->tagList, TAG_PIN_CERTIFICATE),

		GetTagValueSize(&runningTimeEnv->tagList, TAG_PIN_CERTIFICATE), 

		result, &len)!=STATUS_OK)

	{

		return STATUS_FAIL;

	}



	runningTimeEnv->pinPublicKey.modLen=*(result+19);

	if (runningTimeEnv->pinPublicKey.modLen>len-42)

	{

		memcpy(runningTimeEnv->pinPublicKey.modData, result+21, len-42);

		if (TagIsExisted(&runningTimeEnv->tagList, TAG_PIN_PK_REMAINDER))

		{

			memcpy(runningTimeEnv->pinPublicKey.modData+len-42, 

				GetTagValue(&runningTimeEnv->tagList, TAG_PIN_PK_REMAINDER), 

				GetTagValueSize(&runningTimeEnv->tagList, TAG_PIN_PK_REMAINDER));

		}

		else

		{

			SetTVR(TVR_ICC_DATA_MISSING);

			return STATUS_FAIL;

		}

	}

	else

	{

		memcpy(runningTimeEnv->pinPublicKey.modData, result+21, 

			runningTimeEnv->pinPublicKey.modLen);

	}



	if (checkRawPinCertificate(result, len, runningTimeEnv)!=STATUS_OK)

	{
		DPRINTF("checkRawPinCertificate error\n");
		return STATUS_FAIL;

	}



	runningTimeEnv->pinPublicKey.expLen=*(result+20);

	memcpy(runningTimeEnv->pinPublicKey.exp, 

		GetTagValue(&runningTimeEnv->tagList, TAG_PIN_EXPONENT),

		runningTimeEnv->pinPublicKey.expLen);



	runningTimeEnv->pinPublicKey.existed=TRUE;


	DPRINTF("getPinCertificate ok\n");
	return STATUS_OK;

}



//------------------------------------------------------------------------------------------

tti_int32 checkRawDDACertificate(tti_byte *rawDDACertificate, tti_int32 len, 

	RunningTimeEnv *runningTimeEnv)

{

	tti_byte hashSource[BUFFER_SIZE_256BYTE];

	tti_int32 hashSourceLen;

	tti_byte ddolSource[BUFFER_SIZE_256BYTE];

	tti_byte ddolSourceLen;

	tti_byte hashResult[HASH_RESULT_SIZE];

	LOGD("check raw dda certificate\n");

	if ((rawDDACertificate[0]!=0x6a)||(rawDDACertificate[1]!=0x05)||
		(rawDDACertificate[len-1]!=0xbc))
	{
		REPORT_ERR_LINE();
		return STATUS_FAIL;
	}

	if (rawDDACertificate[2]!=0x01)
	{
		REPORT_ERR_LINE();
		return STATUS_FAIL;
	}

	memcpy(hashSource, rawDDACertificate+1, len-HASH_RESULT_SIZE-1-1);
	hashSourceLen=len-HASH_RESULT_SIZE-1-1;

	BuildDOLToStream(&runningTimeEnv->tagList, TAG_DDOL, ddolSource, &ddolSourceLen);
	memcpy(hashSource+hashSourceLen, ddolSource, ddolSourceLen);
	hashSourceLen+=ddolSourceLen;

	//hexdumpEx("ddolSource", ddolSource, ddolSourceLen);

	sha1_csum(hashSource, hashSourceLen, hashResult);

	if (memcmp(rawDDACertificate+len-HASH_RESULT_SIZE-1, 
		hashResult, HASH_RESULT_SIZE)!=0)
	{
		REPORT_ERR_LINE();
		LOGE("hashResult ERR");
		return STATUS_FAIL;
	}



	return STATUS_OK;

}

	
	
	
	//------------------------------------------------------------------------------------------
	tti_int32 checkQpbocRawDDACertificate(tti_byte *rawDDACertificate, tti_int32 len, 
		RunningTimeEnv *runningTimeEnv)
	{
		tti_byte hashSource[BUFFER_SIZE_256BYTE];
		tti_int32 hashSourceLen;
		tti_byte ddolSource[BUFFER_SIZE_256BYTE];
		tti_byte ddolSourceLen;
		tti_byte hashResult[HASH_RESULT_SIZE];
		//tti_uint16 tagList[] = {
		//					TAG_UNPREDICTABLE_NUMBER,
		//					TAG_AMOUNT,
		//					TAG_TRANS_CURRENCY_CODE, 
		//					TAG_QPBOC_CARD_AUTH_DATA, 
		//					TAG_ATC, 
		//					};
		//tti_uint16 templen;
	
		LOGD("check raw dda certificate\n");
	
		if ((rawDDACertificate[0]!=0x6a)||(rawDDACertificate[1]!=0x05)||
			(rawDDACertificate[len-1]!=0xbc))
		{
			REPORT_ERR_LINE();
			return STATUS_FAIL;
		}
	
		if (rawDDACertificate[2]!=0x01)
		{
			REPORT_ERR_LINE();
			return STATUS_FAIL;
		}
	
		memcpy(hashSource, rawDDACertificate+1, len-HASH_RESULT_SIZE-1-1);
		hashSourceLen=len-HASH_RESULT_SIZE-1-1;

		BuildDOLToStream(&runningTimeEnv->tagList, TAG_DDOL, ddolSource, &ddolSourceLen);
		//TagArrsBuildTLV(&runningTimeEnv->tagList, tagList, sizeof(tagList)/sizeof(tagList[0]), ddolSource, &templen);
		//ddolSourceLen = templen;
		memcpy(hashSource+hashSourceLen, ddolSource, ddolSourceLen);
		hashSourceLen+=ddolSourceLen;
		
		//memcpy(hashSource, ddolSource, ddolSourceLen);
		//hashSourceLen = ddolSourceLen;
		
		//hexdumpEx("ddolSource", ddolSource, ddolSourceLen);
    //hexdumpEx("hashSource", hashSource, hashSourceLen);
		sha1_csum(hashSource, hashSourceLen, hashResult);
	
		if (memcmp(rawDDACertificate+len-HASH_RESULT_SIZE-1, 
			hashResult, HASH_RESULT_SIZE)!=0)
		{
			REPORT_ERR_LINE();
			//hexdumpEx("hashResult", hashResult, HASH_RESULT_SIZE);
			LOGE("hashResult ERR");
			return STATUS_FAIL;
		}

		return STATUS_OK;
	
	}


//Jason added on 2014/03/14
tti_int32 checkCDAICCDynamicData(tti_byte *iccDynamicData, tti_int32 len, 
	RunningTimeEnv *runningTimeEnv)
{
	tti_byte iccDynamicNumber[BUFFER_SIZE_32BYTE];
	tti_int32 iccDynamicNumberLen;
	tti_byte iccDynamicCID[BUFFER_SIZE_32BYTE];
	tti_byte *pTagValue;

	pTagValue = GetTagValue(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA);
	if (pTagValue == NULL)
	{
		ReportLine();
		return STATUS_FAIL;
	}

	iccDynamicNumberLen = iccDynamicData[0];
	memcpy(iccDynamicNumber, iccDynamicData+1, iccDynamicNumberLen);

	memcpy(iccDynamicCID, iccDynamicData+1+iccDynamicNumberLen, 1);
	if (memcmp(iccDynamicCID, pTagValue, 1) != 0)
	{
		ReportLine();
		runningTimeEnv->cdaIsNeedParsedCID = TRUE;
		memcpy(runningTimeEnv->cdaParsedCID, iccDynamicCID, 1);
		return STATUS_FAIL;
	}

	return STATUS_OK;
}
//End

//Jason added on 2014/03/14
tti_int32 getGACRspExceptSDDA(RunningTimeEnv *runningTimeEnv, tti_byte *pGACRsp, tti_int32 *pGACRspLen)
{
	tti_int32 tagIndexArr[3];
	tti_uint16 tagArr[3] = {TAG_CRYPTOGRAM_INFO_DATA, TAG_ATC, TAG_ISSUER_APP_DATA};
	tti_int32 i,j;
	tti_int32 tagIndexTmp;
	tti_uint16 tagTmp;
//	tti_int32 len = 0;
	tti_byte tmplen;

	for(i=0;i<3;i++)
	{
		tagIndexArr[i] = GetTagIndex(&runningTimeEnv->tagList, tagArr[i]);
	}
	
	for(i=0;i<2;i++)
	{
		for(j=0;j<2-i;j++)
		{
			if (tagIndexArr[j] > tagIndexArr[j+1])
			{
				tagIndexTmp = tagIndexArr[j];
				tagIndexArr[j] = tagIndexArr[j+1];
				tagIndexArr[j+1] = tagIndexTmp;

				tagTmp = tagArr[j];
				tagArr[j] = tagArr[j+1];
				tagArr[j+1] = tagTmp;			
			}
		}
	}

	*pGACRspLen = 0;
	for(i=0;i<3;i++)
	{
		if (tagIndexArr[i] != -1)
		{
			if (tagArr[i] == TAG_CRYPTOGRAM_INFO_DATA)
			{
				memcpy(pGACRsp + (*pGACRspLen), "\x9f", 1);
				memcpy(pGACRsp + (*pGACRspLen) + 1, "\x27", 1);
			}else if (tagArr[i] == TAG_ATC)
			{
				memcpy(pGACRsp + (*pGACRspLen), "\x9f", 1);
				memcpy(pGACRsp + (*pGACRspLen) + 1, "\x36", 1);
			}else
			{
				memcpy(pGACRsp + (*pGACRspLen), "\x9f", 1);
				memcpy(pGACRsp + (*pGACRspLen) + 1, "\x10", 1);
			}
			tmplen = GetTagValueSize(&runningTimeEnv->tagList, tagArr[i]);
			memcpy(pGACRsp+(*pGACRspLen)+2, &tmplen, 1);
		
			FillDataField(&runningTimeEnv->tagList, tagArr[i], 
				GetTagValueSize(&runningTimeEnv->tagList, tagArr[i]), pGACRsp + (*pGACRspLen) + 3);
			(*pGACRspLen)+= GetTagValueSize(&runningTimeEnv->tagList, tagArr[i]) + 3;
		}
	}

	DPRINTF("getGACRspExceptSDDA response\n");
	printByteArray(pGACRsp, *pGACRspLen);

	return STATUS_OK;
}

//End

//Jason added  on 2014/03/14
//------------------------------------------------------------------------------------------
tti_int32 checkRawCDACertificate(tti_byte *rawCDACertificate, tti_int32 len, 
	RunningTimeEnv *runningTimeEnv, tti_bool isFirstGAC)
{
	tti_byte hashSource[BUFFER_SIZE_256BYTE];
	tti_int32 hashSourceLen;
	tti_byte iccDynamicSource[BUFFER_SIZE_256BYTE];
	tti_int32 iccDynamicSourceLen;
//	tti_byte pdolSource[BUFFER_SIZE_256BYTE];
//	tti_byte pdolSourceLen;	
//	tti_byte cdol1Source[BUFFER_SIZE_256BYTE];
//	tti_byte cdol1SourceLen;	
//	tti_byte cdol2Source[BUFFER_SIZE_256BYTE];
//	tti_byte cdol2SourceLen;
	tti_byte hashResult[HASH_RESULT_SIZE];
	tti_byte *pTagValue;
	tti_int32 gacRsplen;
	tti_int32 tmpLen;
	
	DPRINTF("check raw cda certificate\n");

	if ((rawCDACertificate[0]!=0x6a)||(rawCDACertificate[1]!=0x05)||
		(rawCDACertificate[len-1]!=0xbc))
	{
		ReportLine();
		return STATUS_FAIL;
	}

	if (rawCDACertificate[2]!=0x01)
	{
		ReportLine();
		return STATUS_FAIL;
	}

	iccDynamicSourceLen = rawCDACertificate[3];
	memcpy(iccDynamicSource, rawCDACertificate+4, iccDynamicSourceLen);

	if (checkCDAICCDynamicData(iccDynamicSource, iccDynamicSourceLen, runningTimeEnv) != STATUS_OK)
	{
		ReportLine();
		return STATUS_FAIL;
	}

	memcpy(hashSource, rawCDACertificate+1, len-HASH_RESULT_SIZE-1-1);
	hashSourceLen=len-HASH_RESULT_SIZE-1-1;

	pTagValue = GetTagValue(&runningTimeEnv->tagList, TAG_UNPREDICTABLE_NUMBER);
	if (pTagValue == NULL)
	{
		ReportLine();
		return STATUS_FAIL;
	}
	memcpy(hashSource + hashSourceLen, pTagValue, 4);
	hashSourceLen+=4;

	sha1_csum(hashSource, hashSourceLen, hashResult);

	if (memcmp(rawCDACertificate+len-HASH_RESULT_SIZE-1, 
		hashResult, HASH_RESULT_SIZE)!=0)
	{
		ReportLine();
		return STATUS_FAIL;
	}

	memset(hashSource, 0, sizeof(hashSource));
	memset(hashResult, 0, sizeof(hashResult));
	hashSourceLen = 0;
	
	memcpy(hashSource + hashSourceLen, runningTimeEnv->pdolStream, runningTimeEnv->pdolStreamLen);
	hashSourceLen+=runningTimeEnv->pdolStreamLen;
	
	memcpy(hashSource + hashSourceLen, runningTimeEnv->cdol1Stream, runningTimeEnv->cdol1StreamLen);
	hashSourceLen+=runningTimeEnv->cdol1StreamLen;
	
	if (isFirstGAC == FALSE)
	{
		memcpy(hashSource + hashSourceLen, runningTimeEnv->cdol2Stream, runningTimeEnv->cdol2StreamLen);
		hashSourceLen+=runningTimeEnv->cdol2StreamLen;
	}

	getGACRspExceptSDDA(runningTimeEnv, hashSource + hashSourceLen, &gacRsplen);
	hashSourceLen+=gacRsplen;

	sha1_csum(hashSource, hashSourceLen, hashResult);

	DPRINTF("iccDynamicSource \n");
	printByteArray(iccDynamicSource, iccDynamicSourceLen);
	DPRINTF("hashResult \n");
	printByteArray(hashResult, 20);
	DPRINTF("hashSource \n");
	printByteArray(hashSource, hashSourceLen);

	tmpLen = iccDynamicSource[0];
	if (memcmp(iccDynamicSource+1+tmpLen+1+8, 
		hashResult, HASH_RESULT_SIZE)!=0)
	{
		ReportLine();
		return STATUS_FAIL;
	}

	return STATUS_OK;
}
//End

//------------------------------------------------------------------------------------------

tti_int32 internalAuth(RunningTimeEnv *runningTimeEnv)

{

	tti_byte ddolSource[BUFFER_SIZE_256BYTE];

	tti_byte ddolSourceLen;

	tti_byte response[BUFFER_SIZE_256BYTE];

	tti_uint16 responseLen;

	tti_uint16 tag, tagSize, length, lengthSize;



	DPRINTF("intermal auth\n");



	if (!tagIsInDOL(&runningTimeEnv->tagList, TAG_DDOL, TAG_UNPREDICTABLE_NUMBER))

	{

		return STATUS_FAIL;

	}

	

	BuildDOLToStream(&runningTimeEnv->tagList, TAG_DDOL, ddolSource, &ddolSourceLen);



	if (SmartCardInternalAuth(ddolSource, ddolSourceLen, response, &responseLen)!=STATUS_OK)

	{

		return STATUS_ERROR;

	}



	if (response[0]==0x80)

	{

		if (ParseTlvInfo(response, responseLen, &tag, &tagSize, &length, &lengthSize)!=STATUS_OK)

		{

			return STATUS_ERROR;

		}

		if (responseLen!=tagSize+length+lengthSize)

		{

			return STATUS_ERROR;

		}

		SetTagValue(TAG_DDA_SDAD, response+tagSize+lengthSize, length, &runningTimeEnv->tagList);

	}

	else if (response[0]==0x77)

	{

		if (BuildTagList(response, responseLen, &runningTimeEnv->tagList)!=STATUS_OK)

		{

			return STATUS_ERROR;

		}

	}

	else

	{

		return STATUS_ERROR;

	}



	if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_DDA_SDAD))

	{

		return STATUS_ERROR;

	}

	

	return STATUS_OK;

}



//------------------------------------------------------------------------------------------

tti_int32 performDDA(RunningTimeEnv *runningTimeEnv)

{

	tti_byte ddaData[BUFFER_SIZE_512BYTE];

	int ddaDataLen;

	tti_int32 status;



	DPRINTF("perform dda\n");



	SetTSI(TSI_OFFLINE_DATA_AUTH_PERFORMED);

	

	if (getIccCertificate(runningTimeEnv)!=STATUS_OK)

	{

		return STATUS_FAIL;

	}



	status=internalAuth(runningTimeEnv);

	if (status!=STATUS_OK)

	{

		return status;

	}



	if (!TagIsExisted(&runningTimeEnv->tagList, TAG_DDA_SDAD))

	{

		return STATUS_FAIL;

	}



	ddaDataLen=sizeof(ddaData);



	if (RSA(&runningTimeEnv->iccPublicKey, GetTagValue(&runningTimeEnv->tagList, TAG_DDA_SDAD),

		GetTagValueSize(&runningTimeEnv->tagList, TAG_DDA_SDAD), ddaData, &ddaDataLen)!=STATUS_OK)

	{

		return STATUS_FAIL;

	}



	printByteArray(ddaData, ddaDataLen);



	if (checkRawDDACertificate(ddaData, ddaDataLen, runningTimeEnv)!=STATUS_OK)

	{

		return STATUS_FAIL;

	}



	SetTagValue(TAG_DYNAMIC_DATA, &ddaData[5], ddaData[4], &runningTimeEnv->tagList);



	return STATUS_OK;

}

//Jason added  on 2014/03/14
//------------------------------------------------------------------------------------------
tti_int32 recoveryKeyCDA(RunningTimeEnv *runningTimeEnv)
{
	DPRINTF("recovery Key CDA\n");

	SetTSI(TSI_OFFLINE_DATA_AUTH_PERFORMED);

	if (getIccCertificate(runningTimeEnv)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}

	return STATUS_OK;
}

//------------------------------------------------------------------------------------------
tti_int32 performCDA(RunningTimeEnv *runningTimeEnv, tti_bool isFirstGAC)
{
	tti_byte cdaData[BUFFER_SIZE_512BYTE];
	int cdaDataLen;
	tti_int32 status;
	tti_int32 tmpLen;

	DPRINTF("perform cda\n");
	
	if (!TagIsExisted(&runningTimeEnv->tagList, TAG_DDA_SDAD))
	{
		ReportLine();
		return STATUS_FAIL;
	}

	cdaDataLen=sizeof(cdaData);

	if (RSA(&runningTimeEnv->iccPublicKey, GetTagValue(&runningTimeEnv->tagList, TAG_DDA_SDAD),
		GetTagValueSize(&runningTimeEnv->tagList, TAG_DDA_SDAD), cdaData, &cdaDataLen)!=STATUS_OK)
	{
		ReportLine();
		return STATUS_FAIL;
	}

	printByteArray(cdaData, cdaDataLen);

	status = checkRawCDACertificate(cdaData, cdaDataLen, runningTimeEnv, isFirstGAC);	
	if (status != STATUS_OK)
	{
		ReportLine();
		return STATUS_FAIL;
	}

	SetTagValue(TAG_DYNAMIC_DATA, &cdaData[5], cdaData[4], &runningTimeEnv->tagList);
	tmpLen = cdaData[4];
	SetTagValue(TAG_CRYPTOGRAM, cdaData + 5 + tmpLen + 1, 8, &runningTimeEnv->tagList);

	DPRINTF("perform CDA OK, isFirstGAC = %d\n", isFirstGAC);
	return STATUS_OK;
}
//End

