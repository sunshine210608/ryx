#ifndef _RISK_MANAGE_H_
#define _RISK_MANAGE_H_
#include "OsTypes.h"
#include "defines.h"

#ifdef __cplusplus
extern "C" {
#endif

tti_bool terminalIsOnlineOnly(RunningTimeEnv *runningTimeEnv);



void terminalRiskManagement(RunningTimeEnv *runningTimeEnv);



tti_uint8 getTerminalActionAnalysis(RunningTimeEnv *runningTimeEnv);



void ProcessingRestrictions(RunningTimeEnv *runningTimeEnv);



tti_int32 CardHolderVerifyMethod(RunningTimeEnv *runningTimeEnv);


tti_int32 firstGenerateAC(RunningTimeEnv *runningTimeEnv, tti_byte resultCode);


tti_int32 secondGenerateAC(RunningTimeEnv *runningTimeEnv, tti_byte resultCode);


void getECBalanceAfterGenerateAC(RunningTimeEnv *runningTimeEnv);


tti_bool isCorrectPrior(tti_byte askCid, tti_byte resultCid);


tti_uint8 getSecondTerminalActionAnalysis(RunningTimeEnv *runningTimeEnv);


tti_bool terminalCanGoOnline(RunningTimeEnv *runningTimeEnv);


tti_bool IsNeedDoECTerminalActionAnalysis(RunningTimeEnv *runningTimeEnv);

tti_uint8 getECTerminalActionAnalysis(RunningTimeEnv *runningTimeEnv);

void SetPinAutoClose(tti_bool autoClose, char *autoPIN);

tti_bool checkQpbocAppBecomeExpiration(RunningTimeEnv *runningTimeEnv);



#ifdef __cplusplus
}
#endif

#endif

