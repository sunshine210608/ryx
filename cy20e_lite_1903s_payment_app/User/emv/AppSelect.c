#include <assert.h>
#include <string.h>
#include <stdio.h>
#include "AppSelect.h"
//#include "SDKApi.h"
#include "EmvAppUi.h"
#include "RespParse.h"
#include "emvDebug.h"
#include "emvl2/defines.h"
#include "VposFace.h"
#include "pub.h"
#include "util.h"


int gGetUserAIDStatus = -1;
int gAIDSelectIndex = -1;

int GetUserSelectAidEvent(int *pDlgIndex)
{
#if 0
	int i;
	int pos;
	int count;
	TEvntMask event;
	TEvntMask ActiveMask = 0;	
/*
	TEvntMask EvntMaskTable[]=
	{
		KEYBORADEVM_MASK_KEY_F1,
		KEYBORADEVM_MASK_KEY_F2,
		KEYBORADEVM_MASK_KEY_F3,
		KEYBORADEVM_MASK_KEY_F4
	};
*/
	TEvntMask EvntMaskTable[]=
	{
		KEYBORADEVM_MASK_KEY_1,
		KEYBORADEVM_MASK_KEY_2,
		KEYBORADEVM_MASK_KEY_3,
		KEYBORADEVM_MASK_KEY_4
	};
	ClearScreen();
	
	if(g_CandidateAidCount - *pDlgIndex *4 >4)
	{
		//DisplayIcon(8, 2,  I_DOWN_ARROW);
		//ActiveMask |= KEYBORADEVM_MASK_KEY_DOWN;
		ActiveMask |= KEYBORADEVM_MASK_KEY_F3;
	}
	
	if(*pDlgIndex >0)
	{
		//DisplayIcon(8, 1,  I_UP_ARROW);
		//ActiveMask |= KEYBORADEVM_MASK_KEY_UP;
		ActiveMask |= KEYBORADEVM_MASK_KEY_F2;
	}

	count =g_CandidateAidCount -*pDlgIndex * 4;

	if(count >4)
	{
		count =4;
	}
	
	for(i=0; i<count; i++)
	{
		pos =*pDlgIndex *4 + i;

		DisplayInfoRight(g_CandidateAidList[pos].DispLabel, 2*(i+1));
		
		ActiveMask |=EvntMaskTable[i];
	}

	ActiveMask |= KEYBORADEVM_MASK_KEY_CANCEL;
	event =WaitAKeyDuringTime(ActiveMask, 60); 

	//if(event ==KEYBORADEVM_MASK_KEY_UP)
	if(event ==KEYBORADEVM_MASK_KEY_F2)
	{
		*pDlgIndex -=1;
		return GetUserSelectAidEvent(pDlgIndex);
	}
	//else if(event == KEYBORADEVM_MASK_KEY_DOWN)
	if(event ==KEYBORADEVM_MASK_KEY_F3)
	{
		*pDlgIndex +=1;
		return GetUserSelectAidEvent(pDlgIndex);
	}
	else
	{
		return event;
	}
#endif
	
	return 0;
}

tti_bool gSelectAidAutoClose = FALSE;
tti_int32 gSelectAidAutoIndex = 0;

void SetSelectAidAutoClose(tti_bool bSelectAidAutoClose, tti_int32 iAutoIndex)
{
    gSelectAidAutoClose = bSelectAidAutoClose;
    gSelectAidAutoIndex = iAutoIndex;
}

#if 0
int32 GetUserSelectAidIndexRaw(int *pIndex)
{
#ifndef CY20_EMVL2_KERNEL
    int index;

    index = CJAVA_ShowAidAndWaitSelect("请选择AID", gSelectAidAutoClose, gSelectAidAutoIndex);
	if (index < 0)
	{
		LOGD("User already Cancel,index = %d", index);
		return STATUS_USER_CANCEL;
	}

    *pIndex = index;

    LOGD("User already select AID = %d", index);
    return STATUS_OK;
#else
	return STATUS_OK;
#endif
}
#else
int32 GetUserSelectAidIndexRaw(int *pIndex)
{
	int index = 0;
	//char szDisplayAid[100];
	unsigned short pressKey;

	if (g_CandidateAidCount > 9)
	{
		LOGE("Error: g_CandidateAidCount = %d, too much Aid\n", g_CandidateAidCount);
		return STATUS_USER_CANCEL;
	}

	ClearScreen();
    vDispCenter(2, "请选择卡内应用:", 0);
	while (index < g_CandidateAidCount)
	{
		vDispVarArg(index + 3, "%d\"%s\"", index + 1, g_CandidateAidList[index].DispLabel);
		index++;
	}
	if (IsAutoTrans() == FALSE)
	{
		while(1)
		{
			pressKey = bPressKey();
			if (pressKey == _KEY_CANCEL)
				return STATUS_USER_CANCEL;
			if (pressKey <= _KEY_9 && pressKey >= _KEY_1 && (pressKey - '0') <= g_CandidateAidCount)
			{
				*pIndex = pressKey - '0' - 1;
				return STATUS_OK;
			}
		}
	}else
	{
		//Auto transaction, select the first one
		*pIndex = 0;
		return STATUS_OK;
	}
}
#endif


int GetUserAIDCount()
{
	return g_CandidateAidCount;
}

int GetUserAIDLable(int Index, char *Label)
{
	if (Index >= g_CandidateAidCount)
	{
		return -1;
	}

	strcpy(Label, g_CandidateAidList[Index].DispLabel);
	return 0;
}


int SetUserAIDStatusToAlreadyGet(int aidSelectIndex)
{
	gGetUserAIDStatus = 0;
	gAIDSelectIndex = aidSelectIndex;

	return 0;
}

int32 GetUserSelectAidIndex(int *pIndex)
{
	int i;
	int32 status;

#ifndef CY20_EMVL2_KERNEL
	assert(g_CandidateAidCount > 0);
#endif

	for(i=0; i<g_CandidateAidCount; i++)
	{
		LOGD("AID %s\n", g_CandidateAidList[i].DispLabel);
	}

	if(g_CandidateAidCount ==1 && 
		g_CandidateAidList[0].IsNeedCardholderConfirm ==0)
	{
		*pIndex =0;
		return STATUS_OK;
	}

	/*
	//Jason, 2010/11/02 Select the first App which one no need cardholder to confirm
	//Add Start
	for (i =0; i < g_CandidateAidCount; i++)
	{
		if (g_CandidateAidList[i].IsNeedCardholderConfirm ==0)
		{
			*pIndex =i;
			return STATUS_OK;
		}
	}
	return STATUS_USER_CANCEL;
	//Janson Change End.
	*/
	
	status =GetUserSelectAidIndexRaw(pIndex);
	ClearScreen();

	return status;
}
/*
int TestSelectAID(int *pIndex)
{
	if(GetUserSelectAidIndex(pIndex) != STATUS_OK)
	{
		LOGD("User cancel it");
		return STATUS_FAIL;
	}

	if(*pIndex >=0)
	{		
		LOGD("Selected AID %s", g_CandidateAidList[*pIndex].DispLabel);
	}
	
	return STATUS_OK;
}
*/
void RemoveAid(int index)
{
	int i;

	for(i=index; i< g_CandidateAidCount-1; i++)
	{
		memcpy(&g_CandidateAidList[i],&g_CandidateAidList[i+1], sizeof(g_CandidateAidList[0]));
	}

	g_CandidateAidCount -=1;
}
/*
void TestAppSelect()
{
	int i;
	int Index;
	
	g_CandidateAidCount = 9;

	for(i=0; i<g_CandidateAidCount; i++)
	{
		sprintf(g_CandidateAidList[i].DispLabel, "AID%02d",i);
		g_CandidateAidList[i].IsNeedCardholderConfirm =0;
	}
	
	while(1)
	{
        if(g_CandidateAidCount <= 0)
        {
            break;
        }

		if(TestSelectAID(&Index) != STATUS_OK)
		{
			break;
		}
		
		RemoveAid(Index);
	}
	
}
*/
