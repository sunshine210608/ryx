#ifndef INCLUDE_DATA_TRANS_H
#define INCLUDE_DATA_TRANS_H

#include "Define.h"

int32 TransData(byte SendBuf[], uint16 SendBufLen, byte RecvBuf[], uint16 *RecvBufLen);

#endif

