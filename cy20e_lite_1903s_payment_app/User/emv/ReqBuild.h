#ifndef INCLUDE_REQ_BUILD_H
#define INCLUDE_REQ_BUILD_H

#include "Define.h"

int32 BuildLoadConfigReqData(char buf[], uint16 *size);
int32 BuildGetAidListReqData(char buf[], uint16 *size);
int32 BuildEmvTransReqData(char buf[], uint16 *size);
int32 BuildEmvCompleteReqData(char buf[], uint16 *size);
int32 BuildInitTransReqData(char buf[], uint16 *size);
int32 BuildEndTransReqData(char buf[], uint16 *size);
#endif
