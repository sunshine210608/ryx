#ifndef INCLUDE_UTILS_H
#define INCLUDE_UTILS_H

#include "Define.h"
#include "OsTypes.h"

void PrintByteArray( const byte *buf, uint32 bufsize);
void GetCurrentDateAndTime(byte *pDate, byte *pTime);
void SetCurrentDateAndTime(byte *pDate, byte *pTime);

void PrintCharArray(const byte buf[], uint32 len);
void Bin2HexStr(const char src[], int len, char *dest);
int32 HexStr2Bin(char *to, uint32 size, const unsigned char *p, uint32 len);
//void dbg(const char *fmt, ...);
int SaveOptValue(const char *opt, const char *value);
int GetOptValue(const char *opt, char value[], int size);
int SetSystemDate(int year, int month, int day);
void GetSystemDate(int *year, int *month, int *day);

int SetSystemTime(int hour, int min, int second);
void GetSystemTime(int *hour, int *min, int *second);

int SetSysDate(const char *date);
char *GetSysDate(void);
int SetSysTime(const char *time);
char *GetSysTime(void);
char *GetConfFilePath(const char *filename);
void PrintItemName(const char *name);

void hexdump(byte *buf,int len);
void hexdumpEx(char *prompt, byte *buf, int len);


int SetServerIPAndPort(char *szServerIP, char *szServerPort);
int GetServerIPAndPort(char *szServerIP, char *szServerPort);

void SetKernelType(int KernelType);

int GetKernelType(void);

void SetForceOnline(tti_bool bForceOnline);

tti_bool IsForceOnline(void);

void TranslateTransTypeToString(char *transType, char *transString);

void hexdumpraw(byte *buf,int len);

tti_bool IsAutoTrans(void);

tti_int16 GetTransCurrencyCode(tti_byte *byTransCurrencyCode);



#endif
