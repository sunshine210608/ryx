//
// Created by jay on 2017/3/19.
//
//#include <fcntl.h>
#include "emvDebug.h"
#include "err.h"
#include "wakeupmcu.h"
#include "define.h"

int WakeUpMCU(void)
{
#ifndef CY20_EMVL2_KERNEL	 
	int fd;
    if((fd=open(WAKEUP_PIN_DEV,O_RDWR))<0){
        //LOGE("Open %s failed.",WAKEUP_PIN_DEV);
        return ERR_OPENUART;
    }

    if( write(fd,WAKEUP_PIN_LOW, sizeof(WAKEUP_PIN_LOW)) != sizeof(WAKEUP_PIN_LOW))
    {
        LOGE("%s Error 2",__FUNCTION__);
        return ERR_WRITEUART;
    }
    //TODO maybe we can reduce this value again.
    usleep(100);
    if( write(fd,WAKEUP_PIN_HIGH, sizeof(WAKEUP_PIN_HIGH)) != sizeof(WAKEUP_PIN_HIGH))
    {
        LOGE("%s Error 3",__FUNCTION__);
        return ERR_WRITEUART;
    }
    close(fd);
    //LOGD("%s Done",__FUNCTION__);
    return ERR_NONE;
#else
		return ERR_NONE;
#endif
}

int ReadCTS(void)
{
#ifndef CY20_EMVL2_KERNEL	
    int fd;
    unsigned char buf[4];
    if((fd=open(CTS_PIN_DEV,O_RDONLY))<0){
        LOGE("Open %s failed.",CTS_PIN_DEV);
        return -ERR_OPENUART;
    }

    if( read(fd,buf,4)<0)
    {
        LOGE("%s Error 2",__FUNCTION__);
        return -ERR_WRITEUART;
    }

    close(fd);
    LOGD("%s Done",__FUNCTION__);
    return (int)buf[0];
#else
		return 0;
#endif
}
