
#ifndef INCLUDE_REQ_DATA_ITEM_H
#define INCLUDE_REQ_DATA_ITEM_H

#include "Define.h"

extern ReqDataItem_t g_ReqDataList[REQ_ITEM_COUNT];

DataItemAttr_t *GetRequestItemAttr(uint16 index);

int32 InitRequestItemList(void);
int32 UninitRequestItemList(void);
int32 SetRequestItem(uint16 index, const byte buf[], uint16 len);

int32 SetRequestItemStr(uint16 index, const char *str);

void ResetRequestItemList(void);

int32 ResetRequestItem(uint16 index);

vec_t *GetRequestItemValue(uint16 index);

int32 IsRequestItemExisted(uint16 index);

int32 SetRequestItemInt(uint16 index, uint32 value);

void PrintRequestItemList(void);

int IsInquiryTransaction(void);

int IsReadLogTransaction(void);

#endif

