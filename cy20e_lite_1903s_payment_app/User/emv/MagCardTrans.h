#ifndef __MAGCARDTRANS_H__
#define __MAGCARDTRANS_H__

#include "Define.h"

typedef struct 
{
	char Track1[128];
	char Track2[128];

	char PAN[36];
	char ServiceCode[4];

	byte IsHasReadMagCard;
}MagCardTransData_t;

extern MagCardTransData_t g_MagCardTransData;

void DoPartMagCardTransaction(char *transType);

int32 GetMagicCardData(void);

void DoFullMagCardTransaction(char *transType);

int ParseTrack2Data(const char *TrackData,char *Pan, char ServiceCode[]);

#endif

