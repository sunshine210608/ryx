#include "EmvAppUi.h"
#include "MultiLang.h"

#if 0
int g_PrefferLang;

typedef struct tag_MultiLangTable
{	
	int id;
	char *english;
	char *french;
	char *chinese;
}SMultiLangTable;

SMultiLangTable g_MultiLangTable[]=
{
	{MLSID_APPROVED,	"Approved", 	"Approuve", "\xBD\xD3\xCA\xDC"},
	{MLSID_DECLINED,	"Declined", 	"Refusee", "\xBE\xDC\xBE\xF8"},
	{MLSID_APPROVED_1, "1.Approved", "1.Approuve", "1.\xBD\xD3\xCA\xDC"},
	{MLSID_DECLINED_2,	"2.Declined", 	"2.Refusee", "2.\xBE\xDC\xBE\xF8"},		
};

void MultiLangDisplayRaw(int id, int line, int align)
{
	switch(g_PrefferLang)
	{
		case LANG_ENGLISH:
			if (align == DISPLAY_CENTER_ALIGN)
			{
				//LCD_DispLine(line, 8, g_MultiLangTable[id].chinese, strlen(g_MultiLangTable[id].chinese), 0);
			}else
			{
				//LCD_DispLine(line, 16, g_MultiLangTable[id].chinese, strlen(g_MultiLangTable[id].chinese), 0);
			}
			DisplayStringInFormat(g_MultiLangTable[id].english, (tti_uint8)(line - 1), 
			(tti_uint8)0, (tti_uint8)20, align, DISPLAY_IN_NORMAL);
			break;
			
		case LANG_FRENCH:
			DisplayStringInFormat(g_MultiLangTable[id].french, (tti_uint8)(line - 1), 
			(tti_uint8)0, (tti_uint8)20, align, DISPLAY_IN_NORMAL);
			break;
			
		default:
			DisplayStringInFormat(g_MultiLangTable[id].english, (tti_uint8)(line - 1), 
			(tti_uint8)0, (tti_uint8)20, align, DISPLAY_IN_NORMAL);
			break;
	}
}

void MultiLangDisp(int id, int line)
{
	MultiLangDisplayRaw(id, line, DISPLAY_LEFT_ALIGN);
}

void MultiLangDispCenter(int id, int line)
{
	MultiLangDisplayRaw(id, line, DISPLAY_CENTER_ALIGN);
}

void MultiLangDispRight(int id, int line)
{
	MultiLangDisplayRaw(id, line, DISPLAY_RIGHT_ALIGN);
}
#endif
