#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sys_littlefs.h"
#include "Receipt.h"
#include "emvDebug.h"
#include "util.h"
#include "Transaction.h"
#include "EmvAppUi.h"
#include "ReqDataItem.h"
#include "RespDataItem.h"
#include "ReqDataSet.h"
#include "MagCardTrans.h"
#include "online.h"
#include "emvl2/tag.h"
#include "emvl2/defines.h"
#include "emvl2/utils.h"

MagCardTransData_t g_MagCardTransData;

void ForamtTrack2Data(tti_tchar *track2, tti_byte *formatTrack2, tti_int32 *len)
{
	tti_tchar tmp[256] = {0};
	tti_tchar tmp2[256] = {0};
	tti_tchar tmp3[256] = {0};
	tti_tchar tmp4[256] = {0};
	
	tti_tchar *p;
	
	if (strlen(track2) % 2 != 0)
	{
		sprintf(tmp, "%sF", track2);
	}else
	{
		sprintf(tmp, "%s", track2);
	}

	p = strstr(tmp, "=");
	if (p != NULL)
	{
		strncpy(tmp3, p + 1, strlen(p) - 1 );
		strncpy(tmp2, tmp, strlen(tmp) - strlen(p));
		sprintf(tmp4, "%sD%s", tmp2, tmp3);
	}else
	{
		sprintf(tmp4, "%s", tmp);
	}

	TranslateStringToByteArray(tmp4, formatTrack2, len);
	//hexdumpEx("formatTrack2 data list", formatTrack2, *len);

	return;
}

void BuildMagCardTLVData(byte *TLVData, uint16 *TLVLen)
{
	uint16 totalLen = 0;
	char tmpAscii[1024] = {0}; 
	tti_byte tmpHex[1024] = {0};
	tti_int32 tmpHexLen;
	int32 tmpInt32Data;
	vec_t *pData;

	TLVData[0] = 0xDF;
	TLVData[1] = 0x81;
	TLVData[2] = 0x07;
	ForamtTrack2Data(g_MagCardTransData.Track2, tmpHex, &tmpHexLen);
	TLVData[3] = tmpHexLen;
	memcpy(TLVData + 4, tmpHex, TLVData[3]);
	totalLen = 4 + TLVData[3];

	if (IsRequestItemExisted(REQ_POS_ENTRY_MODE_CODE) == TRUE)
	{
		memset(tmpAscii, 0, sizeof(tmpAscii));
		pData= GetRequestItemValue(REQ_POS_ENTRY_MODE_CODE);
		memcpy(tmpAscii, pData->ptr, pData->len);
		TranslateStringToByteArray(tmpAscii, tmpHex, &tmpHexLen);
		
		TLVData[totalLen] = 0x9F;
		TLVData[totalLen + 1] = 0x39;
		TLVData[totalLen + 2] = 0x01;
		TLVData[totalLen + 3] = tmpHex[0];
		totalLen += 4;
	}
	
	if (IsRequestItemExisted(REQ_AMOUNT_AUTHOR) == TRUE)
	{
		tmpInt32Data = EMVL2_GetAmount() + EMVL2_GetAmountCash();
		//LOGD("tmpInt32Data = %d", tmpInt32Data);
		castAmountToBCDArray(tmpInt32Data, tmpHex);
		
		TLVData[totalLen] = 0x9F;
		TLVData[totalLen + 1] = 0x02;
		TLVData[totalLen + 2] = 0x06;
		memcpy(TLVData + totalLen + 3, tmpHex, 6);
		totalLen += 3 + 6;
	}
	
	*TLVLen = totalLen;
}

int GetMagCardTransAuthRequest(byte *buf, uint16 *bufLen)
{
	uint16 tmpLen;
	//uint16 totalLen = 0;

	BuildMagCardTLVData(buf + 4, &tmpLen);
	//hexdumpEx("BuildMagCardTLVData list", buf + 4,  tmpLen);

	buf[0] = STX;
    buf[1] = 0x42;
    buf[2] = tmpLen/256;
    buf[3] = tmpLen%256;

	*bufLen = tmpLen + 4;

    return STATUS_OK;
}

int32 ParseMagCardAuthHostData(byte buf[], uint16 size)
{
	uint16 dataLen;
	TagList tmpList;
	int32 ret;
	
	if (size < 4)
	{
		return STATUS_FAIL;
	}

	if (buf[0] != STX || buf[1] != 0x02)
	{
		return STATUS_FAIL;
	}

	dataLen = buf[2] * 256 + buf[3];
	if ((dataLen + 4) != size)
	{
		return STATUS_FAIL;
	}

	InitTagList(&tmpList);

	ret = BuildTagListOneLevel(buf + 4, size - 4, &tmpList);
	if (ret != STATUS_OK)
	{
		FreeTagList(&tmpList);
		LOGE("BuildTagListOneLevel ERR");
		return STATUS_FAIL;
	}
	PrintOutTagList(&tmpList, "Parse Host Data");

	if (TagIsExisted(&tmpList, TAG_RESPONSE_CODE) == FALSE || 
		TagIsDuplicate(&tmpList, TAG_RESPONSE_CODE) == TRUE ||
		GetTagValueSize(&tmpList, TAG_RESPONSE_CODE) != 2)
	{
		FreeTagList(&tmpList);
		LOGE("TAG RESPONSE CODE NOT EXIST OR Duplicate or size is not 2");
		return STATUS_FAIL;
	}

	SetRequestItem(REQ_ARC, GetTagValue(&tmpList, TAG_RESPONSE_CODE), GetTagValueSize(&tmpList, TAG_RESPONSE_CODE));	
	FreeTagList(&tmpList);

	return STATUS_OK;
}

int32 MagCardCommWithHost()
{
	byte SendBuf[MAX_COMM_HOST_SEND_BUF_SIZE];
	byte RecvBuf[MAX_COMM_HOST_RECV_BUF_SIZE];
	uint16 SendBufLen = sizeof(SendBuf);
	uint16 RecvBufLen = sizeof(RecvBuf);

	if(GetMagCardTransAuthRequest(SendBuf, &SendBufLen) != STATUS_OK)
	{
        LOGE("Get Auth Request Data Fail.");
		return STATUS_FAIL;
	}
	LOGD("GetMagCardTransAuthRequest SendBuf ");
	hexdump(SendBuf, SendBufLen);

	if(SendAndReceive(SendBuf, SendBufLen, RecvBuf, &RecvBufLen) != STATUS_OK)
	{
        LOGE("Send And Recvive Fail.");
		return STATUS_FAIL;
	}
	
	LOGD("ParseMagCardAuthHostData RecvBuf");
	hexdump(RecvBuf, RecvBufLen);

	if(ParseMagCardAuthHostData(RecvBuf, RecvBufLen) != STATUS_OK)
	{
        LOGE("Parse Host Data Fail.");

		return STATUS_FAIL;
	}

	if(!IsRequestItemExisted(REQ_ARC))
	{
        LOGE("Transaction Response Code Not Exsit.");

		return STATUS_FAIL;
	}

	return STATUS_OK;
}

void showMagCardResult(void)
{
	vec_t *pData;
	
	if(!IsRequestItemExisted(REQ_ARC))
	{
		LOGE("Transaction Response Code Not Exsit.");

		DisplayValidateInfo("Terminated终止");
		return;
	}
	pData = GetRequestItemValue(REQ_ARC);
	if (memcmp(pData->ptr, "00", 2) == 0)
	{
        SetResponseItemStr(RESP_EMV_RESULT_CODE, "0");
		DisplayValidateInfo("Approved接受");
		PrintReceipt();
	}else
	{
		SetResponseItemStr(RESP_EMV_RESULT_CODE, "2");
		DisplayValidateInfo("Declined拒绝");
	}
	return;
}

int32 GetMagCardTransResultRequest(byte *buf, uint16 *bufLen)
{	
	tti_int32 totalLen;
	byte transactionResult;
	vec_t *pData;
	
	if(!IsRequestItemExisted(REQ_ARC))
	{
		//LOGE("Transaction Response Code Not Exsit.");

		//return STATUS_FAIL;
		transactionResult = 0x01;
	}
	else
	{
		pData = GetRequestItemValue(REQ_ARC);
		if (memcmp(pData->ptr, "00", 2) == 0)
		{
			transactionResult = 0x01;
		}else
		{
			transactionResult = 0x02;
		}
	}
	totalLen = 4;	
	buf[4] = 0x03;
	buf[5] = 0x01;
	buf[6] = transactionResult;
	totalLen += 3;

	buf[0] = STX;
    buf[1] = 0xc1;
    buf[2] = (totalLen - 4)/256;
    buf[3] = (totalLen - 4)%256;

	*bufLen = totalLen;

	return STATUS_OK;
}


int32 MagCardTransResultCommWithHost()
{
	byte SendBuf[MAX_COMM_HOST_SEND_BUF_SIZE];
	byte RecvBuf[MAX_COMM_HOST_RECV_BUF_SIZE];
	uint16 SendBufLen = sizeof(SendBuf);
	uint16 RecvBufLen = sizeof(RecvBuf);

	DisplayProcess("交易结果报文处理中");

	if(GetMagCardTransResultRequest(SendBuf, &SendBufLen) != STATUS_OK)
	{
        LOGE("GetTransactionResultBCTC Fail.");
		return STATUS_FAIL;
	}
	LOGD("GetTransactionResultBCTC SendBuf ");
	hexdump(SendBuf, SendBufLen);

	if(SendAndReceive(SendBuf, SendBufLen, RecvBuf, &RecvBufLen) != STATUS_OK)
	{
        LOGE("Send And Recvive Fail.");
		return STATUS_FAIL;
	}

	if(ParseTransResultResponse(RecvBuf, RecvBufLen) != STATUS_OK)
	{
        LOGE("ParseTransResultResponse Fail.");

		return STATUS_FAIL;
	}

	return STATUS_OK;
}


void GetMagCardTransBatchRequest(byte *buf, uint16 *bufLen)
{
	uint16 tmpLen;

	BuildMagCardTLVData(buf + 4, &tmpLen);
	//hexdumpEx("BuildMagCardTLVData list", buf + 4,  tmpLen);

	buf[0] = STX;
	buf[1] = 0x44;
	buf[2] = tmpLen/256;
	buf[3] = tmpLen%256;

	*bufLen = tmpLen + 4;
}

char *GetValidBatchRecordFileName(void);

int SaveMagCardTransBatchDetail(void)
{
#ifndef CY20_EMVL2_KERNEL	
	FILE *fp;
	byte batchCaptureAuth[1024];
	uint16 bufLen;
	
	fp = fopen(GetValidBatchRecordFileName(), "w");	
	if (fp == NULL)
	{
		DisplayValidateInfo("Save Detail Fail");
		return -1;
	}

	GetMagCardTransBatchRequest(batchCaptureAuth, &bufLen);
	LOGD("bufLen = %d", bufLen);

	if (fwrite(batchCaptureAuth, bufLen, 1, fp) != 1)
	{
		fclose(fp);
		LOGE("fwrite error");
		return -1;
	}

	fclose(fp);
	return 0;
#else
  	lfs_file_t fp;
	byte batchCaptureAuth[1024];
	uint16 bufLen;
	int ret;
	
	ret = sys_lfs_file_open(&fp, GetValidBatchRecordFileName(), LFS_O_CREAT|LFS_O_WRONLY);	
	if (ret != 0)
	{
		DisplayValidateInfo("Save Detail Fail");
		return -1;
	}

	GetMagCardTransBatchRequest(batchCaptureAuth, &bufLen);
	LOGD("bufLen = %d", bufLen);

	ret = sys_lfs_file_write(&fp, batchCaptureAuth, bufLen);
	sys_lfs_file_close(&fp);
	if (ret != bufLen)
	{
		LOGE("fwrite error");
		return -1;
	}

	return 0;
#endif
}

void DoFullMagCardTransaction(char *transType)
{
	DisplayProcess("处理中");

	ResetRequestItemList();	
	InitResponseItemList();


	SetReqItemTransDateTime();
	SetReqItemTransSeqCounter();
	SetRequestItemStr(REQ_TRANS_TYPE, transType);	
	SetResponseItemStr(RESP_PAN,	g_MagCardTransData.PAN);	
	SetResponseItemStr(RESP_SERVICE_CODE, g_MagCardTransData.ServiceCode);
	SetResponseItemStr(RESP_TRACK1, g_MagCardTransData.Track1);
	SetResponseItemStr(RESP_TRACK2, g_MagCardTransData.Track2);
	SetRequestItemStr(REQ_POS_ENTRY_MODE_CODE, "02");
	SeReqItemAmount();
	PrintRequestItemList();
	//Jason added on 2012.04.23
	//SetResponseItemStr(RESP_EMV_RESULT_CODE, "0");
	//DisplayValidateInfo("Approved");
	MagCardCommWithHost();
	showMagCardResult();
	MagCardTransResultCommWithHost();
	//PrintReceipt();
	//SaveDetail();
	//SaveMagCardTransBatchDetail();
}

void DoPartMagCardTransaction(char *transType)
{	
	DisplayProcess("处理中");
	ResetRequestItemList();	
	InitResponseItemList();

	SetReqItemTransDateTime();
	SetReqItemTransSeqCounter();
	SetRequestItemStr(REQ_TRANS_TYPE, transType);	
	SetResponseItemStr(RESP_PAN,	g_MagCardTransData.PAN);	
	SetResponseItemStr(RESP_SERVICE_CODE, g_MagCardTransData.ServiceCode);
	SetResponseItemStr(RESP_TRACK1, g_MagCardTransData.Track1);
	SetResponseItemStr(RESP_TRACK2, g_MagCardTransData.Track2);
	SetRequestItemStr(REQ_POS_ENTRY_MODE_CODE, "03");
	SeReqItemAmount();
	PrintRequestItemList();
	//Jason added on 2012.04.23
    MagCardCommWithHost();
    showMagCardResult();
    MagCardTransResultCommWithHost();
	//SetResponseItemStr(RESP_EMV_RESULT_CODE, "0");
	//DisplayValidateInfo("Approved");
	//PrintReceipt();
	//SaveDetail();
	//SaveMagCardTransBatchDetail();
}

/*
int ParseTrack1Data(const char *Track1,char *Pan, char ServiceCode[])
{
	char *p;
	uint8 PanLen;
	p =strchr(Track1, '^');

	if(p == NULL)
	{
		return STATUS_FAIL;
	}

	PanLen = p -Track1 -1;
	memcpy(Pan, Track1 +1, PanLen);
	Pan[PanLen] ='\0';
	
	p +=1;
	p = strchr(p+1, '^');
	if(p == NULL)
	{
		return STATUS_FAIL;
	}

	p +=1;

	if(strlen(p)< 7)
	{
		return STATUS_FAIL;
	}

	memcpy(ServiceCode, p+4, 3);
	
	return STATUS_OK;
}
*/
	
	
int ParseTrack2Data(const char *TrackData,char *Pan, char ServiceCode[])
{
	char *p;
	uint8 PanLen;
	p =strchr(TrackData, '=');

	if(p == NULL)
	{
		LOGD("ParseTrack2Data err, p == NULL\n");
		return STATUS_FAIL;
	}

	PanLen = p -TrackData;
	memcpy(Pan, TrackData, PanLen);
	Pan[PanLen] ='\0';

	memcpy(ServiceCode, p+5, 3);
	
	return STATUS_OK;
}



