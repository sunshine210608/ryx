//#include <jni.h>
#include <stdint.h>
#include <stdio.h>
//#include <memory.h>
//#include <unistd.h>
#include "serial.h"
#include "emvDebug.h"
#include "protocol.h"
#include "resetmcu.h"
#include "Transaction.h"
#include "AppSelect.h"
#include "Define.h"
#include "Transaction.h"
#include "emvl2/customize_emvl2.h"
#include "emvl2/riskmanage.h"
#include "Receipt.h"
#include "Util.h"
#include "ParamDown.h"
#include "EmvAppUi.h"
#include "emvl2/transflow.h"

#ifndef CY20_EMVL2_KERNEL
int AutoTransaction(char *autoAmount, char *autoAmountOther, unsigned char *transactionType);

void DownloadParam(void);

void PrintParam(void);

void Transaction(const char *TransType);

void TestAppSelect(void);

void BatchDataCaptureBCTC(void);

int32 DoReadLogTransaction(void);


JNIEnv *genv;
jobject ginstance;


JNIEXPORT jint JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_wait_1security_1event(JNIEnv *env, jobject instance,
                                                                   jbyteArray status_,
                                                                   jbyteArray data_,
                                                                   jintArray datalen_) {
    jbyte *status = (*env)->GetByteArrayElements(env, status_, NULL);
    jbyte *data = (*env)->GetByteArrayElements(env, data_, NULL);
    jint *datalen = (*env)->GetIntArrayElements(env, datalen_, NULL);

    int ret;
    ret=wait_security_event(status,data,datalen);
    if(ret == 0 )
    {
        (*env)->SetByteArrayRegion(env,status_,0,1,status);
        (*env)->SetByteArrayRegion(env,data_,0,*datalen,data);
        (*env)->SetIntArrayRegion(env,datalen_,0,1,datalen);
    }

    (*env)->ReleaseByteArrayElements(env, status_, status, 0);
    (*env)->ReleaseByteArrayElements(env, data_, data, 0);
    (*env)->ReleaseIntArrayElements(env, datalen_, datalen, 0);
    return ret;
}

JNIEXPORT jint JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_icc_1transaction(JNIEnv *env, jobject instance,
                                                              jbyteArray status_, jbyteArray data_,
                                                              jintArray datalen_) {
    jbyte *status = (*env)->GetByteArrayElements(env, status_, NULL);
    jbyte *data = (*env)->GetByteArrayElements(env, data_, NULL);
    jint *datalen = (*env)->GetIntArrayElements(env, datalen_, NULL);

    // TODO
    int ret;
    ret=icc_transaction(status,data,datalen);
    if(ret == 0 )
    {
        (*env)->SetByteArrayRegion(env,status_,0,1,status);
        (*env)->SetByteArrayRegion(env,data_,0,*datalen,data);
        (*env)->SetIntArrayRegion(env,datalen_,0,1,datalen);
    }

    (*env)->ReleaseByteArrayElements(env, status_, status, 0);
    (*env)->ReleaseByteArrayElements(env, data_, data, 0);
    (*env)->ReleaseIntArrayElements(env, datalen_, datalen, 0);

    return ret;
}

JNIEXPORT jint JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_ResetMCU(JNIEnv *env, jobject instance) {
    return ResetMCU();
}

JNIEXPORT jint JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_wait_1msr_1event(JNIEnv *env, jobject instance,
                                                              jbyteArray status_, jbyteArray data_,
                                                              jintArray datalen_) {
    jbyte *status = (*env)->GetByteArrayElements(env, status_, NULL);
    jbyte *data = (*env)->GetByteArrayElements(env, data_, NULL);
    jint *datalen = (*env)->GetIntArrayElements(env, datalen_, NULL);

    // TODO
    int ret;
    ret=wait_msr_event(status,data,datalen);
    if(ret == 0 )
    {
        (*env)->SetByteArrayRegion(env,status_,0,1,status);
        (*env)->SetByteArrayRegion(env,data_,0,*datalen,data);
        (*env)->SetIntArrayRegion(env,datalen_,0,1,datalen);
    }

    (*env)->ReleaseByteArrayElements(env, status_, status, 0);
    (*env)->ReleaseByteArrayElements(env, data_, data, 0);
    (*env)->ReleaseIntArrayElements(env, datalen_, datalen, 0);

    return ret;
}

JNIEXPORT jint JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_nfc_1detect(JNIEnv *env, jobject instance,
                                                         jbyteArray status_) {
    jbyte *status = (*env)->GetByteArrayElements(env, status_, NULL);

    // TODO
    int ret;
    ret=nfc_detect(status);
    if(ret == 0 )
    {
        (*env)->SetByteArrayRegion(env,status_,0,1,status);
    }

    (*env)->ReleaseByteArrayElements(env, status_, status, 0);

    return ret;
}

JNIEXPORT jint JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_nfc_1deselect(JNIEnv *env, jobject instance,
                                                           jbyteArray status_) {
    jbyte *status = (*env)->GetByteArrayElements(env, status_, NULL);

    // TODO
    int ret;
    ret=nfc_deselect(status);
    if(ret == 0)
    {
        (*env)->SetByteArrayRegion(env,status_,0,1,status);
    }

    (*env)->ReleaseByteArrayElements(env, status_, status, 0);

    return ret;
}

JNIEXPORT jint JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_nfc_1apdu(JNIEnv *env, jobject instance,
                                                       jbyteArray status_, jbyteArray capdudata_,
                                                       jint capdudatalen, jbyteArray rapdudata_,
                                                       jintArray rapdudatalen_) {
    jbyte *status = (*env)->GetByteArrayElements(env, status_, NULL);
    jbyte *capdudata = (*env)->GetByteArrayElements(env, capdudata_, NULL);
    jbyte *rapdudata = (*env)->GetByteArrayElements(env, rapdudata_, NULL);
    jint *rapdudatalen = (*env)->GetIntArrayElements(env, rapdudatalen_, NULL);

    // TODO
    int ret;
    ret=nfc_apdu(status, capdudata, capdudatalen, rapdudata, rapdudatalen);
    if(ret == 0 )
    {
        (*env)->SetByteArrayRegion(env,status_,0,1,status);
        (*env)->SetByteArrayRegion(env,rapdudata_,0,*rapdudatalen,rapdudata);
        (*env)->SetIntArrayRegion(env,rapdudatalen_,0,1,rapdudatalen);
    }

    (*env)->ReleaseByteArrayElements(env, status_, status, 0);
    (*env)->ReleaseByteArrayElements(env, capdudata_, capdudata, 0);
    (*env)->ReleaseByteArrayElements(env, rapdudata_, rapdudata, 0);
    (*env)->ReleaseIntArrayElements(env, rapdudatalen_, rapdudatalen, 0);

    return ret;
}



int CJAVA_EnterPIN(char *title, char *enterPin, unsigned char autoClose, char * autoPin)
{
    jclass   jclazz = (*genv)->FindClass(genv,"com/szsitc/jay/sitc_emvl2/MainActivity");
//2.得到方法
//    jmethodID   (*GetMethodID)(JNIEnv*, jclass, const char*, const char*);
    jmethodID jmethodid1 = (*genv)->GetMethodID(genv,jclazz,"showAndWaitDialogPINEnter",
                                               "(Ljava/lang/String;ZLjava/lang/String;Z)Ljava/lang/String;");

//		jmethodID jmethodid = (*genv)->GetMethodID(genv,jclazz,"sampleTest",
//                                               "()Ljava/lang/String;");

    //jmethodID jmethodid = (*env)->GetMethodID(env,jclazz,"showToast","()V");
//3.得到对象
    jobject jobjs1 = (*genv)->AllocObject(genv,jclazz);
//4.调用方法

    jstring strTitle = (*genv)->NewStringUTF(genv, title);
    jstring strAutoPin = (*genv)->NewStringUTF(genv, autoPin);
    int ret;

    jboolean bAutoClose;
    if (autoClose == 0)
    {
        bAutoClose = FALSE;
    }else
    {
        bAutoClose = TRUE;
    }

    //jstring strEnterPin = (*genv)->CallObjectMethod(genv,ginstance,jmethodid,strTitle, bAutoCancel, strAutoPin, FALSE, status);
    //LOGD("Before CallObjectMethod");


	jstring strEnterPin = (*genv)->CallObjectMethod(genv,ginstance,jmethodid1, strTitle, bAutoClose, strAutoPin, TRUE);
   //jstring strEnterPin = (*genv)->CallObjectMethod(genv,ginstance,jmethodid);
	//LOGD("After CallObjectMethod");
    if (strEnterPin != NULL) {
        const char *tmpEnterPin = (*genv)->GetStringUTFChars(genv, strEnterPin, 0);
        switch (tmpEnterPin[0])
        {
            case '0':
                //Press ok and get pin
                ret = tmpEnterPin[0] - '0';
                strcpy(enterPin, tmpEnterPin + 1);
                break;

            case '1':
                //Cancel
                ret = tmpEnterPin[0] - '0';
                enterPin[0] = '\0';
                break;

            case '2':
                //By pass
                ret = tmpEnterPin[0] - '0';
                enterPin[0] = '\0';
                break;

            default:
                ret = -1;
                break;
        }

        (*genv)->ReleaseStringUTFChars(genv, strEnterPin, tmpEnterPin);
    } else{
		LOGE("strEnterPin = NULL");
        ret = -1;
    }

    (*genv)->DeleteLocalRef (genv, strTitle);
    (*genv)->DeleteLocalRef (genv, strAutoPin);

    return ret;
}


int CJAVA_ShowAidAndWaitSelect(char *title, unsigned char autoClose, int autoChoiceIndex)
{
    jclass   jclazz = (*genv)->FindClass(genv,"com/szsitc/jay/sitc_emvl2/MainActivity");
//2.得到方法
//    jmethodID   (*GetMethodID)(JNIEnv*, jclass, const char*, const char*);
//    jmethodID jmethodid = (*genv)->GetMethodID(genv,jclazz,"showAidAndWaitSelect",
//                                               "(Ljava/lang/String;ZI)I");
		
	jmethodID jmethodid = (*genv)->GetMethodID(genv,jclazz,"showAidAndWaitSelectWithCancel",
                                               "(Ljava/lang/String;ZI)I");
//3.得到对象
    jobject jobjs = (*genv)->AllocObject(genv,jclazz);
//4.调用方法

    jstring strTitle = (*genv)->NewStringUTF(genv, title);
    jint jAutoChoiceIndex = autoChoiceIndex;
    int selectIndex;

    jboolean bAutoClose;
    if (autoClose == 0)
    {
        bAutoClose = FALSE;
    }else
    {
        bAutoClose = TRUE;
    }

    //jstring strEnterPin = (*genv)->CallObjectMethod(genv,ginstance,jmethodid,strTitle, bAutoCancel, strAutoPin, FALSE, status);
    //LOGD("Before CallObjectMethod");


    selectIndex = (*genv)->CallIntMethod(genv,ginstance,jmethodid, strTitle, bAutoClose, jAutoChoiceIndex);
    (*genv)->DeleteLocalRef (genv, strTitle);

    LOGD("Select AIP Index = %d", selectIndex);
    return selectIndex;
}

int CJAVA_ShowYesNoAndWaitSelect(char *title, unsigned char autoClose, int autoChoiceIndex)
{
    jclass   jclazz = (*genv)->FindClass(genv,"com/szsitc/jay/sitc_emvl2/MainActivity");
//2.得到方法
//    jmethodID   (*GetMethodID)(JNIEnv*, jclass, const char*, const char*);
    jmethodID jmethodid = (*genv)->GetMethodID(genv,jclazz,"showYesNoAndWaitSelect",
                                               "(Ljava/lang/String;ZI)I");
//3.得到对象
    jobject jobjs = (*genv)->AllocObject(genv,jclazz);
//4.调用方法

    jstring strTitle = (*genv)->NewStringUTF(genv, title);
    jint jAutoChoiceIndex = autoChoiceIndex;
    int selectIndex;

    jboolean bAutoClose;
    if (autoClose == 0)
    {
        bAutoClose = FALSE;
    }else
    {
        bAutoClose = TRUE;
    }

    selectIndex = (*genv)->CallIntMethod(genv,ginstance,jmethodid, strTitle, bAutoClose, jAutoChoiceIndex);
    (*genv)->DeleteLocalRef (genv, strTitle);

    LOGD("Select Index = %d", selectIndex);
	LOGD("0 is Yes, 1 is No");
    return selectIndex;
}

int CJAVA_ShowApproveDeclineAndWaitSelect(char *title, unsigned char autoClose, int autoChoiceIndex)
{
    jclass   jclazz = (*genv)->FindClass(genv,"com/szsitc/jay/sitc_emvl2/MainActivity");
//2.得到方法
//    jmethodID   (*GetMethodID)(JNIEnv*, jclass, const char*, const char*);
    jmethodID jmethodid = (*genv)->GetMethodID(genv,jclazz,"showApproveDeclineAndWaitSelect",
                                               "(Ljava/lang/String;ZI)I");
//3.得到对象
    jobject jobjs = (*genv)->AllocObject(genv,jclazz);
//4.调用方法

    jstring strTitle = (*genv)->NewStringUTF(genv, title);
    jint jAutoChoiceIndex = autoChoiceIndex;
    int selectIndex;

    jboolean bAutoClose;
    if (autoClose == 0)
    {
        bAutoClose = FALSE;
    }else
    {
        bAutoClose = TRUE;
    }

    //jstring strEnterPin = (*genv)->CallObjectMethod(genv,ginstance,jmethodid,strTitle, bAutoCancel, strAutoPin, FALSE, status);
    //LOGD("Before CallObjectMethod");


    selectIndex = (*genv)->CallIntMethod(genv,ginstance,jmethodid, strTitle, bAutoClose, jAutoChoiceIndex);
    (*genv)->DeleteLocalRef (genv, strTitle);

    LOGD("Select Index = %d", selectIndex);
	LOGD("0 is Approve, 1 is Decline");
    return selectIndex;
}


void CJAVA_ShowLogTextAndWaitConfirm(char *title, char *data, unsigned char autoClose)
{
    jclass   jclazz = (*genv)->FindClass(genv,"com/szsitc/jay/sitc_emvl2/MainActivity");
    jmethodID jmethodid = (*genv)->GetMethodID(genv,jclazz,"showLogTextAndWaitConfirm",
                                               "(Ljava/lang/String;Ljava/lang/String;Z)V");
    jobject jobjs = (*genv)->AllocObject(genv,jclazz);

    jstring strTitle = (*genv)->NewStringUTF(genv, title);
	jstring strData = (*genv)->NewStringUTF(genv, data);
    
    jboolean bAutoClose;
    if (autoClose == 0)
    {
        bAutoClose = FALSE;
    }else
    {
        bAutoClose = TRUE;
    }

    (*genv)->CallVoidMethod(genv,ginstance, jmethodid, strTitle, strData, bAutoClose);
    (*genv)->DeleteLocalRef (genv, strTitle);
    (*genv)->DeleteLocalRef (genv, strData);

    return;
}


void CJAVA_ShowProcess(char *data)
{
	jclass   jclazz = (*genv)->FindClass(genv,"com/szsitc/jay/sitc_emvl2/MainActivity");
    jmethodID jmethodid = (*genv)->GetMethodID(genv,jclazz,"showDialogProcess",
                                               "(Ljava/lang/String;)V");
    jobject jobjs = (*genv)->AllocObject(genv,jclazz);

	jstring strData = (*genv)->NewStringUTF(genv, data);
    

    (*genv)->CallVoidMethod(genv,ginstance, jmethodid, strData);
    (*genv)->DeleteLocalRef (genv, strData);

    return;
}

void CJAVA_PrintData(char *data)
{
	jclass   jclazz = (*genv)->FindClass(genv,"com/szsitc/jay/sitc_emvl2/MainActivity");
    jmethodID jmethodid = (*genv)->GetMethodID(genv,jclazz,"printTexts",
                                               "(Ljava/lang/String;)V");
    jobject jobjs = (*genv)->AllocObject(genv,jclazz);

	jstring strData = (*genv)->NewStringUTF(genv, data);
    

    (*genv)->CallVoidMethod(genv,ginstance, jmethodid, strData);
    (*genv)->DeleteLocalRef (genv, strData);

    return;
}



int CJAVA_Test()
{
    int ret;
    char enterPIN[20] = {0};

	/*
	CJAVA_ShowProcess("进度1");
    sleep(1);
    ret = CJAVA_EnterPIN("CJAVA_Test请输入密码", enterPIN, FALSE, "12345");
    LOGD("ret = %d, enterPIN = %s", ret, enterPIN);
	
	CJAVA_ShowProcess("进度2");
    sleep(1);
    TestAppSelect();

    CJAVA_ShowProcess("进度3");
    sleep(1);
	CJAVA_ShowLogTextAndWaitConfirm("EMVL2", "测试中",FALSE);
	CJAVA_ShowProcess("进度4");
	sleep(1);

	CJAVA_ShowLogTextAndWaitConfirm("EMVL2", "通讯中", TRUE);
	CJAVA_ShowProcess("进度5");
    sleep(1);

	CJAVA_ShowYesNoAndWaitSelect("是否接受交易", FALSE, 0);
	CJAVA_ShowProcess("进度6");	
	sleep(2);

	CJAVA_ShowApproveDeclineAndWaitSelect("请选择接受或者拒绝交易", FALSE, 0);
	CJAVA_ShowProcess("进度7");	
	sleep(2);
	*/

	TestAppSelect();
    return 0;
}


JNIEXPORT jint JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNIGetUserAIDCount(JNIEnv *env, jobject instance) {

    // TODO
    return GetUserAIDCount();
}

JNIEXPORT jstring JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNIGetUserAIDLabelByIndex(JNIEnv *env,
                                                                       jobject instance,
                                                                       jint labelIndex) {

    // TODO
    char buf[128] = {0};
    int index = labelIndex;

    GetUserAIDLable(index, buf);

    return (*env)->NewStringUTF(env, buf);
}

/*
JNIEXPORT jint JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNISetUserAIDIndex(JNIEnv *env, jobject instance,
                                                                jint labelIndex) {

    // TODO

    SetUserAIDStatusToAlreadyGet(labelIndex);

}
*/

JNIEXPORT jint JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNIParaDownload(JNIEnv *env, jobject instance) {
    // TODO
    DownloadParam();
}

JNIEXPORT jint JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_wait_1icc_1msr_1event(JNIEnv *env, jobject instance,
                                                                   jbyteArray status_,
                                                                   jbyteArray data_,
                                                                   jintArray datalen_) {
    jbyte *status = (*env)->GetByteArrayElements(env, status_, NULL);
    jbyte *data = (*env)->GetByteArrayElements(env, data_, NULL);
    jint *datalen = (*env)->GetIntArrayElements(env, datalen_, NULL);

    // TODO


    (*env)->ReleaseByteArrayElements(env, status_, status, 0);
    (*env)->ReleaseByteArrayElements(env, data_, data, 0);
    (*env)->ReleaseIntArrayElements(env, datalen_, datalen, 0);
}


JNIEXPORT void JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNIBatchCapture(JNIEnv *env, jobject instance) {

    // TODO
    //CJAVA_Test();

	//BatchDataCaptureBCTC();
	QpbocBatchDataCaptureBCTC();
}

JNIEXPORT void JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNIClearTransRecord(JNIEnv *env, jobject instance) {

    // TODO
   
    //ClearTransRecord();
    ClearQpbocTransRecord();

}

JNIEXPORT void JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNISetServerIPAndPort(JNIEnv *env, jobject instance,
                                                                   jstring serverIP_,
                                                                   jstring serverPort_) {
    const char *serverIP = (*env)->GetStringUTFChars(env, serverIP_, 0);
    const char *serverPort = (*env)->GetStringUTFChars(env, serverPort_, 0);

    // TODO
    SetServerIPAndPort(serverIP, serverPort);

    (*env)->ReleaseStringUTFChars(env, serverIP_, serverIP);
    (*env)->ReleaseStringUTFChars(env, serverPort_, serverPort);
}

JNIEXPORT jint JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNIEMVL2Transaction(JNIEnv *env, jobject instance) {

    // TODO

    //Transaction("");
    qpbocTransaction(NULL);
}


JNIEXPORT jint JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNIEMVL2ICCInit(JNIEnv *env, jobject instance) {

    // TODO
    Emvl2_InitICCReader();
}



JNIEXPORT void JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNISetTransPara(JNIEnv *env, jobject instance,
                                                             jint transactionType, jint accountType,
                                                             jint amount, jint cashamount) {

    // TODO
    EMVL2_Transaction_SetParaFromUI(transactionType, accountType, amount, cashamount);
}

JNIEXPORT void JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNIReadLog(JNIEnv *env, jobject instance) {

    // TODO
    DoReadLogTransaction();
}


typedef enum{
	DOWNLOAD_TERMINAL_PARA,
	DOWNLOAD_AID,
	DOWNLOAD_CA,
	DOWNLOAD_EXP_FILE,
	AUTO_TRANSACTION,
} EmvL2ActionType;

JNIEXPORT void JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNIEmvL2Action(JNIEnv *env, jobject instance,
                                                            jint actionType) {
    genv = env;
    ginstance = instance;
    // TODO
    int type = actionType;
	char autoAmt[100];
	char autoAmtOther[100];
	unsigned char transactionType[2];

    switch (type)
    {
        case DOWNLOAD_TERMINAL_PARA:
			DownloadTerminalPara();
			break;

		case DOWNLOAD_AID:
			DownloadAIDPara();
			break;

		case DOWNLOAD_CA:
			DownloadCA();
			break;

		case AUTO_TRANSACTION:
			AutoTransaction(autoAmt, autoAmtOther, transactionType);

		default:
			break;
    }

}

JNIEXPORT void JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNIPinAutoClose(JNIEnv *env, jobject instance,
                                                             jboolean autoClose, jstring autoPin_) {
    const char *autoPin = (*env)->GetStringUTFChars(env, autoPin_, 0);

    // TODO
    SetPinAutoClose(autoClose, autoPin);

    (*env)->ReleaseStringUTFChars(env, autoPin_, autoPin);
}

JNIEXPORT void JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNISelectAidAutoClose(JNIEnv *env, jobject instance,
                                                                   jboolean autoClose,
                                                                   jint autoAidIndex) {

    // TODO
    SetSelectAidAutoClose(autoClose, autoAidIndex);
}

JNIEXPORT void JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNILogtextAutoClose(JNIEnv *env, jobject instance,
                                                                 jboolean autoClos) {

    // TODO
    SetLogTextAutoClose(autoClos);
}

																 JNIEXPORT void JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNIUiInit(JNIEnv *env, jobject instance) {

    // TODO
    genv = env;
    ginstance = instance;
}


//char gAutoAmount[20] = {0};
//char gAutoAmountOther[20] = {0};
//unsigned char gAutoTransactionType = 0;


JNIEXPORT jint JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNIAutoTest(JNIEnv *env, jobject instance) {

    // TODO
    genv = env;
    ginstance = instance;
    int ret;

    char gAutoAmount[20] = {0};
    char gAutoAmountOther[20] = {0};
    unsigned char gAutoTransactionType = 0;

    ret = AutoTransaction(gAutoAmount, gAutoAmountOther, &gAutoTransactionType);
    LOGD("AutoTransaction return %d", ret);
    LOGD("gAutoAmount = %s, gAutoAmountOther = %s, gTransactionType = %d", gAutoAmount,
         gAutoAmountOther, gAutoTransactionType);
    return ret;
}


JNIEXPORT jstring JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNIGetAutoAmount(JNIEnv *env, jobject instance) {

    // TODO
    char autoAmount[20] = {0};
    char autoAmountOther[20] = {0};
    unsigned char autoTransactionType = 0;

    GetAutoTestData(autoAmount, autoAmountOther, &autoTransactionType);

    return (*env)->NewStringUTF(env, autoAmount);
}

JNIEXPORT jstring JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNIGetAutoAmountOther(JNIEnv *env, jobject instance) {

    // TODO
    char autoAmount[20] = {0};
    char autoAmountOther[20] = {0};
    unsigned char autoTransactionType = 0;

    GetAutoTestData(autoAmount, autoAmountOther, &autoTransactionType);

    return (*env)->NewStringUTF(env, autoAmountOther);
}

JNIEXPORT jbyte JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNIGetAutoTransactionType(JNIEnv *env,
                                                                       jobject instance) {

    // TODO
    char autoAmount[20] = {0};
    char autoAmountOther[20] = {0};
    unsigned char autoTransactionType = 0;

    GetAutoTestData(autoAmount, autoAmountOther, &autoTransactionType);

    return autoTransactionType;
}

JNIEXPORT void JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNISetKernelType(JNIEnv *env, jobject instance,
                                                              jint KernelType) {

    // TODO
    SetKernelType(KernelType);
}

JNIEXPORT void JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNISetForceOnline(JNIEnv *env, jobject instance,
                                                               jboolean forceOnline) {

    // TODO
    SetForceOnline(forceOnline);
}

JNIEXPORT void JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNIReadLogTransaction(JNIEnv *env, jobject instance) {

    // TODO
    DoReadLogTransaction();
}

JNIEXPORT void JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNIReadBalanceTransaction(JNIEnv *env,
                                                                       jobject instance) {

    // TODO
    DoReadBalanceTransaction();
}

JNIEXPORT void JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNIShowCheckSum(JNIEnv *env, jobject instance,
                                                             jint type) {

    // TODO

    return;
}

JNIEXPORT void JNICALL
Java_com_szsitc_jay_sitc_1emvl2_MainActivity_JNISetPrint(JNIEnv *env, jobject instance,
                                                         jboolean print) {

    // TODO
	SetPrintOrNot(print);
}
																												 
#else

#endif
