#ifndef INCLUDE_ONLINE_H
#define INCLUDE_ONLINE_H

#include "Define.h"

int32 GetAuthRequest(byte *buf, uint16 *bufLen);
int32 SendAndReceive(const byte * SendBuf,uint16 SendBufLen, byte * RecvBuf, uint16 *pRecvBufLen);
int32 ParseHostData(byte buf[], uint16 size);
int32 GetTransResultRequest(byte *buf, uint16 *bufLen);
int32 ParseTransResultResponse(const byte buf[], uint16 size);
int SocketConnect(char *ip, char *port, int timeoutSecond);
int SocketSendData(int fd, unsigned char *buf, int len, int timeoutMs);
int SocketRcvData(int fd, unsigned char *buf, int len, int timeout);
void SocketDisconnect(int32 socketID);
int32 SendAndReceive(const byte * SendBuf,uint16 SendBufLen,
						     byte * RecvBuf, uint16 *pRecvBufLen);
int32 qpbocAuthCommWithHost(void);
int32 GetBatchCaptureAuth(byte *buf, uint16 *bufLen);
int32 GetTransStartRequest(byte *buf, uint16 *bufLen);
int32 GetQpbocBatchCaptureAuth(byte *buf, uint16 *bufLen);
#endif

